//
//  GSMiddlewareInteractor.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

/// GSMiddleware Module Interactor
class GSMiddlewareInteractor{
    weak var _presenter: GSMiddlewarePresenterProtocol?
    private let facade = NOCServicesFacade.sharedHome
}
extension GSMiddlewareInteractor: GSMiddlewareInteractorProtocol {
    func fetchMiddlewareList() {
        let request = ListMiddlewareStatusRequest(dashboardType: "MIDDLEWARE")
        
        self.facade.fetchListMiddlewareStatus(request: request) { response, _ in
            DispatchQueue.main.async {
                self._presenter?.responseMiddlewareList(response: response)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: emptyMessage ?? "", isEmptyData: true)
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: errorMessage ?? "", isEmptyData: false)
            }
        }
    }
}
