//
//  GSPanelPageMiddlewareView.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 09/02/22.
//  Copyright © 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager


/// GSPanelPageMiddleware Module View
class GSPanelPageMiddlewareView: UIViewController {
    
    private var ui : GSPanelPageMiddlewareViewUI?
    public var response:ListMiddlewareStatusResponse.infoMiddleware?
    private let facade = NOCServicesFacade.sharedHome
    private var timer: Timer?
    override func loadView() {
        // setting the custom view as the view controller's view
        ui = GSPanelPageMiddlewareViewUI(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let request = ListOEMRequest(dashboardType: "OEM")
        
        self.facade.listOEM(request: request) { [self] response, _ in
            DispatchQueue.main.async {
                ui?.updateOEMData(response: response)
                ui?.loader.stopAnimating()
                ui?.loader.isHidden = true
            }
        } empty: { [self] emptyMessage in
            DispatchQueue.main.async {
                ui?.loader.stopAnimating()
                ui?.loader.isHidden = true
            }
        } failure: { [self] errorMessage in
            DispatchQueue.main.async {
                ui?.loader.stopAnimating()
                ui?.loader.isHidden = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.timer = Timer.scheduledTimer(timeInterval: 7, target: self, selector: #selector(updateCountDown), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    @objc private func updateCountDown(){
        let request = ListOEMRequest(dashboardType: "OEM")
        
        self.facade.listOEM(request: request) { [self] response, _ in
            DispatchQueue.main.async {
                ui?.updateOEMData(response: response)
               
            }
        } empty: { [self] emptyMessage in
           
        } failure: { [self] errorMessage in
            
        }
    }
}

// MARK: - extending GSPanelPageMiddlewareView to implement the custom ui view delegate
extension GSPanelPageMiddlewareView: GSPanelPageMiddlewareViewUIDelegate {
    
    func notifyDissmisView() {
        dismiss(animated: true, completion: nil)
    }
    
    func getModelData()->ListMiddlewareStatusResponse.infoMiddleware? {
        return response
    }
    
    
}
