//
//  GSPanelPageMiddlewareMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 09/02/22.
//

import UIKit
import SDKGSServicesManager

class GSPanelPageMiddlewareMain: NSObject {
    
    static func createModule(response:ListMiddlewareStatusResponse.infoMiddleware?)->UIViewController{
        
        let viewController  :   GSPanelPageMiddlewareView?   =  GSPanelPageMiddlewareView()
        if let view = viewController {
            view.response = response
            return view
        }
        return UIViewController()
    }
}
