//
//  GSPanelPageMiddlewareViewUI.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 09/02/22.
//  Copyright © 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager
// MARK: GSPanelPageMiddlewareViewUI Delegate -
/// GSPanelPageMiddlewareViewUI Delegate
protocol GSPanelPageMiddlewareViewUIDelegate {
    func getModelData()->ListMiddlewareStatusResponse.infoMiddleware?
    func notifyDissmisView()
}

class GSPanelPageMiddlewareViewUI: UIView {
    
    var delegate: GSPanelPageMiddlewareViewUIDelegate?
    
    let options = ["Reiniciar proceso", "Liberar memoria", "Liberar espacio", "Check de servicio"]
    
    private lazy var imgCheck: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.image = UIImage(named: "icon_checkLightBlueCircle", in: Bundle.local_gs_utils, compatibleWith: nil)
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    private lazy var lbTitle: UILabel = {
        let lb = UILabel()
        lb.text = "Acciones"
        lb.textAlignment = .left
        lb.font = UIFont.Montserrat_Semibold_16
        lb.textColor = GSColorsManager.primaryColor
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    private lazy var line: UIView = {
        let vieline = UIView(frame: .zero)
        vieline.translatesAutoresizingMaskIntoConstraints = false
        vieline.backgroundColor = GSColorsManager.primaryColor
        return vieline
    }()
    
    
    lazy var boxOptions: UIStackView = {
        let stack = UIStackView(frame: .zero)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.alignment = .center
        stack.spacing = 20
        stack.distribution = .fillEqually
        for option in options{
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.black
            view.layer.cornerRadius = 18
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor(hexString: "3E3E3E").cgColor
            
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.numberOfLines = 2
            label.textColor = UIColor(hexString: "3E3E3E")
            label.font = .Montserrat_Semibold_16
            label.text = option
            label.textAlignment = .center
            
            view.addSubview(label)
            
            NSLayoutConstraint.activate([
                view.heightAnchor.constraint(equalToConstant: 36),
                label.topAnchor.constraint(equalTo: view.topAnchor, constant: 5),
                label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
                label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
                label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5),
            ])
            stack.addArrangedSubview(view)
        }
        
        return stack
    }()
    
    lazy var panelView: UIView = {
        let colorChar = delegate?.getModelData()?.monitor?.first?.status == "OK" ? UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16) :
        delegate?.getModelData()?.monitor?.first?.status == "WARNING" ? GSColorsManager.labelGoldColor.withAlphaComponent(0.81) :
        GSColorsManager.contingencyLabelColor.withAlphaComponent(0.81)
        
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor.init(red: 15/255, green: 27/255, blue: 45/255, alpha: 1)
        view.layer.cornerRadius = 15
        view.layer.borderWidth = 0.8
        view.layer.borderColor = colorChar.cgColor
        view.alpha = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var buttonClose: UIButton = {
        let button = UIButton(frame: .zero)
        button.setImage(UIImage(named: "close_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.tintColor = GSColorsManager.labelLightColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(self.onDissmisButton(_:)), for: .touchUpInside)
        return button
    }()
    lazy var lineChart: LineChart = {
        let dataFilter =  delegate?.getModelData()?.monitor?.reversed()
        var lineChart =  LineChart()
        lineChart.translatesAutoresizingMaskIntoConstraints = false
        lineChart.animation.enabled = true
        lineChart.area = true
        lineChart.x.labels.visible = true
        var labels = [String]()
        dataFilter?.forEach({ element in
            labels.append(element.hour?.suffix(10).prefix(5).lowercased() ?? "")
        })
        
        var values = [CGFloat]()
        dataFilter?.forEach({ element in
            values.append(CGFloat(element.value?.floatValue() ?? 0))
        })
        guard values.count == labels.count else{
            return lineChart
        }
        let colorChar = dataFilter?.last?.status == "OK" ? GSColorsManager.labelLightColor :
        dataFilter?.last?.status == "WARNING" ? GSColorsManager.labelGoldColor :
        GSColorsManager.contingencyLabelColor
        lineChart.clear()
        lineChart.colors = colorChar
        lineChart.animation.enabled = false
        lineChart.x.grid.count = CGFloat(labels.count)
        lineChart.y.grid.count = CGFloat(values.count)
        lineChart.x.labels.values = labels
        lineChart.y.labels.visible = true
        lineChart.addLine(values)
        return lineChart
    }()
    lazy var titleCell: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Medium_16.withSize(14)
        label.textColor = GSColorsManager.labelLightColor
        label.numberOfLines = 0
        label.text = delegate?.getModelData()?.serviceName ?? "---"
        return label
    }()
    lazy var ipCell: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Medium_16.withSize(14)
        label.textColor = GSColorsManager.labelLightColor
        label.numberOfLines = 0
        label.text = delegate?.getModelData()?.ip ?? "0.0.0.0"
        return label
    }()
    lazy var sectionCell: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Bold_16.withSize(14)
        label.textColor = GSColorsManager.labelLightColor
        label.numberOfLines = 0
        label.text = "SERVICIO"
        return label
    }()
    lazy var sectionDBCell: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Bold_16.withSize(14)
        label.textColor = GSColorsManager.labelLightColor
        label.numberOfLines = 0
        label.text = "MONITOREO BASE DE DATOS"
        return label
    }()
   
  
    lazy var currentCell: UIView = {
        let colorChar = delegate?.getModelData()?.monitor?.first?.status == "OK" ? UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16) :
        delegate?.getModelData()?.monitor?.first?.status == "WARNING" ? GSColorsManager.labelGoldColor.withAlphaComponent(0.81) :
        GSColorsManager.contingencyLabelColor.withAlphaComponent(0.81)
        
        let view1 = UIView(frame: .zero)
        view1.backgroundColor = colorChar
        view1.translatesAutoresizingMaskIntoConstraints = false
        view1.addSubview(currentValueCell)
        NSLayoutConstraint.activate([
            view1.heightAnchor.constraint(equalToConstant: 40),
            view1.widthAnchor.constraint(equalToConstant: 80),
            currentValueCell.leadingAnchor.constraint(equalTo: view1.leadingAnchor, constant: 10),
            currentValueCell.trailingAnchor.constraint(equalTo: view1.trailingAnchor, constant: -10),
            currentValueCell.centerXAnchor.constraint(equalTo: view1.centerXAnchor),
            currentValueCell.centerYAnchor.constraint(equalTo: view1.centerYAnchor),
            
        ])
        return view1
    }()
    
    lazy var currentValueCell: UILabel = {
        let dataFilter =  delegate?.getModelData()?.monitor?.first?.value ?? "0"
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font =  delegate?.getModelData()?.monitor?.first?.status == "OK" ? .Montserrat_Medium_16.withSize(14) : .Montserrat_Bold_16.withSize(14)
        label.textColor = GSColorsManager.labelLightColor
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = dataFilter
        return label
    }()
    private lazy var lineImage : UIImageView = {
       let image = UIImageView()
        image.image = UIImage(named: "verticalLine", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    lazy var mainStackOEMView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 20
        return stackView
    }()
    lazy var loader : UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.center = mainStackOEMView.center
        activityIndicator.startAnimating()
        return activityIndicator
    }()
    
    lazy var mainStackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 30
        stackView.addArrangedSubview(sectionCell)
        stackView.addArrangedSubview(currentCell)
        stackView.addArrangedSubview(titleCell)
        stackView.addArrangedSubview(ipCell)
        return stackView
    }()
    
    lazy var imageDB: UIImageView = {
        let imageDB = UIImageView(frame: .zero)
        imageDB.translatesAutoresizingMaskIntoConstraints = false
        imageDB.image = UIImage(named: "database_ic", in: .local_gs_utils, compatibleWith: nil)
        imageDB.tintColor = .lightGray
        return imageDB
    }()
    
    lazy var moreInfoBox: UIStackView = {
        let stack = UIStackView(frame: .zero)
        stack.axis = .vertical
        stack.spacing = 10
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        let viewbdname = UIView(frame: .zero)
        viewbdname.translatesAutoresizingMaskIntoConstraints = false
        
        
        let bdName = UILabel(frame: .zero)
        bdName.translatesAutoresizingMaskIntoConstraints = false
        bdName.font = .Montserrat_Regular_14
        bdName.textColor = GSColorsManager.labelLightColor
        bdName.textAlignment = .left
        bdName.numberOfLines = 0
        bdName.text = delegate?.getModelData()?.dataBase ?? "---"
        
        let ipName = UILabel(frame: .zero)
        ipName.translatesAutoresizingMaskIntoConstraints = false
        ipName.font = .Montserrat_Regular_14
        ipName.textColor = GSColorsManager.labelLightColor
        ipName.textAlignment = .left
        ipName.numberOfLines = 0
        ipName.text = delegate?.getModelData()?.ip ?? "0.0.0.0"
        ipName.isHidden = true
        
        viewbdname.addSubview(imageDB)
        viewbdname.addSubview(bdName)
        viewbdname.addSubview(ipName)
        
        NSLayoutConstraint.activate([
            
            imageDB.centerYAnchor.constraint(equalTo: viewbdname.centerYAnchor),
            imageDB.leadingAnchor.constraint(equalTo: viewbdname.leadingAnchor),
            imageDB.heightAnchor.constraint(equalToConstant: 25),
            imageDB.widthAnchor.constraint(equalToConstant: 20),
            
            bdName.topAnchor.constraint(equalTo: viewbdname.topAnchor),
            bdName.leadingAnchor.constraint(equalTo: imageDB.trailingAnchor, constant: 10),
            bdName.trailingAnchor.constraint(equalTo: viewbdname.trailingAnchor),
            
            ipName.topAnchor.constraint(equalTo: bdName.bottomAnchor),
            ipName.leadingAnchor.constraint(equalTo: imageDB.trailingAnchor, constant: 10),
            ipName.trailingAnchor.constraint(equalTo: viewbdname.trailingAnchor),
            ipName.bottomAnchor.constraint(equalTo: viewbdname.bottomAnchor),
            
        ])
        stack.addArrangedSubview(viewbdname)
        return stack
    }()
    
    private lazy var headerVerticalLine: UIView = {
        let view = UIView()
        view.backgroundColor = GSColorsManager.labelColor.withAlphaComponent(0.5)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    convenience init(delegate: GSPanelPageMiddlewareViewUIDelegate) {
        self.init()
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseInOut, .curveEaseOut]) { [self] in
            panelView.alpha = 1
            layoutIfNeeded()
        } completion: { _ in }

    }
    
    fileprivate func setupUIElements() {
        // arrange subviews
        addSubview(panelView)
        panelView.addSubview(mainStackView)
        panelView.addSubview(sectionDBCell)
        panelView.addSubview(moreInfoBox)
        panelView.addSubview(headerVerticalLine)
        panelView.addSubview(lineChart)
        panelView.addSubview(lineImage)
        panelView.addSubview(buttonClose)
        panelView.addSubview(imgCheck)
        panelView.addSubview(lbTitle)
        panelView.addSubview(line)
        panelView.addSubview(mainStackOEMView)
        panelView.addSubview(boxOptions)
        panelView.addSubview(loader)
        
    }
    
    fileprivate func setupConstraints() {
        // add constraints to subviews
        let vieline = UIView(frame: .zero)
        vieline.translatesAutoresizingMaskIntoConstraints = false
        vieline.backgroundColor = GSColorsManager.primaryColor
        panelView.addSubview(vieline)
        
        NSLayoutConstraint.activate([
            sectionCell.widthAnchor.constraint(equalToConstant: 80),
            titleCell.widthAnchor.constraint(equalToConstant: 140),
            
            panelView.centerXAnchor.constraint(equalTo: centerXAnchor),
            panelView.centerYAnchor.constraint(equalTo: centerYAnchor),
            panelView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.8),
            panelView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            
            buttonClose.heightAnchor.constraint(equalToConstant: 38),
            buttonClose.widthAnchor.constraint(equalToConstant: 38),
            buttonClose.topAnchor.constraint(equalTo: panelView.topAnchor, constant: 10),
            buttonClose.trailingAnchor.constraint(equalTo: panelView.trailingAnchor, constant: -10),
            
            lineChart.trailingAnchor.constraint(equalTo: panelView.trailingAnchor, constant: -20),
            lineChart.topAnchor.constraint(equalTo: mainStackView.bottomAnchor, constant: 15),
            lineChart.heightAnchor.constraint(equalTo: panelView.heightAnchor, multiplier: 0.55),
            lineChart.widthAnchor.constraint(equalTo: panelView.widthAnchor, multiplier: 0.65),
            
            mainStackView.topAnchor.constraint(equalTo: panelView.topAnchor, constant: 30),
            mainStackView.leadingAnchor.constraint(equalTo: lineChart.leadingAnchor, constant: -30),
            mainStackView.trailingAnchor.constraint(equalTo: lineChart.trailingAnchor),
            
            
            lineImage.topAnchor.constraint(equalTo: lineChart.topAnchor,constant: -20),
            lineImage.leadingAnchor.constraint(equalTo: mainStackOEMView.trailingAnchor, constant: 20.0),
            lineImage.bottomAnchor.constraint(equalTo: lineChart.bottomAnchor, constant: 20),
            lineImage.widthAnchor.constraint(equalToConstant: 2),
            
            
            sectionDBCell.centerYAnchor.constraint(equalTo: mainStackView.centerYAnchor),
            sectionDBCell.leadingAnchor.constraint(equalTo: panelView.leadingAnchor, constant: 20),
            sectionDBCell.trailingAnchor.constraint(equalTo: mainStackView.leadingAnchor, constant: -20),
            
            
            moreInfoBox.topAnchor.constraint(equalTo: sectionDBCell.bottomAnchor, constant: 10),
            moreInfoBox.leadingAnchor.constraint(equalTo: sectionDBCell.leadingAnchor),
            moreInfoBox.trailingAnchor.constraint(equalTo: sectionDBCell.trailingAnchor),
            
            headerVerticalLine.heightAnchor.constraint(equalToConstant: 1),
            headerVerticalLine.topAnchor.constraint(equalTo: mainStackView.bottomAnchor, constant: 10),
            headerVerticalLine.leadingAnchor.constraint(equalTo: mainStackView.leadingAnchor, constant: 100),
            headerVerticalLine.trailingAnchor.constraint(equalTo: mainStackView.trailingAnchor, constant: -60),
            
            mainStackOEMView.topAnchor.constraint(equalTo: moreInfoBox.bottomAnchor, constant: 20),
            mainStackOEMView.leadingAnchor.constraint(equalTo: moreInfoBox.leadingAnchor),
            mainStackOEMView.trailingAnchor.constraint(equalTo: moreInfoBox.trailingAnchor, constant: -20),
            
            loader.centerXAnchor.constraint(equalTo: mainStackOEMView.centerXAnchor),
            loader.centerYAnchor.constraint(equalTo: mainStackOEMView.centerYAnchor, constant: 50),
            loader.heightAnchor.constraint(equalToConstant: 50),
            loader.widthAnchor.constraint(equalToConstant: 50),
            
//            vieline.heightAnchor.constraint(equalToConstant: 1),
//            vieline.bottomAnchor.constraint(equalTo: moreInfoBox.bottomAnchor, constant: 10),
//            vieline.leadingAnchor.constraint(equalTo: moreInfoBox.leadingAnchor, constant: -10),
//            vieline.trailingAnchor.constraint(equalTo: moreInfoBox.trailingAnchor, constant: 10),
            
            imgCheck.topAnchor.constraint(equalTo: lineChart.bottomAnchor, constant: 40),
            imgCheck.leadingAnchor.constraint(equalTo: panelView.leadingAnchor, constant: 30),
            imgCheck.heightAnchor.constraint(equalToConstant: 20),
            imgCheck.widthAnchor.constraint(equalToConstant: 20),
            lbTitle.centerYAnchor.constraint(equalTo: imgCheck.centerYAnchor),
            lbTitle.leadingAnchor.constraint(equalTo: imgCheck.trailingAnchor, constant: 10),
            line.topAnchor.constraint(equalTo: imgCheck.bottomAnchor, constant: 20),
            line.leadingAnchor.constraint(equalTo: panelView.leadingAnchor, constant: 30),
            line.trailingAnchor.constraint(equalTo: panelView.trailingAnchor, constant: -30),
            line.heightAnchor.constraint(equalToConstant: 1),
            
            boxOptions.leadingAnchor.constraint(equalTo: panelView.leadingAnchor, constant: 30),
            boxOptions.trailingAnchor.constraint(equalTo: panelView.trailingAnchor, constant: -30),
            boxOptions.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 30),
        ])
    }
    
    @objc func onDissmisButton(_ sender: UIButton){
        delegate?.notifyDissmisView()
    }
    
    func updateOEMData(response: ListOEMServiceProtocol.Response){
        
        /*
         OPEN:   AZUL
         CLOSE: ROJO
         ALGO DIFERENTE: AMARILLO
         **/
        mainStackOEMView.removeAllArrangedSubviews()
        
       
        let stackSesion = UIStackView(frame:.zero)
        stackSesion.translatesAutoresizingMaskIntoConstraints = false
        stackSesion.axis = .horizontal
        stackSesion.distribution = .fillEqually
        stackSesion.spacing = 10
       // stackSesion.addArrangedSubview(sesionCell)
        for n in [0,1]{
            let stack = UIStackView(frame:.zero)
            stack.translatesAutoresizingMaskIntoConstraints = false
            stack.axis = .vertical
            switch n{
            case 0:
                let sesionCell = UILabel(frame: .zero)
                sesionCell.translatesAutoresizingMaskIntoConstraints = false
                sesionCell.text = "Sesiones"
                sesionCell.font = .Montserrat_Medium_16.withSize(14)
                sesionCell.textColor = GSColorsManager.labelLightColor
                
                let label = UILabel(frame: .zero)
                label.translatesAutoresizingMaskIntoConstraints = false
                label.text = "Activas"
                label.textAlignment = .center
                label.font = .Montserrat_Medium_16.withSize(14)
                label.textColor = GSColorsManager.labelLightColor
                label.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
                
                let label2 = UILabel(frame: .zero)
                label2.translatesAutoresizingMaskIntoConstraints = false
                label2.text = "\(response?.sessionsNumber ?? "")   "
                label2.textAlignment = .center
                label2.font = .Montserrat_Medium_16.withSize(14)
                label2.textColor = GSColorsManager.labelLightColor
                
                stack.addArrangedSubview(sesionCell)
                stack.addArrangedSubview(label)
                stack.addArrangedSubview(label2)
                NSLayoutConstraint.activate([
                    sesionCell.heightAnchor.constraint(equalToConstant: 30),
                    label.heightAnchor.constraint(equalToConstant: 30),
                    label2.heightAnchor.constraint(equalToConstant: 30)
                ])
                break
            case 1:
                let sesionCell = UILabel(frame: .zero)
                sesionCell.translatesAutoresizingMaskIntoConstraints = false
                sesionCell.text = ""
                sesionCell.font = .Montserrat_Medium_16.withSize(14)
                sesionCell.textColor = GSColorsManager.labelLightColor
                
                let label = UILabel(frame: .zero)
                label.translatesAutoresizingMaskIntoConstraints = false
                label.text = "Inactivas"
                label.textAlignment = .center
                label.font = .Montserrat_Medium_16.withSize(14)
                label.textColor = GSColorsManager.labelLightColor
                label.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
                
                let label2 = UILabel(frame: .zero)
                label2.translatesAutoresizingMaskIntoConstraints = false
                let valuOne = Int(response?.sessionsLimit ?? "0") ?? 0
                let valueTwo = Int(response?.sessionsNumber ?? "0") ?? 0
                label2.text = "\(valuOne - valueTwo)"
                label2.textAlignment = .center
                label2.font = .Montserrat_Medium_16.withSize(14)
                label2.textColor = GSColorsManager.labelLightColor
                
                stack.addArrangedSubview(sesionCell)
                stack.addArrangedSubview(label)
                stack.addArrangedSubview(label2)
                NSLayoutConstraint.activate([
                    sesionCell.heightAnchor.constraint(equalToConstant: 30),
                    label.heightAnchor.constraint(equalToConstant: 30),
                    label2.heightAnchor.constraint(equalToConstant: 30)
                ])
                break
            default:
                break
            }
            stackSesion.addArrangedSubview(stack)
        }
        mainStackOEMView.addArrangedSubview(stackSesion)
        
        
        let sectionOSCell = UILabel(frame: .zero)
        sectionOSCell.translatesAutoresizingMaskIntoConstraints = false
        sectionOSCell.font = .Montserrat_Bold_16.withSize(14)
        sectionOSCell.textColor = GSColorsManager.labelLightColor
        sectionOSCell.numberOfLines = 0
        sectionOSCell.text = "MONITOREO SISTEMA OPERATIVO"
        mainStackOEMView.addArrangedSubview(sectionOSCell)
        
        switch response?.status?.lowercased(){
            
        case "open":
            imageDB.tintColor = GSColorsManager.labelLightColor
            break
        case "close":
            imageDB.tintColor = GSColorsManager.contingencyLabelColor
            break
        default:
            imageDB.tintColor = GSColorsManager.labelGoldColor
            break
        }
        
        let stackMainMonitoreo = UIStackView(frame:.zero)
        stackMainMonitoreo.translatesAutoresizingMaskIntoConstraints = false
        stackMainMonitoreo.axis = .vertical
        for n in [0,1,2,3,4]{
            
            let stackMonitoreo = UIStackView(frame:.zero)
            stackMonitoreo.translatesAutoresizingMaskIntoConstraints = false
            stackMonitoreo.axis = .horizontal
            
            switch n{
            case 0:
                let label = UILabel(frame: .zero)
                label.translatesAutoresizingMaskIntoConstraints = false
                label.text = "File system"
                label.font = .Montserrat_Medium_16.withSize(14)
                label.textColor = GSColorsManager.labelLightColor
                NSLayoutConstraint.activate([
                    label.heightAnchor.constraint(equalToConstant: 30)
                ])
                stackMonitoreo.addArrangedSubview(label)
                break
//            case 1:
//                let label = UILabel(frame: .zero)
//                label.translatesAutoresizingMaskIntoConstraints = false
//                label.text = "   Sesiones"
//                label.font = .Montserrat_Medium_16.withSize(14)
//                label.textColor = GSColorsManager.labelLightColor
//                label.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
//                let label2 = UILabel(frame: .zero)
//                label2.translatesAutoresizingMaskIntoConstraints = false
//                label2.text = "\(response?.sessionsNumber ?? "--") / \(response?.sessionsLimit ?? "--")"
//                label2.textAlignment = .right
//                label2.font = .Montserrat_Medium_16.withSize(14)
//                label2.textColor = GSColorsManager.labelLightColor
//                label2.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
//                stackMonitoreo.addArrangedSubview(label)
//                stackMonitoreo.addArrangedSubview(label2)
//                NSLayoutConstraint.activate([
//                    label.heightAnchor.constraint(equalToConstant: 30),
//                    label2.heightAnchor.constraint(equalToConstant: 30)
//                ])
//                break
            case 1:
                let label = UILabel(frame: .zero)
                label.translatesAutoresizingMaskIntoConstraints = false
                label.text = "   Oracle"
                label.font = .Montserrat_Medium_16.withSize(14)
                label.textColor = GSColorsManager.labelLightColor
                label.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
                let label2 = UILabel(frame: .zero)
                label2.translatesAutoresizingMaskIntoConstraints = false
                label2.text = "\(response?.fileSystemOracle19c ?? "")   "
                label2.textAlignment = .right
                label2.font = .Montserrat_Medium_16.withSize(14)
                label2.textColor = GSColorsManager.labelLightColor
                label2.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
                stackMonitoreo.addArrangedSubview(label)
                stackMonitoreo.addArrangedSubview(label2)
                NSLayoutConstraint.activate([
                    label.heightAnchor.constraint(equalToConstant: 30),
                    label2.heightAnchor.constraint(equalToConstant: 30)
                ])
                break
            case 2:
                let label = UILabel(frame: .zero)
                label.translatesAutoresizingMaskIntoConstraints = false
                label.text = "   Cloudcontrol"
                label.font = .Montserrat_Medium_16.withSize(14)
                label.textColor = GSColorsManager.labelLightColor
                label.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
                let label2 = UILabel(frame: .zero)
                label2.translatesAutoresizingMaskIntoConstraints = false
                label2.text = "\(response?.fileSystemCloudcontrol ?? "")   "
                label2.textAlignment = .right
                label2.font = .Montserrat_Medium_16.withSize(14)
                label2.textColor = GSColorsManager.labelLightColor
                label2.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
                stackMonitoreo.addArrangedSubview(label)
                stackMonitoreo.addArrangedSubview(label2)
                NSLayoutConstraint.activate([
                    label.heightAnchor.constraint(equalToConstant: 30),
                    label2.heightAnchor.constraint(equalToConstant: 30)
                ])
                break
            case 3:
                let label = UILabel(frame: .zero)
                label.translatesAutoresizingMaskIntoConstraints = false
                label.text = "   Gradump"
                label.font = .Montserrat_Medium_16.withSize(14)
                label.textColor = GSColorsManager.labelLightColor
                label.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
                let label2 = UILabel(frame: .zero)
                label2.translatesAutoresizingMaskIntoConstraints = false
                label2.text = "\(response?.fileSystemOradump ?? "")   "
                label2.textAlignment = .right
                label2.font = .Montserrat_Medium_16.withSize(14)
                label2.textColor = GSColorsManager.labelLightColor
                label2.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
                stackMonitoreo.addArrangedSubview(label)
                stackMonitoreo.addArrangedSubview(label2)
                NSLayoutConstraint.activate([
                    label.heightAnchor.constraint(equalToConstant: 30),
                    label2.heightAnchor.constraint(equalToConstant: 30)
                ])
                break
            case 4:
                let label = UILabel(frame: .zero)
                label.translatesAutoresizingMaskIntoConstraints = false
                label.text = "   Grid"
                label.font = .Montserrat_Medium_16.withSize(14)
                label.textColor = GSColorsManager.labelLightColor
                label.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
                let label2 = UILabel(frame: .zero)
                label2.translatesAutoresizingMaskIntoConstraints = false
                label2.text = "\(response?.fileSystemgrid ?? "")   "
                label2.textAlignment = .right
                label2.font = .Montserrat_Medium_16.withSize(14)
                label2.textColor = GSColorsManager.labelLightColor
                label2.backgroundColor = UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16)
                stackMonitoreo.addArrangedSubview(label)
                stackMonitoreo.addArrangedSubview(label2)
                NSLayoutConstraint.activate([
                    label.heightAnchor.constraint(equalToConstant: 30),
                    label2.heightAnchor.constraint(equalToConstant: 30)
                ])
                break
            default:
                break
            }
            stackMainMonitoreo.addArrangedSubview(stackMonitoreo)
        }
       
        mainStackOEMView.addArrangedSubview(stackMainMonitoreo)
    }
}
