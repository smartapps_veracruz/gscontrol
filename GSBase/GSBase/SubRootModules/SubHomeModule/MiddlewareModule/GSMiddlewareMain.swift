//
//  GSMiddlewareMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 22/12/21.
//

import UIKit

class GSMiddlewareMain: NSObject {

    static func createModule()->UIViewController{
        
        let viewController  :   GSMiddlewareView?   =  GSMiddlewareView()
        if let view = viewController {
            let presenter   =   GSMiddlewarePresenter()
            let router      =   GSMiddlewareRouter()
            let interactor  =   GSMiddlewareInteractor()
            
            view.presenter  =   presenter
            
            presenter._view          =   view
            presenter.interactor    =   interactor
            presenter.wireframe        =   router
            
            interactor._presenter    =   presenter
            return view
        }
        return UIViewController()
    }
}
