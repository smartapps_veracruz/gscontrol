//
//  GSMiddlewareViewUI.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager

// MARK: GSMiddlewareViewUI Delegate -
/// GSMiddlewareViewUI Delegate
protocol GSMiddlewareViewUIDelegate {
    func notifyGetParentView()->UIViewController
    func notifyPagerViewController()->UIPageViewController
    func notifyOptionSelected(option: Int)
    func notifyOptionBuilder(builder: [GSPagerMiddlewareType])
}

class GSMiddlewareViewUI: UIView {
    
    var delegate: GSMiddlewareViewUIDelegate?
    var optionMenu = [UIButton]()
    var centerXAnchorSlide: NSLayoutConstraint?
    var leadingAnchotSlide : NSLayoutConstraint?
    lazy var bodyContainer: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        if let parentView = delegate?.notifyGetParentView(), let pager = delegate?.notifyPagerViewController(){
            parentView.addChild(pager)
            pager.view.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(pager.view)
            NSLayoutConstraint.activate([
                pager.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                pager.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                pager.view.topAnchor.constraint(equalTo: view.topAnchor),
                pager.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
            pager.didMove(toParent: parentView)
        }
        return view
    }()
    lazy var slideView: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.primaryColor
        NSLayoutConstraint.activate([
            view.heightAnchor.constraint(equalToConstant: 5)
        ])
        return view
    }()
    
    lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = GSColorsManager.labelLightColor.withAlphaComponent(0.25)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var slideButtonBox: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 20
        return stackView
    }()

    
    convenience init(delegate: GSMiddlewareViewUIDelegate) {
        self.init()
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
        setOptionsMenu(options: [.Salesforce,.PortalesFFM,.IVRIPTV,.Dashboard,.Totalshop,.HogarSeguro, .MesaControl, .Ventas, .Colombia])
        self.delegate?.notifyOptionBuilder(builder:  [.Salesforce,.PortalesFFM,.IVRIPTV,.Dashboard,.Totalshop,.HogarSeguro, .MesaControl, .Ventas, .Colombia])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
    }
    
    fileprivate func setupUIElements() {
        addSubview(slideButtonBox)
        addSubview(separatorView)
        addSubview(slideView)
        addSubview(bodyContainer)
    }
    
    fileprivate func setupConstraints() {
        NSLayoutConstraint.activate([
            
            slideButtonBox.topAnchor.constraint(equalTo: topAnchor),
            slideButtonBox.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            slideButtonBox.heightAnchor.constraint(equalToConstant: 30),
            
            slideView.topAnchor.constraint(equalTo: slideButtonBox.bottomAnchor),
            
            separatorView.topAnchor.constraint(equalTo: slideButtonBox.bottomAnchor, constant: 2.5),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            separatorView.heightAnchor.constraint(equalToConstant: 0.5),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            bodyContainer.topAnchor.constraint(equalTo: slideButtonBox.bottomAnchor),
            bodyContainer.leadingAnchor.constraint(equalTo: leadingAnchor),
            bodyContainer.trailingAnchor.constraint(equalTo: trailingAnchor),
            bodyContainer.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    private func setOptionsMenu(options: [GSPagerMiddlewareType]){
        for (ind, option) in options.enumerated() {
            let button = UIButton(frame: .zero)
            button.tag = option.rawValue
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setTitle(option.onButtonName(), for: .normal)
            button.setTitleColor(GSColorsManager.labelLightColor , for: .normal)
            if button.tag == GSPagerMiddlewareType.Salesforce.rawValue{
                button.alpha = 1
            }else{
                button.alpha = 0.53
            }
            button.titleLabel?.font = .Montserrat_Regular_16
            button.addTarget(self, action: #selector(self.onClickMenuOption(_:)), for: .touchUpInside)
            let onVertical = UIView(frame: .zero)
            onVertical.translatesAutoresizingMaskIntoConstraints = false
            onVertical.backgroundColor = GSColorsManager.primaryColor.withAlphaComponent(0)
            NSLayoutConstraint.activate([
                onVertical.widthAnchor.constraint(equalToConstant: 0.5),
                onVertical.heightAnchor.constraint(equalToConstant: 20)
            ])
            optionMenu.append(button)
            slideButtonBox.addArrangedSubview(button)
            if ind != (options.count - 1){
                slideButtonBox.addArrangedSubview(onVertical)
            }
        }
        if let sizeButton = GSSubHomeViewType(rawValue: optionMenu[0].tag)?.onButtonName().widthOfString(usingFont: .Montserrat_Regular_16.withSize(16)), let buttonSelected = optionMenu.first {
            leadingAnchotSlide = NSLayoutConstraint(item: slideView, attribute: .centerX, relatedBy: .equal, toItem: buttonSelected, attribute: .centerX, multiplier: 1, constant: 0)
            leadingAnchotSlide?.isActive = true
            centerXAnchorSlide = NSLayoutConstraint(item: slideView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: sizeButton)
            centerXAnchorSlide?.isActive = true
        }
    }
    
    private func setSlideViewPosition(option: GSPagerMiddlewareType){
        let buttonFilter = optionMenu.filter { button in
            return button.tag == option.rawValue
        }
        if let buttonSelected = buttonFilter.first {
            let sizeButton = option.onButtonName().widthOfString(usingFont: .Montserrat_Regular_16.withSize(16))
            leadingAnchotSlide?.isActive = false
            centerXAnchorSlide?.isActive = false
            
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut, .curveEaseOut], animations: { [self] in
                leadingAnchotSlide = NSLayoutConstraint(item: slideView, attribute: .centerX, relatedBy: .equal, toItem: buttonSelected, attribute: .centerX, multiplier: 1, constant: 0)
                leadingAnchotSlide?.isActive = true
                centerXAnchorSlide = NSLayoutConstraint(item: slideView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: sizeButton)
                centerXAnchorSlide?.isActive = true
                layoutIfNeeded()
            }, completion: nil)
           
        }
    }
    
    public func swipeTap(tag:Int){
        for button in optionMenu{
            if button.tag == tag{
                button.alpha = 1
            }else{
                button.alpha = 0.53
            }
        }
        if let option = GSPagerMiddlewareType(rawValue: tag){
            setSlideViewPosition(option: option)
        }
    }
    
    @objc func onClickMenuOption(_ sender: UIButton){
        swipeTap(tag: sender.tag)
        for (ind , buttonFilter) in optionMenu.enumerated(){
            if buttonFilter.tag == sender.tag{
                delegate?.notifyOptionSelected(option: ind)
            }
        }
    }
    
    func didUpdateTabs(list: ListMiddlewareStatusServiceProtocol.Response){
        print("Refactor color")
        for button in optionMenu{
            switch button.tag {
            case GSPagerMiddlewareType.Salesforce.rawValue:
                let conteninsRed = list.infoSalesforce?.contains(where: { element in
                    return (element.monitor?.first?.status == "ERROR" || element.monitor?.first?.status == "INACTIVE")
                })
              
                let conteninsWarning = list.infoSalesforce?.contains(where: { element in
                    return element.monitor?.first?.status == "WARNING"
                })
                button.setTitleColor(
                    conteninsRed == true ? GSColorsManager.contingencyLabelColor :
                    conteninsWarning == true ? GSColorsManager.labelGoldColor :
                        GSColorsManager.labelLightColor, for: .normal)
                break
            case GSPagerMiddlewareType.IVRIPTV.rawValue:
                let conteninsRed = list.infoIvrIptv?.contains(where: { element in
                    return (element.monitor?.first?.status == "ERROR" || element.monitor?.first?.status == "INACTIVE")
                })
                let conteninsWarning = list.infoIvrIptv?.contains(where: { element in
                    return element.monitor?.first?.status == "WARNING"
                })
                button.setTitleColor(
                    conteninsRed == true ? GSColorsManager.contingencyLabelColor :
                    conteninsWarning == true ? GSColorsManager.labelGoldColor :
                        GSColorsManager.labelLightColor, for: .normal)
                break
            case GSPagerMiddlewareType.PortalesFFM.rawValue:
                let conteninsRed = list.infoPortalesFFM?.contains(where: { element in
                    return (element.monitor?.first?.status == "ERROR" || element.monitor?.first?.status == "INACTIVE")
                })
                let conteninsWarning = list.infoPortalesFFM?.contains(where: { element in
                    return element.monitor?.first?.status == "WARNING"
                })
                button.setTitleColor(
                    conteninsRed == true ? GSColorsManager.contingencyLabelColor :
                    conteninsWarning == true ? GSColorsManager.labelGoldColor :
                        GSColorsManager.labelLightColor, for: .normal)
                break
            case GSPagerMiddlewareType.Dashboard.rawValue:
                let conteninsRed = list.infoDashboard?.contains(where: { element in
                    return (element.monitor?.first?.status == "ERROR" || element.monitor?.first?.status == "INACTIVE")
                })
                let conteninsWarning = list.infoDashboard?.contains(where: { element in
                    return element.monitor?.first?.status == "WARNING"
                })
                button.setTitleColor(
                    conteninsRed == true ? GSColorsManager.contingencyLabelColor :
                    conteninsWarning == true ? GSColorsManager.labelGoldColor :
                        GSColorsManager.labelLightColor, for: .normal)
                break
            case GSPagerMiddlewareType.Totalshop.rawValue:
                let conteninsRed = list.infoTotalshop?.contains(where: { element in
                    return (element.monitor?.first?.status == "ERROR" || element.monitor?.first?.status == "INACTIVE")
                })
                let conteninsWarning = list.infoTotalshop?.contains(where: { element in
                    return element.monitor?.first?.status == "WARNING"
                })
                button.setTitleColor(
                    conteninsRed == true ? GSColorsManager.contingencyLabelColor :
                    conteninsWarning == true ? GSColorsManager.labelGoldColor :
                        GSColorsManager.labelLightColor, for: .normal)
                break
            case GSPagerMiddlewareType.HogarSeguro.rawValue:
                let conteninsRed = list.infoHogarSeguro?.contains(where: { element in
                    return (element.monitor?.first?.status == "ERROR" || element.monitor?.first?.status == "INACTIVE")
                })
                let conteninsWarning = list.infoHogarSeguro?.contains(where: { element in
                    return element.monitor?.first?.status == "WARNING"
                })
                button.setTitleColor(
                    conteninsRed == true ? GSColorsManager.contingencyLabelColor :
                    conteninsWarning == true ? GSColorsManager.labelGoldColor :
                        GSColorsManager.labelLightColor, for: .normal)
                break
            case GSPagerMiddlewareType.MesaControl.rawValue:
                let conteninsRed = list.infoMesaControl?.contains(where: { element in
                    return (element.monitor?.first?.status == "ERROR" || element.monitor?.first?.status == "INACTIVE")
                })
                let conteninsWarning = list.infoMesaControl?.contains(where: { element in
                    return element.monitor?.first?.status == "WARNING"
                })
                button.setTitleColor(
                    conteninsRed == true ? GSColorsManager.contingencyLabelColor :
                    conteninsWarning == true ? GSColorsManager.labelGoldColor :
                        GSColorsManager.labelLightColor, for: .normal)
                break
            case GSPagerMiddlewareType.Ventas.rawValue:
                let conteninsRed = list.infoVentas?.contains(where: { element in
                    return (element.monitor?.first?.status == "ERROR" || element.monitor?.first?.status == "INACTIVE")
                })
                let conteninsWarning = list.infoVentas?.contains(where: { element in
                    return element.monitor?.first?.status == "WARNING"
                })
                button.setTitleColor(
                    conteninsRed == true ? GSColorsManager.contingencyLabelColor :
                    conteninsWarning == true ? GSColorsManager.labelGoldColor :
                        GSColorsManager.labelLightColor, for: .normal)
                break
            case GSPagerMiddlewareType.Colombia.rawValue:
                let conteninsRed = list.infoColombia?.contains(where: { element in
                    return (element.monitor?.first?.status == "ERROR" || element.monitor?.first?.status == "INACTIVE")
                })
                let conteninsWarning = list.infoColombia?.contains(where: { element in
                    return element.monitor?.first?.status == "WARNING"
                })
                button.setTitleColor(
                    conteninsRed == true ? GSColorsManager.contingencyLabelColor :
                    conteninsWarning == true ? GSColorsManager.labelGoldColor :
                        GSColorsManager.labelLightColor, for: .normal)
                break
            default:
                break
            }
        }
    }
    
}
