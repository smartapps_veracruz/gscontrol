//
//  GSPagerMiddleware.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 08/02/22.
//

import UIKit
import SDKGSServicesManager

enum GSPagerMiddlewareType: Int{
    case Salesforce
    case PortalesFFM
    case IVRIPTV
    case Dashboard
    case Totalshop
    case HogarSeguro
    case MesaControl
    case Ventas
    case Colombia
    
    func onButtonName()->String{
        switch self{
        case .Salesforce:
            return "Salesforce"
        case .PortalesFFM:
            return "Portales/FFM"
        case .IVRIPTV:
            return "IVR/IPTV"
        case .Dashboard:
            return "Dashboard"
        case .Ventas:
            return "Ventas"
        case .Totalshop:
            return "Totalshop"
        case .HogarSeguro:
            return "HS"
        case .MesaControl:
            return "MC"
        case .Colombia:
            return "Colombia"
        } 
    }
}
protocol GSPagerMiddlewareDelegate{
    func onSwipePage(tag:Int)
}

class GSPagerMiddleware: UIPageViewController {
    var delegateSwipe : GSPagerMiddlewareDelegate?
    var navController: UINavigationController?
    var middlewareOptions : [GSPagerMiddlewareType] = []
    var pagerControllers: [UIViewController] = []
    private var activeTag: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        dataSource = self
    }
    
    
    public func onTabOption(option:Int){
        self.setViewControllers([pagerControllers[option]], direction: activeTag > option ? .reverse : .forward, animated: true, completion: nil)
        activeTag = option
    }
    
    public func updateBuildInformation(response: ListMiddlewareStatusServiceProtocol.Response){
        for (ind,controller) in middlewareOptions.enumerated(){
            switch controller {
            case .Salesforce:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response.infoSalesforce)
            case .PortalesFFM:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response.infoPortalesFFM)
            case .IVRIPTV:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response.infoIvrIptv)
            case .Dashboard:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response.infoDashboard)
            case .Totalshop:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response.infoTotalshop)
            case .HogarSeguro:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response.infoHogarSeguro)
            case .MesaControl:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response.infoMesaControl)
            case .Ventas:
                (pagerControllers[ind]as? GSGenericPageMiddlewareView)?.didUpdateData(response: response.infoVentas)
            case .Colombia:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response.infoColombia)
            }
        }
    }
    
    public func onBuilderOptions(options: [GSPagerMiddlewareType]){
        middlewareOptions = options
        for controller in middlewareOptions{
            switch controller {
            case .Salesforce:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: controller)
                pagerControllers.append(view)
            case .PortalesFFM:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: controller)
                pagerControllers.append(view)
            case .IVRIPTV:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: controller)
                pagerControllers.append(view)
            case .Dashboard:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: controller)
                pagerControllers.append(view)
            case .Totalshop:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: controller)
                pagerControllers.append(view)
            case .HogarSeguro:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: controller)
                pagerControllers.append(view)
            case .MesaControl:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: controller)
                pagerControllers.append(view)
            case .Ventas:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: controller)
                pagerControllers.append(view)
            case .Colombia:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: controller)
                pagerControllers.append(view)
            }
            
        }
        self.setViewControllers([pagerControllers[0]], direction: .forward, animated: false, completion: nil)
    }
}



extension GSPagerMiddleware: UIPageViewControllerDelegate, UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = pagerControllers.firstIndex(of: viewController), index > 0
        else{
            return nil
        }
        let before = index - 1
        return pagerControllers[before]
    }
  

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = pagerControllers.firstIndex(of: viewController), index < (pagerControllers.count - 1)
        else{
            return nil
        }
        let after = index + 1
        if after == pagerControllers.lastIndex(of: pagerControllers[pagerControllers.count-1]) {
        }
        return pagerControllers[after]
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            if let currentVC = pageViewController.viewControllers?.first, let currentIndex = pagerControllers.firstIndex(of: currentVC) {
                delegateSwipe?.onSwipePage(tag: currentIndex)
                activeTag = currentIndex
            }
        }
    }
    
}
