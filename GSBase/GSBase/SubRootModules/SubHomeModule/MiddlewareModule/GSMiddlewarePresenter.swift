//
//  GSMiddlewarePresenter.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

/// GSMiddleware Module Presenter
class GSMiddlewarePresenter {
    
    weak var _view: GSMiddlewareViewProtocol?
    var interactor: GSMiddlewareInteractorProtocol?
    var wireframe: GSMiddlewareRouterProtocol?
}

// MARK: - extending GSMiddlewarePresenter to implement it's protocol
extension GSMiddlewarePresenter: GSMiddlewarePresenterProtocol {
    func requestMiddlewareList(withLoader:Bool) {
        if withLoader {
            _view?.showLoader()
        }
        interactor?.fetchMiddlewareList()
    }
    
    func responseMiddlewareList(response: ListMiddlewareStatusServiceProtocol.Response) {
        _view?.dismissLoader()
        _view?.notifyReloadMiddlewareList(list: response)
    }
    
    func responseError(msg: String, isEmptyData: Bool) {
        _view?.dismissLoader()
        _view?.notifyServiceError(msg: msg)
    }
    
}
