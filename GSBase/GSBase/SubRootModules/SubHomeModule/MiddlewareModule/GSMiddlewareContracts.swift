//
//  GSMiddlewareContracts.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager
//MARK: View -
/*
 Should replace "class" with "BaseViewProtocol" if available;
 & that will allow the View to act as a UIViewController;
 & Implement common view functions.
 */
/// GSMiddleware Module View Protocol
protocol GSMiddlewareViewProtocol: AnyObject {
    func showLoader()
    func dismissLoader()
    func notifyReloadMiddlewareList(list: ListMiddlewareStatusServiceProtocol.Response)
    func notifyServiceError(msg: String)
}

//MARK: Interactor -
/// GSMiddleware Module Interactor Protocol
protocol GSMiddlewareInteractorProtocol: AnyObject {
    func fetchMiddlewareList()
}

//MARK: Presenter -
/// GSMiddleware Module Presenter Protocol
protocol GSMiddlewarePresenterProtocol: AnyObject {
    func requestMiddlewareList(withLoader:Bool)
    func responseMiddlewareList(response: ListMiddlewareStatusServiceProtocol.Response)
    func responseError(msg: String, isEmptyData: Bool)
}

//MARK: Router (aka: Wireframe) -
/// GSMiddleware Module Router Protocol
protocol GSMiddlewareRouterProtocol: AnyObject {
    
}
