//
//  GSGenericPageMiddlewareViewUI.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 08/02/22.
//  Copyright © 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager

// MARK: GSGenericPageMiddlewareViewUI Delegate -
/// GSGenericPageMiddlewareViewUI Delegate
protocol GSGenericPageMiddlewareViewUIDelegate {
    func notifyDisplayPanel(response: ListMiddlewareStatusResponse.infoMiddleware?)
}


class GSGenericPageMiddlewareViewUI: UIView {
    
    var delegate: GSGenericPageMiddlewareViewUIDelegate?
    var response : [ListMiddlewareStatusResponse.infoMiddleware]?{
        didSet{
            bodyOptions.reloadData()
        }
    }
    lazy var bodyOptions: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = false
        collectionView.collectionViewLayout = UICollectionViewFlowLayout.init()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 30, right: 0)
        collectionView.alwaysBounceVertical = true
        collectionView.register(GSMiddlewareViewCell.self, forCellWithReuseIdentifier: "GSMiddlewareViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    private lazy var sectionInsets : UIEdgeInsets = {
        let paddingSpace = 15 * (itemsPerRow + 1)
        let availableWidth = self.frame.width - paddingSpace
        let inset = UIEdgeInsets(
        top: 0.0,
        left: 15.0,
        bottom: 0.0,
        right: 15)//availableWidth / itemsPerRow)
        return inset
    }()
    var withModule : GSPagerMiddlewareType?
    private let itemsPerRow: CGFloat = 3
    
    convenience init(delegate :GSGenericPageMiddlewareViewUIDelegate, withModule : GSPagerMiddlewareType) {
        self.init()
        self.withModule = withModule
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
    }
    
    fileprivate func setupUIElements() {
        // arrange subviews
        addSubview(bodyOptions)
    }
    
    fileprivate func setupConstraints() {
        NSLayoutConstraint.activate([
        
            bodyOptions.topAnchor.constraint(equalTo: topAnchor),
            bodyOptions.leadingAnchor.constraint(equalTo: leadingAnchor),
            bodyOptions.trailingAnchor.constraint(equalTo: trailingAnchor),
            bodyOptions.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}

extension GSGenericPageMiddlewareViewUI: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return response?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GSMiddlewareViewCell", for: indexPath) as? GSMiddlewareViewCell{
            
            let colorBorder = response?[indexPath.row].monitor?.first?.status == "OK" ? UIColor.init(red: 0/255, green: 127/255, blue: 255/255, alpha: 0.16) :
            response?[indexPath.row].monitor?.first?.status == "WARNING" ? GSColorsManager.labelGoldColor.withAlphaComponent(0.81) :
            GSColorsManager.contingencyLabelColor.withAlphaComponent(0.81)
            
            
            let colorChar = response?[indexPath.row].monitor?.first?.status == "OK" ? GSColorsManager.labelLightColor :
            response?[indexPath.row].monitor?.first?.status == "WARNING" ? GSColorsManager.labelGoldColor :
            GSColorsManager.contingencyLabelColor
            cell.titleCell.text = response?[indexPath.row].serviceName ?? "SOA vs SOA_12C_HA"
            cell.ipCell.text = response?[indexPath.row].ip ?? "0.0.0.0"
            cell.currentValueCell.text = response?[indexPath.row].monitor?.first?.value ?? "0"
            cell.containerView.layer.borderColor = colorBorder.cgColor
            cell.containerView.layer.borderWidth = 1
            cell.view1.backgroundColor = colorBorder
            cell.containerView.layer.cornerRadius = 15
            cell.currentValueCell.textColor = GSColorsManager.labelLightColor
            cell.currentValueCell.font = response?[indexPath.row].monitor?.first?.status == "OK" ? .Montserrat_Medium_16.withSize(14) : .Montserrat_Bold_16.withSize(14)
            cell.didUpdate(response: response?[indexPath.row].monitor, lineColor: colorChar)
            cell.delegate = self
            cell.tag = indexPath.row
            return cell
        }
        return  UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = (15 * (itemsPerRow + 1) - 15) + sectionInsets.left
        let availableWidth = self.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return  CGSize(width: widthPerItem,  height: widthPerItem * 0.51)
    }

    func collectionView( _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: sectionInsets.top, left: sectionInsets.left, bottom: sectionInsets.bottom, right: sectionInsets.right)
    }
}

extension GSGenericPageMiddlewareViewUI: GSMiddlewareViewCellDelegate{
    func didCellSelected(position: Int) {
        delegate?.notifyDisplayPanel(response: response?[position])
    }
    
    
}
