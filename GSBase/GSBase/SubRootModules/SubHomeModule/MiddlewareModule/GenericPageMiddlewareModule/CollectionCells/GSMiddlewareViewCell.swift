//
//  GSMiddlewareViewCell.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 22/12/21.
//

import UIKit
import Foundation
import SDKGSCommonUtils
import SDKGSServicesManager

protocol GSMiddlewareViewCellDelegate: AnyObject{
    func didCellSelected(position: Int)
}

class GSMiddlewareViewCell: UICollectionViewCell {
    
    weak var delegate: GSMiddlewareViewCellDelegate?
    
    lazy var didTapButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(self.didSelected(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var lineChart: LineChart = {
        var lineChart =  LineChart()
        lineChart.translatesAutoresizingMaskIntoConstraints = false
        lineChart.animation.enabled = true
        lineChart.area = true
        lineChart.x.labels.visible = false
        return lineChart
    }()
    lazy var containerView: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var backgroundCell: UIImageView = {
        let view = UIImageView(frame: .zero)
        view.image = UIImage(named: "cell_background_ic", in: .local_gs_utils, compatibleWith: nil)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var titleCell: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Regular_14
        label.textColor = GSColorsManager.labelLightColor
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    lazy var ipCell: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Regular_14
        label.textColor = GSColorsManager.labelLightColor
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    let view1 = UIView(frame: .zero)
    lazy var currentCell: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(view1)
        view1.addSubview(currentValueCell)
        NSLayoutConstraint.activate([
            view.heightAnchor.constraint(equalToConstant: 30),
            view1.heightAnchor.constraint(equalToConstant: 30),
            view1.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            currentValueCell.leadingAnchor.constraint(equalTo: view1.leadingAnchor, constant: 30),
            currentValueCell.trailingAnchor.constraint(equalTo: view1.trailingAnchor, constant: -30),
            currentValueCell.centerXAnchor.constraint(equalTo: view1.centerXAnchor),
            currentValueCell.centerYAnchor.constraint(equalTo: view1.centerYAnchor),
            
        ])
        return view
    }()
    
    lazy var currentValueCell: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    lazy var mainStackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.addArrangedSubview(titleCell)
        stackView.addArrangedSubview(ipCell)
        stackView.addArrangedSubview(currentCell)
        return stackView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUIElements()
        setupConstraints()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupUIElements() {
        addSubview(containerView)
        containerView.addSubview(backgroundCell)
        containerView.addSubview(mainStackView)
        containerView.addSubview(lineChart)
        containerView.addSubview(didTapButton)
    }
    
    fileprivate func setupConstraints() {
        mainStackView.setContentHuggingPriority(.defaultLow, for: .vertical)
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            backgroundCell.topAnchor.constraint(equalTo: containerView.topAnchor),
            backgroundCell.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            backgroundCell.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            backgroundCell.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            
            mainStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 10),
            mainStackView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 0.5, constant: -10),
            mainStackView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            
            lineChart.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            lineChart.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 5),
            lineChart.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -5),
            lineChart.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 0.5),
            lineChart.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            
            didTapButton.topAnchor.constraint(equalTo: topAnchor),
            didTapButton.leadingAnchor.constraint(equalTo: leadingAnchor),
            didTapButton.trailingAnchor.constraint(equalTo: trailingAnchor),
            didTapButton.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    @objc func didSelected(_ sender: UIButton){
        delegate?.didCellSelected(position: tag)
    }
    
    public func didUpdate(response: [ListMiddlewareStatusResponse.infoMiddleware._monitor]?, lineColor: UIColor){
        let resposeAplicated = response?.reversed()
        var labels = [String]()
        resposeAplicated?.forEach({ element in
            labels.append(element.hour ?? "")
        })
        
        var values = [CGFloat]()
        resposeAplicated?.forEach({ element in
            values.append(CGFloat(element.value?.floatValue() ?? 0))
           // if let value = element.value?.floatValue(){
           //     values.append(CGFloat(value))
           // }
        })
        if CGFloat(labels.count) == CGFloat(values.count){
            lineChart.clear()
            lineChart.colors = lineColor
            lineChart.animation.enabled = false
            lineChart.x.grid.count = CGFloat(labels.count)
            lineChart.y.grid.count = CGFloat(values.count)
            lineChart.x.labels.values = labels
            lineChart.y.labels.visible = false
            lineChart.addLine(values)
        }
      
    }
}
