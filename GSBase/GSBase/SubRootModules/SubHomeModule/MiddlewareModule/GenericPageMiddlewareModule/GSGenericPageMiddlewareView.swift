//
//  GSGenericPageMiddlewareView.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 08/02/22.
//  Copyright © 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

/// GSGenericPageMiddleware Module View
class GSGenericPageMiddlewareView: UIViewController {
    var withModule : GSPagerMiddlewareType?
    private var ui : GSGenericPageMiddlewareViewUI?
    var response : [ListMiddlewareStatusResponse.infoMiddleware]?
    override func loadView() {
        
        ui = GSGenericPageMiddlewareViewUI(delegate: self, withModule: withModule ?? GSPagerMiddlewareType.PortalesFFM)
        view = ui
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ui?.response = response
    }
    
    public func didUpdateData(response: [ListMiddlewareStatusResponse.infoMiddleware]?){
        self.response = response
        ui?.response = response
    }
}


// MARK: - extending GSGenericPageMiddlewareView to implement the custom ui view delegate
extension GSGenericPageMiddlewareView: GSGenericPageMiddlewareViewUIDelegate {
    
    func notifyDisplayPanel(response: ListMiddlewareStatusResponse.infoMiddleware?) {
        let panel = GSPanelPageMiddlewareMain.createModule(response: response)
        panel.modalPresentationStyle = .overFullScreen
        present(panel, animated: false, completion: nil)
    }
}
