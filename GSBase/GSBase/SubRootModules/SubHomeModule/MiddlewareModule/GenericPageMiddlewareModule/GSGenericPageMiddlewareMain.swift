//
//  GSGenericPageMiddlewareMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 08/02/22.
//

import UIKit

class GSGenericPageMiddlewareMain: NSObject {
    
    static func createModule(withModule : GSPagerMiddlewareType)->UIViewController{
        
        let viewController  :    GSGenericPageMiddlewareView?   =   GSGenericPageMiddlewareView()
        if let view = viewController {
            view.withModule = withModule
            return view
        }
        return UIViewController()
    }
}
