//
//  GSMiddlewareView.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager
import SDKGSCommonUtils

/// GSMiddleware Module View
class GSMiddlewareView: UIViewController {
    
    private var ui : GSMiddlewareViewUI?
    var presenter: GSMiddlewarePresenterProtocol?
    var pagerController = GSPagerMiddleware(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    private var timer: Timer?
    
    override func loadView() {
        // setting the custom view as the view controller's view
        ui = GSMiddlewareViewUI(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pagerController.delegateSwipe = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.timer = Timer.scheduledTimer(timeInterval: 7, target: self, selector: #selector(updateCountDown), userInfo: nil, repeats: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc private func updateCountDown(){
        presenter?.requestMiddlewareList(withLoader: false)
    }
}

//// MARK: - extending GSMiddlewareView to implement it's protocol
extension GSMiddlewareView: GSMiddlewareViewProtocol {

    func showLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController{
                GSLoader.show(parent: rootController.view)
            }
        }
    }

    func dismissLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController {
                GSLoader.remove(parent: rootController.view)
            }
        }
    }

    func notifyReloadMiddlewareList(list: ListMiddlewareStatusServiceProtocol.Response) {
        pagerController.updateBuildInformation(response: list)
        ui?.didUpdateTabs(list: list)
    }

    func notifyServiceError(msg: String) {
        
    }
}

// MARK: - extending GSMiddlewareView to implement the custom ui view delegate
extension GSMiddlewareView: GSMiddlewareViewUIDelegate {
    
    func notifyOptionBuilder(builder: [GSPagerMiddlewareType]) {
        pagerController.onBuilderOptions(options: builder)
        presenter?.requestMiddlewareList(withLoader: true)
    }
    
    func notifyOptionSelected(option: Int) {
        pagerController.onTabOption(option: option)
    }
    
    func notifyGetParentView() -> UIViewController {
        return self
    }
    
    func notifyPagerViewController() -> UIPageViewController {
        return pagerController
    }
    
}

extension GSMiddlewareView: GSPagerMiddlewareDelegate{
    func onSwipePage(tag:Int){
        ui?.swipeTap(tag: tag)
    }
}
