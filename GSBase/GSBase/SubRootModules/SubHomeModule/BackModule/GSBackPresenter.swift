//
//  GSBackPresenter.swift
//  GSBase
//
//  Created by Gustavo Tellez on 24/12/21.
//

import Foundation
import SDKGSServicesManager

class GSBackPresenter{
    public weak var view    :   GSBackViewProtocol?
    public var router       :   GSBackRouterProtocol?
    public var interactor   :   GSBackInteractorProtocol?
}

extension GSBackPresenter: GSBackPresenterProtocol{
    
    func requestBackendList(withLoader:Bool) {
        if withLoader{
            view?.showLoader()
        }
        interactor?.fetchBackendList()
    }
    
    func responseBackendList(response: ListBackendStatusServiceProtocol.Response) {
        view?.dismissLoader()
        view?.notifyReloadBackendList(list: response)
    }
    
    func responseError(msg: String, isEmptyData: Bool) {
        view?.dismissLoader()
        view?.notifyServiceError(msg: msg, isEmptyData: isEmptyData)
    }
}
