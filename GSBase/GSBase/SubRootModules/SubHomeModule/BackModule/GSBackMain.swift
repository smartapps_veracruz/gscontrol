//
//  GSBackMain.swift
//  GSBase
//
//  Created by Gustavo Tellez on 24/12/21.
//

import UIKit

open class GSBackMain{
    
    public static func createModule() -> UIViewController{
        
        let viewController : GSBackView? = GSBackView()
        
        if let view = viewController{
            let interactor  =   GSBackInteractor()
            let presenter   =   GSBackPresenter()
            let router      =   GSBackRouter()
            
            view.presenter = presenter
            
            presenter.view          =   view
            presenter.router        =   router
            presenter.interactor    =   interactor
            
            interactor.presenter = presenter
            
            return view
        }
        
        return UIViewController()
    }
}
