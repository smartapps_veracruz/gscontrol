//
//  GSBackInteractor.swift
//  GSBase
//
//  Created by Gustavo Tellez on 24/12/21.
//

import Foundation
import SDKGSServicesManager

class GSBackInteractor{
    public weak var presenter   :   GSBackPresenterProtocol?
    private let facade          =   NOCServicesFacade.sharedHome
}

extension GSBackInteractor: GSBackInteractorProtocol{
    
    func fetchBackendList() {
        let request = ListBackendStatusRequest(dashboardType: "BACK")
        
        self.facade.fetchListBackendStatus(request: request) { response, _ in
            DispatchQueue.main.async {
                self.presenter?.responseBackendList(response: response)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: emptyMessage ?? "", isEmptyData: true)
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: errorMessage ?? "", isEmptyData: false)
            }
        }
    }
}
