//
//  GSBackContracts.swift
//  GSBase
//
//  Created by Gustavo Tellez on 24/12/21.
//

import Foundation
import SDKGSServicesManager


protocol GSBackViewProtocol: AnyObject{
    func showLoader()
    func dismissLoader()
    
    func notifyReloadBackendList(list: ListBackendStatusServiceProtocol.Response)
    func notifyServiceError(msg: String, isEmptyData: Bool)
}

protocol GSBackPresenterProtocol: AnyObject{
    func requestBackendList(withLoader:Bool)
    func responseBackendList(response: ListBackendStatusServiceProtocol.Response)
    
    func responseError(msg: String, isEmptyData: Bool)
}

protocol GSBackInteractorProtocol: AnyObject{
    func fetchBackendList()
}

protocol GSBackRouterProtocol: AnyObject{
}
