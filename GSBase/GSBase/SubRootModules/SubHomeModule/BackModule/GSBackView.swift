//
//  GSBackView.swift
//  GSBase
//
//  Created by Gustavo Tellez on 24/12/21.
//

import UIKit
import SDKGSServicesManager
import SDKGSCommonUtils

class GSBackView: UIViewController {

    private var ui: GSBackViewUI?
    public var presenter: GSBackPresenterProtocol?
    private var timer: Timer?
    var pagerController = GSPagerBack(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    override func loadView() {
        ui = GSBackViewUI(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pagerController.delegateSwipe = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.timer = Timer.scheduledTimer(timeInterval: 7, target: self, selector: #selector(updateCountDown), userInfo: nil, repeats: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc private func updateCountDown(){
        presenter?.requestBackendList(withLoader: false)
    }
    
}

extension GSBackView: GSBackViewUIDelegate{
    func notifyOptionBuilder(builder: [GSPagerBackType]) {
        pagerController.onBuilderOptions(options: builder)
        presenter?.requestBackendList(withLoader: true)
    }
    
    func notifyOptionSelected(option: Int) {
        pagerController.onTabOption(option: option)
    }
    
    func notifyGetParentView() -> UIViewController {
        return self
    }
    
    func notifyPagerViewController() -> UIPageViewController {
        return pagerController
    }
}

extension GSBackView: GSBackViewProtocol{
    
    func showLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController{
                GSLoader.show(parent: rootController.view)
            }
        }
    }

    func dismissLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController {
                GSLoader.remove(parent: rootController.view)
            }
        }
    }

    func notifyReloadBackendList(list: ListBackendStatusServiceProtocol.Response) {
        pagerController.updateBuildInformation(response: list)
        ui?.didUpdateTabs(list: list)
    }
    
    func notifyServiceError(msg: String, isEmptyData: Bool) {
    }
}


extension GSBackView: GSPagerBackDelegate{
    func onSwipePage(tag:Int){
        ui?.swipeTap(tag: tag)
    }
}
