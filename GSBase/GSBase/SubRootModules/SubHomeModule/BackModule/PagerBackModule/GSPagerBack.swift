//
//  GSPagerBack.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 13/06/22.
//

import UIKit
import SDKGSServicesManager

enum GSPagerBackType: Int{
    case TProvioning
    case GSS
    case RelayCorreo
    
    func onButtonName()->String{
        switch self{
        case .TProvioning:
            return "TProvioning"
        case .GSS:
            return "GSS"
        case .RelayCorreo:
            return "Relay Correo"
        }
    }
}

protocol GSPagerBackDelegate{
    func onSwipePage(tag:Int)
}

class GSPagerBack: UIPageViewController {
    
    var delegateSwipe : GSPagerBackDelegate?
    var navController: UINavigationController?
    var middlewareOptions : [GSPagerBackType] = []
    var pagerControllers: [UIViewController] = []
    private var activeTag: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        dataSource = self
    }
    
    
    public func onTabOption(option:Int){
        self.setViewControllers([pagerControllers[option]], direction: activeTag > option ? .reverse : .forward, animated: true, completion: nil)
        activeTag = option
    }
    
    public func updateBuildInformation(response: ListBackendStatusServiceProtocol.Response){
        for (ind,controller) in middlewareOptions.enumerated(){
            switch controller {
            case .TProvioning:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response?.infoProvisioning)
            case .GSS:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response?.infoGSS)
            case .RelayCorreo:
                (pagerControllers[ind] as? GSGenericPageMiddlewareView)?.didUpdateData(response: response?.infoRelayCorreo)
            }
        }
    }
    
    public func onBuilderOptions(options: [GSPagerBackType]){
        middlewareOptions = options
        for controller in middlewareOptions{
            switch controller {
            case .TProvioning:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: .PortalesFFM)
                pagerControllers.append(view)
            case .GSS:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: .PortalesFFM)
                pagerControllers.append(view)
            case .RelayCorreo:
                let view = GSGenericPageMiddlewareMain.createModule(withModule: .PortalesFFM)
                pagerControllers.append(view)
            }
            
        }
        self.setViewControllers([pagerControllers[0]], direction: .forward, animated: false, completion: nil)
    }
}

extension GSPagerBack: UIPageViewControllerDelegate, UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = pagerControllers.firstIndex(of: viewController), index > 0
        else{
            return nil
        }
        let before = index - 1
        return pagerControllers[before]
    }
  

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = pagerControllers.firstIndex(of: viewController), index < (pagerControllers.count - 1)
        else{
            return nil
        }
        let after = index + 1
        if after == pagerControllers.lastIndex(of: pagerControllers[pagerControllers.count-1]) {
        }
        return pagerControllers[after]
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            if let currentVC = pageViewController.viewControllers?.first, let currentIndex = pagerControllers.firstIndex(of: currentVC) {
                delegateSwipe?.onSwipePage(tag: currentIndex)
                activeTag = currentIndex
            }
        }
    }
    
}
