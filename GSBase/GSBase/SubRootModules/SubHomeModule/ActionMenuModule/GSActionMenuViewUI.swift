//
//  GSActionMenuViewUI.swift
//  GSBase
//
//  Created by Gustavo Tellez on 24/12/21.
//

import UIKit
import SDKGSCommonUtils

public protocol GSActionMenuViewUIDelegate{
    func notifyButtonClosePressed()
}

class GSActionMenuViewUI: UIView {
    
    private lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
//    private lazy var borderView: UIView = {
//        let view = UIView()
//        view.backgroundColor = .clear
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
    
    private lazy var imgCheck: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.image = UIImage(named: "icon_checkLightBlueCircle", in: Bundle.local_gs_utils, compatibleWith: nil)
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    private lazy var lbTitle: UILabel = {
        let lb = UILabel()
        lb.text = "Acciones"
        lb.textAlignment = .left
        lb.font = UIFont.Montserrat_Semibold_16
        lb.textColor = GSColorsManager.primaryColor
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    private lazy var btnClose: UIButton = {
        let btn = UIButton()
        btn.addTarget(self, action: #selector(self.onClickCloseOption(_:)), for: .touchUpInside)
        btn.setImage(UIImage(named: "icon_closeBlueCircle", in: Bundle.local_gs_utils, compatibleWith: nil), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    private lazy var optionsMenuStack: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private var optionsMenu : [UIButton] = []
    public var titleOptions : [String] = [] {
        didSet{
            buildOptionsMenu()
        }
    }
    
    public var delegate             :   GSActionMenuViewUIDelegate?
    private let screen              =   UIScreen.main.bounds
    private let statusBarHeight     =   UIApplication.getStatusBarHeight()
    
    public init(){
        super.init(frame: CGRect(x: -(screen.width * 0.24),
                                 y: statusBarHeight,
                                 width: screen.width * 0.24,
                                 height: screen.height - (statusBarHeight * 2)))
        
        buildUIElements()
        buildContraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildUIElements(){
        self.setGradientBackground()
        addViewStyle(viewParent: self, corners: [.bottomLeft, .bottomRight],
                          radius: 30.0,
                          borderColor: GSColorsManager.buttonBlueAqua,
                          borderWidth: 1.0)
        [imgCheck, lbTitle, btnClose].forEach { component in
            headerView.addSubview(component)
        }
        
        [headerView, optionsMenuStack].forEach { component in
            self.addSubview(component)
        }
    }
    
    private func buildContraints(){
        NSLayoutConstraint.activate([
            headerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 20.0),
            headerView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.0),
            headerView.heightAnchor.constraint(equalToConstant: 40.0),
            headerView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.0),
            
            imgCheck.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            imgCheck.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            imgCheck.heightAnchor.constraint(equalToConstant: 20.0),
            imgCheck.widthAnchor.constraint(equalToConstant: 20.0),
            
            lbTitle.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            lbTitle.leadingAnchor.constraint(equalTo: imgCheck.trailingAnchor, constant: 15.0),
            lbTitle.trailingAnchor.constraint(equalTo: btnClose.leadingAnchor, constant: -15.0),
            
            btnClose.topAnchor.constraint(equalTo: headerView.topAnchor),
            btnClose.widthAnchor.constraint(equalToConstant: 40.0),
            btnClose.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            btnClose.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),
            
            optionsMenuStack.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 40.0),
            optionsMenuStack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.0),
            optionsMenuStack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.0),
            optionsMenuStack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -80.0),
            
//            borderView.topAnchor.constraint(equalTo: topAnchor, constant: 3),
//            borderView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 3),
//            borderView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -3),
//            borderView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -3),
        ])
    }
    
    private func setGradientBackground() {
        let colorTop =  GSColorsManager.borderBlue.cgColor
        let colorBottom = GSColorsManager.borderBlue.cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.bounds
        
        self.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func addViewStyle(viewParent: UIView, corners : UIRectCorner, radius : CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        let path = UIBezierPath(roundedRect: viewParent.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        viewParent.layer.mask = mask
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path // Reuse the Bezier path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.frame = viewParent.bounds
        viewParent.layer.addSublayer(borderLayer)
    }
    
    private func buildOptionsMenu(){
        
        for (index, title) in titleOptions.enumerated(){
            let button = UIButton(frame: .zero)
            button.tag = index
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setTitle(title, for: .normal)
            button.setTitleColor(GSColorsManager.buttonBlueAqua, for: .normal)
            button.titleLabel?.font = .Montserrat_Semibold_16
            button.layer.cornerRadius = 20.0
            button.backgroundColor = UIColor.black
            button.addTarget(self, action: #selector(self.onClickMenuOption(_:)), for: .touchUpInside)
         
            NSLayoutConstraint.activate([
                button.heightAnchor.constraint(equalToConstant: 40),
            ])
            
            optionsMenu.append(button)
            optionsMenuStack.addArrangedSubview(button)
        }
    }
    
    @objc func onClickCloseOption(_ sender: UIButton){
        delegate?.notifyButtonClosePressed()
    }
    
    @objc func onClickMenuOption(_ sender: UIButton){
        print("El usuario presiono el boton \(sender.tag)...")
    }
}
