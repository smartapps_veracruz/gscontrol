//
//  GSActionMenuView.swift
//  GSBase
//
//  Created by Gustavo Tellez on 24/12/21.
//

import UIKit

public enum GSFlowType{
    case applicationFront
    case back
}

class GSActionMenuView: UIViewController {
    
    private var actionMenuView : GSActionMenuViewUI = {
        let view = GSActionMenuViewUI()
        return view
    }()
    
    public var flowType : GSFlowType?
    private let screen  = UIScreen.main.bounds

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        let tap = UITapGestureRecognizer(target: self, action: #selector(closeFlow))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        
        buildUI()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showActionMenu()
    }
    
    private func buildUI(){
        actionMenuView.delegate = self
        actionMenuView.titleOptions = ["Reiniciar proceso", "Liberar memoria",
                                       "Liberar espacio", "Check de servicio",
                                       "Balanceo", "Redeploybipe",
                                       "Reinicio de datasource", "Reinicio IIS"]
        
        self.view.addSubview(actionMenuView)
    }
    
    @objc func closeFlow(){
        hideActionMenu {
        }
    }
    
    private func showActionMenu(){
        UIView.animate(withDuration: 0.3) {
            self.actionMenuView.frame.origin.x = 0.0
        }
    }
    
    public func hideActionMenu(completionHandler: @escaping () -> Void){
        UIView.animate(withDuration: 0.3) {
            self.actionMenuView.frame.origin.x = -self.actionMenuView.frame.width

        } completion: { isFinish in
            if isFinish{
                self.dismiss(animated: false) {
                    completionHandler()
                }
            }
        }
    }
}

extension GSActionMenuView: GSActionMenuViewUIDelegate{
    
    func notifyButtonClosePressed() {
        closeFlow()
    }
}

extension GSActionMenuView : UIGestureRecognizerDelegate{
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == self.view
    }
}
