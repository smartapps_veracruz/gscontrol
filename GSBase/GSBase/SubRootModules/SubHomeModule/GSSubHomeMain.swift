//
//  GSSubHomeMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 21/12/21.
//

import UIKit

class GSSubHomeMain: NSObject {

    static func createModule()->UIViewController{
        
        let viewController  :   GSSubHomeView?   =  GSSubHomeView()
        if let view = viewController {
            return view
        }
        return UIViewController()
    }

}
