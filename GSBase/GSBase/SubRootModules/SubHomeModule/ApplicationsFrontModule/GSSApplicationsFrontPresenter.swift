//
//  GSSApplicationsFrontPresenter.swift
//  GSBase
//
//  Created by Gustavo Tellez on 23/12/21.
//

import Foundation
import SDKGSServicesManager

class GSSApplicationsFrontPresenter{
    
    public weak var view    :   GSSApplicationsFrontViewProtocol?
    public var router       :   GSSApplicationsFrontRouterProtocol?
    public var interactor   :   GSSApplicationsFrontInteractorProtocol?
}
 
extension GSSApplicationsFrontPresenter: GSSApplicationsFrontPresenterProtocol{
    
    func requestAppsFrontList(withLoader:Bool) {
        if withLoader{
            view?.showLoader()
        }
        interactor?.fetchAppsFrontList()
    }
    
    func responseAppsFrontList(response: ListFrontStatusResponse?) {
        view?.dismissLoader()
        
        if let data = response{
            view?.notifyReloadAppsFrontList(list: data.getAppsFrontList())
            
        }else{
            view?.notifyServiceError(msg: "No hay información por mostrar.", isEmptyData: true)
        }
    }
    
    func responseError(msg: String, isEmptyData: Bool) {
        view?.dismissLoader()
        view?.notifyServiceError(msg: msg, isEmptyData: isEmptyData)
    }
}
