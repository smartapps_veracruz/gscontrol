//
//  GSSApplicationsFrontInteractor.swift
//  GSBase
//
//  Created by Gustavo Tellez on 23/12/21.
//

import Foundation
import SDKGSServicesManager

class GSSApplicationsFrontInteractor{
    public weak var presenter   :   GSSApplicationsFrontPresenterProtocol?
    private let facade          =   NOCServicesFacade.sharedHome
}

extension GSSApplicationsFrontInteractor: GSSApplicationsFrontInteractorProtocol{
    
    func fetchAppsFrontList() {
        let request = ListFrontStatusRequest(dashboardType: "APPLICATIONS")
        
        facade.fetchListFrontStatus(request: request) { response, _ in
            DispatchQueue.main.async {
                self.presenter?.responseAppsFrontList(response: response)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: emptyMessage ?? "", isEmptyData: true)
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: errorMessage ?? "", isEmptyData: false)
            }
        }
    }
}
