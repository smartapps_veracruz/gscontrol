//
//  GSSApplicationsFrontViewUI.swift
//  GSBase
//
//  Created by Gustavo Tellez on 23/12/21.
//

import UIKit
import SDKGSServicesManager
import SDKGSCommonUtils

class GSSApplicationsFrontViewUI: UIView {
    
    private lazy var stackView : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 0
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = GSColorsManager.labelLightColor.withAlphaComponent(0.25)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var appsFrontCollection: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collection.backgroundColor = .clear
        collection.isPagingEnabled = false
        collection.delegate = self
        collection.dataSource = self
        collection.collectionViewLayout = UICollectionViewFlowLayout.init()
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.register(GSHeaderExceptionCollectionCell.self, forCellWithReuseIdentifier: GSHeaderExceptionCollectionCell.identifier)
        collection.register(GSExceptionStatusCollectionCell.self, forCellWithReuseIdentifier: GSExceptionStatusCollectionCell.identifier)
        return collection
    }()
    
    public var appsFrontList : [GSAppsFrontList] = [] {
        didSet{
            updateExceptionList()
            appsFrontCollection.reloadData()
        }
    }
    
    public var delegate: GSSApplicationsFrontViewUIProtocol?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public init(delegate: GSSApplicationsFrontViewUIProtocol){
        super.init(frame: CGRect.zero)
        
        self.delegate = delegate
        buildUIElements()
        buildConstraints()
    }
    
    private func buildUIElements(){
        self.backgroundColor = UIColor.clear
        
        [stackView, separatorView, appsFrontCollection].forEach { component in
            self.addSubview(component)
        }
    }
    
    private func buildConstraints(){
        
        NSLayoutConstraint.activate([
            
            stackView.topAnchor.constraint(equalTo: self.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            separatorView.leadingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: 5.0),
            separatorView.widthAnchor.constraint(equalToConstant: 0.5),
            separatorView.heightAnchor.constraint(equalTo: stackView.heightAnchor, multiplier: 7/8),
            separatorView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            appsFrontCollection.topAnchor.constraint(equalTo: self.topAnchor),
            appsFrontCollection.leadingAnchor.constraint(equalTo: separatorView.trailingAnchor, constant: 5.0),
            appsFrontCollection.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -30.0),
            appsFrontCollection.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    private func updateExceptionList(){
        
        if appsFrontList.count == 0{
            separatorView.isHidden = true
            stackView.removeAllArrangedSubviews()
            
        }else{
            if stackView.arrangedSubviews.isEmpty{
                let view =  UIView()
                view.backgroundColor = UIColor.clear
                view.translatesAutoresizingMaskIntoConstraints = false
                
                stackView.addArrangedSubview(view)
                separatorView.isHidden = false
                
                for exception in appsFrontList[0].getExceptionsList(){
                    let lb = UILabel()
                    lb.text = exception
                    lb.textAlignment = .right
                    lb.numberOfLines = 2
                    lb.font = UIFont.Montserrat_Medium_12
                    lb.textColor = GSColorsManager.labelStrongColor
                    lb.translatesAutoresizingMaskIntoConstraints = false

                    stackView.addArrangedSubview(lb)
                }
            }
        }
    }
}

extension GSSApplicationsFrontViewUI: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return appsFrontList.count == 0 ? 0 : appsFrontList[0].getExceptionsList().count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return appsFrontList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let totalException = appsFrontList.count == 0 ? 1 : appsFrontList[0].getExceptionsList().count + 1
        
        let heigthItem = appsFrontCollection.frame.height / CGFloat(totalException)
        let widthItem = appsFrontCollection.frame.width / CGFloat(appsFrontList.count)
        
        return CGSize(width: widthItem, height: heigthItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GSHeaderExceptionCollectionCell.identifier, for: indexPath) as! GSHeaderExceptionCollectionCell
            
            cell.lbName.text = appsFrontList[indexPath.row].name
            
            return cell
            
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GSExceptionStatusCollectionCell.identifier, for: indexPath) as! GSExceptionStatusCollectionCell
            
            let exceptionApp = appsFrontList[indexPath.row].exceptions[indexPath.section - 1]
            cell.updateStatus(to: exceptionApp.status)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if indexPath.section != 0{
            let exception = appsFrontList[indexPath.row].exceptions[indexPath.section - 1]
            print(exception)
        
            if exception.status != "OK"{
                delegate?.notifyStatusSelected()
            }
        }
    }
}
