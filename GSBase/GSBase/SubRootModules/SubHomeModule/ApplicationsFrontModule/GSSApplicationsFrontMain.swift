//
//  GSSApplicationsFrontMain.swift
//  GSBase
//
//  Created by Gustavo Tellez on 23/12/21.
//

import Foundation
import UIKit

open class GSSApplicationsFrontMain{
    
    public static func createModule() -> UIViewController{
        
        let viewController : GSSApplicationsFrontView? = GSSApplicationsFrontView()
        
        if let view = viewController{
            let interactor  =   GSSApplicationsFrontInteractor()
            let presenter   =   GSSApplicationsFrontPresenter()
            let router      =   GSSApplicationsFrontRouter()
            
            view.presenter = presenter
            
            presenter.view          =   view
            presenter.router        =   router
            presenter.interactor    =   interactor
            
            interactor.presenter = presenter
            
            return view
        }
        
        return UIViewController()
    }
}
