//
//  GSSApplicationsFrontContracts.swift
//  GSBase
//
//  Created by Gustavo Tellez on 23/12/21.
//

import Foundation
import SDKGSServicesManager

protocol GSSApplicationsFrontViewUIProtocol: AnyObject{
    func notifyStatusSelected()
}

protocol GSSApplicationsFrontViewProtocol: AnyObject{
    func showLoader()
    func dismissLoader()
    
    func notifyReloadAppsFrontList(list: [GSAppsFrontList])
    func notifyServiceError(msg: String, isEmptyData: Bool)
}

protocol GSSApplicationsFrontPresenterProtocol: AnyObject{
    func requestAppsFrontList(withLoader:Bool)
    func responseAppsFrontList(response: ListFrontStatusResponse?)
    
    func responseError(msg: String, isEmptyData: Bool)
}

protocol GSSApplicationsFrontInteractorProtocol: AnyObject{
    func fetchAppsFrontList()
}

protocol GSSApplicationsFrontRouterProtocol: AnyObject{
}
