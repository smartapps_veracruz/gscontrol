//
//  GSSApplicationsFrontView.swift
//  GSBase
//
//  Created by Gustavo Tellez on 23/12/21.
//

import UIKit
import SDKGSServicesManager
import SDKGSCommonUtils

class GSSApplicationsFrontView: UIViewController {
    
    private var ui: GSSApplicationsFrontViewUI?
    public var presenter: GSSApplicationsFrontPresenterProtocol?
    private var timer: Timer?
    override func loadView() {
        ui = GSSApplicationsFrontViewUI(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.requestAppsFrontList(withLoader: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(updateCountDown), userInfo: nil, repeats: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc private func updateCountDown(){
        presenter?.requestAppsFrontList(withLoader: false)
    }
    
}

extension GSSApplicationsFrontView: GSSApplicationsFrontViewUIProtocol{
    func notifyStatusSelected() {
        let view = GSActionMenuView()
        view.modalPresentationStyle = .overFullScreen
        present(view, animated: false, completion: nil)
    }
}

extension GSSApplicationsFrontView: GSSApplicationsFrontViewProtocol{
    
    func showLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController{
                GSLoader.show(parent: rootController.view)
            }
        }
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController {
                GSLoader.remove(parent: rootController.view)
            }
        }
    }
    
    func notifyReloadAppsFrontList(list: [GSAppsFrontList]) {
        ui?.appsFrontList = list
    }
    
    func notifyServiceError(msg: String, isEmptyData: Bool) {
        if isEmptyData{
            print(msg)
        }else{
            print(msg)
        }
    }
}
