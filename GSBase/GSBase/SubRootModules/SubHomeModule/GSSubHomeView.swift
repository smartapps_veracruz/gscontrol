//
//  GSSubHomeView.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

/// GSSubHome Module View
class GSSubHomeView: UIViewController {
    
    private var ui : GSSubHomeViewUI?
    var pagerController = GSPagerSubHome(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    private var timer: Timer?
    override func loadView() {
        // setting the custom view as the view controller's view
        ui = GSSubHomeViewUI(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let  button = ui?.optionMenu.first{
            ui?.onClickMenuOption(button)
        }
        self.timer = Timer.scheduledTimer(timeInterval: 7, target: self, selector: #selector(updateCountDown), userInfo: nil, repeats: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.timer?.invalidate()
        self.timer = nil
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc private func updateCountDown(){
        (ui?.navigationBottom as? GSGraphicsViewUI)?.presenter?.requestGraphicsList(withLoader: false)
    }
}


// MARK: - extending GSSubHomeView to implement the custom ui view delegate
extension GSSubHomeView: GSSubHomeViewUIDelegate {
    func notifyOptionBuilder(builder: [GSSubHomeViewType]) {
        pagerController.onBuilderOptions(options: builder)
    }
    
    func notifyOptionSelected(option: Int) {
        pagerController.onTabOption(option: option)
    }
    
    func notifyGetParentView() -> UIViewController {
        return self
    }
    
    func notifyPagerViewController() -> UIPageViewController {
        return pagerController
    }
    
    
}
