//
//  GSSubProcAndArqMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 21/12/21.
//

import UIKit

class GSSubProcAndArqMain: NSObject {
    
    static func createModule()->UIViewController{
        
        let viewController  :   GSSubProcAndArqView?   =  GSSubProcAndArqView()
        if let view = viewController {
            let presenter   =   GSSubProcAndArqPresenter()
            let router      =   GSSubProcAndArqRouter()
            let interactor  =   GSSubProcAndArqInteractor()
            
            view.presenter  =   presenter
            
            presenter._view          =   view
            presenter.interactor    =   interactor
            presenter.wireframe        =   router
            
            interactor._presenter    =   presenter
            return view
        }
        return UIViewController()
    }
}
