//
//  GSSubProcAndArqContracts.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

//MARK: View -
/*
 Should replace "class" with "BaseViewProtocol" if available;
 & that will allow the View to act as a UIViewController;
 & Implement common view functions.
 */
/// GSSubProcAndArq Module View Protocol
protocol GSSubProcAndArqViewProtocol: AnyObject {
    func showLoader()
    func dismissLoader()
    func notifyErrorMessage(message: String)
    func notifyReloadListSearch(list: SearchDocumentServiceProtocol.Response)
    func notifyDisplayDocumentDetail(list: DocumentDetailServiceProtocol.Response)
}

//MARK: Interactor -
/// GSSubProcAndArq Module Interactor Protocol
protocol GSSubProcAndArqInteractorProtocol:AnyObject {
    func feachDocument(content: String)
    func fetchDocumentDetail(id: String)
}

//MARK: Presenter -
/// GSSubProcAndArq Module Presenter Protocol
protocol GSSubProcAndArqPresenterProtocol:AnyObject {
    func requestDocumentDetail(id:String)
    func responseDocumentDetail(response: DocumentDetailServiceProtocol.Response)
    func requestSearchDocument(content: String)
    func responseSearchDocument(response: SearchDocumentServiceProtocol.Response)
    func responseError(msg: String, isEmptyData: Bool, type:GSSubProcAndArqServiceType)
}

//MARK: Router (aka: Wireframe) -
/// GSSubProcAndArq Module Router Protocol
protocol GSSubProcAndArqRouterProtocol:AnyObject {
    
}
