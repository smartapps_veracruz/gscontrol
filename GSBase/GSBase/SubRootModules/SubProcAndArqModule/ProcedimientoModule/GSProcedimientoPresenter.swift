//
//  GSProcedimientoPresenter.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

/// GSProcedimiento Module Presenter
class GSProcedimientoPresenter {
    
    weak var _view: GSProcedimientoViewProtocol?
    var interactor: GSProcedimientoInteractorProtocol?
    var wireframe: GSProcedimientoRouterProtocol?
    
}

// MARK: - extending GSProcedimientoPresenter to implement it's protocol
extension GSProcedimientoPresenter: GSProcedimientoPresenterProtocol {
    
    func requestListDocumentByCategory(id:String) {
        _view?.showLoader()
        interactor?.fetchListDocumentByCategory(id:id)
    }
    
    func responseListDocumentByCategory(response: ListDocumentsByCategoryServiceProtocol.Response) {
        _view?.dismissLoader()
        _view?.notifyReloadListDocumentByCategory(list: response)
    }
    
    func requestListCategory() {
        _view?.showLoader()
        interactor?.fetchListCategory()
    }
    
    func responseListCategory(response: ListCategoriesServiceProtocol.Response) {
        _view?.dismissLoader()
        _view?.notifyReloadListCategory(list: response)
    }
    
    func responseError(msg: String, isEmptyData: Bool, type: GSProcedimientoServiceType) {
        _view?.dismissLoader()
        if isEmptyData{
            switch type{
            case .category:
                _view?.notifyReloadListCategory(list: [])
                break
            case .document:
                _view?.notifyReloadListDocumentByCategory(list: [])
                break
            }
        }
    }
    
    
}
