//
//  GSProcedimientoContracts.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager
//MARK: View -
/*
 Should replace "class" with "BaseViewProtocol" if available;
 & that will allow the View to act as a UIViewController;
 & Implement common view functions.
 */
/// GSProcedimiento Module View Protocol
protocol GSProcedimientoViewProtocol: AnyObject {
    func showLoader()
    func dismissLoader()
    func notifyReloadListDocumentByCategory(list: ListDocumentsByCategoryServiceProtocol.Response)
    func notifyReloadListCategory(list: ListCategoriesServiceProtocol.Response)
    func notifyServiceError(msg: String)
}

//MARK: Interactor -
/// GSProcedimiento Module Interactor Protocol
protocol GSProcedimientoInteractorProtocol:AnyObject {
    func fetchListDocumentByCategory(id: String)
    func fetchListCategory()
}

//MARK: Presenter -
/// GSProcedimiento Module Presenter Protocol
protocol GSProcedimientoPresenterProtocol:AnyObject {
    func requestListDocumentByCategory(id:String)
    func responseListDocumentByCategory(response: ListDocumentsByCategoryServiceProtocol.Response)
    func requestListCategory()
    func responseListCategory(response: ListCategoriesServiceProtocol.Response)
    func responseError(msg: String, isEmptyData: Bool, type: GSProcedimientoServiceType)
}

//MARK: Router (aka: Wireframe) -
/// GSProcedimiento Module Router Protocol
protocol GSProcedimientoRouterProtocol:AnyObject {
   
}
