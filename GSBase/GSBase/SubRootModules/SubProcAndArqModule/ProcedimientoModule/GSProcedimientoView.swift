//
//  GSProcedimientoView.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager
import SDKGSCommonUtils

protocol GSProcedimientoViewDelegate: AnyObject{
    func notifyRequestProcedimientoDetail(withTitle: String, id: Int)
    func notifySearchText(_ text: String)
}
/// GSProcedimiento Module View
class GSProcedimientoView: UIViewController {
    private var ui : GSProcedimientoViewUI?
    var presenter: GSProcedimientoPresenterProtocol?
    weak var delegate: GSProcedimientoViewDelegate?
    override func loadView() {
       
        ui = GSProcedimientoViewUI(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.requestListCategory()
 
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - extending GSProcedimientoView to implement it's protocol
extension GSProcedimientoView: GSProcedimientoViewProtocol {
    
    func showLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController{
                GSLoader.show(parent: rootController.view)
            }
        }
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController {
                GSLoader.remove(parent: rootController.view)
            }
        }
    }
    
    func notifyReloadListDocumentByCategory(list: ListDocumentsByCategoryServiceProtocol.Response) {
        ui?.procedimientosDocumentByCategory = list
    }
    
    func notifyReloadListCategory(list: ListCategoriesServiceProtocol.Response) {
        ui?.procedimientosCategory = list
    }
    
    func notifyServiceError(msg: String) {
        ui?.procedimientosCategory = []
    }
    
    
}

// MARK: - extending GSProcedimientoView to implement the custom ui view delegate
extension GSProcedimientoView: GSProcedimientoViewUIDelegate {
    func notifyOpenDocumentDetail(withTitle: String, id: Int) {
        delegate?.notifyRequestProcedimientoDetail(withTitle: withTitle, id: id)
    }
    

    func notifyOpenProcedimiento(id: String) {
        presenter?.requestListDocumentByCategory(id: id)
    }
    
    func notifySearchText(_ text: String) {
        self.view.endEditing(true)
        delegate?.notifySearchText(text)
    }
}
