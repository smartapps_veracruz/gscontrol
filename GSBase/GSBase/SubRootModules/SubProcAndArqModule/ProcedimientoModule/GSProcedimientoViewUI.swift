//
//  GSProcedimientoViewUI.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager

// MARK: GSProcedimientoViewUI Delegate -
/// GSProcedimientoViewUI Delegate
protocol GSProcedimientoViewUIDelegate {
    func notifyOpenDocumentDetail(withTitle: String, id: Int)
    func notifyOpenProcedimiento(id: String)
    func notifySearchText(_ text: String)
}

class GSProcedimientoViewUI: UIView {
    
    private lazy var txtSearch: UITextField = {
        let txt = UITextField()
        txt.textColor = GSColorsManager.cellLabel
        txt.font = .Montserrat_Medium_16
        txt.textAlignment = .center
        txt.keyboardType = .asciiCapable
        txt.translatesAutoresizingMaskIntoConstraints = false
        return txt
    }()
    
    private lazy var btnSearch: UIButton = {
        let btn = UIButton()
        btn.setTitle("Buscar", for: .normal)
        btn.titleLabel?.font = .Montserrat_Semibold_16
        btn.setTitleColor(GSColorsManager.labelColor, for: .normal)
        btn.layer.borderColor = GSColorsManager.labelColor.cgColor
        btn.layer.borderWidth = 1.0
        btn.layer.cornerRadius = 6.0
        btn.addTarget(self, action: #selector(onClickSearchButton), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    var delegate: GSProcedimientoViewUIDelegate?
    
    lazy var bodyOptions: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = false
        collectionView.keyboardDismissMode = .onDrag
        collectionView.collectionViewLayout = UICollectionViewFlowLayout.init()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.contentInset = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
        collectionView.alwaysBounceVertical = true
        collectionView.register(GenericDocumentViewCell.self, forCellWithReuseIdentifier: "GenericDocumentViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    var procedimientoOpen: Int?
    var procedimientosCategory : ListCategoriesServiceProtocol.Response?{
        didSet{
            bodyOptions.reloadData()
        }
    }
    var procedimientosDocumentByCategory : ListDocumentsByCategoryServiceProtocol.Response?{
        didSet{
            bodyOptions.reloadData()
        }
    }
    
    private let sectionInsets = UIEdgeInsets(
      top: 0.0,
      left: 15.0,
      bottom: 0.0,
      right: 30.0)
    private let itemsPerRow: CGFloat = 2
    
    convenience init(delegate: GSProcedimientoViewUIDelegate) {
        self.init()
        
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        txtSearch.addBottomBorderWithColor(GSColorsManager.primaryColor, width: 1.0)
    }
    
    fileprivate func setupUIElements() {
        // arrange subviews
        addSubview(txtSearch)
        addSubview(btnSearch)
        addSubview(bodyOptions)
    }
    
    fileprivate func setupConstraints() {
        NSLayoutConstraint.activate([
            
            txtSearch.topAnchor.constraint(equalTo: topAnchor, constant: 10.0),
            txtSearch.widthAnchor.constraint(equalTo: bodyOptions.widthAnchor, multiplier: 0.5, constant: -194),
            txtSearch.heightAnchor.constraint(equalToConstant: 40.0),
            txtSearch.trailingAnchor.constraint(equalTo: btnSearch.leadingAnchor, constant: -20.0),
            
            btnSearch.bottomAnchor.constraint(equalTo: txtSearch.bottomAnchor),
            btnSearch.widthAnchor.constraint(equalToConstant: 100.0),
            btnSearch.heightAnchor.constraint(equalToConstant: 28.0),
            btnSearch.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -60.0),
            
            bodyOptions.topAnchor.constraint(equalTo: btnSearch.bottomAnchor, constant: 10),
            bodyOptions.leadingAnchor.constraint(equalTo: leadingAnchor),
            bodyOptions.trailingAnchor.constraint(equalTo: trailingAnchor),
            bodyOptions.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
        // add constraints to subviews
    }
    
    @objc func onClickSearchButton(){
        delegate?.notifySearchText(txtSearch.text ?? "")
    }
}


extension GSProcedimientoViewUI: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return procedimientosCategory?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GenericDocumentViewCell", for: indexPath) as? GenericDocumentViewCell{
            cell.titleCell.text = procedimientosCategory?[indexPath.row].description
            cell.delegate = self
            cell.indexPathSectionCustom = indexPath.row
            if indexPath.row == procedimientoOpen{
                cell.elementsToDisplay = procedimientosDocumentByCategory
                cell.documentMainBoxing.isHidden = false
                cell.buttonContinue.image = UIImage(named: "cell_button_down_ic", in: .local_gs_utils, compatibleWith: nil)
            }else{
                cell.elementsToDisplay = []
                cell.documentMainBoxing.isHidden = true
                cell.buttonContinue.image = UIImage(named: "cell_button_ic", in: .local_gs_utils, compatibleWith: nil)
            }
            return cell
        }
        return  UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if procedimientoOpen == indexPath.row{
            procedimientoOpen = nil
            bodyOptions.reloadData()
        }else{
            procedimientoOpen = indexPath.row
            delegate?.notifyOpenProcedimiento(id: procedimientosCategory?[indexPath.row].categoryId ?? "")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = 30 * (itemsPerRow + 1)
        let availableWidth = self.frame.width - paddingSpace - 60
        let widthPerItem = availableWidth / itemsPerRow
        if  procedimientoOpen == indexPath.row{
            return  CGSize(width: widthPerItem, height: 220)
        }else if indexPath.row % 2 == 1 && (procedimientoOpen == (indexPath.row - 1)){
            return  CGSize(width: widthPerItem, height: 220)
        }else if indexPath.row % 2 == 0 && (procedimientoOpen == (indexPath.row + 1)){
            return  CGSize(width: widthPerItem, height: 220)
        }else{
            return  CGSize(width: widthPerItem, height: 80)
        }
    }
    
    func collectionView( _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}

extension GSProcedimientoViewUI:GenericDocumentViewCellDelegate{
    func notifyDocumentSelected(id: Int, title: String) {
        delegate?.notifyOpenDocumentDetail(withTitle: title, id: id)
    }
}
