//
//  GSSearchProcessMain.swift
//  GSBase
//
//  Created by Gustavo Tellez on 31/12/21.
//

import Foundation
import UIKit
import SDKGSServicesManager

protocol GSSearchProcessMainDelegate:AnyObject{
    func notifyDocument(id: Int, withTitle: String)
}

class GSSearchProcessMain{
    
    public var ui: GSSearchProcessViewUI?
    weak var delegate: GSSearchProcessMainDelegate?
    public func showModule(onView parentView: UIView, withSearchText: String, withResults: SearchDocumentServiceProtocol.Response, delegate: GSSearchProcessMainDelegate){
        self.delegate = delegate
        ui = GSSearchProcessViewUI()
        ui?.processList = withResults
        ui?.lbTitle.text = """
                            RESULTADOS “\(withSearchText)”
                        """.uppercased()
        ui?.alpha = 0.0
        ui?.delegate = self
        ui?.frame = parentView.bounds
        parentView.addSubview(ui!)
        
        UIView.animate(withDuration: 0.5) {
            self.ui?.alpha = 1.0
        }
    }
    
    public func dismissModule(){
        
        UIView.animate(withDuration: 0.5) {
            self.ui?.alpha = 0.0
            
        } completion: { isFinished in
            
            if isFinished{
                self.ui?.removeFromSuperview()
            }
        }
    }
}

extension GSSearchProcessMain: GSSearchProcessViewUIProtocol{
    func notifyDocumentSelected(id: Int, withTitle: String) {
        delegate?.notifyDocument(id: id, withTitle: withTitle)
    }
    
    func notifyBackAction() {
        self.dismissModule()
    }
}
