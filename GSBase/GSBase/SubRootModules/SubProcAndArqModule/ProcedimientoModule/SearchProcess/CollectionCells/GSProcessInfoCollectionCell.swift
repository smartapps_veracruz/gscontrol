//
//  GSProcessInfoCollectionCell.swift
//  GSBase
//
//  Created by Gustavo Tellez on 31/12/21.
//

import UIKit
import SDKGSCommonUtils

class GSProcessInfoCollectionCell: UICollectionViewCell {
    
    public lazy var lbTitle: UILabel = {
        let lb = UILabel()
        lb.textAlignment = .left
        lb.numberOfLines = 1
        lb.font = UIFont.Montserrat_Semibold_14
        lb.textColor = GSColorsManager.contingencyCellLabelColor
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    public lazy var lbDescription: UILabel = {
        let lb = UILabel()
        lb.textAlignment = .left
        lb.numberOfLines = 0
        lb.font = UIFont.Montserrat_Regular_12
        lb.textColor = GSColorsManager.labelLightColor
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    public lazy var lbDate: UILabel = {
        let lb = UILabel()
        lb.textAlignment = .left
        lb.numberOfLines = 1
        lb.font = UIFont.Montserrat_Regular_10
        lb.textColor = GSColorsManager.labelLightColor
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    private lazy var stackView : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 0.0
        stack.distribution = .fillProportionally
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    public static let identifier  = "GSProcessInfoCollectionCell"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildUIElements()
        buildConstraints()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildUIElements(){
        
        stackView.addArrangedSubview(lbTitle)
        stackView.addArrangedSubview(lbDescription)
        stackView.addArrangedSubview(lbDate)
        
        self.contentView.addSubview(stackView)
    }
    
    private func buildConstraints(){
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20.0),
            stackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            
            lbTitle.heightAnchor.constraint(equalToConstant: 30),
            lbDate.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
}
