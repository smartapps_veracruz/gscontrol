//
//  GSSearchProcessViewUI.swift
//  GSBase
//
//  Created by Gustavo Tellez on 31/12/21.
//

import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager

protocol GSSearchProcessViewUIProtocol: AnyObject{
    func notifyBackAction()
    func notifyDocumentSelected(id: Int, withTitle: String)
}

class GSSearchProcessViewUI: UIView {
    
    private lazy var imgBackground: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "background_body_ic", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    private lazy var line: UIImageView = {
       let image = UIImageView()
        image.image = UIImage(named: "verticalLine", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    private lazy var btnBack: UIButton = {
        let btn = UIButton()
        btn.addTarget(self, action: #selector(onClickBackButton), for: .touchUpInside)
        btn.setImage(UIImage(named: "icon_leftArrowButton", in: Bundle.local_gs_utils, compatibleWith: nil), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    lazy var lbTitle: UILabel = {
        let lb = UILabel()
        lb.font = UIFont.Montserrat_Semibold_14
        lb.textAlignment = .left
        lb.numberOfLines = 1
        lb.minimumScaleFactor = 0.6
        lb.adjustsFontSizeToFitWidth = true
        lb.textColor = GSColorsManager.contingencyCellLabelColor
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    private lazy var processCollection: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collection.backgroundColor = .clear
        collection.isPagingEnabled = false
        collection.delegate = self
        collection.dataSource = self
        collection.collectionViewLayout = UICollectionViewFlowLayout.init()
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.register(GSProcessInfoCollectionCell.self, forCellWithReuseIdentifier: GSProcessInfoCollectionCell.identifier)
       
        return collection
    }()
    
    public var processList: SearchDocumentServiceProtocol.Response?{
        didSet{
            processCollection.reloadData()
        }
    }
    
    public var delegate: GSSearchProcessViewUIProtocol?
    
    public init(){
        super.init(frame: CGRect.zero)
        self.backgroundColor = GSColorsManager.noneColor
        buildUIElements()
        buildConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func buildUIElements() {

        self.addSubview(imgBackground)
        self.addSubview(line)
        self.addSubview(btnBack)
        self.addSubview(lbTitle)
        self.addSubview(processCollection)
    }
    
    func buildConstraints() {
        NSLayoutConstraint.activate([
            imgBackground.topAnchor.constraint(equalTo: self.topAnchor),
            imgBackground.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            imgBackground.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            imgBackground.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            line.topAnchor.constraint(equalTo: self.topAnchor, constant: 90.0),
            line.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10.0),
            line.bottomAnchor.constraint(equalTo: bottomAnchor),
            line.widthAnchor.constraint(equalToConstant: 1),
            
            btnBack.topAnchor.constraint(equalTo: self.topAnchor, constant: 40.0),
            btnBack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 80.0),
            btnBack.heightAnchor.constraint(equalToConstant: 40.0),
            btnBack.widthAnchor.constraint(equalToConstant: 40.0),
            
            lbTitle.topAnchor.constraint(equalTo: self.topAnchor, constant: 40.0),
            lbTitle.leadingAnchor.constraint(equalTo: btnBack.trailingAnchor),
            lbTitle.heightAnchor.constraint(equalToConstant: 40.0),
            lbTitle.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -50.0),
            
            processCollection.topAnchor.constraint(equalTo: btnBack.bottomAnchor, constant: 15.0),
            processCollection.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 80.0),
            processCollection.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -50.0),
            processCollection.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    @objc func onClickBackButton(){
        delegate?.notifyBackAction()
    }
}

extension GSSearchProcessViewUI: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return processList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let widthItem = processCollection.frame.width
        let heightItem : CGFloat = 100.0
        
        return  CGSize(width: widthItem,  height: heightItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GSProcessInfoCollectionCell.identifier, for: indexPath) as! GSProcessInfoCollectionCell
        
        cell.lbTitle.text = processList?[indexPath.row].documentName
        cell.lbDescription.text = processList?[indexPath.row].summary
       // cell.lbDate.text = processList[indexPath.row].lastChange
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.notifyDocumentSelected(id: processList?[indexPath.row].documentId ?? 0, withTitle: processList?[indexPath.row].documentName ?? "")
    }
}
