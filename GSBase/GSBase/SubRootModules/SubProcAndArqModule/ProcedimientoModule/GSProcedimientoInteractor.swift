//
//  GSProcedimientoInteractor.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

/// GSProcedimiento Module Interactor
enum GSProcedimientoServiceType: Int{
    case category
    case document
}
class GSProcedimientoInteractor{
    weak var _presenter: GSProcedimientoPresenterProtocol?
    private let facade = NOCServicesFacade.sharedNocTeca
}

extension GSProcedimientoInteractor: GSProcedimientoInteractorProtocol {
    
    func fetchListDocumentByCategory(id:String) {
        let request = ListDocumentsByCategoryRequest(categoryId:id)
        self.facade.fetchListDocumentByCategory(request: request) { response, _ in
            DispatchQueue.main.async {
                self._presenter?.responseListDocumentByCategory(response: response)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: emptyMessage ?? "", isEmptyData: true, type: .document)
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: errorMessage ?? "", isEmptyData: false, type: .document)
            }
        }
    }
    
    func fetchListCategory() {
        let request = BaseRequestEmpty()
        self.facade.fetchListCategory(request: request) { response, _ in
            DispatchQueue.main.async {
                self._presenter?.responseListCategory(response: response)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: emptyMessage ?? "", isEmptyData: true, type: .category)
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: errorMessage ?? "", isEmptyData: false, type: .category)
            }
        }
    }
    

}
