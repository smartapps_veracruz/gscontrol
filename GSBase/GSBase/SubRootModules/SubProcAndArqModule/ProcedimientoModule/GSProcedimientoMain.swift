//
//  GSProcedimientoMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 22/12/21.
//

import UIKit

class GSProcedimientoMain: NSObject {
    
    static func createModule(delegate: GSProcedimientoViewDelegate?)->UIViewController{
        
        let viewController  :   GSProcedimientoView?   =  GSProcedimientoView()
        
        if let view = viewController, let dlg = delegate{
            
            let presenter   =   GSProcedimientoPresenter()
            let router      =   GSProcedimientoRouter()
            let interactor  =  GSProcedimientoInteractor()
            
            view.presenter  =   presenter
            view.delegate   =   dlg
            
            presenter._view         =   view
            presenter.interactor    =   interactor
            presenter.wireframe     =   router
            
            interactor._presenter   =   presenter
            
            return view
        }
        return UIViewController()
    }
}
