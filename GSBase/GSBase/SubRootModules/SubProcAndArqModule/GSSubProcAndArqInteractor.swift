//
//  GSSubProcAndArqInteractor.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager
/// GSSubProcAndArq Module Interactor
enum GSSubProcAndArqServiceType: Int{
    case search
    case documentDetail
}

class GSSubProcAndArqInteractor{
    weak var _presenter: GSSubProcAndArqPresenterProtocol?
    private let facade = NOCServicesFacade.sharedNocTeca
}
extension GSSubProcAndArqInteractor: GSSubProcAndArqInteractorProtocol {
    
    func fetchDocumentDetail(id: String) {
        let request = DocumentDetailRequest(documentId: id)
        self.facade.fetchDocumentDetail(request: request) { response, _ in
            DispatchQueue.main.async {
                self._presenter?.responseDocumentDetail(response: response)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: emptyMessage ?? "", isEmptyData: true, type: .documentDetail)
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: errorMessage ?? "", isEmptyData: false, type: .documentDetail)
            }
        }
    }
    
    func feachDocument(content: String) {
        let request = SearchDocumentRequest(content: content)
        self.facade.searchDocument(request: request) { response, _ in
            DispatchQueue.main.async {
                self._presenter?.responseSearchDocument(response: response)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: emptyMessage ?? "", isEmptyData: true, type: .search)
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: errorMessage ?? "", isEmptyData: false, type: .search)
            }
        }
    }
}
