//
//  GSArquitecturaViewUI.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

// MARK: GSArquitecturaViewUI Delegate -
/// GSArquitecturaViewUI Delegate
protocol GSArquitecturaViewUIDelegate {
    // Send Events to Module View, that will send events to the Presenter; which will send events to the Receiver e.g. Protocol OR Component.
    func notifyOnArquitecturaSelected(withTitle: String, id: Int)
}

class GSArquitecturaViewUI: UIView {
    
    var delegate: GSArquitecturaViewUIDelegate?
    lazy var bodyOptions: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = false
        collectionView.collectionViewLayout = UICollectionViewFlowLayout.init()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.contentInset = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
        collectionView.alwaysBounceVertical = true
        collectionView.register(GenericDocumentViewCell.self, forCellWithReuseIdentifier: "GenericDocumentViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    private let sectionInsets = UIEdgeInsets(
      top: 0.0,
      left: 15.0,
      bottom: 0.0,
      right: 30.0)
    private let itemsPerRow: CGFloat = 2
    var arquitecturas : ListArquitecturesServiceProtocol.Response?{
        didSet{
            bodyOptions.reloadData()
        }
    }
    
    convenience init(delegate: GSArquitecturaViewUIDelegate) {
        self.init()
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        
    }
    
    fileprivate func setupUIElements() {
        // arrange subviews
        addSubview(bodyOptions)
    }
    
    fileprivate func setupConstraints() {
        NSLayoutConstraint.activate([
            bodyOptions.topAnchor.constraint(equalTo: topAnchor),
            bodyOptions.leadingAnchor.constraint(equalTo: leadingAnchor),
            bodyOptions.trailingAnchor.constraint(equalTo: trailingAnchor),
            bodyOptions.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
        // add constraints to subviews
    }
}


extension GSArquitecturaViewUI: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arquitecturas?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GenericDocumentViewCell", for: indexPath) as? GenericDocumentViewCell{
            cell.titleCell.text = arquitecturas?[indexPath.row].name
            return cell
        }
        return  UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.notifyOnArquitecturaSelected(withTitle: arquitecturas?[indexPath.row].name ?? "", id: arquitecturas?[indexPath.row].architectureId ?? 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = 30 * (itemsPerRow + 1)
        let availableWidth = self.frame.width - paddingSpace - 60
        let widthPerItem = availableWidth / itemsPerRow
        return  CGSize(width: widthPerItem,  height: 80)
    }
    
    func collectionView( _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}
