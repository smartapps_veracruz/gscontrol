//
//  GSArquitecturaPresenter.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

/// GSArquitectura Module Presenter
class GSArquitecturaPresenter {
    
    weak var _view: GSArquitecturaViewProtocol?
    var interactor: GSArquitecturaInteractorProtocol?
    var wireframe: GSArquitecturaRouterProtocol?
    
}

// MARK: - extending GSArquitecturaPresenter to implement it's protocol
extension GSArquitecturaPresenter: GSArquitecturaPresenterProtocol {
    func requestArquitectureDetail(id: String) {
        _view?.showLoader()
        interactor?.fetchArquitectureDetail(id: id)
    }
    
    func responseArquitectureDetail(response: ArquitectureDetailServiceProtocol.Response) {
        _view?.dismissLoader()
        _view?.notifyDisplayArquitectureDetail(list: response)
    }
    
    func requestListArquitecture() {
        _view?.showLoader()
        interactor?.fetchListArquitecture()
    }
    
    func responseListArquitecture(response: ListArquitecturesServiceProtocol.Response) {
        _view?.dismissLoader()
        _view?.notifyReloadListArquitecture(list: response)
    }
    
    func responseError(msg: String, isEmptyData: Bool, type: GSArquitecturaServiceType) {
        _view?.dismissLoader()
        if isEmptyData{
            switch type{
            case .arquitecture:
                _view?.notifyReloadListArquitecture(list: [])
                break
            case .arquitectureDetail:
                
                break
            }
        }
    }
    
    
}
