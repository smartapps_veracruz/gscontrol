//
//  GSArquitecturaContracts.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

//MARK: View -
/*
 Should replace "class" with "BaseViewProtocol" if available;
 & that will allow the View to act as a UIViewController;
 & Implement common view functions.
 */
/// GSArquitectura Module View Protocol
protocol GSArquitecturaViewProtocol: AnyObject {
    func showLoader()
    func dismissLoader()
    func notifyDisplayArquitectureDetail(list: ArquitectureDetailServiceProtocol.Response)
    func notifyReloadListArquitecture(list: ListArquitecturesServiceProtocol.Response)
    func notifyServiceError(msg: String)
}

//MARK: Interactor -
/// GSArquitectura Module Interactor Protocol
protocol GSArquitecturaInteractorProtocol:AnyObject {
    func fetchArquitectureDetail(id: String)
    func fetchListArquitecture()
}

//MARK: Presenter -
/// GSArquitectura Module Presenter Protocol
protocol GSArquitecturaPresenterProtocol:AnyObject {
    func requestArquitectureDetail(id:String)
    func responseArquitectureDetail(response: ArquitectureDetailServiceProtocol.Response)
    func requestListArquitecture()
    func responseListArquitecture(response: ListArquitecturesServiceProtocol.Response)
    func responseError(msg: String, isEmptyData: Bool, type: GSArquitecturaServiceType)
}

//MARK: Router (aka: Wireframe) -
/// GSArquitectura Module Router Protocol
protocol GSArquitecturaRouterProtocol:AnyObject {
   
}
