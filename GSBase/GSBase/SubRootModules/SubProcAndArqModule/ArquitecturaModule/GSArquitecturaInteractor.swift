//
//  GSArquitecturaInteractor.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

/// GSArquitectura Module Interactor
enum GSArquitecturaServiceType: Int{
    case arquitecture
    case arquitectureDetail
}
class GSArquitecturaInteractor{
    weak var _presenter: GSArquitecturaPresenterProtocol?
    private let facade = NOCServicesFacade.sharedNocTeca
}
extension GSArquitecturaInteractor: GSArquitecturaInteractorProtocol {
    func fetchArquitectureDetail(id: String) {
        let request = ArquitectureDetailRequest(architectureId: id)
        self.facade.fetchArquitectureDetail(request: request) { response, _ in
            DispatchQueue.main.async {
                self._presenter?.responseArquitectureDetail(response: response)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: emptyMessage ?? "", isEmptyData: true, type: .arquitecture)
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: errorMessage ?? "", isEmptyData: false, type: .arquitecture)
            }
        }
    }
    
    func fetchListArquitecture() {
        let request = BaseRequestEmpty()
        self.facade.fetchListArquitectures(request: request) { response, _ in
            DispatchQueue.main.async {
                self._presenter?.responseListArquitecture(response: response)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: emptyMessage ?? "", isEmptyData: true, type: .arquitectureDetail)
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self._presenter?.responseError(msg: errorMessage ?? "", isEmptyData: false, type: .arquitectureDetail)
            }
        }
    }
    

}
