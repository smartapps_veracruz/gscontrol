//
//  GSArquitecturaView.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 22/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager
import SDKGSCommonUtils

/// GSArquitectura Module View
class GSArquitecturaView: UIViewController {
    
    private var ui : GSArquitecturaViewUI?
    var presenter: GSArquitecturaPresenterProtocol?
    private var tempTitleToOpen = ""
    override func loadView() {
       
        ui = GSArquitecturaViewUI(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.requestListArquitecture()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - extending GSArquitecturaView to implement it's protocol
extension GSArquitecturaView: GSArquitecturaViewProtocol {
    func showLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController{
                GSLoader.show(parent: rootController.view)
            }
        }
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController {
                GSLoader.remove(parent: rootController.view)
            }
        }
    }
  
    func notifyDisplayArquitectureDetail(list: ArquitectureDetailServiceProtocol.Response) {
        _ = GSDocumentDetailViewUI(parent: self, title: tempTitleToOpen, withStringPage: list.first?.image ?? "", fromWhere: .Arquitectura)
        tempTitleToOpen = ""
    }
    
    func notifyReloadListArquitecture(list: ListArquitecturesServiceProtocol.Response) {
        ui?.arquitecturas = list
    }
    
    func notifyServiceError(msg: String) {
        ui?.arquitecturas = []
    }
}

// MARK: - extending GSArquitecturaView to implement the custom ui view delegate
extension GSArquitecturaView: GSArquitecturaViewUIDelegate {
    func notifyOnArquitecturaSelected(withTitle: String, id: Int) {
        presenter?.requestArquitectureDetail(id: "\(id)")
        tempTitleToOpen = withTitle
    }
}
