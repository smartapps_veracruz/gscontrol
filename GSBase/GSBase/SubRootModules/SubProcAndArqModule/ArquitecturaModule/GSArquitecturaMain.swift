//
//  GSArquitecturaMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 22/12/21.
//

import UIKit

class GSArquitecturaMain: NSObject {
    
    static func createModule()->UIViewController{
        
        let viewController  :   GSArquitecturaView?   =  GSArquitecturaView()
        if let view = viewController {
            let presenter   =   GSArquitecturaPresenter()
            let router      =   GSArquitecturaRouter()
            let interactor  =  GSArquitecturaInteractor()
            
            view.presenter  =   presenter
            
            presenter._view          =   view
            presenter.interactor    =   interactor
            presenter.wireframe        =   router
            
            interactor._presenter    =   presenter
            return view
        }
        return UIViewController()
    }
}
