//
//  GSDocumentDetailViewUI.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 05/01/22.
//  Copyright © 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import WebKit
import SDKGSCommonUtils
import PDFKit

enum GSDocumentDetailViewUIType: Int{
    case Arquitectura
    case Procedimiento
}

class GSDocumentDetailViewUI: UIView {
    
    private var parentView: UIViewController?
    private var topAnchorCustom : NSLayoutConstraint?
    private var leadingAnchorCustom : NSLayoutConstraint?
    private var trailingAnchorCustom : NSLayoutConstraint?
    private var bottomAnchorCustom : NSLayoutConstraint?
    private var heightAnchorCustom : NSLayoutConstraint?
    private var centerxAnchorCustom : NSLayoutConstraint?
    
    lazy var titleMessage: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    private lazy var line : UIImageView = {
       let image = UIImageView()
        image.image = UIImage(named: "verticalLine", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    lazy var curseWebView: WKWebView = {
        let web = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
        web.translatesAutoresizingMaskIntoConstraints = false
        web.allowsBackForwardNavigationGestures = false
        return web
    }()
    lazy var navigationView: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = UIImage(named: "icon_leftArrowButton", in: .local_gs_utils, compatibleWith: nil)
        let btn = UIButton()
        btn.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(img)
        view.addSubview(btn)
        view.addSubview(titleMessage)
        NSLayoutConstraint.activate([
            btn.heightAnchor.constraint(equalToConstant: 50.0),
            btn.widthAnchor.constraint(equalToConstant: 50.0),
            btn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            btn.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            img.heightAnchor.constraint(equalToConstant: 40.0),
            img.widthAnchor.constraint(equalToConstant: 40.0),
            img.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            img.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            titleMessage.leadingAnchor.constraint(equalTo: btn.trailingAnchor, constant: 20),
            titleMessage.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -60),
            titleMessage.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        return view
    }()
  
    convenience init(parent: UIViewController, title: String, withStringPage: String, fromWhere: GSDocumentDetailViewUIType) {
        self.init(frame: UIScreen.main.bounds)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = GSColorsManager.noneColor
        self.titleMessage.text = title
        if fromWhere == .Arquitectura{
            titleMessage.font = .Montserrat_Semibold_16
            titleMessage.textColor = GSColorsManager.labelStrongColor
            titleMessage.textAlignment = .left
        }else{
            titleMessage.font = .Montserrat_Bold_18
            titleMessage.textColor = GSColorsManager.contingencyCellLabelColor
            titleMessage.textAlignment = .center
        }
        
        self.parentView = parent
        self.alpha = 0
        if fromWhere == .Arquitectura{
            let solution = withStringPage.components(separatedBy: "base64,")
            if let imageData = Data(base64Encoded: solution.last ?? "", options: Data.Base64DecodingOptions.ignoreUnknownCharacters), let imageToDisplay = UIImage(data: imageData), let pdfPage = PDFPage(image: imageToDisplay){
                    let pdfDocument = PDFDocument()
                    pdfDocument.insert(pdfPage, at: 0)
                    if let data = pdfDocument.dataRepresentation() {
                        curseWebView.load(data, mimeType: "application/pdf", characterEncodingName: "utf-8", baseURL: URL(fileURLWithPath: ""))
                }
            }
            
        }else if fromWhere == .Procedimiento{
            self.curseWebView.loadHTMLString(withStringPage, baseURL: nil)
        }
        self.setupUIElements()
        self.setupConstraints()
        UIView.animate(withDuration: 0.1) {
            self.alpha = 1
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        
    }
    
    fileprivate func setupUIElements() {
        // arrange subviews
        self.parentView?.view.addSubview(self)
        addSubview(line)
        addSubview(navigationView)
        addSubview(curseWebView)
    }
    
    fileprivate func setupConstraints() {
        // add constraints to subviews
        guard let parentView = parentView else{
            return
        }
        topAnchorCustom = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0)
        trailingAnchorCustom = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0)
        leadingAnchorCustom = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0)
        bottomAnchorCustom = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0)
        topAnchorCustom?.isActive = true
        trailingAnchorCustom?.isActive = true
        leadingAnchorCustom?.isActive = true
        bottomAnchorCustom?.isActive = true
        
        NSLayoutConstraint.activate([
            navigationView.topAnchor.constraint(equalTo: topAnchor),
            navigationView.leadingAnchor.constraint(equalTo: leadingAnchor),
            navigationView.trailingAnchor.constraint(equalTo: trailingAnchor),
            navigationView.heightAnchor.constraint(equalToConstant: 80),
            
            line.topAnchor.constraint(equalTo: navigationView.bottomAnchor),
            line.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10.0),
            line.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.6),
            line.widthAnchor.constraint(equalToConstant: 1),
            
            curseWebView.topAnchor.constraint(equalTo: navigationView.bottomAnchor),
            curseWebView.leadingAnchor.constraint(equalTo: line.trailingAnchor, constant: 20),
            curseWebView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            curseWebView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20)
        ])
    }
    
    
    @objc private func backAction(_ sender: UIButton){
        UIView.animate(withDuration: 0.1) {
            self.alpha = 0
        } completion: { completed in
            self.topAnchorCustom?.isActive = false
            self.trailingAnchorCustom?.isActive = false
            self.leadingAnchorCustom?.isActive = false
            self.bottomAnchorCustom?.isActive = false
            self.centerxAnchorCustom?.isActive = false
            self.removeFromSuperview()
        }
    }
}
