//
//  DocumentViewCell.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 11/01/22.
//

import UIKit
import SDKGSCommonUtils

protocol DocumentViewCellDelegate: AnyObject{
    func notifyIdTap(id:Int)
}

class DocumentViewCell: UICollectionViewCell {
    weak var delegate: DocumentViewCellDelegate?
    private lazy var buttonSelected: UIButton = {
        let button  = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(self.onDocumentSelected(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var box: UIView = {
        let box = UIView(frame: .zero)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.backgroundColor = .clear
        box.layer.cornerRadius = 10
        box.layer.borderWidth = 1
        box.layer.borderColor = GSColorsManager.primaryColor.withAlphaComponent(0.6).cgColor
        return box
    }()
    
    private lazy var imageSelected: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "pdf_ic", in: .local_gs_utils, compatibleWith: nil)
        return image
    }()
    
    lazy var titleMessage: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Regular_12
        label.textColor = GSColorsManager.labelExtraLightColor.withAlphaComponent(0.6)
        label.numberOfLines = 5
        label.textAlignment = .center
        return label
    }()
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUIElements()
        setupConstraints()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupUIElements() {
        addSubview(box)
        box.addSubview(titleMessage)
        box.addSubview(imageSelected)
        box.addSubview(buttonSelected)
    }
    
    fileprivate func setupConstraints() {
        NSLayoutConstraint.activate([
            box.topAnchor.constraint(equalTo: topAnchor),
            box.leadingAnchor.constraint(equalTo: leadingAnchor),
            box.trailingAnchor.constraint(equalTo: trailingAnchor),
            box.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            imageSelected.topAnchor.constraint(equalTo: box.topAnchor, constant: 15),
            imageSelected.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -15),
            imageSelected.heightAnchor.constraint(equalToConstant: 15),
            imageSelected.widthAnchor.constraint(equalToConstant: 15),
            titleMessage.centerYAnchor.constraint(equalTo: box.centerYAnchor),
            titleMessage.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 30),
            titleMessage.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -30),
            
            buttonSelected.topAnchor.constraint(equalTo: box.topAnchor),
            buttonSelected.leadingAnchor.constraint(equalTo: box.leadingAnchor),
            buttonSelected.trailingAnchor.constraint(equalTo: box.trailingAnchor),
            buttonSelected.bottomAnchor.constraint(equalTo: box.bottomAnchor),
        ])
    }
    
    @objc func onDocumentSelected(_ sender: UIButton){
        delegate?.notifyIdTap(id: tag)
    }
}
