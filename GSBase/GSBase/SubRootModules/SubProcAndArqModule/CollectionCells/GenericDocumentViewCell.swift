//
//  GenericDocumentViewCell.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 22/12/21.
//

import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager

protocol GenericDocumentViewCellDelegate{
    func notifyDocumentSelected(id:Int, title: String)
}

class GenericDocumentViewCell: UICollectionViewCell {
    var delegate : GenericDocumentViewCellDelegate?
    private let sectionInsets = UIEdgeInsets(
      top: 0.0,
      left: 10.0,
      bottom: 10.0,
      right: 10.0)
    private let itemsPerRow: CGFloat = 2
    private lazy var containerView: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.cell
        return view
    }()
    
    lazy var documentMainBoxing: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = false
        collectionView.keyboardDismissMode = .onDrag
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.alwaysBounceVertical = false
        collectionView.register(DocumentViewCell.self, forCellWithReuseIdentifier: "DocumentViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()

    lazy var titleCell: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Medium_16
        label.textColor = GSColorsManager.cellLabel 
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    lazy var buttonContinue: UIImageView = {
        let button = UIImageView(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.image = UIImage(named: "cell_button_ic", in: .local_gs_utils, compatibleWith: nil)
        return button
    }()
    var indexPathSectionCustom = 0
    var elementsToDisplay : ListDocumentsByCategoryServiceProtocol.Response?{
        didSet{
            documentMainBoxing.reloadData()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUIElements()
        setupConstraints()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupUIElements() {
        addSubview(containerView)
        addSubview(documentMainBoxing)
        containerView.addSubview(titleCell)
        containerView.addSubview(buttonContinue)
    }
    
    fileprivate func setupConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerView.heightAnchor.constraint(equalToConstant: 60),
            
            documentMainBoxing.topAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 10),
            documentMainBoxing.leadingAnchor.constraint(equalTo: leadingAnchor),
            documentMainBoxing.trailingAnchor.constraint(equalTo: trailingAnchor),
            documentMainBoxing.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            
            titleCell.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            titleCell.leadingAnchor.constraint(equalTo: containerView.leadingAnchor ,constant: 40),
            titleCell.trailingAnchor.constraint(equalTo: buttonContinue.leadingAnchor, constant: -5),
            
            buttonContinue.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            buttonContinue.heightAnchor.constraint(equalToConstant: 35),
            buttonContinue.widthAnchor.constraint(equalToConstant: 35),
            buttonContinue.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20)
        ])
    }
}

extension GenericDocumentViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return elementsToDisplay?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentViewCell", for: indexPath) as? DocumentViewCell{
            cell.tag = indexPath.row
            cell.delegate = self
            cell.titleMessage.text = elementsToDisplay?[indexPath.row].documentName
            return cell
        }
        return  UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = 20 * (itemsPerRow + 1)
        let availableWidth = self.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return  CGSize(width: widthPerItem, height: 130)
    }
    
    func collectionView( _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}

extension GenericDocumentViewCell: DocumentViewCellDelegate{
    func notifyIdTap(id: Int) {
        delegate?.notifyDocumentSelected(id: elementsToDisplay?[id].documentId ?? 0, title: elementsToDisplay?[id].documentName ?? "")
    }
}
