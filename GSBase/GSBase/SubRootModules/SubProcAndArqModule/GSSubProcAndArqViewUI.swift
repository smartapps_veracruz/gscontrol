//
//  GSSubProcAndArqViewUI.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSCommonUtils

// MARK: GSSubProcAndArqViewUI Delegate -
/// GSSubProcAndArqViewUI Delegate
protocol GSSubProcAndArqViewUIDelegate {
    func notifyGetParentView()->UIViewController
    func notifyPagerViewController()->UIPageViewController
    func notifyOptionSelected(option: Int)
    func notifyOptionBuilder(builder: [GSSubHomeViewType])
}

class GSSubProcAndArqViewUI: UIView {
    
    var delegate: GSSubProcAndArqViewUIDelegate?
    var optionMenu = [UIButton]()
    var centerXAnchorSlide: NSLayoutConstraint?
    var leadingAnchotSlide : NSLayoutConstraint?
    
    private lazy var line : UIImageView = {
       let image = UIImageView()
        image.image = UIImage(named: "verticalLine", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = GSColorsManager.labelLightColor.withAlphaComponent(0.25)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var slideView: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.primaryColor
        NSLayoutConstraint.activate([
            view.heightAnchor.constraint(equalToConstant: 5)
        ])
        return view
    }()
    
    lazy var slideButtonBox: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 20
        return stackView
    }()
    
    lazy var bodyContainer: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        if let parentView = delegate?.notifyGetParentView(), let pager = delegate?.notifyPagerViewController(){
            parentView.addChild(pager)
            pager.view.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(pager.view)
            NSLayoutConstraint.activate([
                pager.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                pager.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                pager.view.topAnchor.constraint(equalTo: view.topAnchor),
                pager.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
            pager.didMove(toParent: parentView)
        }
        return view
    }()
    convenience init(delegate: GSSubProcAndArqViewUIDelegate) {
        self.init()
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
        setOptionsMenu(options: [.Procedimientos,.Arquitecturas])
        self.delegate?.notifyOptionBuilder(builder: [.Procedimientos,.Arquitecturas])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
    }
    
    fileprivate func setupUIElements() {
        // arrange subviews
        addSubview(line)
        addSubview(slideButtonBox)
        addSubview(separatorView)
        addSubview(slideView)
        addSubview(bodyContainer)
    }
    
    fileprivate func setupConstraints() {
        // add constraints to subviews
        NSLayoutConstraint.activate([
            slideButtonBox.topAnchor.constraint(equalTo: topAnchor),
            slideButtonBox.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            slideButtonBox.heightAnchor.constraint(equalToConstant: 30),
            
            slideView.topAnchor.constraint(equalTo: slideButtonBox.bottomAnchor),
            
            separatorView.topAnchor.constraint(equalTo: slideButtonBox.bottomAnchor, constant: 2.5),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12.0),
            separatorView.heightAnchor.constraint(equalToConstant: 0.5),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            line.topAnchor.constraint(equalTo: slideButtonBox.bottomAnchor, constant: 50.0),
            line.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10.0),
            line.bottomAnchor.constraint(equalTo: bottomAnchor),
            line.widthAnchor.constraint(equalToConstant: 1),
            
            bodyContainer.topAnchor.constraint(equalTo: slideButtonBox.bottomAnchor, constant: 10),
            bodyContainer.leadingAnchor.constraint(equalTo: leadingAnchor),
            bodyContainer.trailingAnchor.constraint(equalTo: trailingAnchor),
            bodyContainer.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    private func setOptionsMenu(options: [GSSubHomeViewType]){
        for (ind, option) in options.enumerated() {
            let button = UIButton(frame: .zero)
            button.tag = option.rawValue
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setTitle(option.onButtonName(), for: .normal)
            if button.tag == GSSubHomeViewType.Procedimientos.rawValue{
                button.setTitleColor(GSColorsManager.labelColor, for: .normal)
            }else{
                button.setTitleColor(GSColorsManager.labelColor.withAlphaComponent(0.53), for: .normal)
            }
            button.titleLabel?.font = .Montserrat_Semibold_16
            button.addTarget(self, action: #selector(self.onClickMenuOption(_:)), for: .touchUpInside)
            let onVertical = UIView(frame: .zero)
            onVertical.translatesAutoresizingMaskIntoConstraints = false
            onVertical.backgroundColor = GSColorsManager.primaryColor
            NSLayoutConstraint.activate([
                onVertical.widthAnchor.constraint(equalToConstant: 1),
                onVertical.heightAnchor.constraint(equalToConstant: 20)
            ])
            optionMenu.append(button)
            slideButtonBox.addArrangedSubview(button)
            slideButtonBox.addArrangedSubview(onVertical)
        }
        if let sizeButton = GSSubHomeViewType(rawValue: optionMenu[0].tag)?.onButtonName().widthOfString(usingFont: .Montserrat_Semibold_16.withSize(18)), let buttonSelected = optionMenu.first {
            leadingAnchotSlide = NSLayoutConstraint(item: slideView, attribute: .centerX, relatedBy: .equal, toItem: buttonSelected, attribute: .centerX, multiplier: 1, constant: 0)
            leadingAnchotSlide?.isActive = true
            centerXAnchorSlide = NSLayoutConstraint(item: slideView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: sizeButton)
            centerXAnchorSlide?.isActive = true
        }
    }
    
    private func setSlideViewPosition(option: GSSubHomeViewType){
        let buttonFilter = optionMenu.filter { button in
            return button.tag == option.rawValue
        }
        if let buttonSelected = buttonFilter.first {
          let sizeButton = option.onButtonName().widthOfString(usingFont: .Montserrat_Semibold_16.withSize(18))
            leadingAnchotSlide?.isActive = false
            centerXAnchorSlide?.isActive = false
            
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut, .curveEaseOut], animations: { [self] in
                leadingAnchotSlide = NSLayoutConstraint(item: slideView, attribute: .centerX, relatedBy: .equal, toItem: buttonSelected, attribute: .centerX, multiplier: 1, constant: 0)
                leadingAnchotSlide?.isActive = true
                centerXAnchorSlide = NSLayoutConstraint(item: slideView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: sizeButton)
                centerXAnchorSlide?.isActive = true
                layoutIfNeeded()
            }, completion: nil)
           
        }
    }
    
    @objc private func onClickMenuOption(_ sender: UIButton){
        for button in optionMenu{
            if button.tag == sender.tag{
                button.setTitleColor(GSColorsManager.labelColor, for: .normal)
            }else{
                button.setTitleColor(GSColorsManager.labelColor.withAlphaComponent(0.53), for: .normal)
            }
        }
        if let option = GSSubHomeViewType(rawValue: sender.tag){
            setSlideViewPosition(option: option)
        }
        for (ind , buttonFilter) in optionMenu.enumerated(){
            if buttonFilter.tag == sender.tag{
                delegate?.notifyOptionSelected(option: ind)
            }
        }
    }
}
