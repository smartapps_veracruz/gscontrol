//
//  GSSubProcAndArqView.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager
import SDKGSCommonUtils

/// GSSubProcAndArq Module View
class GSSubProcAndArqView: UIViewController {
    private var temporalSearch = ""
    private var tempTitleToOpen = ""
    private var ui : GSSubProcAndArqViewUI?
    var presenter: GSSubProcAndArqPresenterProtocol?
    var pagerController = GSPagerSubHome(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    override func loadView() {
        // setting the custom view as the view controller's view
        ui = GSSubProcAndArqViewUI(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - extending GSSubProcAndArqView to implement it's protocol
extension GSSubProcAndArqView: GSSubProcAndArqViewProtocol {
    func notifyErrorMessage(message: String) {
        let alert = UIAlertController(title: "NOC IT", message: message, preferredStyle: .alert)
        let aceptAlert = UIAlertAction(title: "Aceptar", style: .default, handler: nil)

        alert.addAction(aceptAlert)
        present(alert, animated: true, completion: nil)
    }
    
    func notifyDisplayDocumentDetail(list: [DocumentDetailResponse]) {
        _ = GSDocumentDetailViewUI(parent: self, title: tempTitleToOpen, withStringPage: list.first?.documentContent ?? "" ,fromWhere: .Procedimiento)
        tempTitleToOpen = ""
    }
    
    func showLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController{
                GSLoader.show(parent: rootController.view)
            }
        }
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            if let rootController = UIApplication.shared.windows.first!.rootViewController {
                GSLoader.remove(parent: rootController.view)
            }
        }
    }
    
    func notifyReloadListSearch(list: [SearchDocumentResponse]) {
        let view = GSSearchProcessMain()
        view.showModule(onView: self.view, withSearchText: temporalSearch, withResults: list, delegate: self)
        temporalSearch = ""
    }
}

// MARK: - extending GSSubProcAndArqView to implement the custom ui view delegate
extension GSSubProcAndArqView: GSSubProcAndArqViewUIDelegate {
    func notifyGetParentView() -> UIViewController {
        return self
    }
    
    func notifyPagerViewController() -> UIPageViewController {
        return pagerController
    }
    
    func notifyOptionSelected(option: Int) {
        pagerController.onTabOption(option: option)
    }
    
    func notifyOptionBuilder(builder: [GSSubHomeViewType]) {
        pagerController.onBuilderOptions(options: builder, delegate: self)
    }
    
    
}

extension GSSubProcAndArqView: GSProcedimientoViewDelegate{
    func notifyRequestProcedimientoDetail(withTitle: String, id: Int) {
        tempTitleToOpen = withTitle
        presenter?.requestDocumentDetail(id: "\(id)")
    }
    
    
    func notifySearchText(_ text: String) {
        temporalSearch = text
        presenter?.requestSearchDocument(content: text)
    }
}

extension GSSubProcAndArqView:GSSearchProcessMainDelegate{
    func notifyDocument(id: Int, withTitle: String) {
        tempTitleToOpen = withTitle
        presenter?.requestDocumentDetail(id: "\(id)")
    }
}
