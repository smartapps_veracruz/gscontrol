//
//  GSSubProcAndArqPresenter.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSServicesManager

/// GSSubProcAndArq Module Presenter
class GSSubProcAndArqPresenter {
    
    weak var _view: GSSubProcAndArqViewProtocol?
    var interactor: GSSubProcAndArqInteractorProtocol?
    var wireframe: GSSubProcAndArqRouterProtocol?
}

// MARK: - extending GSSubProcAndArqPresenter to implement it's protocol
extension GSSubProcAndArqPresenter: GSSubProcAndArqPresenterProtocol {
    
    func requestDocumentDetail(id: String) {
        _view?.showLoader()
        interactor?.fetchDocumentDetail(id: id)
    }
    
    func responseDocumentDetail(response: DocumentDetailServiceProtocol.Response) {
        _view?.dismissLoader()
        _view?.notifyDisplayDocumentDetail(list: response)
    }
    
    
    func requestSearchDocument(content: String) {
        _view?.showLoader()
        interactor?.feachDocument(content: content)
    }
    
    func responseSearchDocument(response: SearchDocumentServiceProtocol.Response) {
        _view?.dismissLoader()
        _view?.notifyReloadListSearch(list: response)
    }
    
    func responseError(msg: String, isEmptyData: Bool, type: GSSubProcAndArqServiceType) {
        _view?.dismissLoader()
        if isEmptyData{
            switch type {
            case .search:
                _view?.notifyReloadListSearch(list: [])
                break
            case .documentDetail:
                _view?.notifyDisplayDocumentDetail(list: [])
                break
            }
        }else{
            _view?.notifyErrorMessage(message: "Sin resultados")
        }
    }
    
    
}
