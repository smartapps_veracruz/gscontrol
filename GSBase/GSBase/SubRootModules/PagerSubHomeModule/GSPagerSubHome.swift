//
//  GSPagerSubHome.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 21/12/21.
//

import UIKit
enum GSSubHomeViewType: Int{
    case AplicativosFront
    case Middleware
    case Back
    case Procedimientos
    case Arquitecturas
    
    func onButtonName()->String{
        switch self{
        case .AplicativosFront:
            return "Aplicativos / Front"
        case .Middleware:
            return "Middleware - F5"
        case .Back:
            return "Back"
        case .Procedimientos:
            return "Procedimientos"
        case .Arquitecturas:
            return "Arquitecturas"
        }
    }
}
class GSPagerSubHome: UIPageViewController {

    var navController: UINavigationController?
    var pagerControllers: [UIViewController] = []
    private var activeTag: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    public func onTabOption(option:Int){
        self.setViewControllers([pagerControllers[option]], direction: activeTag > option ? .reverse : .forward, animated: true, completion: nil)
        activeTag = option
    }
    
    public func onBuilderOptions(options: [GSSubHomeViewType],
                                 delegate: GSProcedimientoViewDelegate? = nil){
        for controller in options{
            switch controller {
            case .AplicativosFront:
                let view = GSSApplicationsFrontMain.createModule()
                pagerControllers.append(view)
            case .Middleware:
                let view = GSMiddlewareMain.createModule()
                pagerControllers.append(view)
            case .Back:
                let view = GSBackMain.createModule()
                view.view.backgroundColor = .clear
                pagerControllers.append(view)
            case .Procedimientos:
                let view = GSProcedimientoMain.createModule(delegate: delegate)
                pagerControllers.append(view)
            case .Arquitecturas:
                let view = GSArquitecturaMain.createModule()
                pagerControllers.append(view)
            }
        }
        if options.contains(GSSubHomeViewType.Middleware){
            self.setViewControllers([pagerControllers[1]], direction: .forward, animated: false, completion: nil)
        }else{
            self.setViewControllers([pagerControllers[0]], direction: .forward, animated: false, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
