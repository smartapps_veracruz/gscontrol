//
//  GSOperationMenuProtocol.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 22/12/21.
//

import Foundation
import UIKit

protocol GSOperationMenuViewProtocol: AnyObject {
    func goToHome()
}

//MARK: Interactor -
protocol GSOperationMenuInteractorProtocol: AnyObject {
    // Fetch Object from Data Layer
}

//MARK: Presenter -
protocol GSOperationMenuPresenterProtocol: AnyObject {
    func goToHome()
}

//MARK: Router (aka: Wireframe) -
protocol GSOperationMenuRouterProtocol:AnyObject {
    func goToHome()
    
}

protocol GSOperationMenuDelegateProtocol:AnyObject {
    func goToHome()
}
