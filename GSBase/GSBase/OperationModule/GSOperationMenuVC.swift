//
//  GSOperationMenuVC.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 22/12/21.
//


import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager

class GSOperationMenuVC: UIViewController {
    private var ui : GSOperationMenuContainer?
    var presenter: GSOperationMenuPresenterProtocol?
    
    
    override func loadView() {
        ui = GSOperationMenuContainer(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension GSOperationMenuVC: GSOperationMenuViewProtocol {
    
    func goToHome() {
        presenter?.goToHome()
    }
    
    
}

extension GSOperationMenuVC: GSOperationMenuDelegateProtocol {
    
}


