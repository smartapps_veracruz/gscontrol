//
//  GSOperationMenuMain.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 22/12/21.
//

import UIKit

class GSOperationMenuMain: NSObject {

    static func createModule()->UIViewController{
        
        let viewController  :   GSOperationMenuVC?   =  GSOperationMenuVC()
        if let view = viewController {
            let presenter   =   GSOperationMenuPresenter()
            let router      =   GSOperationMenuRouter()
            let interactor  =   GSOperationMenuInteractor()
            
            view.presenter  =   presenter
            
            presenter.view          =   view
            presenter.interactor    =   interactor
            presenter.router        =   router
            
            interactor.presenter    =   presenter
            return view
        }
        return UIViewController()
    }
}
