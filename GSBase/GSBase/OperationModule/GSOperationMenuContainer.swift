//
//  GSOperationMenuContainer.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 22/12/21.
//

import Foundation
import UIKit

class GSOperationMenuContainer : UIView {
    
    var delegate: GSOperationMenuDelegateProtocol?
    lazy var topBackground: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "background_top_ic", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var backgroundImage : UIImageView = {
       let image = UIImageView()
        image.image = UIImage(named: "first_background", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var iconCentral : UIImageView = {
       let image = UIImageView()
        image.image = UIImage(named: "logo ti", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    lazy var firstLine : OperatioMenuFirstLine = {
        let view = OperatioMenuFirstLine()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var secondLine : OperationMenuSecondLine = {
        let view = OperationMenuSecondLine()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    
    convenience init(delegate: GSOperationMenuDelegateProtocol) {
        self.init()
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
    
        
    }
    
    @objc func handleTap(_ sender: Any) {
        delegate?.goToHome()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func setupUIElements() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.addSubview(backgroundImage)
        addSubview(topBackground)
        self.addGestureRecognizer(tap)
        
        [iconCentral,firstLine,secondLine].forEach { (component) in
            backgroundImage.addSubview(component)
        }

    }
    
    fileprivate func setupConstraints() {
        
        NSLayoutConstraint.activate([
            topBackground.topAnchor.constraint(equalTo: topAnchor),
            topBackground.leadingAnchor.constraint(equalTo: leadingAnchor),
            topBackground.trailingAnchor.constraint(equalTo: trailingAnchor),
            topBackground.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.2, constant: 0),
            
            backgroundImage.topAnchor.constraint(equalTo: self.topAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            iconCentral.topAnchor.constraint(equalTo: self.topAnchor, constant: 100),
            iconCentral.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            iconCentral.leadingAnchor.constraint(equalTo: leadingAnchor),
            iconCentral.trailingAnchor.constraint(equalTo: trailingAnchor),
            iconCentral.heightAnchor.constraint(equalToConstant: 120),
            
            firstLine.topAnchor.constraint(equalTo: iconCentral.bottomAnchor,constant: 100),
            firstLine.centerXAnchor.constraint(equalTo: self.centerXAnchor),

            secondLine.topAnchor.constraint(equalTo: firstLine.bottomAnchor,constant: 70),
            secondLine.centerXAnchor.constraint(equalTo: self.centerXAnchor)
            
            
        ])
        
    }
    
}
