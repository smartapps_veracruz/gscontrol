//
//  GSOperationMenuPresenter.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 22/12/21.
//

import Foundation

class GSOperationMenuPresenter {

    
    weak var view: GSOperationMenuViewProtocol?
    var interactor: GSOperationMenuInteractorProtocol?
    var router: GSOperationMenuRouterProtocol?
    
}

extension GSOperationMenuPresenter: GSOperationMenuPresenterProtocol {
    func goToHome() {
        router?.goToHome()
        
    }
    
    
}
