//
//  OperationMenuSecondLine.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 23/12/21.
//

import Foundation
import UIKit

class OperationMenuSecondLine : UIView {
    
    lazy var stackView : UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = 57
        return stackView
    }()
    
    lazy var firstCard : OperationCardView = {
        let view = OperationCardView(text: "Portales", nameIcon: "totalplay_icon_single")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var secondCard : OperationCardView = {
        let view = OperationCardView(text: "CRM", nameIcon: "totalplay_icon_single")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var thirdCard : OperationCardView = {
        let view = OperationCardView(text: "Qlik Sense", nameIcon: "qlink_icon")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var fourthCard : OperationCardView = {
        let view = OperationCardView(text: "Salesforce", nameIcon: "salesForce_icon")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    


    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        setupUIElements()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func setupUIElements() {
        
        [firstCard,secondCard,thirdCard,fourthCard].forEach { (component) in
            self.addSubview(component)
        }

    }
    
    fileprivate func setupConstraints() {
        
        NSLayoutConstraint.activate([
            
            firstCard.topAnchor.constraint(equalTo: self.topAnchor),
            firstCard.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            firstCard.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            secondCard.topAnchor.constraint(equalTo: self.topAnchor),
            secondCard.leadingAnchor.constraint(equalTo: firstCard.trailingAnchor,constant: 57),
            secondCard.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            thirdCard.topAnchor.constraint(equalTo: self.topAnchor),
            thirdCard.leadingAnchor.constraint(equalTo: secondCard.trailingAnchor,constant: 57),
            thirdCard.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            fourthCard.topAnchor.constraint(equalTo: self.topAnchor),
            fourthCard.leadingAnchor.constraint(equalTo: thirdCard.trailingAnchor,constant: 57),
            fourthCard.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            fourthCard.bottomAnchor.constraint(equalTo: self.bottomAnchor),

            
        
        ])
        
    }
    
}
