//
//  OpetionCardView.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 23/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

class OperationCardView : UIView {
    

    lazy var circle : UIImageView = {
       let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var circleBackGround : UIImageView = {
       let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "Elipse azul", in: .local_gs_utils, compatibleWith: nil)
        return image
    }()
    
    lazy var rectangle : UIImageView = {
       let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var icon : UIImageView = {
       let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var label : UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.font = .Montserrat_Regular_14
        label.textColor = GSColorsManager.labelLightColor
        label.textAlignment = .center
        return label
    }()
    
    
    init(text : String, nameIcon : String, colorCircle : String = "blue_circle" , colorRectangle : String = "rectangle_blue"){
        super.init(frame: CGRect.zero)
        self.backgroundColor = UIColor.clear
    
        circle.image = UIImage(named: colorCircle, in: .local_gs_utils, compatibleWith: nil)
        rectangle.image = UIImage(named: colorRectangle, in: .local_gs_utils, compatibleWith: nil)
        icon.image = UIImage(named: nameIcon, in: .local_gs_utils, compatibleWith: nil)
        label.text = text
        
        setupUIElements()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func setupUIElements() {
        
        [rectangle,circleBackGround,circle,icon,label].forEach { (component) in
            self.addSubview(component)
        }

    }
    
    fileprivate func setupConstraints() {
        
        NSLayoutConstraint.activate([
            
            rectangle.topAnchor.constraint(equalTo: self.topAnchor),
            rectangle.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            rectangle.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            rectangle.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            circleBackGround.centerXAnchor.constraint(equalTo: rectangle.centerXAnchor),
            circleBackGround.topAnchor.constraint(equalTo: rectangle.topAnchor,constant: -40),
            
            circle.centerXAnchor.constraint(equalTo: circleBackGround.centerXAnchor),
            circle.centerYAnchor.constraint(equalTo: circleBackGround.centerYAnchor),

            icon.centerXAnchor.constraint(equalTo: circle.centerXAnchor),
            icon.centerYAnchor.constraint(equalTo: circle.centerYAnchor),

            //label.centerXAnchor.constraint(equalTo: rectangle.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: rectangle.centerYAnchor),
            label.leadingAnchor.constraint(equalTo: rectangle.leadingAnchor,constant: 20),
            label.trailingAnchor.constraint(equalTo: rectangle.trailingAnchor,constant: -20)
        
        ])
        
    }
    
}

