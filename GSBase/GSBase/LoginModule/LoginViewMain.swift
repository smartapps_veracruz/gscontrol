//
//  LoginViewMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 02/05/22.
//

import UIKit

class LoginViewMain: NSObject {
    
    static func createModule()->UIViewController{
        
        let viewController  :   LoginViewController?   =  LoginViewController()
        if let view = viewController {
            return view
        }
        return UIViewController()
    }
}
