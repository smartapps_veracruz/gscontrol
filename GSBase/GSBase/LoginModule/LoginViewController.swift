//
//  LoginViewController.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 02/05/22.
//

import UIKit
import Firebase
import SDKGSCommonUtils
import FirebaseFirestore

struct LoginResponse: Codable{
    let credenciales: String?
    let usuario: String?
    let informacion: Informacion?
    
    struct Informacion: Codable{
        let idKaralundi: String?
        let nombre: String?
        let apellido: String?
        let foto: String?
    }
}

class LoginViewController: UIViewController {

    lazy var background: UIImageView = {
        let image = UIImageView(image: UIImage(named: "background_login_ic", in: .local_gs_utils, compatibleWith: nil))
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var scroll: UIScrollView = {
        let scroll = UIScrollView(frame: .zero)
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    lazy var viewContainer: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "logo ti", in: .local_gs_utils, compatibleWith: nil)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    lazy var username: UITextField = {
        let textfield = UITextField(frame: .zero)
        textfield.tag = 100
        textfield.delegate = self
        textfield.layer.cornerRadius = 20
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.backgroundColor = .black.withAlphaComponent(0.5)
        textfield.textAlignment = .center
        textfield.keyboardType = .asciiCapableNumberPad
        textfield.attributedPlaceholder = "Usuario".decorative(color: GSColorsManager.labelLightColor, font: .systemFont(ofSize: 14))
        textfield.tintColor = GSColorsManager.labelLightColor
        textfield.textColor = GSColorsManager.labelLightColor
        return textfield
    }()
    lazy var credential: UITextField = {
        let textfield = UITextField(frame: .zero)
        textfield.tag = 101
        textfield.delegate = self
        textfield.layer.cornerRadius = 20
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.backgroundColor = .black.withAlphaComponent(0.5)
        textfield.textAlignment = .center
        textfield.attributedPlaceholder  = "Contraseña".decorative(color: GSColorsManager.labelLightColor, font: .systemFont(ofSize: 14))
        textfield.tintColor = GSColorsManager.labelLightColor
        textfield.textColor = GSColorsManager.labelLightColor
        return textfield
    }()
    
    lazy var buttonContinue: UIButton = {
        let btn = UIButton()
        btn.setTitle("INGRESAR", for: .normal)
        btn.titleLabel?.font = .Montserrat_Regular_16
        btn.setTitleColor(GSColorsManager.cellLabel, for: .normal)
        btn.layer.borderColor = GSColorsManager.labelLightColor.cgColor
        btn.layer.borderWidth = 0.5
        btn.layer.cornerRadius = 20
        btn.addTarget(self, action: #selector(onLoginAction(_:)), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = GSColorsManager.noneColor
        view.addSubview(background)
        view.addSubview(scroll)
        scroll.addSubview(viewContainer)
        viewContainer.addSubview(logoImageView)
        viewContainer.addSubview(username)
        viewContainer.addSubview(credential)
        viewContainer.addSubview(buttonContinue)
        NSLayoutConstraint.activate([
            background.topAnchor.constraint(equalTo: view.topAnchor),
            background.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            background.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            background.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            scroll.topAnchor.constraint(equalTo: view.topAnchor),
            scroll.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scroll.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scroll.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scroll.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            logoImageView.topAnchor.constraint(equalTo: scroll.topAnchor, constant: 50),
            logoImageView.leadingAnchor.constraint(equalTo: scroll.leadingAnchor, constant: 30),
            logoImageView.trailingAnchor.constraint(equalTo: scroll.trailingAnchor, constant: -30),
            logoImageView.heightAnchor.constraint(equalToConstant: 130),
            logoImageView.centerXAnchor.constraint(equalTo: scroll.centerXAnchor),
            
            viewContainer.topAnchor.constraint(equalTo: scroll.topAnchor),
            viewContainer.centerXAnchor.constraint(equalTo: scroll.centerXAnchor),
            viewContainer.leadingAnchor.constraint(equalTo: scroll.leadingAnchor),
            viewContainer.trailingAnchor.constraint(equalTo: scroll.trailingAnchor),
            viewContainer.bottomAnchor.constraint(equalTo: scroll.bottomAnchor),
            viewContainer.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height),
            
            username.centerXAnchor.constraint(equalTo: viewContainer.centerXAnchor),
            username.centerYAnchor.constraint(equalTo: viewContainer.centerYAnchor),
            username.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.3),
            username.heightAnchor.constraint(equalToConstant: 40),
            
            credential.centerXAnchor.constraint(equalTo: viewContainer.centerXAnchor),
            credential.topAnchor.constraint(equalTo: username.bottomAnchor, constant: 30),
            credential.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.3),
            credential.heightAnchor.constraint(equalToConstant: 40),
            
            buttonContinue.bottomAnchor.constraint(equalTo: viewContainer.bottomAnchor, constant: -30),
            buttonContinue.centerXAnchor.constraint(equalTo: viewContainer.centerXAnchor),
            buttonContinue.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width * 0.3),
            buttonContinue.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func onLoginAction(_ sender: UIButton){
        loginServices()
    }
    
    private func loginServices(){
        let db = Firestore.firestore()
        let auth =  db.collection("auth")
        let query = auth
            .whereField("credenciales", isEqualTo: credential.text ?? "")
            .whereField("usuario", isEqualTo: username.text ?? "")
        query.getDocuments { (querySnapshot, err) in
            if let _ = err {
                self.onErrorMessageLogin()
            } else {
                if querySnapshot?.documents.count != 0{
                   
                    if let data = querySnapshot?.documents.first?.data(){
                        let response = GSServicesManager.shareManager.decodeDictionary(JSONObject: data, entity: LoginResponse.self)
                        GSFacetecAuthViewStatic.manager.latestExternalDatabaseRefID = response.informacion?.idKaralundi ?? ""
                        GSFacetecAuthViewStatic.manager.fotoUsuario = response.informacion?.foto ?? ""
                        GSFacetecAuthViewStatic.manager.nombreCompleto = "\(response.informacion?.nombre ?? "") \(response.informacion?.apellido ?? "")"
                    }
                    let module = GSOperationMenuMain.createModule()
                    self.navigationController?.pushViewController(module, animated: false)
                }else{
                    self.onErrorMessageLogin()
                }
               
            }
        }
    }
    
    private func onErrorMessageLogin(){
        let alert = UIAlertController(title: "NOC IT", message: "Usuario ó contraseña es incorrecta.", preferredStyle: .alert)
        let aceptAlert = UIAlertAction(title: "Aceptar", style: .default, handler: nil)

        alert.addAction(aceptAlert)
        self.present(alert, animated: true, completion: nil)
    }
}

extension LoginViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag{
        case 100:
            credential.becomeFirstResponder()
            break
        case 101:
            loginServices()
            break
        default:
            break
        }
        return false
    }
}
