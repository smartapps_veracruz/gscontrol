//
//  GSSubHome.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 21/12/21.
//

import UIKit
import SDKGSCommonUtils


enum GSHomeViewUIType: Int{
    case Home
    case ProcAndArq
    case Profile
    
    func onButtonName()->String{
        switch self{
        case .Home:
            return "Home"
        case .ProcAndArq:
            return "Procedimientos / Arquitecturas"
        case .Profile:
            return "Perfil"
        }
    }
}

class GSPagerHome: UIPageViewController {
    
    var navController: UINavigationController?
    var pagerControllers: [UIViewController] = []
    
    private var activeTag: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    public func onTabOption(option:Int){
        if option != 10{
            self.setViewControllers([pagerControllers[option]], direction: activeTag > option ? .reverse : .forward, animated: true, completion: nil)
            activeTag = option
        }
    }
    
    public func onBuilderOptions(options: [GSHomeViewUIType]){
        for controller in options{
            switch controller {
            case .Home:
                let view = GSSubHomeMain.createModule()
                pagerControllers.append(view)
            case .ProcAndArq:
                let view = GSSubProcAndArqMain.createModule()
                pagerControllers.append(view)
            case .Profile: break
                
            }
        }
        self.setViewControllers([pagerControllers[0]], direction: .forward, animated: false, completion: nil)
    }
}
