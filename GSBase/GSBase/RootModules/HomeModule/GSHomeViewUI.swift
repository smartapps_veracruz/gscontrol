//
//  GSHomeViewUI.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSCommonUtils
import Firebase
import FirebaseStorage

// MARK: GSHomeViewUI Delegate -
/// GSHomeViewUI Delegate
protocol GSHomeViewUIDelegate {
    func notifyGetParentView()->UIViewController
    func notifyPagerViewController()->UIPageViewController
    func notifyOptionSelected(option: Int)
    func notifyOptionBuilder(builder: [GSHomeViewUIType])
    // Send Events to Module View, that will send events to the Presenter; which will send events to the Receiver e.g. Protocol OR Component.
}

class GSHomeViewUI: UIView {
    
    var delegate: GSHomeViewUIDelegate?
    private var optionsType : [GSHomeViewUIType] = [.Home, .ProcAndArq]
    private var optionMenu = [UIButton]()
    
    lazy var profileView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var imgProfile: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFill
        imgView.layer.cornerRadius = 27.0
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.clipsToBounds = true
        return imgView
    }()
    
    lazy var lbProfileName: UILabel = {
        let lb = UILabel()
        lb.text = GSFacetecAuthViewStatic.manager.nombreCompleto
        lb.textColor = GSColorsManager.labelColor
        lb.font = .Montserrat_Semibold_16
        lb.numberOfLines = 2
        lb.minimumScaleFactor = 0.7
        lb.adjustsFontSizeToFitWidth = true
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    lazy var navigationTop: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.noneColor
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "background_top_ic", in: .local_gs_utils, compatibleWith: nil)
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 5
        
        let homeButton = UIButton(frame: .zero)
        homeButton.translatesAutoresizingMaskIntoConstraints = false
        homeButton.setImage(UIImage(named: "home_button_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
        homeButton.tag = GSHomeViewUIType.Home.rawValue
        homeButton.addTarget(self, action: #selector(self.onClickMenuOption(_:)), for: .touchUpInside)
        NSLayoutConstraint.activate([
            homeButton.heightAnchor.constraint(equalToConstant: 50),
            homeButton.widthAnchor.constraint(equalToConstant: 50),
        ])
        optionMenu.append(homeButton)
        
        let courseButton = UIButton(frame: .zero)
        courseButton.translatesAutoresizingMaskIntoConstraints = false
        courseButton.setImage(UIImage(named: "course_button_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
        courseButton.tag = 100
        courseButton.setTitleColor( UIColor(hexString: "3E3E3E"), for: .normal)
        courseButton.setTitle("Cursos", for: .normal)
        courseButton.titleLabel?.font = .Montserrat_Regular_14
        courseButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        courseButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        courseButton.contentHorizontalAlignment = .leading
        NSLayoutConstraint.activate([
            courseButton.heightAnchor.constraint(equalToConstant: 50),
            courseButton.widthAnchor.constraint(equalToConstant: "Cursos".widthOfString(usingFont: .Montserrat_Regular_14) + 70)
        ])
        optionMenu.append(courseButton)
        
        let procAndArqButton = UIButton(frame: .zero)
        procAndArqButton.translatesAutoresizingMaskIntoConstraints = false
        procAndArqButton.setImage(UIImage(named: "proc_arq_button_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
        procAndArqButton.setTitleColor(GSColorsManager.labelColor, for: .normal)
        procAndArqButton.setTitle("Procedimientos / Arquitecturas", for: .normal)
        procAndArqButton.titleLabel?.font = .Montserrat_Regular_14
        procAndArqButton.tag = GSHomeViewUIType.ProcAndArq.rawValue
        procAndArqButton.addTarget(self, action: #selector(self.onClickMenuOption(_:)), for: .touchUpInside)
        procAndArqButton.contentHorizontalAlignment = .leading
        procAndArqButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        procAndArqButton.titleEdgeInsets = UIEdgeInsets(top: 0, left:  10, bottom: 0, right: 10)
        NSLayoutConstraint.activate([
            procAndArqButton.heightAnchor.constraint(equalToConstant: 50),
            procAndArqButton.widthAnchor.constraint(equalToConstant: "Procedimientos / Arquitecturas".widthOfString(usingFont: .Montserrat_Regular_14) + 70)
        ])
        optionMenu.append(procAndArqButton)
        
        let messageButton = UIButton(frame: .zero)
        messageButton.translatesAutoresizingMaskIntoConstraints = false
        messageButton.setImage(UIImage(named: "message_button_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
        messageButton.setTitleColor(UIColor(hexString: "3E3E3E"), for: .normal)
        messageButton.setTitle("Comunicados", for: .normal)
        messageButton.titleLabel?.font = .Montserrat_Regular_14
        messageButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        messageButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        messageButton.tag = 100
        messageButton.contentHorizontalAlignment = .leading
        NSLayoutConstraint.activate([
            messageButton.heightAnchor.constraint(equalToConstant: 50),
            messageButton.widthAnchor.constraint(equalToConstant: "Comunicados".widthOfString(usingFont: .Montserrat_Regular_14) + 70)
        ])
        optionMenu.append(messageButton)
        
        let profileButton = UIButton(frame: .zero)
        profileButton.tag = GSHomeViewUIType.Profile.rawValue
        profileButton.translatesAutoresizingMaskIntoConstraints = false
        let lpgr = UITapGestureRecognizer(target: self, action: #selector(handleLongPress))
        profileButton.addGestureRecognizer(lpgr)
        
        optionMenu.append(profileButton)
        
        profileView.addSubview(imgProfile)
        profileView.addSubview(lbProfileName)
        profileView.addSubview(profileButton)
        
        NSLayoutConstraint.activate([
            profileView.heightAnchor.constraint(equalToConstant: 100.0),
            
            lbProfileName.trailingAnchor.constraint(equalTo: profileView.trailingAnchor),
            lbProfileName.widthAnchor.constraint(equalToConstant: 160.0),
            lbProfileName.centerYAnchor.constraint(equalTo: profileView.centerYAnchor),
            
            imgProfile.heightAnchor.constraint(equalToConstant: 54.0),
            imgProfile.widthAnchor.constraint(equalToConstant: 54.0),
            imgProfile.centerYAnchor.constraint(equalTo: profileView.centerYAnchor),
            imgProfile.trailingAnchor.constraint(equalTo: lbProfileName.leadingAnchor, constant: -14.0),
            
            profileButton.topAnchor.constraint(equalTo: profileView.topAnchor),
            profileButton.leadingAnchor.constraint(equalTo: profileView.leadingAnchor),
            profileButton.trailingAnchor.constraint(equalTo: profileView.trailingAnchor),
            profileButton.bottomAnchor.constraint(equalTo: profileView.bottomAnchor),
        ])
        
        let onHomeVertical = UIView(frame: .zero)
        onHomeVertical.translatesAutoresizingMaskIntoConstraints = false
        onHomeVertical.backgroundColor = GSColorsManager.primaryColor.withAlphaComponent(0.6)
        NSLayoutConstraint.activate([
            onHomeVertical.widthAnchor.constraint(equalToConstant: 0.5),
            onHomeVertical.heightAnchor.constraint(equalToConstant: 30)
        ])
        let onProcAndArqVertical = UIView(frame: .zero)
        onProcAndArqVertical.translatesAutoresizingMaskIntoConstraints = false
        onProcAndArqVertical.backgroundColor = GSColorsManager.primaryColor.withAlphaComponent(0.6)
        NSLayoutConstraint.activate([
            onProcAndArqVertical.widthAnchor.constraint(equalToConstant: 0.5),
            onProcAndArqVertical.heightAnchor.constraint(equalToConstant: 30)
        ])
        
        stackView.addArrangedSubview(homeButton)
        stackView.addArrangedSubview(courseButton)
        stackView.addArrangedSubview(onHomeVertical)
        stackView.addArrangedSubview(procAndArqButton)
        stackView.addArrangedSubview(onProcAndArqVertical)
        stackView.addArrangedSubview(messageButton)
        stackView.addArrangedSubview(profileView)
        
        view.addSubview(image)
        view.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            image.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: UIApplication.getStatusBarHeight()),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        return view
    }()
    
    lazy var bodyContainer: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.noneColor
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "background_body_ic", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(image)
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor),
            image.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            image.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        if let parentView = delegate?.notifyGetParentView(), let pager = delegate?.notifyPagerViewController(){
            parentView.addChild(pager)
            pager.view.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(pager.view)
            NSLayoutConstraint.activate([
                pager.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                pager.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                pager.view.topAnchor.constraint(equalTo: view.topAnchor),
                pager.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
            pager.didMove(toParent: parentView)
        }
        return view
    }()
    
    convenience init(delegate: GSHomeViewUIDelegate) {
        self.init()
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
        self.delegate?.notifyOptionBuilder(builder: optionsType)
        let storage = Storage.storage()
        storage.reference(forURL:  GSFacetecAuthViewStatic.manager.fotoUsuario).getData(maxSize: 2 * 1024 * 1024) { data, error in
            if let error = error {
               // Uh-oh, an error occurred!
             } else {
                 DispatchQueue.main.async {
                     self.imgProfile.image = UIImage(data: data!)!
                 }
             }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
    }
    
    fileprivate func setupUIElements() {
        // arrange subviews
        addSubview(navigationTop)
        addSubview(bodyContainer)
    }
    
    fileprivate func setupConstraints() {
        // add constraints to subviews
        NSLayoutConstraint.activate([
            navigationTop.topAnchor.constraint(equalTo: topAnchor),
            navigationTop.leadingAnchor.constraint(equalTo: leadingAnchor),
            navigationTop.trailingAnchor.constraint(equalTo: trailingAnchor),
            navigationTop.heightAnchor.constraint(equalToConstant: 90),
    
            
            bodyContainer.topAnchor.constraint(equalTo: navigationTop.bottomAnchor),
            bodyContainer.leadingAnchor.constraint(equalTo: leadingAnchor),
            bodyContainer.trailingAnchor.constraint(equalTo: trailingAnchor),
            bodyContainer.bottomAnchor.constraint(equalTo: bottomAnchor),
            
        ])
    }
    
    @objc private func onClickMenuOption(_ sender: UIButton){
        for button in optionMenu{
            if button.tag == sender.tag{
                if GSHomeViewUIType(rawValue: sender.tag) == .ProcAndArq{
                    sender.backgroundColor = GSColorsManager.buttonSelected
                    sender.setTitleColor(.white, for: .normal)
                }else if GSHomeViewUIType(rawValue: sender.tag) == .Home{
                    sender.setImage(UIImage(named: "home_button_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
                }
            }else{
                button.backgroundColor = .clear
                button.setTitleColor(
                    (GSHomeViewUIType(rawValue: button.tag) == .ProcAndArq ||  GSHomeViewUIType(rawValue: button.tag) == .Home ||
                     GSHomeViewUIType(rawValue: button.tag) == .Profile) ?
                    GSColorsManager.labelColor :
                    GSColorsManager.labelColor.withAlphaComponent(0.5), for: .normal)
                if GSHomeViewUIType(rawValue: button.tag) == .Home{
                    button.setImage(UIImage(named: "home_button_dissable_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
                }
            }
        }
        
        for (ind , buttonFilter) in optionsType.enumerated(){
            if buttonFilter.rawValue == sender.tag{
                delegate?.notifyOptionSelected(option: ind)
            }
        }
        
    }
    
    @objc private func handleLongPress(){
        delegate?.notifyOptionSelected(option: 10)
    }
}
