//
//  GSHomeView.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSCommonUtils

/// GSHome Module View
class GSHomeView: UIViewController {
    
    private var ui : GSHomeViewUI?
    var pagerController = GSPagerHome(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    override func loadView() {
        ui = GSHomeViewUI(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = GSColorsManager.noneColor
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - extending GSHomeView to implement the custom ui view delegate
extension GSHomeView: GSHomeViewUIDelegate {
    func notifyOptionSelected(option: Int) {
        pagerController.onTabOption(option: option)
    }
    
    func notifyOptionBuilder(builder: [GSHomeViewUIType]) {
        pagerController.onBuilderOptions(options: builder)
    }
    
    func notifyPagerViewController() -> UIPageViewController {
        return pagerController
    }
    
    func notifyGetParentView() -> UIViewController {
        return self
    }
}
