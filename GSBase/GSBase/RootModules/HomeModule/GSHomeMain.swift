//
//  GSHomeMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 21/12/21.
//

import UIKit

class GSHomeMain: NSObject {

    static func createModule()->UIViewController{
        
        let viewController  :   GSHomeView?   =  GSHomeView()
        if let view = viewController {
         
            return view
        }
        return UIViewController()
    }
}
