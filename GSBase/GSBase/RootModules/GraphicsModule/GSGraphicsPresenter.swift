//
//  GSGraphicsPresenter.swift
//  GSBase
//
//  Created by Gustavo Tellez on 29/12/21.
//

import Foundation
import SDKGSServicesManager

class GSGraphicsPresenter{
    public weak var view    :   GSGraphicsViewProtocol?
    public var router       :   GSGraphicsRouterProtocol?
    public var interactor   :   GSGraphicsInteractorProtocol?
}

extension GSGraphicsPresenter: GSGraphicsPresenterProtocol{
    func requestGraphicsList(withLoader: Bool) {
        if withLoader == true{
            view?.showLoader()
        }
        interactor?.fetchGraphicsList()
    }
    
    func responseGraphicsList(response: ListSplunkResponse?) {
        view?.notifyReloadGraphicsList(list: response)
        view?.dismissLoader()
    }
    
    func responseError(msg: String, isEmptyData: Bool) {
        view?.dismissLoader()
    }
    
}
