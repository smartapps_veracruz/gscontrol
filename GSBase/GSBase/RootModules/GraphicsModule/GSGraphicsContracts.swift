//
//  GSGraphicsContracts.swift
//  GSBase
//
//  Created by Gustavo Tellez on 29/12/21.
//

import Foundation
import SDKGSServicesManager

protocol GSGraphicsViewProtocol: AnyObject {
    func showLoader()
    func dismissLoader()
    func notifyReloadGraphicsList(list: ListSplunkServiceProtocol.Response)
    func notifyServiceError(msg: String)
}

protocol GSGraphicsPresenterProtocol: AnyObject{
    func requestGraphicsList(withLoader:Bool)
    func responseGraphicsList(response: ListSplunkServiceProtocol.Response)
    func responseError(msg: String, isEmptyData: Bool)
}

protocol GSGraphicsInteractorProtocol: AnyObject {
    func fetchGraphicsList()
}

protocol GSGraphicsRouterProtocol: AnyObject {
}
