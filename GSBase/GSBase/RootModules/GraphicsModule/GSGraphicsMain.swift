//
//  GSGraphicsMain.swift
//  GSBase
//
//  Created by Gustavo Tellez on 29/12/21.
//

import Foundation
import UIKit


open class GSGraphicsMain{
    
    public static func createModule() -> UIView{
        
        let viewUI : GSGraphicsViewUI? = GSGraphicsViewUI()
        
        if let view = viewUI{
            let interactor  =   GSGraphicsInteractor()
            let presenter   =   GSGraphicsPresenter()
            let router      =   GSGraphicsRouter()
            
            view.presenter = presenter
            
            presenter.view          =   view
            presenter.router        =   router
            presenter.interactor    =   interactor
            
            interactor.presenter    =   presenter
            
            return view
        }
        
        return UIView()
    }
}
