//
//  GSGraphicsViewUI.swift
//  GSBase
//
//  Created by Gustavo Tellez on 29/12/21.
//

import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager

class GSGraphicsViewUI: UIView {
    lazy var maxiBox: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    public lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var loader : UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.center = center
        activityIndicator.startAnimating()
        return activityIndicator
    }()
    
    private lazy var lbHeader: UILabel = {
        let lb = UILabel()
        lb.text = "Dependecias de servicio"
        lb.textAlignment = .left
        lb.numberOfLines = 1
        lb.font = UIFont.Montserrat_Semibold_16
        lb.textColor = GSColorsManager.labelColor
        lb.translatesAutoresizingMaskIntoConstraints = false
        
        return lb
    }()
    
    private lazy var headerHorizontalLine: UIView = {
        let view = UIView()
        view.backgroundColor = GSColorsManager.labelColor.withAlphaComponent(0.5)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var headerVerticalLine: UIView = {
        let view = UIView()
        view.backgroundColor = GSColorsManager.labelColor.withAlphaComponent(0.5)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var separatorTop: UIView = {
        let view = UIView()
        view.backgroundColor = GSColorsManager.labelLightColor.withAlphaComponent(0.25)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var graphicsCollection: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collection.backgroundColor = GSColorsManager.collectionColor
        collection.isPagingEnabled = false
        collection.delegate = self
        collection.backgroundColor = .clear
        collection.dataSource = self
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.scrollDirection = .horizontal
        collection.collectionViewLayout = flowLayout
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.register(GSGraphicCollectionCell.self, forCellWithReuseIdentifier: GSGraphicCollectionCell.identifier)
        return collection
    }()
    
    private lazy var separatorBottom: UIView = {
        let view = UIView()
        view.backgroundColor = GSColorsManager.labelLightColor.withAlphaComponent(0.25)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var graphicsList: ListSplunkServiceProtocol.Response = nil{
        didSet{
            graphicsCollection.reloadData()
        }
    }
    
    public var presenter: GSGraphicsPresenterProtocol?
    
    public init(){
        super.init(frame: CGRect.zero)
        self.backgroundColor = UIColor.clear
        self.translatesAutoresizingMaskIntoConstraints = false
        
        buildUIElements()
        buildConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildUIElements(){
        
        addSubview(loader)
        headerView.addSubview(lbHeader)
        headerView.addSubview(headerHorizontalLine)
        headerView.addSubview(headerVerticalLine)
        maxiBox.addSubview(headerView)
        maxiBox.addSubview(separatorTop)
        maxiBox.addSubview(graphicsCollection)
        maxiBox.addSubview(separatorBottom)
        addSubview(maxiBox)
    }
    
    private func buildConstraints(){
        NSLayoutConstraint.activate([
            
            loader.centerXAnchor.constraint(equalTo: centerXAnchor),
            loader.centerYAnchor.constraint(equalTo: centerYAnchor),
            loader.heightAnchor.constraint(equalToConstant: 50),
            loader.widthAnchor.constraint(equalToConstant: 50),
            
            maxiBox.topAnchor.constraint(equalTo: topAnchor),
            maxiBox.leadingAnchor.constraint(equalTo: leadingAnchor),
            maxiBox.trailingAnchor.constraint(equalTo: trailingAnchor),
            maxiBox.heightAnchor.constraint(equalToConstant: 150),
            
            headerView.topAnchor.constraint(equalTo: maxiBox.topAnchor, constant: 20.0),
            headerView.leadingAnchor.constraint(equalTo: maxiBox.leadingAnchor, constant: 20.0),
            headerView.widthAnchor.constraint(equalToConstant: 250.0),
            
            lbHeader.topAnchor.constraint(equalTo: headerView.topAnchor),
            lbHeader.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            lbHeader.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            lbHeader.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),
            
            headerHorizontalLine.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            headerHorizontalLine.heightAnchor.constraint(equalToConstant: 1.0),
            headerHorizontalLine.widthAnchor.constraint(equalToConstant: 40.0),
            headerHorizontalLine.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            
            headerVerticalLine.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.5),
            headerVerticalLine.widthAnchor.constraint(equalToConstant: 1.0),
            headerVerticalLine.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            headerVerticalLine.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),
            
            separatorTop.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 10.0),
            separatorTop.leadingAnchor.constraint(equalTo: maxiBox.leadingAnchor, constant: 10.0),
            separatorTop.heightAnchor.constraint(equalToConstant: 0.5),
            separatorTop.trailingAnchor.constraint(equalTo: maxiBox.trailingAnchor),
            
            graphicsCollection.topAnchor.constraint(equalTo: separatorTop.bottomAnchor, constant: 1.0),
            graphicsCollection.leadingAnchor.constraint(equalTo: maxiBox.leadingAnchor, constant: 10.0),
            graphicsCollection.trailingAnchor.constraint(equalTo: maxiBox.trailingAnchor),
            graphicsCollection.bottomAnchor.constraint(equalTo: separatorBottom.topAnchor),
            
            separatorBottom.leadingAnchor.constraint(equalTo: maxiBox.leadingAnchor, constant: 10.0),
            separatorBottom.heightAnchor.constraint(equalToConstant: 0.5),
            separatorBottom.trailingAnchor.constraint(equalTo: maxiBox.trailingAnchor),
            separatorBottom.bottomAnchor.constraint(equalTo: maxiBox.bottomAnchor)
        ])
    }
}

extension GSGraphicsViewUI: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return graphicsList != nil ? 3 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: 230,  height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 50
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GSGraphicCollectionCell.identifier, for: indexPath) as! GSGraphicCollectionCell
        switch indexPath.row{
        case 0:
            cell.imgGraphic.image = UIImage(named: "img_graphic1", in: .local_gs_utils, compatibleWith: nil)
            cell.nameGraph.text = "Traza Salesforce"
            cell.didUpdate(data: graphicsList?
                            .salesforce?.listGraph ?? [], lineColor: UIColor(hexString: "8BB9B7"))
            cell.currentValueCell.text = "\(graphicsList?.salesforce?.skip ?? 0)"
            break
        case 1:
            cell.imgGraphic.image = UIImage(named: "img_graphic2", in: .local_gs_utils, compatibleWith: nil)
            cell.nameGraph.text = "Traza Internet"
            cell.didUpdate(data: graphicsList?.internetTrace?.listGraphInfo ?? [], lineColor: UIColor(hexString: "8BB9B7"))
            cell.currentValueCell.text = "\(graphicsList?.internetTrace?.skip?.skip ?? 0)"
            break
        default:
            cell.imgGraphic.image = UIImage(named: "img_graphic3", in: .local_gs_utils, compatibleWith: nil)
            cell.nameGraph.text = "Traza Data center"
            cell.didUpdate(data: graphicsList?.dataCenter?.listGraphInfo ?? [], lineColor: UIColor(hexString: "8BB9B7"))
            cell.currentValueCell.text = "\(graphicsList?.dataCenter?.skip?.skip ?? 0)"
            break
        }
        return cell
    }
}

extension GSGraphicsViewUI: GSGraphicsViewProtocol{
    func showLoader() {
        DispatchQueue.main.async { [self] in
            loader.isHidden = false
            loader.startAnimating()
        }
    }

    func dismissLoader() {
        DispatchQueue.main.async { [self] in
            if graphicsList != nil{
                loader.isHidden = true
                loader.stopAnimating()
            }
        }

    }
    
    func notifyReloadGraphicsList(list: ListSplunkResponse?) {
        graphicsList = list
    }
    
    func notifyServiceError(msg: String) {
        graphicsList = nil
    }
    
}
