//
//  GSGraphicsInteractor.swift
//  GSBase
//
//  Created by Gustavo Tellez on 29/12/21.
//

import Foundation
import SDKGSServicesManager

class GSGraphicsInteractor{
    public weak var presenter: GSGraphicsPresenterProtocol?
    private let facade = NOCServicesFacade.sharedHome
}

extension GSGraphicsInteractor: GSGraphicsInteractorProtocol{
    func fetchGraphicsList() {
        let request = ListSplunkRequest(dashboardType: "SPLUNK")
        
        self.facade.listSplunk(request: request){ response, _ in
            DispatchQueue.main.async {
                self.presenter?.responseGraphicsList(response: response)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: emptyMessage ?? "", isEmptyData: true)
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: errorMessage ?? "", isEmptyData: false)
            }
        }
    }
    
    
}
