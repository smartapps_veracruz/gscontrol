//
//  GSGraphicCollectionCell.swift
//  GSBase
//
//  Created by Gustavo Tellez on 29/12/21.
//

import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager

class GSGraphicCollectionCell: UICollectionViewCell {
    
    public lazy var imgGraphic: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    lazy var nameGraph: UILabel = {
        let label = UILabel(frame: .zero)
        label.text = "Traza Salesforce"
        label.font = .Montserrat_Regular_16
        label.numberOfLines = 0
        label.textColor = GSColorsManager.labelLightColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var nameJumpGraph: UILabel = {
        let label = UILabel(frame: .zero)
        label.text = "Saltos"
        label.textAlignment = .center
        label.font = .Montserrat_Medium_12
        label.textColor = GSColorsManager.primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
 
    lazy var currentCell: UIView = {
        let view1 = UIView(frame: .zero)
        view1.backgroundColor =  UIColor.init(red: 17/255, green: 51/255, blue: 89/255, alpha: 0.7)
        view1.translatesAutoresizingMaskIntoConstraints = false
        view1.addSubview(currentValueCell)
        NSLayoutConstraint.activate([
            currentValueCell.leadingAnchor.constraint(equalTo: view1.leadingAnchor),
            currentValueCell.trailingAnchor.constraint(equalTo: view1.trailingAnchor),
            currentValueCell.centerXAnchor.constraint(equalTo: view1.centerXAnchor),
            currentValueCell.centerYAnchor.constraint(equalTo: view1.centerYAnchor),
            
        ])
        return view1
    }()
    
    lazy var currentValueCell: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Medium_16.withSize(14)
        label.textColor = GSColorsManager.labelLightColor
        label.textAlignment = .center
        label.text = "0"
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = GSColorsManager.labelLightColor.withAlphaComponent(0.25)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    public lazy var imgGraphicBackground: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.image = UIImage(named: "backgroundGrap", in: .local_gs_utils, compatibleWith: nil)
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    public lazy var imgGraphicBackground2: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.image = UIImage(named: "background_body_ic", in: .local_gs_utils, compatibleWith: nil)
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    
    lazy var lineChart: LineChart = {
        var lineChart =  LineChart()
        lineChart.translatesAutoresizingMaskIntoConstraints = false
        lineChart.animation.enabled = true
        lineChart.dots.visible = true
        lineChart.area = true
        lineChart.x.labels.visible = false
        return lineChart
    }()
    public static let identifier  = "GSGraphicCollectionCell"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildUIElements()
        buildConstraints()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildUIElements(){
        contentView.addSubview(separatorView)
        contentView.addSubview(nameGraph)
        contentView.addSubview(currentCell)
        contentView.addSubview(nameJumpGraph)
        contentView.addSubview(imgGraphicBackground2)
        contentView.addSubview(imgGraphicBackground)
        contentView.addSubview(lineChart)
        contentView.addSubview(imgGraphic)
    }
    
    private func buildConstraints(){
        NSLayoutConstraint.activate([
            separatorView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10.0),
            separatorView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            separatorView.widthAnchor.constraint(equalToConstant: 0.5),
            separatorView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10.0),
            
            
            imgGraphic.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10),
            imgGraphic.trailingAnchor.constraint(equalTo: separatorView.leadingAnchor, constant: -5.0),
            imgGraphic.heightAnchor.constraint(equalToConstant: 20),
            imgGraphic.widthAnchor.constraint(equalToConstant: 50),
            
            nameGraph.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 5),
            nameGraph.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            nameGraph.trailingAnchor.constraint(equalTo: imgGraphic.leadingAnchor),
            nameGraph.heightAnchor.constraint(equalToConstant: 22),
            
            lineChart.topAnchor.constraint(equalTo: nameGraph.bottomAnchor, constant: 5),
            lineChart.leadingAnchor.constraint(equalTo: nameGraph.leadingAnchor, constant: -10),
            lineChart.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5),
            lineChart.trailingAnchor.constraint(equalTo: nameGraph.trailingAnchor),
            
            imgGraphicBackground2.centerYAnchor.constraint(equalTo: centerYAnchor),
            imgGraphicBackground2.centerXAnchor.constraint(equalTo: centerXAnchor),
            imgGraphicBackground2.heightAnchor.constraint(equalTo: heightAnchor),
            imgGraphicBackground2.leadingAnchor.constraint(equalTo: leadingAnchor),
            imgGraphicBackground2.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            imgGraphicBackground.centerYAnchor.constraint(equalTo: lineChart.centerYAnchor),
            imgGraphicBackground.centerXAnchor.constraint(equalTo: lineChart.centerXAnchor),
            imgGraphicBackground.heightAnchor.constraint(equalTo: lineChart.heightAnchor),
            imgGraphicBackground.leadingAnchor.constraint(equalTo: lineChart.leadingAnchor),
            imgGraphicBackground.trailingAnchor.constraint(equalTo: lineChart.trailingAnchor),
            
            nameJumpGraph.topAnchor.constraint(equalTo: imgGraphic.bottomAnchor, constant: 10),
            nameJumpGraph.leadingAnchor.constraint(equalTo: imgGraphic.leadingAnchor),
            nameJumpGraph.trailingAnchor.constraint(equalTo: imgGraphic.trailingAnchor),
            
            currentCell.topAnchor.constraint(equalTo: nameJumpGraph.bottomAnchor, constant: 5),
            currentCell.leadingAnchor.constraint(equalTo: imgGraphic.leadingAnchor),
            currentCell.trailingAnchor.constraint(equalTo: imgGraphic.trailingAnchor),
            currentCell.bottomAnchor.constraint(equalTo: lineChart.bottomAnchor, constant: -5),
            
        ])
    }
    
    public func didUpdate(data: [listGraphInfo], lineColor: UIColor){
        var labels = [String]()
        data.forEach({ element in
            labels.append(element.dataTime ?? "")
        })

        var values = [CGFloat]()
        data.forEach({ element in
            values.append(element.trace16175 == nil ? 0 : CGFloat(element.trace16175 ?? 0))
        })
        if CGFloat(labels.count) == CGFloat(values.count){
            lineChart.clear()
            lineChart.dots.color = lineColor
            lineChart.colors = lineColor
            lineChart.animation.enabled = false
            lineChart.x.grid.count =  CGFloat(labels.count)
            lineChart.y.grid.count =  CGFloat(values.count)
            lineChart.x.labels.values = labels
            lineChart.y.labels.visible = false
            lineChart.x.labels.visible = false
            lineChart.y.axis.visible = false
            lineChart.x.axis.visible = false
            lineChart.y.grid.visible = false
            lineChart.x.grid.visible = false
            lineChart.addLine(values)
        }
    }
    
    public func didUpdate(data: [skipInfo], lineColor: UIColor){
        var labels = [String]()
        data.forEach({ element in
            labels.append(element.ipFrom ?? "")
        })

        var values = [CGFloat]()
        data.forEach({ element in
            if let skip = element.skip{
                values.append(CGFloat(skip))
            }
           // values.append(element.skip == nil ? CGFloat(Float.random(in:  4..<16)) : CGFloat(element.skip ?? 0))
        })
        if CGFloat(labels.count) == CGFloat(values.count){
            lineChart.clear()
            lineChart.colors = lineColor
            lineChart.animation.enabled = false
            lineChart.x.grid.count =  CGFloat(labels.count)
            lineChart.y.grid.count =  CGFloat(values.count)
            lineChart.x.labels.values = labels
            lineChart.y.labels.visible = false
            lineChart.x.labels.visible = false
            lineChart.y.axis.visible = false
            lineChart.x.axis.visible = false
            lineChart.y.grid.visible = false
            lineChart.x.grid.visible = false
            lineChart.addLine(values)
        }
    }
}
