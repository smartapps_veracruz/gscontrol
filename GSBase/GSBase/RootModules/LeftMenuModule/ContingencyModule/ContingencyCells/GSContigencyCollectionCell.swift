//
//  GSContigencyCellView.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

internal class GSContingencyCollectionCell: UICollectionViewCell {
    public static let identifier = "GSContingencyCollectionCell"
    public var enumID: Int = 0
    
    public var titleString: String = "" {
        didSet {
            let font: UIFont = self.getCorrectFontSize(currentSize: 14)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            let style = [
                NSAttributedString.Key.paragraphStyle: paragraphStyle,
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.foregroundColor: GSColorsManager.contingencyCellLabelColor
            ]
            self.titleLabel.attributedText = NSMutableAttributedString(string: self.titleString, attributes: style as [NSAttributedString.Key: Any])
        }
    }
    
    public var isEnabled: Bool = false {
        didSet {
            self.isUserInteractionEnabled = self.isEnabled
            self.containerView.alpha = self.isEnabled ? 1 : 0.4
        }
    }
    
    private lazy var containerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black.withAlphaComponent(0.8)
        view.layer.cornerRadius = 20
        view.layer.borderWidth = 1
        view.layer.borderColor = GSColorsManager.viewBorderColor.cgColor
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUIElements()
        self.setupConstraints()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupUIElements() {
        self.addSubview(self.containerView)
        self.containerView.addSubview(self.titleLabel)
    }
    
    fileprivate func setupConstraints() {
        NSLayoutConstraint.activate([
            self.containerView.widthAnchor.constraint(equalTo: self.widthAnchor),
            self.containerView.heightAnchor.constraint(equalTo: self.heightAnchor),
            
            self.titleLabel.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 2),
            self.titleLabel.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 30),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: -30),
            self.titleLabel.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: -2),
        ])
    }
    
    private func getCorrectFontSize(currentSize: CGFloat) -> UIFont{
        let font: UIFont = .Montserrat_Semibold_14.withSize(currentSize)
        let stringSize = self.titleString.widthOfString(usingFont: font)
        let labelWidth = ((self.bounds.width - 80) * 2)
        if stringSize >= labelWidth && currentSize >= 5 {
            return self.getCorrectFontSize(currentSize: currentSize - 1)
        } else {
            return font
        }
    }
}
