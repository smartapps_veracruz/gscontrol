//
//  GSAdministrativesPresenter.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
class GSAdministrativesPresenter {
    var interactor: GSAdministrativesInteractorProtocol?
    weak var view: GSAdministrativesViewProtocol?
    var router: GSAdministrativesRouterProtocol?
    
}



extension GSAdministrativesPresenter: GSAdministrativesPresenterProtocol {
    
}
