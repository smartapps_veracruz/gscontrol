//
//  GSAdministrativesProtocols.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
protocol GSAdministrativesViewProtocol: AnyObject {
    
}

protocol GSAdministrativesInteractorProtocol: AnyObject {
    
}

protocol GSAdministrativesPresenterProtocol: AnyObject {
    
}

protocol GSAdministrativesRouterProtocol: AnyObject {
    
}
