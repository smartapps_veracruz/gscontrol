//
//  GSAdministrativesView.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit
class GSAdministrativesView: UIViewController {
    var presenter: GSAdministrativesPresenterProtocol?
    private var ui: GSAdministrativesViewUI?
    
    
    override func loadView() {
        ui = GSAdministrativesViewUI(
            delegate: self
        )
        view = ui
    }
    
}

extension GSAdministrativesView: GSAdministrativesViewProtocol {
    
}

extension GSAdministrativesView: GSAdministrativesViewUIDelegate {
    
}
