//
//  GSAdministrativesViewUI.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

internal enum GSAdministrativesTypes: Int {
    case Billing
    case Appraisal

    public func getTitle() -> String {
        switch self {
        case .Billing:
            return "Switch Over Facturación"
        case .Appraisal:
            return "Switch Over Tasación"
        }
    }
}


protocol GSAdministrativesViewUIDelegate {
    
}

class GSAdministrativesViewUI: UIView{
    var delegate: GSAdministrativesViewUIDelegate?
    
    private var mainSectionCells: [GSAdministrativesTypes] = [.Billing, .Appraisal]
    
    private let cellsPerRow: CGFloat = 3
    
    lazy var mainContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        return view
    }()
    
    private lazy var mainCollection: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.collectionViewLayout = UICollectionViewFlowLayout.init()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(GSContingencyCollectionCell.self,
                                forCellWithReuseIdentifier: GSContingencyCollectionCell.identifier)
        collectionView.tag = 0
        return collectionView
    }()
    
    
    public convenience init(
        delegate: GSAdministrativesViewUIDelegate){
            self.init()
            self.delegate = delegate
            
            setUI()
            setConstraints()
            self.mainCollection.reloadData()
        }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUI(){
        self.addSubview(self.mainContainerView)
        self.mainContainerView.addSubview(self.mainCollection)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.mainContainerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 50),
            self.mainContainerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.mainContainerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.mainContainerView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.mainCollection.topAnchor.constraint(equalTo: self.mainContainerView.topAnchor),
            self.mainCollection.leadingAnchor.constraint(equalTo: self.mainContainerView.leadingAnchor, constant: 50),
            self.mainCollection.trailingAnchor.constraint(equalTo: self.mainContainerView.trailingAnchor, constant: -50),
            self.mainCollection.bottomAnchor.constraint(equalTo: self.mainContainerView.bottomAnchor),
        ])
    }
    
    
}

extension GSAdministrativesViewUI: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = 40 * (self.cellsPerRow + 1)
        let availableWidth = self.mainCollection.frame.width - paddingSpace
        let widthPerItem = availableWidth / self.cellsPerRow
        return  CGSize(width: widthPerItem,  height: 40)
    }
    
    func collectionView( _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mainSectionCells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GSContingencyCollectionCell.identifier, for: indexPath) as! GSContingencyCollectionCell
        
        let cellType = self.mainSectionCells[indexPath.row]
        
        cell.isEnabled = false
        cell.titleString = cellType.getTitle()
        cell.enumID = cellType.rawValue
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellType = self.mainSectionCells[indexPath.row]

        print(cellType.getTitle())
    }
}


