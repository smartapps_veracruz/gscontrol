//
//  GSAdministrativesMain.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit

open class GSAdministrativesMain{
    public static func createModule() -> UIViewController {
        let viewController: GSAdministrativesView? = GSAdministrativesView()
        if let view = viewController {
            let presenter = GSAdministrativesPresenter()
            let router = GSAdministrativesRouter()
            let interactor = GSAdministrativesInteractor()
            
            view.presenter = presenter
            
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = router

            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
