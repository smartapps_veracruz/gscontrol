//
//  GSContingencyView.swift
//  GSBase
//
//  Created by Branchbit on 25/12/21.
//

import Foundation
import UIKit
class GSContingencyView: UIViewController {
    var presenter: GSContingencyPresenterProtocol?
    private var ui: GSContingencyViewUI?
    
    
    override func loadView() {
        ui = GSContingencyViewUI(
            delegate: self
        )
        view = ui
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension GSContingencyView: GSContingencyViewProtocol {
    
}

extension GSContingencyView: GSContingencyViewUIDelegate {
    func notifyGetParentVC() -> UIViewController {
        return self
    }
    
    func notifyDismiss() {
        self.dismiss(animated: false, completion: nil)
    }
}
