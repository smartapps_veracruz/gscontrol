//
//  GSContingencyMain.swift
//  GSBase
//
//  Created by Branchbit on 25/12/21.
//

import Foundation
import UIKit

open class GSContingencyMain{
    public static func createModule() -> UIViewController {
        let viewController: GSContingencyView? = GSContingencyView()
        if let view = viewController {
            let presenter = GSContingencyPresenter()
            let interactor = GSContingencyInteractor()
            
            
            view.presenter = presenter
            
            presenter.view = view
            presenter.interactor = interactor
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
