//
//  GSIPTVMain.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit

open class GSIPTVMain{
    public static func createModule() -> UIViewController {
        let viewController: GSIPTVView? = GSIPTVView()
        if let view = viewController {
            let presenter = GSIPTVPresenter()
            let router = GSIPTVRouter()
            let interactor = GSIPTVInteractor()
            
            
            view.presenter = presenter
            
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = router
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
