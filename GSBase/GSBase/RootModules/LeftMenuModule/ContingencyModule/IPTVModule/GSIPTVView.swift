//
//  GSIPTVView.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit
class GSIPTVView: UIViewController {
    var presenter: GSIPTVPresenterProtocol?
    private var ui: GSIPTVViewUI?
    
    
    override func loadView() {
        ui = GSIPTVViewUI(
            delegate: self
        )
        view = ui
    }
    
}

extension GSIPTVView: GSIPTVViewProtocol {
    
}

extension GSIPTVView: GSIPTVViewUIDelegate {
    
}
