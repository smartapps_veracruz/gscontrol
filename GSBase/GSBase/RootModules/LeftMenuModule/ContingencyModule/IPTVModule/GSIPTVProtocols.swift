//
//  GSIPTVProtocols.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
protocol GSIPTVViewProtocol: AnyObject {
    
}

protocol GSIPTVInteractorProtocol: AnyObject {
    
}

protocol GSIPTVPresenterProtocol: AnyObject {
    
}

protocol GSIPTVRouterProtocol: AnyObject {
    
}
