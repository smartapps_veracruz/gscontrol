//
//  GSEnterpriseOverflowView.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

class GSEnterpriseOverflowView: UIViewController {
    var presenter: GSEnterpriseOverflowPresenterProtocol?
    private var ui: GSEnterpriseOverflowViewUI?
    private var currentStatus: String = ""
    
    
    override func loadView() {
        ui = GSEnterpriseOverflowViewUI(
            delegate: self
        )
        view = ui
    }
    
    override func viewDidLoad() {
        DispatchQueue.main.async {
            self.showLoader()
            self.presenter?.requestOverflowStatus()
        }
    }
    
    private func showLoader(){
        DispatchQueue.main.async {
            if let ui = self.ui {
                GSLoader.show(parent: ui)
            }
        }
    }
    
    private func dissmissLoader(){
        DispatchQueue.main.async {
            if let ui = self.ui {
                GSLoader.remove(parent: ui)
            }
        }
    }
    
}

extension GSEnterpriseOverflowView: GSEnterpriseOverflowViewProtocol {
    func notifySuccessOverflowStatus(status: String) {
        DispatchQueue.main.async {
            self.dissmissLoader()
            self.ui?.setStatus(status: status)
            self.currentStatus = status
        }
    }
    
    func notifyError(msg: String) {
        self.dissmissLoader()
        print("Error: \(msg)")
    }
}

extension GSEnterpriseOverflowView: GSEnterpriseOverflowViewUIDelegate {
    func notifyDismiss() {
        self.dismiss(animated: false, completion: nil)
    }
    
    func notifyOverflow() {
        let validateAuth = GSFacetecAuthMain.createModule(delegate: self)
        validateAuth.modalPresentationStyle = .overFullScreen
        present(validateAuth, animated: true, completion: nil)
    }

}

extension GSEnterpriseOverflowView: GSFacetecAuthViewDelegate{
    func notifySuccessValidation() {
        if !self.currentStatus.isEmpty {
            var newStatus = ""
            if self.currentStatus == "1" {
                newStatus = "0"
            } else if self.currentStatus == "0" {
                newStatus = "1"
            }
            if !newStatus.isEmpty {
                let viewController = GSProgressViewMain.createModule(delegate: self, fromWhere: GSApplicativesTypes.EnterpriseOverflow, from: "CDMX", to: "Monterrey", setStatus: newStatus)
                viewController.modalPresentationStyle = .overFullScreen
                present(viewController, animated: false, completion: nil)
            }
        }
    }
}

extension GSEnterpriseOverflowView: GSProgressViewProtocol {
    func notifyCompleted() {
        DispatchQueue.main.async {
            self.showLoader()
            self.presenter?.requestOverflowStatus()
        }
    }
}
