//
//  GSEnterpriseOverflowViewUI.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

protocol GSEnterpriseOverflowViewUIDelegate {
    func notifyDismiss()
    func notifyOverflow()
}

class GSEnterpriseOverflowViewUI: UIView{
    var delegate: GSEnterpriseOverflowViewUIDelegate?
    
    lazy var mainView: GSGenericFrontView = {
        let view = GSGenericFrontView(delegate: self, viewType: .contingency, appearAnimation: .center, showDarkBackground: false)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var enterpriseOverflowContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var titleLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Semibold_20.withSize(22)
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.text = "Desborde de llamadas en IVR Empresarial"
        label.textAlignment = .center
        return label
    }()
    
    lazy var currentStatusContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var currentStatusTitleLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Regular_16
        label.textColor = GSColorsManager.labelLightColor
        label.text = "Estatus actual:"
        label.textAlignment = .center
        return label
    }()
    lazy var currentStatusLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Bold_16
        label.textColor = GSColorsManager.labelLightColor
        label.textAlignment = .center
        label.text = " "
        return label
    }()
    
    lazy var activateButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(" ", for: UIControl.State.normal)
        button.tintColor = .black
        button.backgroundColor = GSColorsManager.contingencyCellLabelColor
        button.layer.cornerRadius = 20
        button.titleLabel?.font = .Montserrat_Semibold_14
        button.addTarget(self, action: #selector(self.onClick), for: UIControl.Event.touchUpInside)
        return button
    }()
    
    public convenience init(
        delegate: GSEnterpriseOverflowViewUIDelegate){
            self.init()
            self.delegate = delegate
            
            setUI()
            setConstraints()
        }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUI(){
        self.addSubview(self.mainView)
        
        self.mainView.contentView.addSubview(self.enterpriseOverflowContainerView)
        self.enterpriseOverflowContainerView.addSubview(self.titleLabel)
        self.enterpriseOverflowContainerView.addSubview(self.currentStatusContainerView)
        
        self.currentStatusContainerView.addSubview(self.currentStatusTitleLabel)
        self.currentStatusContainerView.addSubview(self.currentStatusLabel)
        
        self.enterpriseOverflowContainerView.addSubview(self.activateButton)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.mainView.topAnchor.constraint(equalTo: self.topAnchor),
            self.mainView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.mainView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.mainView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.enterpriseOverflowContainerView.topAnchor.constraint(equalTo: self.mainView.contentView.topAnchor, constant: 80),
            self.enterpriseOverflowContainerView.leadingAnchor.constraint(equalTo: self.mainView.contentView.leadingAnchor, constant: 180),
            self.enterpriseOverflowContainerView.trailingAnchor.constraint(equalTo: self.mainView.contentView.trailingAnchor, constant: -180),
            self.enterpriseOverflowContainerView.bottomAnchor.constraint(equalTo: self.mainView.contentView.bottomAnchor, constant: -80),
            
            self.titleLabel.topAnchor.constraint(equalTo: self.enterpriseOverflowContainerView.topAnchor),
            self.titleLabel.leadingAnchor.constraint(equalTo: self.enterpriseOverflowContainerView.leadingAnchor),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.enterpriseOverflowContainerView.trailingAnchor),
            
            self.currentStatusContainerView.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 60),
            self.currentStatusContainerView.centerXAnchor.constraint(equalTo: self.enterpriseOverflowContainerView.centerXAnchor),
            
            self.currentStatusTitleLabel.topAnchor.constraint(equalTo: self.currentStatusContainerView.topAnchor),
            self.currentStatusTitleLabel.leadingAnchor.constraint(equalTo: self.currentStatusContainerView.leadingAnchor),
            self.currentStatusTitleLabel.bottomAnchor.constraint(equalTo: self.currentStatusContainerView.bottomAnchor),
            
            self.currentStatusLabel.topAnchor.constraint(equalTo: self.currentStatusContainerView.topAnchor),
            self.currentStatusLabel.leadingAnchor.constraint(equalTo: self.currentStatusTitleLabel.trailingAnchor, constant: 10),
            self.currentStatusLabel.trailingAnchor.constraint(equalTo: self.currentStatusContainerView.trailingAnchor),
            self.currentStatusLabel.bottomAnchor.constraint(equalTo: self.currentStatusContainerView.bottomAnchor),
            
            self.activateButton.topAnchor.constraint(equalTo: self.currentStatusContainerView.bottomAnchor, constant: 90),
            self.activateButton.centerXAnchor.constraint(equalTo: self.enterpriseOverflowContainerView.centerXAnchor),
            self.activateButton.heightAnchor.constraint(equalToConstant: 40),
            self.activateButton.widthAnchor.constraint(equalToConstant: 250),
        ])
    }
    
    public func setStatus(status: String){
        if status == "1" {
            self.currentStatusLabel.text = "Activo"
            self.activateButton.setTitle("DESACTIVAR", for: UIControl.State.normal)
        } else if status == "0" {
            self.currentStatusLabel.text = "Inactivo"
            self.activateButton.setTitle("ACTIVAR", for: UIControl.State.normal)
        }
    }
    
    @objc func onClick(){
        self.delegate?.notifyOverflow()
    }
}

extension GSEnterpriseOverflowViewUI: GSGenericFrontProtocol {
    func notifyDismiss() {
        self.delegate?.notifyDismiss()
    }
}
