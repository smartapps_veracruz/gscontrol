//
//  GSEnterpriseOverflowProtocols.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
protocol GSEnterpriseOverflowViewProtocol: AnyObject {
    func notifySuccessOverflowStatus(status: String)
    
    func notifyError(msg: String)
}

protocol GSEnterpriseOverflowInteractorProtocol: AnyObject {
    func getOverflowStatus()
}

protocol GSEnterpriseOverflowPresenterProtocol: AnyObject {
    func requestOverflowStatus()
    
    func responseOverflowStatus(status: String)
    
    func responseError(msg: String)
}

protocol GSEnterpriseOverflowRouterProtocol: AnyObject {
    
}
