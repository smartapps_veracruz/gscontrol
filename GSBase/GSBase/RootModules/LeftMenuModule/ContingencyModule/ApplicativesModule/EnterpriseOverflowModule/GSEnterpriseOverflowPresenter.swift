//
//  GSEnterpriseOverflowPresenter.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
class GSEnterpriseOverflowPresenter {
    var interactor: GSEnterpriseOverflowInteractorProtocol?
    weak var view: GSEnterpriseOverflowViewProtocol?
    var router: GSEnterpriseOverflowRouterProtocol?
}



extension GSEnterpriseOverflowPresenter: GSEnterpriseOverflowPresenterProtocol {
    func requestOverflowStatus() {
        self.interactor?.getOverflowStatus()
    }
    
    func responseOverflowStatus(status: String) {
        self.view?.notifySuccessOverflowStatus(status: status)
    }
    
    func responseError(msg: String) {
        self.view?.notifyError(msg: msg)
    }
}
