//
//  GSEnterpriseOverflowInteractor.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import SDKGSServicesManager

class GSEnterpriseOverflowInteractor{
    var presenter: GSEnterpriseOverflowPresenterProtocol?
    private let facade = NOCServicesFacade.sharedContingency
}

extension GSEnterpriseOverflowInteractor: GSEnterpriseOverflowInteractorProtocol {
    func getOverflowStatus() {
        let request = PanicButtonQueryRequest(ip: "127.2.1.0", password: "Middle100$", userId: "26351")
        self.facade.queryPanicButton(request: request) { response, _ in
            DispatchQueue.main.async {
                self.presenter?.responseOverflowStatus(status: response?.result ?? "")
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: emptyMessage ?? "")
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: errorMessage ?? "")
            }
        }
    }
}
