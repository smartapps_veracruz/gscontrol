//
//  GSEnterpriseOverflowMain.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import UIKit

open class GSEnterpriseOverflowMain{
    public static func createModule() -> UIViewController {
        let viewController: GSEnterpriseOverflowView? = GSEnterpriseOverflowView()
        if let view = viewController {
            let presenter = GSEnterpriseOverflowPresenter()
            let router = GSEnterpriseOverflowRouter()
            let interactor = GSEnterpriseOverflowInteractor()
            
            
            view.presenter = presenter
            
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = router
            
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
