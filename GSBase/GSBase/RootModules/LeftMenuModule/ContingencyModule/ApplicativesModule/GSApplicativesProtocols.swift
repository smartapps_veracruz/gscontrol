//
//  GSApplicativesProtocols.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
protocol GSApplicativesViewProtocol: AnyObject {
    
}

protocol GSApplicativesInteractorProtocol: AnyObject {
    
}

protocol GSApplicativesPresenterProtocol: AnyObject {
    
}

protocol GSApplicativesRouterProtocol: AnyObject {
    
}
