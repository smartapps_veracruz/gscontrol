//
//  GSApplicativesMain.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit

open class GSApplicativesMain{
    public static func createModule() -> UIViewController {
        let viewController: GSApplicativesView? = GSApplicativesView()
        if let view = viewController {
            let presenter = GSApplicativesPresenter()
            let router = GSApplicativesRouter()
            let interactor = GSApplicativesInteractor()
            
            
            view.presenter = presenter
            
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = router
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
