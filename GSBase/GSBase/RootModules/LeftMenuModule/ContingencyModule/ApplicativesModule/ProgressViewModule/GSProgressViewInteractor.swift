//
//  GSProgressViewInteractor.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import SDKGSServicesManager

class GSProgressViewInteractor{
    var presenter: GSProgressViewPresenterProtocol?
    private let facade = NOCServicesFacade.sharedContingency
}

extension GSProgressViewInteractor: GSProgressViewInteractorProtocol {
    func postUpdateEnterpriseOverflow(status: String) {
        let request = PanicButtonUpdateRequest(ip: "127.2.1.0", password: "Middle100$", userId: "26351", estatusBP: status)
        self.facade.updatePanicButton(request: request) { response, _ in
            DispatchQueue.main.async {
                self.presenter?.responseSuccessEnterpriseOverflow()
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: emptyMessage ?? "")
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: errorMessage ?? "")
            }
        }
    }
    
    func getResidentialOverflow(cityEntity: GSResidenceEntity) {
        let request = CCContingencyUpdateRequest(ip: "127.2.1.0", password: "Middle100$", userId: "26351", ctcId: cityEntity.cityId)
        self.facade.updateCallCenterContingency(request: request) { _, _ in
            DispatchQueue.main.async {
                self.presenter?.responseSuccessResidentialOverflow()
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: emptyMessage ?? "")
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: errorMessage ?? "")
            }
        }
    }
}
