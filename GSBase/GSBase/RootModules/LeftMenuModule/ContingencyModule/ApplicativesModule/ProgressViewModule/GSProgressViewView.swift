//
//  GSProgressViewView.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import UIKit
public protocol GSProgressViewProtocol {
    func notifyCompleted()
}

class GSProgressViewView: UIViewController {
    var presenter: GSProgressViewPresenterProtocol?
    private var ui: GSProgressViewViewUI?
    internal var fromWhere: GSApplicativesTypes?
    internal var from: String = ""
    internal var to: String = ""
    internal var cityEntity: GSResidenceEntity?
    internal var setStatus: String = ""
    internal var delegate: GSProgressViewProtocol?
    
    override func loadView() {
        ui = GSProgressViewViewUI(fromWhere: self.fromWhere,
                                  cityEntity: self.cityEntity,
                                  setStatus: self.setStatus,
                                  from: self.from,
                                  to: self.to,
                                  delegate: self)
        ui?.alpha = 0
        view = ui
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.ui?.alpha = 1
        }
    }
}

extension GSProgressViewView: GSProgressViewViewProtocol {
    func notifySuccessEnterpriseOverflow() {
        self.ui?.setProgress(progress: 1)
    }
    
    func notifySuccessResidentialOverflow() {
        self.ui?.setProgress(progress: 1)
    }
    
    func notifyError(msg: String) {
        print("Error: \(msg)")
        self.ui?.notifyError(msg: msg)
    }
}

extension GSProgressViewView: GSProgressViewViewUIDelegate {
    func notifyEnterpriseOverflow() {
        if !self.setStatus.isEmpty {
            self.presenter?.requestEnterpriseOverflow(status: self.setStatus)
        }
    }
    
    func notifyResidentialOverflow() {
        if let city = self.cityEntity {
            self.presenter?.requestResidentialOverflow(cityEntity: city)
        }
    }
    
    func notifyDismiss() {
        self.delegate?.notifyCompleted()
        self.dismiss(animated: false, completion: { [self] in
            switch fromWhere{
            case .ResidentialOverflow, .EnterpriseOverflow:
                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifierListeningHome"), object: nil)
                break
            default:
                break
            }
        })
    }
}
