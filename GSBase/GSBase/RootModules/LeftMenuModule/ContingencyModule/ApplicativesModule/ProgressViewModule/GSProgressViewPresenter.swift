//
//  GSProgressViewPresenter.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
class GSProgressViewPresenter {
    var interactor: GSProgressViewInteractorProtocol?
    weak var view: GSProgressViewViewProtocol?
    var router: GSProgressViewRouterProtocol?
}



extension GSProgressViewPresenter: GSProgressViewPresenterProtocol {
    func requestEnterpriseOverflow(status: String) {
        self.interactor?.postUpdateEnterpriseOverflow(status: status)
    }
    
    func responseSuccessEnterpriseOverflow() {
        self.view?.notifySuccessEnterpriseOverflow()
    }
    
    func requestResidentialOverflow(cityEntity: GSResidenceEntity) {
        self.interactor?.getResidentialOverflow(cityEntity: cityEntity)
    }
    
    func responseSuccessResidentialOverflow() {
        self.view?.notifySuccessResidentialOverflow()
    }
    
    func responseError(msg: String) {
        self.view?.notifyError(msg: msg)
    }
}
