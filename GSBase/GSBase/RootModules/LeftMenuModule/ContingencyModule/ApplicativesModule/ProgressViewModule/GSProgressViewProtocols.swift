//
//  GSProgressViewProtocols.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
protocol GSProgressViewViewProtocol: AnyObject {
    func notifySuccessResidentialOverflow()
    
    func notifySuccessEnterpriseOverflow()
    
    func notifyError(msg: String)
}

protocol GSProgressViewInteractorProtocol: AnyObject {
    func getResidentialOverflow(cityEntity: GSResidenceEntity)
    
    func postUpdateEnterpriseOverflow(status: String)
}

protocol GSProgressViewPresenterProtocol: AnyObject {
    func requestResidentialOverflow(cityEntity: GSResidenceEntity)
    func responseSuccessResidentialOverflow()
    
    func requestEnterpriseOverflow(status: String)
    func responseSuccessEnterpriseOverflow()
    
    func responseError(msg: String)
}

protocol GSProgressViewRouterProtocol: AnyObject {
    
}
