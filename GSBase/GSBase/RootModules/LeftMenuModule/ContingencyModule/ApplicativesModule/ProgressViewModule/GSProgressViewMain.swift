//
//  GSProgressViewMain.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import UIKit

open class GSProgressViewMain{
    public static func createModule(delegate: GSProgressViewProtocol, fromWhere: GSApplicativesTypes, from: String, to: String, cityEntity: GSResidenceEntity? = nil, setStatus: String = "") -> UIViewController {
        let viewController: GSProgressViewView? = GSProgressViewView()
        if let view = viewController {
            let presenter = GSProgressViewPresenter()
            let router = GSProgressViewRouter()
            let interactor = GSProgressViewInteractor()
            
            
            view.presenter = presenter
            view.fromWhere = fromWhere
            view.cityEntity = cityEntity
            view.setStatus = setStatus
            view.from = from
            view.to = to
            view.delegate = delegate
            
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = router
            
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
