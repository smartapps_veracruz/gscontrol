//
//  GSProgressViewViewUI.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

protocol GSProgressViewViewUIDelegate {
    func notifyDismiss()
    func notifyResidentialOverflow()
    func notifyEnterpriseOverflow()
}

class GSProgressViewViewUI: UIView{
    var delegate: GSProgressViewViewUIDelegate?
    private var fromWhere: GSApplicativesTypes?
    private var cityEntity: GSResidenceEntity?
    private var setStatus: String = ""
    private var to: String = ""
    
    private var progressBarBlackColumnsWidthAnchor: NSLayoutConstraint = NSLayoutConstraint()
    
    private var auxTimer: Timer?
    
    private lazy var darkBackgroundView: UIView = {
        let background = UIView()
        background.translatesAutoresizingMaskIntoConstraints = false
        background.backgroundColor = .black.withAlphaComponent(0.7)
        return background
    }()
    
    private lazy var mainContainerView: UIView = {
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = .black
        container.layer.cornerRadius = 21
        container.layer.borderWidth = 2
        container.layer.borderColor = GSColorsManager.viewBorderColor.withAlphaComponent(0.2).cgColor
        container.clipsToBounds = true
        
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "background_top_ic", in: .local_gs_utils, compatibleWith: nil)
        
        container.addSubview(image)
        
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: container.topAnchor),
            image.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: container.trailingAnchor),
            image.heightAnchor.constraint(equalToConstant: 150),
        ])
        
        return container
    }()
    
    
    private lazy var titleContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        let separator = UIView()
        separator.translatesAutoresizingMaskIntoConstraints = false
        separator.backgroundColor = GSColorsManager.viewBorderColor.withAlphaComponent(1)
        
        view.addSubview(separator)
        
        NSLayoutConstraint.activate([
            separator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            separator.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            separator.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            separator.heightAnchor.constraint(equalToConstant: 1)
        ])
        
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Bold_18
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.textAlignment = .center
        return label
    }()
    
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    // success_ic
    private lazy var successIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        image.image = UIImage(named: "success_ic", in: .local_gs_utils, compatibleWith: nil)
        image.tintColor = GSColorsManager.successColor
        image.alpha = 0
        return image
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Regular_20
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.textAlignment = .center
        return label
    }()
    
    
    private lazy var senderImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        image.image = UIImage(named: "buildingSelect", in: .local_gs_utils, compatibleWith: nil)
        return image
    }()
    private lazy var senderNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = GSColorsManager.buttonBlueAqua
        label.font = .Montserrat_Bold_18
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.numberOfLines = 3
        return label
    }()
    
    private lazy var progressBarContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.layer.cornerRadius = 12
        view.layer.borderColor = GSColorsManager.buttonBlueAqua.cgColor
        view.layer.borderWidth = 1
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var progressBarblackColumnsContainerView: UIView = {
       let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        
        let iterations = Int((UIScreen.main.bounds.width / 10))
        
        for n in 0 ..< iterations {
            let column = UIView()
            column.backgroundColor = .black
            column.translatesAutoresizingMaskIntoConstraints = false
            
            view.addSubview(column)
            
            NSLayoutConstraint.activate([
                column.topAnchor.constraint(equalTo: view.topAnchor, constant: -1),
                column.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 1),
                column.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: CGFloat((12 * n) + 6)),
                column.widthAnchor.constraint(equalToConstant: 6),
            ])
        }
        
        return view
    }()
    
    private lazy var progressBarView: UIProgressView = {
        let progress = UIProgressView()
        progress.translatesAutoresizingMaskIntoConstraints = false
        progress.progressViewStyle = .bar
        progress.trackTintColor = .clear
        progress.progressTintColor = GSColorsManager.labelStrongColor
        
        return progress
    }()
    
    // success_ic
    private lazy var receiverImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        image.image = UIImage(named: "buildingSelect", in: .local_gs_utils, compatibleWith: nil)
        return image
    }()
    private lazy var receiverNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = GSColorsManager.buttonBlueAqua
        label.textAlignment = .center
        label.font = .Montserrat_Bold_18
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.numberOfLines = 3
        return label
    }()
    
    
    private lazy var executionStatusLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Medium_20
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.numberOfLines = 3
        label.textAlignment = .center
        label.alpha = 0
        label.text = " "
        return label
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("OK", for: UIControl.State.normal)
        button.tintColor = .black
        button.backgroundColor = GSColorsManager.buttonBlueAqua
        button.alpha = 0
        button.isUserInteractionEnabled = false
        button.addTarget(self, action: #selector(self.onClick), for: UIControl.Event.touchUpInside)
        button.layer.cornerRadius = 20
        return button
    }()
    
    
    
    
    public convenience init(fromWhere: GSApplicativesTypes?,
                            cityEntity: GSResidenceEntity? = nil,
                            setStatus: String = "",
                            from: String,
                            to: String,
                            delegate: GSProgressViewViewUIDelegate){
        self.init()
        self.delegate = delegate
        
        self.fromWhere = fromWhere
        self.to = to
        
        self.cityEntity = cityEntity
        
        self.setStatus = setStatus
        
        self.setUI()
        self.setConstraints()
        
        self.titleLabel.text = self.fromWhere?.getTitle()
        self.senderNameLabel.text = from
        self.subtitleLabel.text = "\(fromWhere?.getShortTitle() ?? "") \(self.cityEntity?.cityName ?? "") en proceso ..."
        self.receiverNameLabel.text = to
        
        if fromWhere == .ResidentialOverflow && self.cityEntity != nil {
            self.delegate?.notifyResidentialOverflow()
            self.setProgress(progress: 0.50)
        }else if fromWhere == .EnterpriseOverflow && !self.setStatus.isEmpty {
            self.delegate?.notifyEnterpriseOverflow()
            self.setProgress(progress: 0.50)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.setProgress(progress: 0.45)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    self.setProgress(progress: 0.65)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        self.setProgress(progress: 1)
                    }
                }
            }
        }
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setUI(){
        self.addSubview(self.darkBackgroundView)
        self.addSubview(self.mainContainerView)
        
        self.mainContainerView.addSubview(self.titleContainerView)
        
        self.titleContainerView.addSubview(self.titleLabel)
        
        self.mainContainerView.addSubview(self.contentView)
        
        self.contentView.addSubview(self.successIcon)
        self.contentView.addSubview(self.subtitleLabel)
        
        self.contentView.addSubview(self.senderImage)
        self.contentView.addSubview(self.senderNameLabel)
        
        self.contentView.addSubview(self.progressBarContainer)
        self.progressBarContainer.addSubview(self.progressBarView)
        self.progressBarContainer.addSubview(self.progressBarblackColumnsContainerView)
        
        self.contentView.addSubview(self.receiverImage)
        self.contentView.addSubview(self.receiverNameLabel)
        
        self.contentView.addSubview(self.executionStatusLabel)
        self.contentView.addSubview(self.continueButton)
    }
    
    private func setConstraints(){
        self.progressBarBlackColumnsWidthAnchor = self.progressBarblackColumnsContainerView.widthAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            self.darkBackgroundView.topAnchor.constraint(equalTo: self.topAnchor),
            self.darkBackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.darkBackgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.darkBackgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.mainContainerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 30),
            self.mainContainerView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -40),
            self.mainContainerView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40),
            self.mainContainerView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40),
            
            self.titleContainerView.topAnchor.constraint(equalTo: self.mainContainerView.topAnchor),
            self.titleContainerView.leadingAnchor.constraint(equalTo: self.mainContainerView.leadingAnchor, constant: 40),
            self.titleContainerView.trailingAnchor.constraint(equalTo: self.mainContainerView.trailingAnchor, constant: -40),
            self.titleContainerView.heightAnchor.constraint(equalToConstant: 130),
            
            self.titleLabel.centerYAnchor.constraint(equalTo: self.titleContainerView.centerYAnchor, constant: 20),
            self.titleLabel.leadingAnchor.constraint(equalTo: self.titleContainerView.leadingAnchor),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.titleContainerView.trailingAnchor),
            
            self.contentView.topAnchor.constraint(equalTo: self.titleContainerView.bottomAnchor, constant: 20),
            self.contentView.leadingAnchor.constraint(equalTo: self.mainContainerView.leadingAnchor, constant: 60),
            self.contentView.trailingAnchor.constraint(equalTo: self.mainContainerView.trailingAnchor, constant: -60),
            self.contentView.bottomAnchor.constraint(equalTo: self.mainContainerView.bottomAnchor, constant: -60),
            
            self.successIcon.bottomAnchor.constraint(equalTo: self.subtitleLabel.topAnchor, constant: -30),
            self.successIcon.centerXAnchor.constraint(equalTo: self.mainContainerView.centerXAnchor),
            self.successIcon.widthAnchor.constraint(equalToConstant: 55),
            self.successIcon.heightAnchor.constraint(equalToConstant: 55),
            
            self.subtitleLabel.bottomAnchor.constraint(equalTo: self.progressBarContainer.topAnchor, constant: -50),
            self.subtitleLabel.leadingAnchor.constraint(equalTo: self.progressBarContainer.leadingAnchor),
            self.subtitleLabel.trailingAnchor.constraint(equalTo: self.progressBarContainer.trailingAnchor),
            
            self.senderImage.centerYAnchor.constraint(equalTo: self.progressBarContainer.centerYAnchor),
            self.senderImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            self.senderImage.heightAnchor.constraint(equalToConstant: 120),
            self.senderImage.widthAnchor.constraint(equalToConstant: 120),
            
            self.senderNameLabel.topAnchor.constraint(equalTo: self.senderImage.bottomAnchor, constant: 20),
            self.senderNameLabel.leadingAnchor.constraint(equalTo: self.senderImage.leadingAnchor),
            self.senderNameLabel.trailingAnchor.constraint(equalTo: self.senderImage.trailingAnchor),
            
            self.progressBarContainer.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            self.progressBarContainer.leadingAnchor.constraint(equalTo: self.senderImage.trailingAnchor, constant: 20),
            self.progressBarContainer.trailingAnchor.constraint(equalTo: self.receiverImage.leadingAnchor, constant: -20),
            
            self.progressBarblackColumnsContainerView.leadingAnchor.constraint(equalTo: self.progressBarView.leadingAnchor),
            self.progressBarblackColumnsContainerView.topAnchor.constraint(equalTo: self.progressBarView.topAnchor, constant: -1),
            self.progressBarblackColumnsContainerView.bottomAnchor.constraint(equalTo: self.progressBarView.bottomAnchor, constant: 1),
            self.progressBarBlackColumnsWidthAnchor,
            
            self.progressBarView.topAnchor.constraint(equalTo: self.progressBarContainer.topAnchor, constant: 25),
            self.progressBarView.leadingAnchor.constraint(equalTo: self.progressBarContainer.leadingAnchor, constant: 25),
            self.progressBarView.trailingAnchor.constraint(equalTo: self.progressBarContainer.trailingAnchor, constant: -25),
            self.progressBarView.bottomAnchor.constraint(equalTo: self.progressBarContainer.bottomAnchor, constant: -25),
            self.progressBarView.heightAnchor.constraint(equalToConstant: 30),
            
            self.receiverImage.centerYAnchor.constraint(equalTo: self.progressBarContainer.centerYAnchor),
            self.receiverImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            self.receiverImage.heightAnchor.constraint(equalToConstant: 120),
            self.receiverImage.widthAnchor.constraint(equalToConstant: 120),
            
            self.receiverNameLabel.topAnchor.constraint(equalTo: self.receiverImage.bottomAnchor, constant: 20),
            self.receiverNameLabel.leadingAnchor.constraint(equalTo: self.receiverImage.leadingAnchor),
            self.receiverNameLabel.trailingAnchor.constraint(equalTo: self.receiverImage.trailingAnchor),
            
            self.executionStatusLabel.topAnchor.constraint(equalTo: self.progressBarContainer.bottomAnchor, constant: 50),
            self.executionStatusLabel.leadingAnchor.constraint(equalTo: self.progressBarContainer.leadingAnchor),
            self.executionStatusLabel.trailingAnchor.constraint(equalTo: self.progressBarContainer.trailingAnchor),
            
            self.continueButton.topAnchor.constraint(equalTo: self.executionStatusLabel.bottomAnchor, constant: 80),
            self.continueButton.centerXAnchor.constraint(equalTo: self.progressBarContainer.centerXAnchor),
            self.continueButton.widthAnchor.constraint(equalToConstant: 250),
            self.continueButton.heightAnchor.constraint(equalToConstant: 40),
            
        ])
    }
    
    public func setProgress(progress: Float) {
        var auxProgress = progress
        
        if progress > 1.0 {
            auxProgress = 1.0
        } else if progress < 0 {
            auxProgress = 0
        }
        
        var currentProgress: Float = self.progressBarView.progress
        
        if let timer = self.auxTimer {
            timer.invalidate()
            self.auxTimer = nil
        }
        
        var progressBarBlackColumnsWidth: CGFloat = 0.0
        
        Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { timer in
            self.auxTimer = timer
            if currentProgress <= auxProgress {
                currentProgress += 0.005
                self.progressBarView.setProgress(currentProgress, animated: true)
                self.subtitleLabel.text = "\(self.fromWhere?.getShortTitle() ?? "") \(self.cityEntity?.cityName ?? "") en proceso ... \(Int(currentProgress * 100))%"
                
                progressBarBlackColumnsWidth = CGFloat(Int(self.progressBarView.bounds.width * CGFloat(currentProgress)))
                self.progressBarBlackColumnsWidthAnchor.constant = progressBarBlackColumnsWidth
                self.layoutIfNeeded()
            }else {
                if self.progressBarView.progress >= 1 {
                    self.animateSuccess()
                }
                timer.invalidate()
                self.auxTimer = nil
            }
        }
    }
    
    public func notifyError(msg: String){
        self.setProgress(progress: 0)
        UIView.animate(withDuration: 0.3) {
            self.subtitleLabel.text = msg
            self.continueButton.alpha = 1
            self.continueButton.isUserInteractionEnabled = true
            self.layoutIfNeeded()
        }
    }
    
    private func animateSuccess(){
        UIView.animate(withDuration: 0.3) {
            self.successIcon.alpha = 1
            self.senderImage.alpha = 0
            self.senderNameLabel.alpha = 0
            self.receiverImage.alpha = 0
            self.receiverNameLabel.alpha = 0
            self.continueButton.alpha = 1
            self.continueButton.isUserInteractionEnabled = true
            self.executionStatusLabel.alpha = 1
            self.progressBarView.progressTintColor = GSColorsManager.successColor
            self.subtitleLabel.text = "¡\(self.fromWhere?.getShortTitle() ?? "") \(self.cityEntity?.cityName ?? "") realizado correctamente!"
            if self.fromWhere == .ResidentialOverflow {
                self.executionStatusLabel.text = "Resultado de ejecución OK"
            }else if self.fromWhere == .EnterpriseOverflow {
                self.executionStatusLabel.text = "Resultado de ejecución" + "\n" + "Estatus: \(self.setStatus == "0" ? "INACTIVO" : "ACTIVO")"
            }
            self.subtitleLabel.textColor = GSColorsManager.successColor
            self.layoutIfNeeded()
        }
    }
    
    @objc func onClick(){
        UIView.animate(withDuration: 0.5) {
            self.alpha = 0
        } completion: { _ in
            self.delegate?.notifyDismiss()
        }
    }
}




