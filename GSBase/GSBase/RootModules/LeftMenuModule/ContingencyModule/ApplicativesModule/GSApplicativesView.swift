//
//  GSApplicativesView.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit
class GSApplicativesView: UIViewController {
    var presenter: GSApplicativesPresenterProtocol?
    private var ui: GSApplicativesViewUI?
    private var moduleType : GSApplicativesTypes?
    
    override func loadView() {
        ui = GSApplicativesViewUI(
            delegate: self
        )
        view = ui
    }
    
}

extension GSApplicativesView: GSApplicativesViewProtocol {
    
}

extension GSApplicativesView: GSApplicativesViewUIDelegate {
    func notifyNavigateTo(module: GSApplicativesTypes) {
        var viewController: UIViewController? = nil
        switch module {
            case .ResidentialOverflow:
                viewController = GSResidentialOverflowMain.createModule()
                break
            case .EnterpriseOverflow:
                viewController = GSEnterpriseOverflowMain.createModule()
                break
            default:
                break
        }
        if let vc = viewController {
            vc.modalPresentationStyle = .overFullScreen
            present(vc, animated: false, completion: nil)
        }
        
//        moduleType = module
//        let validateAuth = GSFacetecAuthMain.createModule(delegate: self)
//        validateAuth.modalPresentationStyle = .overFullScreen
//        present(validateAuth, animated: true, completion: nil)
    }
}


//extension GSApplicativesView: GSFacetecAuthViewDelegate{
//    func notifySuccessValidation() {
//        var viewController: UIViewController? = nil
//
//    }
//}
