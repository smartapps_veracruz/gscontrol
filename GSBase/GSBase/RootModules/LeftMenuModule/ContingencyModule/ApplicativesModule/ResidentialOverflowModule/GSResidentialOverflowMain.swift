//
//  GSResidentialOverflowMain.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import UIKit

open class GSResidentialOverflowMain{
    public static func createModule() -> UIViewController {
        let viewController: GSResidentialOverflowView? = GSResidentialOverflowView()
        if let view = viewController {
            let presenter = GSResidentialOverflowPresenter()
            let router = GSResidentialOverflowRouter()
            let interactor = GSResidentialOverflowInteractor()
            
            view.presenter = presenter
            
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = router
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
