//
//  GSResidentialOverflowInteractor.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import SDKGSServicesManager
class GSResidentialOverflowInteractor{
    var presenter: GSResidentialOverflowPresenterProtocol?
    
    private var facade = NOCServicesFacade.sharedContingency
    
    
}

extension GSResidentialOverflowInteractor: GSResidentialOverflowInteractorProtocol {
    func getCitiesStatuses() {
        let request = CCContingencyQueryRequest(ip: "127.2.1.0", password: "Middle100$", userId: "26351", ctcStatus: "0")
        self.facade.queryCallCenterContingency(request: request) { response, _ in
            guard let cities = response?.listTKM else {
                return
            }
            DispatchQueue.main.async {
                self.presenter?.responseCitiesStatuses(response: cities)
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: emptyMessage ?? "")
            }
        } failure: { failureMessage in
            DispatchQueue.main.async {
                self.presenter?.responseError(msg: failureMessage ?? "")
            }
        }

    }
    
}
