//
//  GSResidentialOverflowViewUI.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager

protocol GSResidentialOverflowViewUIDelegate {
    func notifyDismiss()
    func notifyOverflow(city: GSResidenceEntity)
}

class GSResidentialOverflowViewUI: UIView{
    var delegate: GSResidentialOverflowViewUIDelegate?
    
    private var gsCheckButtons: [GSCheckButton] = []
    
    private var selectedCity: GSResidenceEntity?
    
    lazy var mainView: GSGenericFrontView = {
        let view = GSGenericFrontView(delegate: self, viewType: .contingency, appearAnimation: .center, showDarkBackground: false)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var residentialOverflowContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var titleLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Semibold_20.withSize(22)
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.text = "Desborde de llamadas en IVR Residencial"
        label.textAlignment = .center
        return label
    }()
    
    lazy var cityOptionsContainerView: UIScrollView = {
       let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    lazy var cityOptionsStack: UIStackView = {
       let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 50
        stack.axis = .vertical
        return stack
    }()
    
    lazy var assignButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("ASIGNAR", for: UIControl.State.normal)
        button.tintColor = .black
        button.backgroundColor = GSColorsManager.contingencyCellLabelColor
        button.layer.cornerRadius = 20
        button.titleLabel?.font = .Montserrat_Semibold_14
        button.addTarget(self, action: #selector(self.onClick), for: UIControl.Event.touchUpInside)
        return button
    }()
    
    public convenience init(
        delegate: GSResidentialOverflowViewUIDelegate){
            self.init()
            self.delegate = delegate
            
            setUI()
            setConstraints()
        }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUI(){
        self.addSubview(self.mainView)
        
        self.mainView.contentView.addSubview(self.residentialOverflowContainerView)
        self.residentialOverflowContainerView.addSubview(self.titleLabel)
        self.residentialOverflowContainerView.addSubview(self.cityOptionsContainerView)
        
        self.cityOptionsContainerView.addSubview(self.cityOptionsStack)
        
        self.residentialOverflowContainerView.addSubview(self.assignButton)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.mainView.topAnchor.constraint(equalTo: self.topAnchor),
            self.mainView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.mainView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.mainView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.residentialOverflowContainerView.topAnchor.constraint(equalTo: self.mainView.contentView.topAnchor, constant: 80),
            self.residentialOverflowContainerView.leadingAnchor.constraint(equalTo: self.mainView.contentView.leadingAnchor, constant: 180),
            self.residentialOverflowContainerView.trailingAnchor.constraint(equalTo: self.mainView.contentView.trailingAnchor, constant: -180),
            self.residentialOverflowContainerView.bottomAnchor.constraint(equalTo: self.mainView.contentView.bottomAnchor, constant: -80),
            
            self.titleLabel.topAnchor.constraint(equalTo: self.residentialOverflowContainerView.topAnchor),
            self.titleLabel.leadingAnchor.constraint(equalTo: self.residentialOverflowContainerView.leadingAnchor),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.residentialOverflowContainerView.trailingAnchor),
            
            self.cityOptionsContainerView.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 70),
            self.cityOptionsContainerView.leadingAnchor.constraint(equalTo: self.residentialOverflowContainerView.leadingAnchor),
            self.cityOptionsContainerView.trailingAnchor.constraint(equalTo: self.residentialOverflowContainerView.centerXAnchor),
            self.cityOptionsContainerView.bottomAnchor.constraint(equalTo: self.residentialOverflowContainerView.bottomAnchor),
            
            self.cityOptionsStack.topAnchor.constraint(equalTo: self.cityOptionsContainerView.topAnchor),
            self.cityOptionsStack.leadingAnchor.constraint(equalTo: self.cityOptionsContainerView.leadingAnchor),
            self.cityOptionsStack.trailingAnchor.constraint(equalTo: self.cityOptionsContainerView.trailingAnchor),
            self.cityOptionsStack.bottomAnchor.constraint(equalTo: self.cityOptionsContainerView.bottomAnchor),
            
            self.assignButton.centerYAnchor.constraint(equalTo: self.residentialOverflowContainerView.centerYAnchor, constant: -40),
            self.assignButton.leadingAnchor.constraint(equalTo: self.residentialOverflowContainerView.centerXAnchor, constant: 40),
            self.assignButton.trailingAnchor.constraint(equalTo: self.residentialOverflowContainerView.trailingAnchor, constant: -50),
            self.assignButton.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    private func setAssignButtonEnabled(enabled: Bool){
        self.assignButton.isUserInteractionEnabled = enabled
        self.assignButton.alpha = enabled ? 1 : 0.4
    }
    
    private func areAllOptionsUnchecked() -> Bool {
        for checkButton in self.gsCheckButtons {
            if checkButton.checked {
                return false
            }
        }
        return true
    }
    
    @objc func onClick(){
        if let city = self.selectedCity {
            self.delegate?.notifyOverflow(city: city)
        }
    }
    
    func notifySetCityOptions(cities: [CCContingencyQueryResponse._listTKM]){
        print(cities)
        for button in self.gsCheckButtons {
            button.removeFromSuperview()
        }
        self.selectedCity = nil
        self.gsCheckButtons.removeAll()
        
        for option in cities {
            let checkButton = GSCheckButton(checked: option.ctcStatus == "1", delegate: self)
            checkButton.id = option.ctcId ?? ""
            checkButton.titleString = option.ctcNameTkm ?? ""
            
            if option.ctcStatus == "1" {
                self.selectedCity = GSResidenceEntity(cityId: option.ctcId ?? "", cityName: option.ctcNameTkm ?? "", cityStatus: "1")
            }
            
            self.gsCheckButtons.append(checkButton)
            self.cityOptionsStack.addArrangedSubview(checkButton)
        }
    }
}


extension GSResidentialOverflowViewUI: GSCheckButtonProtocol {
    func notifyOnCheckButtonClicked(_ sender: GSCheckButton) {
        print(sender.titleString)
        for checkButton in self.gsCheckButtons {
            if checkButton != sender {
                checkButton.checked = false
            }
        }
        self.selectedCity = GSResidenceEntity(cityId: sender.id, cityName: sender.titleString, cityStatus: "1")
        self.setAssignButtonEnabled(enabled: !self.areAllOptionsUnchecked())
    }
}

extension GSResidentialOverflowViewUI: GSGenericFrontProtocol {
    func notifyDismiss() {
        self.delegate?.notifyDismiss()
    }
}
