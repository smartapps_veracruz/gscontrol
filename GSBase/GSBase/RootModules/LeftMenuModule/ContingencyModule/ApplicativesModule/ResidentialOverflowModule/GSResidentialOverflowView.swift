//
//  GSResidentialOverflowView.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import UIKit
import SDKGSServicesManager
import SDKGSCommonUtils

class GSResidentialOverflowView: UIViewController {
    private var temporalOption: GSResidenceEntity?
    var presenter: GSResidentialOverflowPresenterProtocol?
    private var ui: GSResidentialOverflowViewUI?
    
    
    override func loadView() {
        ui = GSResidentialOverflowViewUI(
            delegate: self
        )
        view = ui
    }
    
    override func viewDidLoad() {
        DispatchQueue.main.async {
            self.showLoader()
            self.presenter?.requestCitiesStatuses()
        }
    }
    
    private func showLoader(){
        DispatchQueue.main.async {
            if let ui = self.ui {
                GSLoader.show(parent: ui)
            }
        }
    }
    
    private func dissmissLoader(){
        DispatchQueue.main.async {
            if let ui = self.ui {
                GSLoader.remove(parent: ui)
            }
        }
    }
}

extension GSResidentialOverflowView: GSResidentialOverflowViewProtocol {
    func notifyError(msg: String) {
        self.dissmissLoader()
        print("Error: \(msg)")
    }
    
    func notifyCitiesStatusesResponse(response: [CCContingencyQueryResponse._listTKM]) {
        self.dissmissLoader()
        self.ui?.notifySetCityOptions(cities: response)
    }
}

extension GSResidentialOverflowView: GSResidentialOverflowViewUIDelegate {
    func notifyDismiss() {
        self.dismiss(animated: false, completion: nil)
    }
    func notifyOverflow(city: GSResidenceEntity) {
        temporalOption = city
        let validateAuth = GSFacetecAuthMain.createModule(delegate: self)
        validateAuth.modalPresentationStyle = .overFullScreen
        present(validateAuth, animated: true, completion: nil)
    }
}

extension GSResidentialOverflowView: GSProgressViewProtocol {
    func notifyCompleted() {
        DispatchQueue.main.async {
            self.showLoader()
            self.presenter?.requestCitiesStatuses()
        }
    }
}

extension GSResidentialOverflowView: GSFacetecAuthViewDelegate{
    func notifySuccessValidation() {
        if let city = temporalOption{
            let viewController = GSProgressViewMain.createModule(delegate: self, fromWhere: GSApplicativesTypes.ResidentialOverflow, from: "CDMX", to: "Monterrey", cityEntity: city)
            viewController.modalPresentationStyle = .overFullScreen
            present(viewController, animated: false, completion: nil)
        }
    }
}
