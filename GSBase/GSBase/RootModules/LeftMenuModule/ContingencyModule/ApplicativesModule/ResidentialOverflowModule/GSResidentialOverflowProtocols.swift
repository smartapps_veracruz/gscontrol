//
//  GSResidentialOverflowProtocols.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import SDKGSServicesManager

protocol GSResidentialOverflowViewProtocol: AnyObject {
    func notifyCitiesStatusesResponse(response: [CCContingencyQueryResponse._listTKM])
    
    func notifyError(msg: String)
}

protocol GSResidentialOverflowInteractorProtocol: AnyObject {
    func getCitiesStatuses()
}

protocol GSResidentialOverflowPresenterProtocol: AnyObject {
    func requestCitiesStatuses()
    
    func responseCitiesStatuses(response: [CCContingencyQueryResponse._listTKM])
    
    func responseError(msg: String)
}

protocol GSResidentialOverflowRouterProtocol: AnyObject {
    
}
