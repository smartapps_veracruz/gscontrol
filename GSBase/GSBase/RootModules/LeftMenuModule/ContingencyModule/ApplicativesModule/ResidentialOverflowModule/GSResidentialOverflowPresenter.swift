//
//  GSResidentialOverflowPresenter.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import SDKGSServicesManager

class GSResidentialOverflowPresenter {
    var interactor: GSResidentialOverflowInteractorProtocol?
    weak var view: GSResidentialOverflowViewProtocol?
    var router: GSResidentialOverflowRouterProtocol?
    
}



extension GSResidentialOverflowPresenter: GSResidentialOverflowPresenterProtocol {
    func responseError(msg: String) {
        self.view?.notifyError(msg: msg)
    }
    
    func requestCitiesStatuses() {
        self.interactor?.getCitiesStatuses()
    }
    
    func responseCitiesStatuses(response: [CCContingencyQueryResponse._listTKM]) {
        self.view?.notifyCitiesStatusesResponse(response: response)
    }
    
    
}
