//
//  GSResidentialOverflowEntity.swift
//  GSBase
//
//  Created by Branchbit on 30/12/21.
//

import Foundation

public struct GSResidenceEntity: Decodable{
    public let cityId: String
    public let cityName: String
    public let cityStatus: String
}
