//
//  GSApplicativesViewUI.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

public enum GSApplicativesTypes: Int {
    case ResidentialOverflow
    case EnterpriseOverflow
    case BGPCommutation
    case SalesforceSwitch
    case FFMAppsSwitch
    case FFMWebSwitch
    case DashboardSwitch
    case PortalsSwitch
    case SalesSwitch
    case SacureHomeSwitch
    case IVRIPTVSwitch
    
    case FrontDashboardSwitch
    case FrontFFMSwitch
    
    public func getTitle() -> String {
        switch self {
        case .ResidentialOverflow:
            return "Desborde de llamadas en IVR Residencial"
        case .EnterpriseOverflow:
            return "Desborde de llamadas en IVR Empresarial"
        case .BGPCommutation:
            return "Conmutación de enlace BGP (Salesforce)"
        case .SalesforceSwitch:
            return "Switch Over Salesforce"
        case .FFMAppsSwitch:
            return "Switch FFM APPS"
        case .FFMWebSwitch:
            return "Switch Over FFM WEB"
        case .DashboardSwitch:
            return "Switch Over Dashboard"
        case .PortalsSwitch:
            return "Switch Portales"
        case .SalesSwitch:
            return "Switch Over Ventas"
        case .SacureHomeSwitch:
            return "Switch Over Hogar Seguro"
        case .IVRIPTVSwitch:
            return "Switch Over IVR / IPTV"
        case .FrontDashboardSwitch:
            return "Switch Over Dashboard"
        case .FrontFFMSwitch:
            return "Switch Over FFM"
        }
    }
    
    public func getShortTitle() -> String {
        switch self {
        case .ResidentialOverflow:
            return "Desborde de llamadas en IVR"
        case .EnterpriseOverflow:
            return "Desborde de llamadas en IVR"
        case .BGPCommutation:
            return "Conmutación de enlace BGP (Salesforce)"
        case .SalesforceSwitch:
            return "Switch Over Salesforce"
        case .FFMAppsSwitch:
            return "Switch FFM APPS"
        case .FFMWebSwitch:
            return "Switch Over FFM WEB"
        case .DashboardSwitch:
            return "Switch Over Dashboard"
        case .PortalsSwitch:
            return "Switch Portales"
        case .SalesSwitch:
            return "Switch Over Ventas"
        case .SacureHomeSwitch:
            return "Switch Over Hogar Seguro"
        case .IVRIPTVSwitch:
            return "Switch Over IVR / IPTV"
        case .FrontDashboardSwitch:
            return "Switch Over Dashboard"
        case .FrontFFMSwitch:
            return "Switch Over FFM"
        }
    }
}

protocol GSApplicativesViewUIDelegate {
    func notifyNavigateTo(module: GSApplicativesTypes)
}

class GSApplicativesViewUI: UIView{
    var delegate: GSApplicativesViewUIDelegate?
    private var upperSectionCells: [GSApplicativesTypes] = [.ResidentialOverflow,
                                                            .EnterpriseOverflow,
                                                            .BGPCommutation,
                                                            .SalesforceSwitch,
                                                            .FFMAppsSwitch,
                                                            .FFMWebSwitch,
                                                            .DashboardSwitch,
                                                            .PortalsSwitch,
                                                            .SalesSwitch,
                                                            .SacureHomeSwitch,
                                                            .IVRIPTVSwitch]
    
    private var frontSectionCells: [GSApplicativesTypes] = [.FrontDashboardSwitch, .FrontFFMSwitch]
    
    private let cellsPerRow: CGFloat = 3
    
    lazy var applicativesContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        return view
    }()
    
    private lazy var upperCollection: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.collectionViewLayout = UICollectionViewFlowLayout.init()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(GSContingencyCollectionCell.self,
                                forCellWithReuseIdentifier: GSContingencyCollectionCell.identifier)
        collectionView.tag = 0
        return collectionView
    }()
    
    private lazy var frontTitleLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Front"
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.font = .Montserrat_Semibold_16
        return label
    }()
    
    private lazy var frontLabelSeparatorView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.contingencyCellLabelColor
        return view
    }()
    
    private lazy var frontCollection: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.collectionViewLayout = UICollectionViewFlowLayout.init()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(GSContingencyCollectionCell.self,
                                forCellWithReuseIdentifier: GSContingencyCollectionCell.identifier)
        collectionView.tag = 1
        return collectionView
    }()
    
    public convenience init(
        delegate: GSApplicativesViewUIDelegate){
            self.init()
            self.delegate = delegate
            
            setUI()
            setConstraints()
            self.upperCollection.reloadData()
            self.frontCollection.reloadData()
        }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUI(){
        self.addSubview(self.applicativesContainerView)
        self.applicativesContainerView.addSubview(self.upperCollection)
        self.applicativesContainerView.addSubview(self.frontCollection)
        
        self.applicativesContainerView.addSubview(self.frontTitleLabel)
        self.applicativesContainerView.addSubview(self.frontLabelSeparatorView)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.applicativesContainerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 50),
            self.applicativesContainerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.applicativesContainerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.applicativesContainerView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.upperCollection.topAnchor.constraint(equalTo: self.applicativesContainerView.topAnchor),
            self.upperCollection.leadingAnchor.constraint(equalTo: self.applicativesContainerView.leadingAnchor, constant: 50),
            self.upperCollection.trailingAnchor.constraint(equalTo: self.applicativesContainerView.trailingAnchor, constant: -50),
            self.upperCollection.bottomAnchor.constraint(equalTo: self.frontTitleLabel.topAnchor, constant: -50),
            
            self.frontTitleLabel.leadingAnchor.constraint(equalTo: self.applicativesContainerView.leadingAnchor, constant: 20),
            self.frontTitleLabel.bottomAnchor.constraint(equalTo: self.frontCollection.topAnchor, constant: -30),
            
            self.frontLabelSeparatorView.leadingAnchor.constraint(equalTo: self.frontTitleLabel.trailingAnchor, constant: 20),
            self.frontLabelSeparatorView.trailingAnchor.constraint(equalTo: self.applicativesContainerView.trailingAnchor, constant: -20),
            self.frontLabelSeparatorView.centerYAnchor.constraint(equalTo: self.frontTitleLabel.centerYAnchor),
            self.frontLabelSeparatorView.heightAnchor.constraint(equalToConstant: 0.5),
            
            self.frontCollection.heightAnchor.constraint(equalToConstant: 80),
            self.frontCollection.leadingAnchor.constraint(equalTo: self.applicativesContainerView.leadingAnchor, constant: 50),
            self.frontCollection.trailingAnchor.constraint(equalTo: self.applicativesContainerView.trailingAnchor, constant: -50),
            self.frontCollection.bottomAnchor.constraint(equalTo: self.applicativesContainerView.bottomAnchor, constant: 0),
        ])
    }
}

extension GSApplicativesViewUI: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = 40 * (self.cellsPerRow + 1)
        let availableWidth = self.upperCollection.frame.width - paddingSpace
        let widthPerItem = availableWidth / self.cellsPerRow
        return  CGSize(width: widthPerItem,  height: 40)
    }
    
    func collectionView( _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 40
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 0, bottom: 0.0, right: 0)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 0 ? self.upperSectionCells.count : self.frontSectionCells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GSContingencyCollectionCell.identifier, for: indexPath) as! GSContingencyCollectionCell
        
        let cellType: GSApplicativesTypes?
        if collectionView.tag == 0 {
            cellType = self.upperSectionCells[indexPath.row]
        }else {
            cellType = self.frontSectionCells[indexPath.row]
        }
        if let cellType = cellType {
            cell.isEnabled = (cellType == .ResidentialOverflow || cellType == .EnterpriseOverflow)
            cell.titleString = cellType.getTitle()
            cell.enumID = cellType.rawValue
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellType: GSApplicativesTypes?
        if collectionView.tag == 0 {
            cellType = self.upperSectionCells[indexPath.row]
        }else {
            cellType = self.frontSectionCells[indexPath.row]
        }
        if let cellType = cellType {
            print(cellType.getTitle())
            self.delegate?.notifyNavigateTo(module: cellType)
        }
    }
}

