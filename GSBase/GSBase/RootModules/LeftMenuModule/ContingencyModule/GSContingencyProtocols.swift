//
//  GSContingencyProtocols.swift
//  GSBase
//
//  Created by Branchbit on 25/12/21.
//

import Foundation
protocol GSContingencyViewProtocol: AnyObject {
    
}

protocol GSContingencyInteractorProtocol: AnyObject {
    
}

protocol GSContingencyPresenterProtocol: AnyObject {
    
}

protocol GSContingencyRouterProtocol: AnyObject {
    
}
