//
//  GSDatabasesProtocols.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
protocol GSDatabasesViewProtocol: AnyObject {
    
}

protocol GSDatabasesInteractorProtocol: AnyObject {
    
}

protocol GSDatabasesPresenterProtocol: AnyObject {
    
}

protocol GSDatabasesRouterProtocol: AnyObject {
    
}
