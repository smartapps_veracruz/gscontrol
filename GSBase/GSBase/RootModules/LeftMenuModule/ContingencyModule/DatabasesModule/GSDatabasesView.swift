//
//  GSDatabasesView.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit
class GSDatabasesView: UIViewController {
    var presenter: GSDatabasesPresenterProtocol?
    private var ui: GSDatabasesViewUI?
    
    
    override func loadView() {
        ui = GSDatabasesViewUI(
            delegate: self
        )
        view = ui
    }
    
}

extension GSDatabasesView: GSDatabasesViewProtocol {
    
}

extension GSDatabasesView: GSDatabasesViewUIDelegate {
    
}
