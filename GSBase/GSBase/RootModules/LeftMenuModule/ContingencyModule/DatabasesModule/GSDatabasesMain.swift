//
//  GSDatabasesMain.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit

open class GSDatabasesMain{
    public static func createModule() -> UIViewController {
        let viewController: GSDatabasesView? = GSDatabasesView()
        if let view = viewController {
            let presenter = GSDatabasesPresenter()
            let router = GSDatabasesRouter()
            let interactor = GSDatabasesInteractor()
            
            
            view.presenter = presenter
            
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = router
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
