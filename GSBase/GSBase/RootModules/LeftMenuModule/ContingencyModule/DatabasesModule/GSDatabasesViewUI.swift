//
//  GSDatabasesViewUI.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

internal enum GSDatabasesTypes: Int {
    case FFMDB
    case BRMDB
    case U31DB
    case CasandraDB
    case IPTVDB
    
    case DBInstanceSOA
    case DBInstanceBRM
    case DBInstanceFFM
    case DBInstanceIPTV
    case DBInstanceTR
    case DBInstanceROU
    case DBInstancePayments
    
    public func getTitle() -> String {
        switch self {
        case .FFMDB:
            return "Switch over Base de Datos FFM"
        case .BRMDB:
            return "Switch over Base de Datos BRM"
        case .U31DB:
            return "Switch over Base de Datos U31"
        case .CasandraDB:
            return "Switch over Base de Datos Casandra"
        case .IPTVDB:
            return "Switch over Base de Datos IPTV"
        case .DBInstanceSOA:
            return "SOA"
        case .DBInstanceBRM:
            return "BRM"
        case .DBInstanceFFM:
            return "FFM"
        case .DBInstanceIPTV:
            return "IPTV"
        case .DBInstanceTR:
            return "TR"
        case .DBInstanceROU:
            return "ROU"
        case .DBInstancePayments:
            return "Pagos"
        }
    }
}

protocol GSDatabasesViewUIDelegate {
    
}

class GSDatabasesViewUI: UIView{
    var delegate: GSDatabasesViewUIDelegate?
    
    
    private var mainSectionCells: [GSDatabasesTypes] = [.FFMDB,
                                                        .BRMDB,
                                                        .U31DB,
                                                        .CasandraDB,
                                                        .IPTVDB]
    
    private var dbInstanceCells: [GSDatabasesTypes] = [.DBInstanceSOA,
                                                       .DBInstanceBRM,
                                                       .DBInstanceFFM,
                                                       .DBInstanceIPTV,
                                                       .DBInstanceTR,
                                                       .DBInstanceROU,
                                                       .DBInstancePayments]
    
    private let cellsPerRow: CGFloat = 3
    
    lazy var administrativesContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        return view
    }()
    
    private lazy var mainCollection: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.collectionViewLayout = UICollectionViewFlowLayout.init()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(GSContingencyCollectionCell.self,
                                forCellWithReuseIdentifier: GSContingencyCollectionCell.identifier)
        collectionView.tag = 0
        return collectionView
    }()
    
    private lazy var dbInstanceTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Reinicio por instancia de Bases de Datos"
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.font = .Montserrat_Semibold_16
        return label
    }()
    
    private lazy var dbInstanceLabelSeparatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.contingencyCellLabelColor
        return view
    }()
    
    private lazy var dbInstanceCollection: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.collectionViewLayout = UICollectionViewFlowLayout.init()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(GSContingencyCollectionCell.self,
                                forCellWithReuseIdentifier: GSContingencyCollectionCell.identifier)
        collectionView.tag = 1
        return collectionView
    }()
    
    
    public convenience init(
        delegate: GSDatabasesViewUIDelegate){
            self.init()
            self.delegate = delegate
            
            setUI()
            setConstraints()
            
            self.mainCollection.reloadData()
            self.dbInstanceCollection.reloadData()
        }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUI(){
        self.addSubview(self.administrativesContainerView)
        self.administrativesContainerView.addSubview(self.mainCollection)
        self.administrativesContainerView.addSubview(self.dbInstanceCollection)
        
        self.administrativesContainerView.addSubview(self.dbInstanceTitleLabel)
        self.administrativesContainerView.addSubview(self.dbInstanceLabelSeparatorView)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.administrativesContainerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 50),
            self.administrativesContainerView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.administrativesContainerView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.administrativesContainerView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.mainCollection.topAnchor.constraint(equalTo: self.administrativesContainerView.topAnchor),
            self.mainCollection.leadingAnchor.constraint(equalTo: self.administrativesContainerView.leadingAnchor, constant: 50),
            self.mainCollection.trailingAnchor.constraint(equalTo: self.administrativesContainerView.trailingAnchor, constant: -50),
            self.mainCollection.bottomAnchor.constraint(equalTo: self.administrativesContainerView.centerYAnchor, constant: -120),
            
            self.dbInstanceTitleLabel.topAnchor.constraint(equalTo: self.mainCollection.bottomAnchor, constant: 50),
            self.dbInstanceTitleLabel.leadingAnchor.constraint(equalTo: self.administrativesContainerView.leadingAnchor, constant: 20),
            
            self.dbInstanceLabelSeparatorView.leadingAnchor.constraint(equalTo: self.dbInstanceTitleLabel.trailingAnchor, constant: 20),
            self.dbInstanceLabelSeparatorView.trailingAnchor.constraint(equalTo: self.administrativesContainerView.trailingAnchor, constant: -20),
            self.dbInstanceLabelSeparatorView.centerYAnchor.constraint(equalTo: self.dbInstanceTitleLabel.centerYAnchor),
            self.dbInstanceLabelSeparatorView.heightAnchor.constraint(equalToConstant: 0.5),
            
            self.dbInstanceCollection.topAnchor.constraint(equalTo: self.dbInstanceTitleLabel.bottomAnchor, constant: 30),
            self.dbInstanceCollection.leadingAnchor.constraint(equalTo: self.administrativesContainerView.leadingAnchor, constant: 50),
            self.dbInstanceCollection.trailingAnchor.constraint(equalTo: self.administrativesContainerView.trailingAnchor, constant: -50),
            self.dbInstanceCollection.bottomAnchor.constraint(equalTo: self.administrativesContainerView.bottomAnchor),
        ])
    }
}




extension GSDatabasesViewUI: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = 40 * (self.cellsPerRow + 1)
        let availableWidth = self.mainCollection.frame.width - paddingSpace
        let widthPerItem = availableWidth / self.cellsPerRow
        return  CGSize(width: widthPerItem,  height: 40)
    }
    
    func collectionView( _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 40
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 0, bottom: 0.0, right: 0)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 0 ? self.mainSectionCells.count : self.dbInstanceCells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GSContingencyCollectionCell.identifier, for: indexPath) as! GSContingencyCollectionCell
        
        let cellType: GSDatabasesTypes?
        if collectionView.tag == 0 {
            cellType = self.mainSectionCells[indexPath.row]
        }else {
            cellType = self.dbInstanceCells[indexPath.row]
        }
        if let cellType = cellType {
            cell.isEnabled = false
            cell.titleString = cellType.getTitle()
            cell.enumID = cellType.rawValue
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cellType: GSDatabasesTypes?
        if collectionView.tag == 0 {
            cellType = self.mainSectionCells[indexPath.row]
        }else {
            cellType = self.dbInstanceCells[indexPath.row]
        }
        if let cellType = cellType {
            print(cellType.getTitle())
        }
    }
}

