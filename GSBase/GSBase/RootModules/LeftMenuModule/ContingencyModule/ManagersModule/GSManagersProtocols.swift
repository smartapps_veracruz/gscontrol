//
//  GSManagersProtocols.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
protocol GSManagersViewProtocol: AnyObject {
    
}

protocol GSManagersInteractorProtocol: AnyObject {
    
}

protocol GSManagersPresenterProtocol: AnyObject {
    
}

protocol GSManagersRouterProtocol: AnyObject {
    
}
