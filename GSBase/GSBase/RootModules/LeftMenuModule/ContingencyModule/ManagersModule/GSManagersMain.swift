//
//  GSManagersMain.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit

open class GSManagersMain{
    public static func createModule() -> UIViewController {
        let viewController: GSManagersView? = GSManagersView()
        if let view = viewController {
            let presenter = GSManagersPresenter()
            let router = GSManagersRouter()
            let interactor = GSManagersInteractor()
            
            
            view.presenter = presenter
            
            presenter.view = view
            presenter.interactor = interactor
            presenter.router = router
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
