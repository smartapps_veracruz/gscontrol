//
//  GSManagersView.swift
//  GSBase
//
//  Created by Branchbit on 29/12/21.
//

import Foundation
import UIKit
class GSManagersView: UIViewController {
    var presenter: GSManagersPresenterProtocol?
    private var ui: GSManagersViewUI?
    
    
    override func loadView() {
        ui = GSManagersViewUI(
            delegate: self
        )
        view = ui
    }
    
}

extension GSManagersView: GSManagersViewProtocol {
    
}

extension GSManagersView: GSManagersViewUIDelegate {
    
}
