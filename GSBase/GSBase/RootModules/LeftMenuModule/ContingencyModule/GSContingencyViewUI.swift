//
//  GSContingencyViewUI.swift
//  GSBase
//
//  Created by Branchbit on 25/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

private enum GSContingencyPages: Int {
    case IPTV
    case Administratives
    case Applicatives
    case Managers
    case Databases
    
    internal func getTitle() -> String {
        switch self {
        case .IPTV:
            return "IPTV"
        case .Administratives:
            return "Administrativos"
        case .Applicatives:
            return "Aplicativos"
        case .Managers:
            return "Gestores"
        case .Databases:
            return "Bases de datos"
        }
    }
}

protocol GSContingencyViewUIDelegate {
    func notifyDismiss()
    func notifyGetParentVC() -> UIViewController
}

class GSContingencyViewUI: UIView{
    var delegate: GSContingencyViewUIDelegate?
    private var contingencyPagerViewControllers: [UIViewController] = []
    private var currentActivePage: Int = 0
    
    private var highLightViewCenterXConstraint = NSLayoutConstraint()
    private var highLightViewWidthConstraint = NSLayoutConstraint()
    
    private var leftSeparatorTrailingConstraint = NSLayoutConstraint()
    private var rightSeparatorLeadingConstraint = NSLayoutConstraint()
    
    private var pages: [GSContingencyPages] = [.IPTV, .Administratives, .Applicatives, .Managers, .Databases]
    
    lazy var mainView: GSGenericFrontView = {
        let view = GSGenericFrontView(delegate: self, viewType: .contingency)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var pagerViewController: UIPageViewController = {
       let pager = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.delegate?.notifyGetParentVC().addChild(pager)
        pager.view.translatesAutoresizingMaskIntoConstraints = false
        return pager
    }()
    
    lazy var pageButtonContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.spacing = 20
        stack.distribution = .equalCentering
        
        view.addSubview(stack)
        
        for page in self.pages {
            let button = UIView()
            button.translatesAutoresizingMaskIntoConstraints = false
            button.backgroundColor = .clear
            button.tag = page.rawValue
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.onPageTapped(_:)))
            tapGesture.numberOfTapsRequired = 1
            button.addGestureRecognizer(tapGesture)
            button.isUserInteractionEnabled = true
            
            let buttonTitle = UILabel()
            buttonTitle.translatesAutoresizingMaskIntoConstraints = false
            buttonTitle.font = .Montserrat_Regular_16
            buttonTitle.textColor = GSColorsManager.labelLightColor
            buttonTitle.text = page.getTitle()
            buttonTitle.numberOfLines = 1
            buttonTitle.tag = page.rawValue
            
            button.addSubview(buttonTitle)
            
            NSLayoutConstraint.activate([
                buttonTitle.topAnchor.constraint(equalTo: button.topAnchor, constant: 5),
                buttonTitle.leadingAnchor.constraint(equalTo: button.leadingAnchor, constant: 20),
                buttonTitle.trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: -20),
                buttonTitle.bottomAnchor.constraint(equalTo: button.bottomAnchor, constant: -5),
            ])
            
            stack.addArrangedSubview(button)
        }
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: view.topAnchor),
            stack.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            stack.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            stack.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])

        return view
    }()
    
    lazy var highLightView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.contingencyViewBorderColor.withAlphaComponent(0.15)
        
        let topBorder = UIView()
        topBorder.translatesAutoresizingMaskIntoConstraints = false
        topBorder.backgroundColor = GSColorsManager.contingencyViewBorderColor
        
        let leftBorder = UIView()
        leftBorder.translatesAutoresizingMaskIntoConstraints = false
        leftBorder.backgroundColor = GSColorsManager.contingencyViewBorderColor
        
        let rightBorder = UIView()
        rightBorder.translatesAutoresizingMaskIntoConstraints = false
        rightBorder.backgroundColor = GSColorsManager.contingencyViewBorderColor
        
        let bottomBorder = UIView()
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        bottomBorder.backgroundColor = GSColorsManager.contingencyLabelColor
        
        view.addSubview(topBorder)
        view.addSubview(leftBorder)
        view.addSubview(rightBorder)
        view.addSubview(bottomBorder)
        
        NSLayoutConstraint.activate([
            topBorder.leadingAnchor.constraint(equalTo: leftBorder.leadingAnchor),
            topBorder.bottomAnchor.constraint(equalTo: view.topAnchor, constant: -2),
            topBorder.trailingAnchor.constraint(equalTo: rightBorder.trailingAnchor),
            topBorder.heightAnchor.constraint(equalToConstant: 1),
        
            leftBorder.topAnchor.constraint(equalTo: topBorder.bottomAnchor),
            leftBorder.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5),
            leftBorder.trailingAnchor.constraint(equalTo: view.leadingAnchor, constant: -2),
            leftBorder.widthAnchor.constraint(equalToConstant: 1),
            
            rightBorder.topAnchor.constraint(equalTo: topBorder.bottomAnchor),
            rightBorder.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5),
            rightBorder.leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: 2),
            rightBorder.widthAnchor.constraint(equalToConstant: 1),
            
            bottomBorder.topAnchor.constraint(equalTo: view.bottomAnchor),
            bottomBorder.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            bottomBorder.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            bottomBorder.heightAnchor.constraint(equalToConstant: 4),
        ])
        
        
        return view
    }()
    
    lazy var leftSeparator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.contingencyViewBorderColor
        return view
    }()
    
    lazy var rightSeparator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.contingencyViewBorderColor
        return view
    }()
    
    lazy var pageContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    public convenience init(
        delegate: GSContingencyViewUIDelegate){
            self.init()
            self.delegate = delegate
            
            self.setUI()
            self.setConstraints()
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                self.buildControllerOptions(pages: self.pages)
            }
        }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUI(){
        self.addSubview(self.mainView)
        self.mainView.contentView.addSubview(self.pageButtonContainerView)
        self.mainView.contentView.addSubview(self.highLightView)
        self.mainView.contentView.addSubview(self.leftSeparator)
        self.mainView.contentView.addSubview(self.rightSeparator)
        self.mainView.contentView.addSubview(self.pageContainerView)
        self.pageContainerView.addSubview(self.pagerViewController.view)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.mainView.topAnchor.constraint(equalTo: self.topAnchor),
            self.mainView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.mainView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.mainView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.pageButtonContainerView.topAnchor.constraint(equalTo: self.mainView.contentView.topAnchor, constant: 40),
            self.pageButtonContainerView.leadingAnchor.constraint(equalTo: self.mainView.contentView.leadingAnchor, constant: 20),
            self.pageButtonContainerView.trailingAnchor.constraint(equalTo: self.mainView.contentView.trailingAnchor, constant: -20),
//            self.pageButtonContainerScroll.heightAnchor.constraint(equalToConstant: 30),
            
            self.highLightView.topAnchor.constraint(equalTo: self.pageButtonContainerView.topAnchor),
            self.highLightView.bottomAnchor.constraint(equalTo: self.pageButtonContainerView.bottomAnchor),
            
            self.leftSeparator.topAnchor.constraint(equalTo: self.pageButtonContainerView.bottomAnchor, constant: -5),
            self.leftSeparator.leadingAnchor.constraint(equalTo: self.mainView.contentView.leadingAnchor),
            self.leftSeparator.heightAnchor.constraint(equalToConstant: 1),
            
            self.rightSeparator.topAnchor.constraint(equalTo: self.pageButtonContainerView.bottomAnchor, constant: -5),
            self.rightSeparator.trailingAnchor.constraint(equalTo: self.mainView.contentView.trailingAnchor),
            self.rightSeparator.heightAnchor.constraint(equalToConstant: 1),
            
            self.pageContainerView.topAnchor.constraint(equalTo: self.leftSeparator.bottomAnchor, constant: 20),
            self.pageContainerView.leadingAnchor.constraint(equalTo: self.mainView.contentView.leadingAnchor),
            self.pageContainerView.trailingAnchor.constraint(equalTo: self.mainView.contentView.trailingAnchor),
            self.pageContainerView.bottomAnchor.constraint(equalTo: self.mainView.contentView.bottomAnchor),
            
            self.pagerViewController.view.topAnchor.constraint(equalTo: self.pageContainerView.topAnchor),
            self.pagerViewController.view.leadingAnchor.constraint(equalTo: self.pageContainerView.leadingAnchor),
            self.pagerViewController.view.trailingAnchor.constraint(equalTo: self.pageContainerView.trailingAnchor),
            self.pagerViewController.view.bottomAnchor.constraint(equalTo: self.pageContainerView.bottomAnchor),
        ])
    }
    
    private func buildControllerOptions(pages: [GSContingencyPages]){
        for vController in pages {
            switch vController {
            case .IPTV:
                self.contingencyPagerViewControllers.append(GSIPTVMain.createModule())
                break
            case .Administratives:
                self.contingencyPagerViewControllers.append(GSAdministrativesMain.createModule())
                break
            case .Applicatives:
                self.contingencyPagerViewControllers.append(GSApplicativesMain.createModule())
                break
            case .Managers:
                self.contingencyPagerViewControllers.append(GSManagersMain.createModule())
                break
            case .Databases:
                self.contingencyPagerViewControllers.append(GSDatabasesMain.createModule())
                break
            }
        }
        self.navigateToPage(page: 2, animate: false)
    }
    
    @objc func onPageTapped(_ sender: UITapGestureRecognizer){
        if let view = sender.view {
            self.navigateToPage(page:  view.tag)
        }
    }
    
    private func highLightSelectedPage(page: Int){
        for subview in self.pageButtonContainerView.subviews {
            if subview is UIStackView, let stack = subview as? UIStackView {
                for stackSubview in stack.subviews {
                    for buttonSubview in stackSubview.subviews {
                        if buttonSubview is UILabel, let label = buttonSubview as? UILabel {
                            if label.tag != page {
                                label.textColor = GSColorsManager.labelLightColor
                            }else {
                                self.highLightViewWidthConstraint.isActive = false
                                self.highLightViewCenterXConstraint.isActive = false
                                
                                self.leftSeparatorTrailingConstraint.isActive = false
                                self.rightSeparatorLeadingConstraint.isActive = false
                                
                                self.highLightViewWidthConstraint = self.highLightView.widthAnchor.constraint(equalTo: stackSubview.widthAnchor)
                                self.highLightViewCenterXConstraint = self.highLightView.centerXAnchor.constraint(equalTo: stackSubview.centerXAnchor)
                                
                                self.leftSeparatorTrailingConstraint = self.leftSeparator.trailingAnchor.constraint(equalTo: self.highLightView.leadingAnchor, constant: -2)
                                self.rightSeparatorLeadingConstraint = self.rightSeparator.leadingAnchor.constraint(equalTo: self.highLightView.trailingAnchor, constant: 2)
                                
                                UIView.transition(with: label, duration: 0.5, options: .transitionCrossDissolve) {
                                    label.textColor = GSColorsManager.contingencyLabelColor
                                }
                                
                                UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseInOut, .curveEaseOut]) {
                                    NSLayoutConstraint.activate([
                                        self.highLightViewWidthConstraint,
                                        self.highLightViewCenterXConstraint,
                                        
                                        self.leftSeparatorTrailingConstraint,
                                        self.rightSeparatorLeadingConstraint
                                    ])
                                    self.layoutIfNeeded()
                                }
                            }
                        }
                    }
                }
                return
            }
        }
    }
    
    private func navigateToPage(page: Int, animate: Bool = true){
        self.pagerViewController.setViewControllers([self.contingencyPagerViewControllers[page]], direction: self.currentActivePage > page ? .reverse : .forward, animated: animate, completion: nil)
        self.highLightSelectedPage(page: page)
        self.currentActivePage = page
    }
}

extension GSContingencyViewUI: GSGenericFrontProtocol {
    func notifyDismiss() {
        self.delegate?.notifyDismiss()
    }
}
