//
//  GSNotificationsProtocols.swift
//  GSBase
//
//  Created by Branchbit on 22/12/21.
//

import Foundation
protocol GSNotificationsViewProtocol: AnyObject {
    
}

protocol GSNotificationsInteractorProtocol: AnyObject {
    
}

protocol GSNotificationsPresenterProtocol: AnyObject {
    
}
