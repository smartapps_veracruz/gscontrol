//
//  GSNotificationsView.swift
//  GSBase
//
//  Created by Branchbit on 22/12/21.
//

import Foundation
import UIKit
class GSNotificationsView: UIViewController {
    var presenter: GSNotificationsPresenterProtocol?
    private var ui: GSNotificationsViewUI?
    
    
    override func loadView() {
        ui = GSNotificationsViewUI(
            delegate: self
        )
        view = ui
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension GSNotificationsView: GSNotificationsViewProtocol {
    
}

extension GSNotificationsView: GSNotificationsViewUIDelegate {
    func notifyDismiss() {
        self.dismiss(animated: false, completion: nil)
    }
}
