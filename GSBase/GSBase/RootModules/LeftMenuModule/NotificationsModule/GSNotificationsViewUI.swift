//
//  GSNotificationsViewUI.swift
//  GSBase
//
//  Created by Branchbit on 22/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

protocol GSNotificationsViewUIDelegate {
    func notifyDismiss()
}

class GSNotificationsViewUI: UIView{
    var delegate: GSNotificationsViewUIDelegate?
    
    var pickerViewElements: [String] = ["checklogic",
                                        "correo",
                                        "bd_con_afect",
                                        "bd_sin_afect",
                                        "Dash_Web_Concurrencia_BD",
                                        "dns",
                                        "GSS_FFM",
                                        "GSS_SF",
                                        "GSA_Lite",
                                        "infra_con_afect",
                                        "infra_sin_afect",
                                        "ISP",
                                        "llave_maestra",
                                        "na109_paquetes_con_afect",
                                        "na109_paquetes_sin_afect",
                                        "manager",
                                        "micro_cloud",
                                        "micro_onpremise",
                                        "puntobaz",
                                        "Quadient",
                                        "swichover_u2000Qro",
                                        "swichover_u2000Tul",
                                        "SAP",
                                        "oxxo"]
    
    lazy var pickerContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var pickerView: UIPickerView = {
       let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        picker.backgroundColor = .clear
        picker.dataSource = self
        picker.delegate = self
        picker.clipsToBounds = false
        
        let selectorView = UIView()
        selectorView.translatesAutoresizingMaskIntoConstraints = false
        selectorView.layer.borderWidth = 1
        selectorView.layer.cornerRadius = 19
        selectorView.layer.borderColor = #colorLiteral(red: 0.3529411765, green: 0.5058823529, blue: 0.6862745098, alpha: 1)
        
        picker.addSubview(selectorView)
        
        NSLayoutConstraint.activate([
            selectorView.trailingAnchor.constraint(equalTo: picker.trailingAnchor),
            selectorView.heightAnchor.constraint(equalToConstant: 70),
            selectorView.leadingAnchor.constraint(equalTo: picker.leadingAnchor, constant: -20),
            selectorView.centerYAnchor.constraint(equalTo: picker.centerYAnchor),
        ])
        
        return picker
    }()
    
    lazy var mainView: GSGenericFrontView = {
        let view = GSGenericFrontView(delegate: self, viewType: .notification)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var sendButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = #colorLiteral(red: 0, green: 0.631372549, blue: 0.8470588235, alpha: 1)
        button.setTitle("ENVIAR", for: UIControl.State.normal)
        button.setTitleColor(.black, for: UIControl.State.normal)
        button.titleLabel?.font = .Montserrat_Semibold_14
        button.addTarget(self, action: #selector(onClickAction), for: UIControl.Event.touchUpInside)
        button.layer.cornerRadius = 17
        return button
    }()
    
    
    lazy var notificationStatusContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.isHidden = true
        view.alpha = 0
        return view
    }()
    
    lazy var notificationStatusIcon: UIImageView = {
       let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.tintColor = GSColorsManager.successColor
        view.image = UIImage(named: "success_ic", in: .local_gs_utils, compatibleWith: nil)
        return view
    }()
    
    lazy var notificationStatusTitleLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.text = "¡Notificación enviada con éxito!"
        label.textColor = GSColorsManager.successColor
        label.font = .Montserrat_Medium_20
        return label
    }()
    
    lazy var notificationStatusEmailLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.text = ""
        label.textColor = GSColorsManager.labelLightColor
        label.font = .Montserrat_Medium_20
        return label
    }()

    
    public convenience init(
        delegate: GSNotificationsViewUIDelegate){
            self.init()
            self.delegate = delegate
            
            setUI()
            setConstraints()
        }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUI(){
        self.addSubview(mainView)
        
        self.mainView.contentView.addSubview(self.pickerContainerView)
        self.pickerContainerView.addSubview(self.pickerView)
        self.pickerContainerView.addSubview(self.sendButton)
        
        self.mainView.contentView.addSubview(self.notificationStatusContainerView)
        self.notificationStatusContainerView.addSubview(self.notificationStatusIcon)
        self.notificationStatusContainerView.addSubview(self.notificationStatusTitleLabel)
        self.notificationStatusContainerView.addSubview(self.notificationStatusEmailLabel)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.mainView.topAnchor.constraint(equalTo: self.topAnchor),
            self.mainView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.mainView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.mainView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.pickerContainerView.topAnchor.constraint(equalTo: self.mainView.contentView.topAnchor),
            self.pickerContainerView.leadingAnchor.constraint(equalTo: self.mainView.contentView.leadingAnchor),
            self.pickerContainerView.trailingAnchor.constraint(equalTo: self.mainView.contentView.trailingAnchor),
            self.pickerContainerView.bottomAnchor.constraint(equalTo: self.mainView.contentView.bottomAnchor),
            
            self.pickerView.topAnchor.constraint(equalTo: self.pickerContainerView.topAnchor, constant: 0),
            self.pickerView.leadingAnchor.constraint(equalTo: self.pickerContainerView.leadingAnchor, constant: 140),
            self.pickerView.widthAnchor.constraint(equalToConstant: 540),
            self.pickerView.bottomAnchor.constraint(equalTo: self.pickerContainerView.bottomAnchor, constant: 0),
            
            self.sendButton.centerYAnchor.constraint(equalTo: self.pickerView.centerYAnchor),
            self.sendButton.leadingAnchor.constraint(equalTo: self.pickerView.trailingAnchor, constant: 35),
            self.sendButton.trailingAnchor.constraint(equalTo: self.pickerContainerView.trailingAnchor, constant: -80),
            self.sendButton.heightAnchor.constraint(equalToConstant: 40),
            
            
//            self.notificationStatusContainerView.topAnchor.constraint(equalTo: self.mainView.contentView.topAnchor),
            self.notificationStatusContainerView.leadingAnchor.constraint(equalTo: self.mainView.contentView.leadingAnchor),
            self.notificationStatusContainerView.trailingAnchor.constraint(equalTo: self.mainView.contentView.trailingAnchor),
            self.notificationStatusContainerView.bottomAnchor.constraint(equalTo: self.mainView.contentView.centerYAnchor),
            
            self.notificationStatusIcon.topAnchor.constraint(equalTo: self.notificationStatusContainerView.topAnchor),
            self.notificationStatusIcon.centerXAnchor.constraint(equalTo: self.notificationStatusContainerView.centerXAnchor),
            self.notificationStatusIcon.widthAnchor.constraint(equalToConstant: 55),
            self.notificationStatusIcon.heightAnchor.constraint(equalToConstant: 55),
            
            self.notificationStatusTitleLabel.topAnchor.constraint(equalTo: self.notificationStatusIcon.bottomAnchor, constant: 20),
            self.notificationStatusTitleLabel.leadingAnchor.constraint(equalTo: self.notificationStatusContainerView.leadingAnchor),
            self.notificationStatusTitleLabel.trailingAnchor.constraint(equalTo: self.notificationStatusContainerView.trailingAnchor),
            
            self.notificationStatusEmailLabel.topAnchor.constraint(equalTo: self.notificationStatusTitleLabel.bottomAnchor, constant: 20),
            self.notificationStatusEmailLabel.leadingAnchor.constraint(equalTo: self.notificationStatusContainerView.leadingAnchor),
            self.notificationStatusEmailLabel.trailingAnchor.constraint(equalTo: self.notificationStatusContainerView.trailingAnchor),
            self.notificationStatusEmailLabel.bottomAnchor.constraint(equalTo: self.notificationStatusContainerView.bottomAnchor),
        ])
    }
    
    
    @objc func onClickAction(){
        let selectedService = self.pickerViewElements[self.pickerView.selectedRow(inComponent: 0)]
        print(selectedService)
        notificationStatusEmailLabel.text = selectedService
        UIView.animate(withDuration: 0.2) {
            self.pickerContainerView.alpha = 0
            self.layoutIfNeeded()
        } completion: { _ in
            self.pickerContainerView.isHidden = true
            self.notificationStatusContainerView.isHidden = false
            UIView.animate(withDuration: 0.2) {
                self.notificationStatusContainerView.alpha = 1
                self.layoutIfNeeded()
            } completion: { _ in
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    self.mainView.viewDismissAnimation()
                }
            }
        }
    }
}
extension GSNotificationsViewUI: GSGenericFrontProtocol {
    func notifyDismiss() {
        self.delegate?.notifyDismiss()
    }
}

extension GSNotificationsViewUI: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewElements.count
    }
}

extension GSNotificationsViewUI: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        pickerView.subviews.forEach { element in
            element.isHidden = (element.backgroundColor != nil)
        }
        return 80
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var elementLabel: GSLabelPadding? = (view as? GSLabelPadding)

        if elementLabel == nil {
            elementLabel = GSLabelPadding(withInsets: 0, 0 , 20, 20)
        }
        if let label = elementLabel {
            label.textAlignment = .left
            label.text = pickerViewElements[row]
            label.font = .Montserrat_Regular_16
            label.textColor = GSColorsManager.labelLightColor

            return label
        }

        return UILabel()
    }
    
}
