//
//  GSNotificationsMain.swift
//  GSBase
//
//  Created by Branchbit on 22/12/21.
//

import Foundation
import UIKit

open class GSNotificationsMain{
    public static func createModule() -> UIViewController {
        let viewController: GSNotificationsView? = GSNotificationsView()
        if let view = viewController {
            let presenter = GSNotificationsPresenter()
            let interactor = GSNotificationsInteractor()
            
            view.presenter = presenter
            
            presenter.view = view
            presenter.interactor = interactor
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
