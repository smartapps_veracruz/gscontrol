//
//  GSLeftMenuContracts.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

//MARK: View -
/*
 Should replace "class" with "BaseViewProtocol" if available;
 & that will allow the View to act as a UIViewController;
 & Implement common view functions.
 */
/// GSLeftMenu Module View Protocol
protocol GSLeftMenuViewProtocol: AnyObject {
    func notifyGetContext()->UIViewController?
}

//MARK: Interactor -
/// GSLeftMenu Module Interactor Protocol
protocol GSLeftMenuInteractorProtocol: AnyObject {
}

//MARK: Presenter -
/// GSLeftMenu Module Presenter Protocol
protocol GSLeftMenuPresenterProtocol: AnyObject {
    func requestRootModule()
    func requestFacetecAuthModule()
    func requestContingencyModule()
    func requestNotificationsModule()
    func requestCallsModule()
}

//MARK: Router (aka: Wireframe) -
/// GSLeftMenu Module Router Protocol
protocol GSLeftMenuRouterProtocol: AnyObject {
    func navigateToToRoot()
    func navigateToFacetecAuthModule(view: UIViewController)
    func navigateToContingencyModule(view: UIViewController)
    func navigateToNotificationsModule(view: UIViewController)
    func navigateToCallsModule(view: UIViewController)
}
