//
//  GSCallingProtocols.swift
//  GSBase
//
//  Created by Branchbit on 27/12/21.
//

import Foundation
protocol GSCallingViewProtocol: AnyObject {
    
}

protocol GSCallingInteractorProtocol: AnyObject {
    
}

protocol GSCallingPresenterProtocol: AnyObject {
    
}
