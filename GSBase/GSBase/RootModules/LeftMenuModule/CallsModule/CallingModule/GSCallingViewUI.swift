//
//  GSCallingViewUI.swift
//  GSBase
//
//  Created by Branchbit on 27/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

protocol GSCallingViewUIDelegate {
    func notifyDismiss()
}

class GSCallingViewUI: UIView{
    var delegate: GSCallingViewUIDelegate?
    
    private var contactsToCall: [GSContactEntity] = []
    private var serviceCalling: String = ""
    
    private var dummyTalkTimer: Timer?
    internal var dummyTalkTimerAux: Bool? = true
    
    lazy var mainView: GSGenericFrontView = {
        let view = GSGenericFrontView(delegate: self, viewType: .calling, showDarkBackground: true, showCloseButton: false)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var callingContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var serviceCallingLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Semibold_20
        label.text = self.serviceCalling
        label.textAlignment = .center
        label.textColor = GSColorsManager.labelStrongColor
        return label
    }()
    
    lazy var callStatusLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Regular_20
        label.text = "Realizando llamada …"
        label.textAlignment = .center
        label.textColor = GSColorsManager.labelStrongColor
        return label
    }()
    
    
    lazy var contactsInCallContainerView: GSCallingContactsListView = {
        let view = GSCallingContactsListView(contacts: self.contactsToCall)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var endCallButtonView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        view.layer.cornerRadius = 17
        view.layer.borderWidth = 1
        view.layer.borderColor = GSColorsManager.contingencyViewBorderColor.cgColor
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickAction))
        tapGesture.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGesture)
        view.isUserInteractionEnabled = true
        
        let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.image = UIImage(named: "callPhone_ic", in: .local_gs_utils, compatibleWith: nil)
        icon.contentMode = .scaleAspectFit
        icon.backgroundColor = .clear
        icon.tintColor = GSColorsManager.contingencyLabelColor
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "TERMINAR"
        label.font = .Montserrat_Semibold_14
        label.textColor = GSColorsManager.contingencyLabelColor
        label.numberOfLines = 1
        
        view.addSubview(icon)
        view.addSubview(label)
        
        NSLayoutConstraint.activate([
            icon.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            icon.centerYAnchor.constraint(equalTo: label.centerYAnchor),
            icon.widthAnchor.constraint(equalToConstant: 15),
            icon.heightAnchor.constraint(equalToConstant: 15),
            
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 8),
            label.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8),
        ])
        
        return view
    }()
    
    public convenience init(
        delegate: GSCallingViewUIDelegate,
        contactsToCall: [GSContactEntity],
        serviceCalling: String){
            self.init()
            self.delegate = delegate
            
            self.contactsToCall = contactsToCall
            self.serviceCalling = serviceCalling
            
            setUI()
            setConstraints()
            
            // Quitar esta línea para no simular charla
            self.dummyTalk()
        }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUI(){
        self.addSubview(self.mainView)
        self.mainView.contentView.addSubview(self.callingContainerView)
        
        self.callingContainerView.addSubview(self.contactsInCallContainerView)
        self.callingContainerView.addSubview(self.serviceCallingLabel)
        self.callingContainerView.addSubview(self.callStatusLabel)
        self.callingContainerView.addSubview(self.endCallButtonView)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.mainView.topAnchor.constraint(equalTo: self.topAnchor),
            self.mainView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.mainView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.mainView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.callingContainerView.topAnchor.constraint(equalTo: self.mainView.contentView.topAnchor),
            self.callingContainerView.leadingAnchor.constraint(equalTo: self.mainView.contentView.leadingAnchor),
            self.callingContainerView.trailingAnchor.constraint(equalTo: self.mainView.contentView.trailingAnchor),
            self.callingContainerView.bottomAnchor.constraint(equalTo: self.mainView.contentView.bottomAnchor),
            
            self.serviceCallingLabel.topAnchor.constraint(equalTo: self.callingContainerView.topAnchor, constant: 30),
            self.serviceCallingLabel.leadingAnchor.constraint(equalTo: self.callingContainerView.leadingAnchor, constant: 250),
            self.serviceCallingLabel.trailingAnchor.constraint(equalTo: self.callingContainerView.trailingAnchor, constant: -250),
            
            self.callStatusLabel.topAnchor.constraint(equalTo: self.serviceCallingLabel.bottomAnchor, constant: 30),
            self.callStatusLabel.leadingAnchor.constraint(equalTo: self.callingContainerView.leadingAnchor, constant: 250),
            self.callStatusLabel.trailingAnchor.constraint(equalTo: self.callingContainerView.trailingAnchor, constant: -250),
            
            self.contactsInCallContainerView.topAnchor.constraint(equalTo: self.callStatusLabel.bottomAnchor, constant: 30),
            self.contactsInCallContainerView.leadingAnchor.constraint(equalTo: self.callingContainerView.leadingAnchor, constant: 100),
            self.contactsInCallContainerView.trailingAnchor.constraint(equalTo: self.callingContainerView.trailingAnchor, constant: -100),
            self.contactsInCallContainerView.bottomAnchor.constraint(equalTo: self.endCallButtonView.topAnchor, constant: -40),
            
            self.endCallButtonView.centerXAnchor.constraint(equalTo: self.callingContainerView.centerXAnchor),
            self.endCallButtonView.bottomAnchor.constraint(equalTo: self.callingContainerView.bottomAnchor, constant: -50),
        ])
    }
    
    @objc func onClickAction(){
        self.endCall()
    }
    
    func endCall(){
        self.contactsInCallContainerView.isUserInteractionEnabled = false
        self.contactsInCallContainerView.setTalking(contacts: [])
        if let timer = self.dummyTalkTimer {
            timer.invalidate()
        }
        UIView.animate(withDuration: 0.2) {
            self.endCallButtonView.alpha = 0.4
            self.contactsInCallContainerView.alpha = 0
            self.callStatusLabel.text = "Llamada terminada"
        } completion: { _ in
            self.mainView.viewDismissAnimation()
        }
    }
    
    func dummyTalk(){
        var auxRandomArr: [GSContactEntity] = []
         Timer.scheduledTimer(withTimeInterval: 2, repeats: true){ timer in
             guard let _ = self.dummyTalkTimerAux else {
                 timer.invalidate()
                 return
             }
            self.dummyTalkTimer = timer
            auxRandomArr.removeAll()
            for _ in 0..<self.contactsToCall.count/2 {
                if let auxRand = self.contactsToCall.randomElement() {
                    auxRandomArr.append(auxRand)
                }
            }
            self.contactsInCallContainerView.setTalking(contacts: auxRandomArr)
        }
    }
}


extension GSCallingViewUI: GSGenericFrontProtocol {
    func notifyDismiss() {
        self.delegate?.notifyDismiss()
    }
}
