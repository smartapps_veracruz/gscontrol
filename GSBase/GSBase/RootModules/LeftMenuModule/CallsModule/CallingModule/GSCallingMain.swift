//
//  GSCallingMain.swift
//  GSBase
//
//  Created by Branchbit on 27/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

open class GSCallingMain{
    public static func createModule(contactsToCall: [GSContactEntity], serviceCalling: String) -> UIViewController {
        let viewController: GSCallingView? = GSCallingView()
        if let view = viewController {
            let presenter = GSCallingPresenter()
            let interactor = GSCallingInteractor()
            
            view.presenter = presenter
            view.contactsToCall = contactsToCall
            view.serviceCalling = serviceCalling
            
            presenter.view = view
            presenter.interactor = interactor
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
