//
//  GSCallingView.swift
//  GSBase
//
//  Created by Branchbit on 27/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

class GSCallingView: UIViewController {
    var presenter: GSCallingPresenterProtocol?
    private var ui: GSCallingViewUI?
    internal var contactsToCall: [GSContactEntity] = []
    internal var serviceCalling: String = ""
    
    
    override func loadView() {
        ui = GSCallingViewUI(
            delegate: self,
            contactsToCall: self.contactsToCall,
            serviceCalling: self.serviceCalling
        )
        view = ui
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension GSCallingView: GSCallingViewProtocol {
    
}

extension GSCallingView: GSCallingViewUIDelegate {
    func notifyDismiss() {
        self.ui?.dummyTalkTimerAux = nil
        self.dismiss(animated: false, completion: nil)
    }
}
