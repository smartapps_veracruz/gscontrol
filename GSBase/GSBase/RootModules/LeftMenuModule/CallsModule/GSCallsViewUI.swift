//
//  GSCallsViewUI.swift
//  GSBase
//
//  Created by Branchbit on 25/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

protocol GSCallsViewUIDelegate {
    func notifyDismiss()
    func notifyCall(contactsList: [GSContactEntity], selectedService: String)
}

class GSCallsViewUI: UIView{
    var delegate: GSCallsViewUIDelegate?
    
    lazy var mainView: GSGenericFrontView = {
        let view = GSGenericFrontView(delegate: self, viewType: .call)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var pickerViewElements: [String] = ["Activaciones", "Salesforce", "U2000"]
    
    var contactList: [GSContactEntity] = [GSContactEntity(name: "Raúl Cruz Carreto",
                                                          phoneNumber: "3894715978"),
                                          GSContactEntity(name: "Fernando Campos",
                                                          phoneNumber: "3824112978"),
                                          GSContactEntity(name: "Gaston Caramuro",
                                                          phoneNumber: "3124612978"),
                                          GSContactEntity(name: "Raúl Cruz Carreto 2",
                                                          phoneNumber: "3895712058"),
                                          GSContactEntity(name: "Fernando Campos 2",
                                                          phoneNumber: "3898712078"),
                                          GSContactEntity(name: "Gaston Caramuro 2",
                                                          phoneNumber: "3894012478"),
                                          GSContactEntity(name: "Raúl Cruz Carreto 3",
                                                          phoneNumber: "3885471190"),
                                          GSContactEntity(name: "Fernando Campos 3",
                                                          phoneNumber: "1894754278"),
                                          GSContactEntity(name: "Gaston Caramuro 3",
                                                          phoneNumber: "8512712078"),]
    
    lazy var pickerContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var pickerView: UIPickerView = {
       let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        picker.backgroundColor = .clear
        picker.dataSource = self
        picker.delegate = self
        picker.clipsToBounds = false
        
        let selectorView = UIView()
        selectorView.translatesAutoresizingMaskIntoConstraints = false
        selectorView.layer.borderWidth = 1
        selectorView.layer.cornerRadius = 19
        selectorView.layer.borderColor = #colorLiteral(red: 0.3529411765, green: 0.5058823529, blue: 0.6862745098, alpha: 1)
        
        picker.addSubview(selectorView)
        
        NSLayoutConstraint.activate([
            selectorView.trailingAnchor.constraint(equalTo: picker.trailingAnchor),
            selectorView.heightAnchor.constraint(equalToConstant: 70),
            selectorView.leadingAnchor.constraint(equalTo: picker.leadingAnchor, constant: -20),
            selectorView.centerYAnchor.constraint(equalTo: picker.centerYAnchor),
        ])
        
        return picker
    }()
    
    lazy var callButtonView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.callsBackgroundColor
        view.layer.cornerRadius = 17
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickAction))
        tapGesture.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGesture)
        view.isUserInteractionEnabled = true
        
        let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.image = UIImage(named: "callPhone_ic", in: .local_gs_utils, compatibleWith: nil)
        icon.contentMode = .scaleAspectFit
        icon.backgroundColor = .clear
        icon.tintColor = .black
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "LLAMAR"
        label.font = .Montserrat_Semibold_14
        label.textColor = .black
        label.numberOfLines = 1
        
        view.addSubview(icon)
        view.addSubview(label)
        
        NSLayoutConstraint.activate([
            icon.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            icon.centerYAnchor.constraint(equalTo: label.centerYAnchor),
            icon.widthAnchor.constraint(equalToConstant: 15),
            icon.heightAnchor.constraint(equalToConstant: 15),
            
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: 8),
            label.leadingAnchor.constraint(equalTo: icon.trailingAnchor, constant: 10),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8),
        ])
        
        return view
    }()
    
    
    lazy var contactsContainer: GSContactsCheckListView = {
        let view = GSContactsCheckListView(contactsList: self.contactList, allChecked: true, checkListdelegate: self)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    
    public convenience init(
        delegate: GSCallsViewUIDelegate){
            self.init()
            self.delegate = delegate
            
            setUI()
            setConstraints()
            
            self.setEnableCallButton(enabled: !self.contactsContainer.areNoneSelected())
        }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUI(){
        self.addSubview(mainView)
        
        self.mainView.contentView.addSubview(self.pickerContainerView)
        self.pickerContainerView.addSubview(self.pickerView)
        self.pickerContainerView.addSubview(self.callButtonView)
        
        self.mainView.contentView.addSubview(self.contactsContainer)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.mainView.topAnchor.constraint(equalTo: self.topAnchor),
            self.mainView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.mainView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.mainView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.pickerContainerView.topAnchor.constraint(equalTo: self.mainView.contentView.topAnchor),
            self.pickerContainerView.leadingAnchor.constraint(equalTo: self.mainView.contentView.leadingAnchor),
            self.pickerContainerView.trailingAnchor.constraint(equalTo: self.mainView.contentView.trailingAnchor),
            self.pickerContainerView.bottomAnchor.constraint(equalTo: self.mainView.contentView.centerYAnchor, constant: 100),
            
            self.pickerView.topAnchor.constraint(equalTo: self.pickerContainerView.topAnchor, constant: 0),
            self.pickerView.leadingAnchor.constraint(equalTo: self.pickerContainerView.leadingAnchor, constant: 140),
            self.pickerView.widthAnchor.constraint(equalToConstant: 540),
            self.pickerView.bottomAnchor.constraint(equalTo: self.pickerContainerView.bottomAnchor, constant: 0),
            
            self.callButtonView.centerYAnchor.constraint(equalTo: self.pickerView.centerYAnchor),
            self.callButtonView.leadingAnchor.constraint(equalTo: self.pickerView.trailingAnchor, constant: 30),
            
            self.contactsContainer.topAnchor.constraint(equalTo: self.pickerContainerView.bottomAnchor),
            self.contactsContainer.leadingAnchor.constraint(equalTo: self.mainView.contentView.leadingAnchor, constant: 150),
            self.contactsContainer.trailingAnchor.constraint(equalTo: self.mainView.contentView.trailingAnchor, constant: -150),
            self.contactsContainer.heightAnchor.constraint(equalToConstant: 50),
            
        ])
    }
    
    func setEnableCallButton(enabled: Bool){
        UIView.animate(withDuration: 0.2) {
            self.callButtonView.alpha = enabled ? 1 : 0.4
            self.callButtonView.isUserInteractionEnabled = enabled
        }
    }
    
    
    @objc func onClickAction(){
        
        let selectedService = self.pickerViewElements[self.pickerView.selectedRow(inComponent: 0)]
        print(selectedService)
        self.delegate?.notifyCall(contactsList: self.contactsContainer.getSelectedContacts(), selectedService: selectedService)
    }
}

extension GSCallsViewUI: GSGenericFrontProtocol {
    func notifyDismiss() {
        self.delegate?.notifyDismiss()
    }
}

extension GSCallsViewUI: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewElements.count
    }
}

extension GSCallsViewUI: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        pickerView.subviews.forEach { element in
            element.isHidden = (element.backgroundColor != nil)
        }
        return 80
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var elementLabel: GSLabelPadding? = (view as? GSLabelPadding)

        if elementLabel == nil {
            elementLabel = GSLabelPadding(withInsets: 0, 0 , 20, 20)
        }

        if let label = elementLabel {
            label.textAlignment = .left
            label.text = pickerViewElements[row]
            label.font = .Montserrat_Regular_16
            label.textColor = GSColorsManager.labelLightColor

            return label
        }

        return UILabel()
    }
    
}

extension GSCallsViewUI: ContactsCheckListProtocol {
    func notifyContactSelected() {
        self.setEnableCallButton(enabled: !self.contactsContainer.areNoneSelected())
    }
}

