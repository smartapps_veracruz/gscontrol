//
//  GSCallsMain.swift
//  GSBase
//
//  Created by Branchbit on 25/12/21.
//

import Foundation
import UIKit

open class GSCallsMain{
    public static func createModule() -> UIViewController {
        let viewController: GSCallsView? = GSCallsView()
        if let view = viewController {
            let presenter = GSCallsPresenter()
            let interactor = GSCallsInteractor()
            
            
            view.presenter = presenter
            
            presenter.view = view
            presenter.interactor = interactor
            
            
            interactor.presenter = presenter
            return view
        }
        return UIViewController()
    }
    
    
}
