//
//  GSCallsProtocols.swift
//  GSBase
//
//  Created by Branchbit on 25/12/21.
//

import Foundation
protocol GSCallsViewProtocol: AnyObject {
    
}

protocol GSCallsInteractorProtocol: AnyObject {
    
}

protocol GSCallsPresenterProtocol: AnyObject {
    
}

protocol GSCallsRouterProtocol: AnyObject {
    
}
