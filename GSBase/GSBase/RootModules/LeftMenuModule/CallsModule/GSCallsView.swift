//
//  GSCallsView.swift
//  GSBase
//
//  Created by Branchbit on 25/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

class GSCallsView: UIViewController {
    var presenter: GSCallsPresenterProtocol?
    private var ui: GSCallsViewUI?
    
    
    override func loadView() {
        ui = GSCallsViewUI(
            delegate: self
        )
        view = ui
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension GSCallsView: GSCallsViewProtocol {
    
}

extension GSCallsView: GSCallsViewUIDelegate {
    func notifyCall(contactsList: [GSContactEntity], selectedService: String) {
        print("calling")
        let gs = GSCallingMain.createModule(contactsToCall: contactsList, serviceCalling: selectedService)
        gs.modalPresentationStyle = .overFullScreen
        self.present(gs, animated: false, completion: nil)
    }
    
    func notifyDismiss() {
        self.dismiss(animated: false, completion: nil)
    }
}
