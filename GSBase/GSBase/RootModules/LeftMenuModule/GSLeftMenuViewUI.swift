//
//  GSLeftMenuViewUI.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSCommonUtils

// MARK: GSLeftMenuViewUI Delegate -
/// GSLeftMenuViewUI Delegate
protocol GSLeftMenuViewUIDelegate {
    func notifyPresentView()
    
    func notifyPresentContingencyModule()
    func notifyPresentNotificationsModule()
    func notifyPresentCallsModule()
    func notifyPresentButtonRedModule()
    func notifyPresentRootModule()
    // Send Events to Module View, that will send events to the Presenter; which will send events to the Receiver e.g. Protocol OR Component.
}

class GSLeftMenuViewUI: UIView {
    
    var delegate: GSLeftMenuViewUIDelegate?
    lazy var navigationTop: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.noneColor
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "background_top_ic", in: .local_gs_utils, compatibleWith: nil)
        image.contentMode = .scaleAspectFill
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        view.addSubview(image)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        let logoImage = UIImageView(frame: .zero)
        logoImage.image = UIImage(named: "logo_ic", in: .local_gs_utils, compatibleWith: nil)
        logoImage.contentMode = .scaleAspectFit
        logoImage.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(logoImage)
        logoImage.isUserInteractionEnabled = true
        logoImage.addGestureRecognizer(tap)
        
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: view.topAnchor),
            image.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            image.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            image.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            logoImage.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            logoImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            logoImage.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            logoImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 20),
        ])
        return view
    }()
    
    lazy var bodyContainer: UIView = {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = GSColorsManager.noneColor
        return view
    }()
    
    lazy var extraButtonLeading: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.spacing = CGFloat(UIScreen.main.bounds.height / 7.5)
        stackView.addArrangedSubview(contingenciaButton)
        stackView.addArrangedSubview(socialButton)
        stackView.addArrangedSubview(callButton)
        return stackView
    }()
    
    lazy var FacetecAuth: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "red_button_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
        button.tag = 101
        button.addTarget(self, action: #selector(self.onClickAction(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var contingenciaButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "contingencia_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: 50),
            button.widthAnchor.constraint(equalToConstant: 60)
        ])
        button.tag = 102
        button.addTarget(self, action: #selector(self.onClickAction(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var socialButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "social_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
       // button.addTarget(self, action: #selector(self.onClickAction(_:)), for: .touchUpInside)
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: 50),
            button.widthAnchor.constraint(equalToConstant: 80)
        ])
        button.tag = 103
        return button
    }()
    
    lazy var callButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "call_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
       // button.addTarget(self, action: #selector(self.onClickAction(_:)), for: .touchUpInside)
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: 50),
            button.widthAnchor.constraint(equalToConstant: 60)
        ])
        button.tag = 104
        return button
    }()
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        delegate?.notifyPresentRootModule()
    }
    
    convenience init(delegate: GSLeftMenuViewUIDelegate) {
        self.init()
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()

    }
    
    fileprivate func setupUIElements() {
        // arrange subviews
        addSubview(navigationTop)
        addSubview(bodyContainer)
        bodyContainer.addSubview(FacetecAuth)
        bodyContainer.addSubview(extraButtonLeading)
    }
    
    fileprivate func setupConstraints() {
        // add constraints to subviews
        NSLayoutConstraint.activate([
            navigationTop.topAnchor.constraint(equalTo: topAnchor),
            navigationTop.leadingAnchor.constraint(equalTo: leadingAnchor),
            navigationTop.trailingAnchor.constraint(equalTo: trailingAnchor),
            navigationTop.heightAnchor.constraint(equalToConstant: 100),
            
            bodyContainer.topAnchor.constraint(equalTo: navigationTop.bottomAnchor),
            bodyContainer.leadingAnchor.constraint(equalTo: leadingAnchor),
            bodyContainer.trailingAnchor.constraint(equalTo: trailingAnchor),
            bodyContainer.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            FacetecAuth.topAnchor.constraint(equalTo: bodyContainer.topAnchor, constant: 50),
            FacetecAuth.centerXAnchor.constraint(equalTo: bodyContainer.centerXAnchor),
            FacetecAuth.widthAnchor.constraint(equalTo: bodyContainer.widthAnchor, multiplier: 0.9, constant: 0),
            FacetecAuth.heightAnchor.constraint(equalTo: FacetecAuth.widthAnchor, multiplier: 1.2, constant: 0),
            
            extraButtonLeading.topAnchor.constraint(equalTo: FacetecAuth.bottomAnchor, constant: 30),
            extraButtonLeading.leadingAnchor.constraint(equalTo: bodyContainer.leadingAnchor, constant: -5),
            extraButtonLeading.trailingAnchor.constraint(equalTo: bodyContainer.trailingAnchor)
            
        ])
    }
    
    @objc private func onClickAction(_ sender: UIButton){
        switch sender.tag{
        case 101:
            delegate?.notifyPresentButtonRedModule()
            break
            
        case 102:
            delegate?.notifyPresentContingencyModule()
            break
            
        case 103:
            delegate?.notifyPresentNotificationsModule()
            break
            
        case 104:
            delegate?.notifyPresentCallsModule()
            break
            
        default:
            delegate?.notifyPresentView()
            break
        }
    }
}
