//
//  LeftMenuMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 21/12/21.
//

import UIKit

class GSLeftMenuMain: NSObject {

    static func createModule()->UIViewController{
        
        let viewController  :   GSLeftMenuView?   =  GSLeftMenuView()
        if let view = viewController {
            let presenter   =   GSLeftMenuPresenter()
            let router      =   GSLeftMenuRouter()
            let interactor  =   GSLeftMenuInteractor()
            
            view.presenter  =   presenter
            
            presenter._view          =   view
            presenter.interactor    =   interactor
            presenter.wireframe        =   router
            
            interactor._presenter    =   presenter
            return view
        }
        return UIViewController()
    }
}
