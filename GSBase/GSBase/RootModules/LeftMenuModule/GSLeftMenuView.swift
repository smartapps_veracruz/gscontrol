//
//  GSLeftMenuView.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import SDKGSCommonUtils

/// GSLeftMenu Module View
class GSLeftMenuView: UIViewController {
    
    private var ui : GSLeftMenuViewUI?
    var presenter: GSLeftMenuPresenterProtocol?
    
    override func loadView() {
        // setting the custom view as the view controller's view
        ui = GSLeftMenuViewUI(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = GSColorsManager.noneColor
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - extending GSLeftMenuView to implement it's protocol
extension GSLeftMenuView: GSLeftMenuViewProtocol {
    func notifyGetContext() -> UIViewController? {
        return self
    }
}

// MARK: - extending GSLeftMenuView to implement the custom ui view delegate
extension GSLeftMenuView: GSLeftMenuViewUIDelegate {
    func notifyPresentContingencyModule() {
        self.presenter?.requestContingencyModule()
    }
    
    func notifyPresentNotificationsModule() {
        self.presenter?.requestNotificationsModule()
    }
    
    func notifyPresentCallsModule() {
        self.presenter?.requestCallsModule()
    }
    
    func notifyPresentButtonRedModule() {
//        presenter?.requestFacetecAuthModule()
        let gs = GSFacetecAuthMain.createModule(delegate: self)
        gs.modalPresentationStyle = .overFullScreen
        present(gs, animated: true, completion: nil)
    }
    
    func notifyPresentRootModule() {
        presenter?.requestRootModule()
    }
    
    func notifyPresentView() {
        let vc = GSFrontViewController()
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
}


extension GSLeftMenuView : GSFacetecAuthViewDelegate{
    func notifySuccessValidation() {
        let gs = NotificationSwitchMain.createModule()
        gs.modalPresentationStyle = .overFullScreen
        present(gs, animated: true, completion: nil)
    }
    
    
}
