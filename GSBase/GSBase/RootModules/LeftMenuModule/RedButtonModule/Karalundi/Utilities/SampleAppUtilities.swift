import Foundation
import UIKit
import FaceTecSDK
import AVFoundation

class SampleAppUtilities: NSObject, FaceTecCustomAnimationDelegate {
    
    
    // Reference to app's main view controller
    let sampleAppVC: GSFacetecAuthView!
    
    var currentTheme = Config.wasSDKConfiguredWithConfigWizard ? "Config Wizard Theme" : "FaceTec Theme"
    var themeTransitionTextTimer: Timer!
    
    var networkIssueDetected = false
    
    init(vc: GSFacetecAuthView) {
        sampleAppVC = vc

        if #available(iOS 13.0, *) {
            // For iOS 13+, use the rounded system font for displayed text
            if let roundedDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: .body).withDesign(.rounded) {
            }
        }
    }
    
//    func startSessionTokenConnectionTextTimer() {
//        themeTransitionTextTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(showSessionTokenConnectionText), userInfo: nil, repeats: false)
//    }
//    
//    @objc func showSessionTokenConnectionText() {
//        UIView.animate(withDuration: 0.6) {
//        }
//    }

//    func hideSessionTokenConnectionText() {
//        UIView.animate(withDuration: 0.6) {
//        }
//    }
    
//    func fadeOutMainUIAndPrepareForFaceTecSDK() {
//        enableButtons(shouldEnable: false) {
//            if(self.networkIssueDetected) {
//                self.networkIssueDetected = false
//                return
//            }
//
//            UIView.animate(withDuration: 0.3) {
//            };
//        }
//    }
    
    
//    func handleErrorGettingServerSessionToken() {
//        networkIssueDetected = true
//        displayStatus(statusString: "Session could not be started due to an unexpected issue during the network request.")
//        hideSessionTokenConnectionText();
//    }
    
    func displayStatus(statusString: String) {
        DispatchQueue.main.async {
            print("----------\(statusString)---------------------")
        }
    }
    
    func showThemeSelectionMenu() {
        let themeSelectionMenu = UIAlertController(title: nil, message: "Select a Theme", preferredStyle: .actionSheet)
        
        let selectDefaultThemeAction = UIAlertAction(title: "FaceTec Theme", style: .default) {
            (_) -> Void in self.handleThemeSelection(theme: "FaceTec Theme")
        }
        if(Config.wasSDKConfiguredWithConfigWizard == true) {
            let selectDevConfigThemeAction = UIAlertAction(title: "Config Wizard Theme", style: .default) {
                (_) -> Void in self.handleThemeSelection(theme: "Config Wizard Theme")
            }
            themeSelectionMenu.addAction(selectDevConfigThemeAction)
        }
        let selectPseudoFullscreenThemeAction = UIAlertAction(title: "Pseudo-Fullscreen", style: .default) {
            (_) -> Void in self.handleThemeSelection(theme: "Pseudo-Fullscreen")
        }
        let selectWellRoundedThemeAction = UIAlertAction(title: "Well-Rounded", style: .default) {
            (_) -> Void in self.handleThemeSelection(theme: "Well-Rounded")
        }
        let selectBitcoinExchangeThemeAction = UIAlertAction(title: "Bitcoin Exchange", style: .default) {
            (_) -> Void in self.handleThemeSelection(theme: "Bitcoin Exchange")
        }
        let selectEKYCThemeAction = UIAlertAction(title: "eKYC", style: .default) {
            (_) -> Void in self.handleThemeSelection(theme: "eKYC")
        }
        let selectSampleBankThemeAction = UIAlertAction(title: "Sample Bank", style: .default) {
            (_) -> Void in self.handleThemeSelection(theme: "Sample Bank")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        themeSelectionMenu.addAction(selectDefaultThemeAction)
        themeSelectionMenu.addAction(selectPseudoFullscreenThemeAction)
        themeSelectionMenu.addAction(selectWellRoundedThemeAction)
        themeSelectionMenu.addAction(selectBitcoinExchangeThemeAction)
        themeSelectionMenu.addAction(selectEKYCThemeAction)
        themeSelectionMenu.addAction(selectSampleBankThemeAction)
        themeSelectionMenu.addAction(cancelAction)
        // Must use popover controller for iPad
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = themeSelectionMenu.popoverPresentationController {
                popoverController.sourceView = sampleAppVC.view
                popoverController.sourceRect = CGRect(x: sampleAppVC.view.bounds.midX, y: sampleAppVC.view.bounds.midY, width: 0, height: 0)
                popoverController.permittedArrowDirections = []
            }
        }
        // Remove negative width constraint that causes layout conflict warning -- non-critical unfixed Apple bug
        for subview in themeSelectionMenu.view.subviews {
            for constraint in subview.constraints where constraint.debugDescription.contains("width == - 16") {
                subview.removeConstraint(constraint)
            }
        }
        
        sampleAppVC.present(themeSelectionMenu, animated: true, completion: nil)
    }
    
    func showAuditTrailImages() {
        var auditTrailAndIDScanImages: [UIImage] = []
        let latestFaceTecSessionResult = sampleAppVC.latestSessionResult
        let latestFaceTecIDScanResult = sampleAppVC.latestIDScanResult
        
        // Update audit trail.
        if latestFaceTecSessionResult?.auditTrailCompressedBase64 != nil {
            for compressedBase64EncodedAuditTrailImage in (latestFaceTecSessionResult?.auditTrailCompressedBase64)! {
                let dataDecoded : Data = Data(base64Encoded: compressedBase64EncodedAuditTrailImage, options: .ignoreUnknownCharacters)!
                let decodedimage = UIImage(data: dataDecoded)
                auditTrailAndIDScanImages.append(decodedimage!)
            }
        }
        
        if latestFaceTecIDScanResult != nil
            && latestFaceTecIDScanResult?.frontImagesCompressedBase64 != nil
            && (latestFaceTecIDScanResult?.frontImagesCompressedBase64?.count)! > 0
        {
            let dataDecoded : Data = Data(base64Encoded: (latestFaceTecIDScanResult?.frontImagesCompressedBase64?[0])!, options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            auditTrailAndIDScanImages.append(decodedimage!)
        }
        
        if auditTrailAndIDScanImages.count == 0 {
            displayStatus(statusString: "No audit trail images available.")
            return
        }
        for auditImage in auditTrailAndIDScanImages.reversed() {
            addDismissableImageToInterface(image: auditImage)
        }
    }
    
    @objc func dismissImageView(tap: UITapGestureRecognizer){
        let tappedImage = tap.view!
        tappedImage.removeFromSuperview()
    }
    
    // Place a UIImage onto the main interface in a stack that can be popped by tapping on the image
    func addDismissableImageToInterface(image: UIImage) {
        let imageView = UIImageView(image: image)
        imageView.frame = UIScreen.main.bounds
        
        // Resize image to better fit device's display
        // Remove this option to view image full screen
        let screenSize = UIScreen.main.bounds
        let ratio = screenSize.width / image.size.width
        let size = (image.size).applying(CGAffineTransform(scaleX: 0.5 * ratio, y: 0.5 * ratio))
        let hasAlpha = false
        let scale: CGFloat = 0.0
        UIGraphicsBeginImageContextWithOptions(size, hasAlpha, scale)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        imageView.image = scaledImage
        imageView.contentMode = .center
        
        // Tap on image to dismiss view
        imageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissImageView(tap:)))
        imageView.addGestureRecognizer(tap)
        
        sampleAppVC.view.addSubview(imageView)
    }
    
    func handleThemeSelection(theme: String) {
        currentTheme = theme
        ThemeHelpers.setAppTheme(theme: theme)
        updateThemeTransitionView()

        // Set this class as the delegate to handle the FaceTecCustomAnimationDelegate methods. This delegate needs to be applied to the current FaceTecCustomization object before starting a new Session in order to use FaceTecCustomAnimationDelegate methods to provide a new instance of a custom UIView that will be displayed for the method-specified animation.
        if(Config.currentCustomization.customAnimationDelegate == nil) {
            Config.currentCustomization.customAnimationDelegate = self
            FaceTec.sdk.setCustomization(Config.currentCustomization)
        }
    }
    
    func updateThemeTransitionView() {
        var transitionViewImage: UIImage? = nil
        var transitionTextColor = Config.currentCustomization.guidanceCustomization.foregroundColor
        switch currentTheme {
            case "FaceTec Theme":
                break
            case "Config Wizard Theme":
                break
            case "Pseudo-Fullscreen":
                break
            case "Well-Rounded":
                transitionViewImage = UIImage(named: "well_rounded_bg")
                transitionTextColor = Config.currentCustomization.frameCustomization.backgroundColor
                break
            case "Bitcoin Exchange":
                transitionViewImage = UIImage(named: "bitcoin_exchange_bg")
                transitionTextColor = Config.currentCustomization.frameCustomization.backgroundColor
                break
            case "eKYC":
                transitionViewImage = UIImage(named: "ekyc_bg")
                break
            case "Sample Bank":
                transitionViewImage = UIImage(named: "sample_bank_bg")
                transitionTextColor = Config.currentCustomization.frameCustomization.backgroundColor
                break
            default:
                break
        }
        
    }
    
    func enableButtons(shouldEnable: Bool, completion: (() -> ())? = nil) {
        DispatchQueue.main.async {
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
                if completion != nil {
                    completion!()
                }
            }
        }
    }
    
//    func onCreateNewResultScreenActivityIndicatorView() -> UIView? {
//        var activityIndicatorView: UIView? = nil
//        switch currentTheme {
//            case "FaceTec Theme":
//                break
//            case "Config Wizard Theme":
//                break
//            case "Pseudo-Fullscreen":
//                activityIndicatorView = PseudoFullscreenActivityIndicatorView()
//                break
//            case "Well-Rounded":
//                activityIndicatorView = WellRoundedActivityIndicatorView()
//                break
//            case "Bitcoin Exchange":
//                break
//            case "eKYC":
//                activityIndicatorView = EKYCActvityIndicatorView()
//                break
//            case "Sample Bank":
//                break
//            default:
//                break
//        }
//        return activityIndicatorView
//    }
    
//    func onCreateNFCStartingAnimationView() -> UIView? {
//        return NFCStartingAnimationView()
//    }
    
//    func onCreateNFCScanningAnimationView() -> UIView? {
//        var scanningAnimationView: UIView? = nil
//        switch currentTheme {
//            case "FaceTec Theme":
//                break
//            case "Config Wizard Theme":
//                break
//            case "Pseudo-Fullscreen":
//                scanningAnimationView = NFCScanningAnimationViewBlack()
//                break
//            case "Well-Rounded":
//                scanningAnimationView = NFCScanningAnimationViewGreen()
//                break
//            case "Bitcoin Exchange":
//                break
//            case "eKYC":
//                scanningAnimationView = NFCScanningAnimationViewRed()
//                break
//            case "Sample Bank":
//                break
//            default:
//                break
//        }
//
//        if scanningAnimationView == nil {
//            scanningAnimationView = NFCScanningAnimationView()
//        }
//
//        return scanningAnimationView
//    }
    
//    func onCreateNFCSkipOrErrorAnimationView() -> UIView? {
//        var skipOrErrorAnimationView: UIView? = nil
//        switch currentTheme {
//            case "FaceTec Theme":
//                break
//            case "Config Wizard Theme":
//                break
//            case "Pseudo-Fullscreen":
//                skipOrErrorAnimationView = PseudoFullscreenUnsuccessView()
//                break
//            case "Well-Rounded":
//                skipOrErrorAnimationView = UIImageView(image: UIImage(named: "warning_green"))
//                break
//            case "Bitcoin Exchange":
//                skipOrErrorAnimationView = UIImageView(image: UIImage(named: "warning_orange"))
//                break
//            case "eKYC":
//                skipOrErrorAnimationView = EKYCUnsuccessView()
//                break
//            case "Sample Bank":
//                skipOrErrorAnimationView = UIImageView(image: UIImage(named: "warning_white"))
//                break
//            default:
//                break
//        }
//
//        return skipOrErrorAnimationView
//    }
    
//    func onCreateNewResultScreenSuccessAnimationView() -> UIView? {
//        var successAnimationView: UIView? = nil
//        switch currentTheme {
//            case "FaceTec Theme":
//                break
//            case "Config Wizard Theme":
//                break
//            case "Pseudo-Fullscreen":
//                successAnimationView = PseudoFullscreenSuccessView()
//                break
//            case "Well-Rounded":
//                successAnimationView = WellRoundedSuccessView()
//                break
//            case "Bitcoin Exchange":
//                break
//            case "eKYC":
//                successAnimationView = EKYCSuccessView()
//                break
//            case "Sample Bank":
//                break
//            default:
//                break
//        }
//        return successAnimationView
//    }
    
//    func onCreateNewResultScreenUnsuccessAnimationView() -> UIView? {
//        var unsuccessAnimationView: UIView? = nil
//        switch currentTheme {
//            case "FaceTec Theme":
//                break
//            case "Config Wizard Theme":
//                break
//            case "Pseudo-Fullscreen":
//                unsuccessAnimationView = PseudoFullscreenUnsuccessView()
//                break
//            case "Well-Rounded":
//                unsuccessAnimationView = WellRoundedUnsuccessView()
//                break
//            case "Bitcoin Exchange":
//                break
//            case "eKYC":
//                unsuccessAnimationView = EKYCUnsuccessView()
//                break
//            case "Sample Bank":
//                break
//            default:
//                break
//        }
//        return unsuccessAnimationView
//    }

    
    public static func setOCRLocalization() {
        // Set the strings to be used for group names, field names, and placeholder texts for the FaceTec ID Scan User OCR Confirmation Screen.
        // DEVELOPER NOTE: For this demo, we are using the template json file, 'FaceTec_OCR_Customization.json,' as the parameter in calling this API.
        // For the configureOCRLocalization API parameter, you may use any dictionary object that follows the same structure and key naming as the template json file, 'FaceTec_OCR_Customization.json'.
        if let path = Bundle.main.path(forResource: "FaceTec_OCR_Customization", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
                if let jsonDictionary = jsonObject as? Dictionary<String, AnyObject> {
                    FaceTec.sdk.configureOCRLocalization(dictionary: jsonDictionary)
                }
            } catch {
                print("Error loading JSON for OCR Localization")
            }
        }
    }
}
