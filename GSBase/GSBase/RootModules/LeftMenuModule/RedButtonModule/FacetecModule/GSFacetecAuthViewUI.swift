//
//  GSFacetecAuthViewUI.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 23/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

// MARK: GSFacetecAuthViewUI Delegate -
/// GSFacetecAuthViewUI Delegate
protocol GSFacetecAuthViewUIDelegate {
    // Send Events to Module View, that will send events to the Presenter; which will send events to the Receiver e.g. Protocol OR Component.
}

class GSFacetecAuthViewUI: UIView {
    
    var delegate: GSFacetecAuthViewUIDelegate?
        
    
    lazy var faceImage: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "faceImage", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    convenience init(delegate: GSFacetecAuthViewUIDelegate) {
        self.init()
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        
    }
    
    fileprivate func setupUIElements() {
        // arrange subviews
        addSubview(faceImage)
    }
    
    fileprivate func setupConstraints() {
        // add constraints to subviews
        
        NSLayoutConstraint.activate([
            faceImage.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            faceImage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -30),
            faceImage.widthAnchor.constraint(equalToConstant: 100),
            faceImage.heightAnchor.constraint(equalToConstant: 100)
            
        ])
    }
}
