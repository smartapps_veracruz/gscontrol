//
//  GSFacetecAuthContracts.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 23/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

//MARK: View -
/*
 Should replace "class" with "BaseViewProtocol" if available;
 & that will allow the View to act as a UIViewController;
 & Implement common view functions.
 */
/// GSFacetecAuth Module View Protocol
protocol GSFacetecAuthViewProtocol: AnyObject {
  
}

//MARK: Interactor -
/// GSFacetecAuth Module Interactor Protocol
protocol GSFacetecAuthInteractorProtocol:AnyObject {
   
}

//MARK: Presenter -
/// GSFacetecAuth Module Presenter Protocol
protocol GSFacetecAuthPresenterProtocol:AnyObject {
    
}

//MARK: Router (aka: Wireframe) -
/// GSFacetecAuth Module Router Protocol
protocol GSFacetecAuthRouterProtocol:AnyObject {
    // Show Details of Entity Object coming from ParentView Controller.
    // func showDetailsFor(object: GSFacetecAuthEntity, parentViewController viewController: UIViewController)
}
