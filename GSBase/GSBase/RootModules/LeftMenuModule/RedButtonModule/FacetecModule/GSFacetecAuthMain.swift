//
//  GSFacetecAuthMain.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 23/12/21.
//

import UIKit


class GSFacetecAuthMain: NSObject {
    
    static func createModule(delegate: GSFacetecAuthViewDelegate)->UIViewController{
        
        let viewController  :   GSFacetecAuthView?   =  GSFacetecAuthView()
        if let view = viewController {
            let presenter   =   GSFacetecAuthPresenter()
            let router      =   GSFacetecAuthRouter()
            let interactor  =   GSFacetecAuthInteractor()
            
            view.presenter  =   presenter
            view.delegate = delegate
            presenter._view          =   view
            presenter.interactor    =   interactor
            presenter.wireframe        =   router
            
            interactor._presenter    =   presenter
            return view
        }
        return UIViewController()
    }
}
