//
//  GSFacetecAuthPresenter.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 23/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

/// GSFacetecAuth Module Presenter
class GSFacetecAuthPresenter {
    
    weak var _view: GSFacetecAuthViewProtocol?
    var interactor: GSFacetecAuthInteractorProtocol?
    var wireframe: GSFacetecAuthRouterProtocol?
}

// MARK: - extending GSFacetecAuthPresenter to implement it's protocol
extension GSFacetecAuthPresenter: GSFacetecAuthPresenterProtocol {
    
}
