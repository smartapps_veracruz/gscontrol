//
//  GSLeftMenuPresenter.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

/// GSLeftMenu Module Presenter
class GSLeftMenuPresenter {
    
    weak var _view: GSLeftMenuViewProtocol?
    var interactor: GSLeftMenuInteractorProtocol?
    var wireframe: GSLeftMenuRouterProtocol?
}

// MARK: - extending GSLeftMenuPresenter to implement it's protocol
extension GSLeftMenuPresenter: GSLeftMenuPresenterProtocol {
    func requestContingencyModule() {
        if let view = _view?.notifyGetContext(){
            wireframe?.navigateToContingencyModule(view: view)
        }
    }
    
    func requestNotificationsModule() {
        if let view = _view?.notifyGetContext(){
            wireframe?.navigateToNotificationsModule(view: view)
        }
    }
    
    func requestCallsModule() {
        if let view = _view?.notifyGetContext(){
            wireframe?.navigateToCallsModule(view: view)
        }
    }
    
    func requestRootModule() {
        wireframe?.navigateToToRoot()
    }

    func requestFacetecAuthModule() {
        if let view = _view?.notifyGetContext(){
            wireframe?.navigateToFacetecAuthModule(view: view)
        }
    }
}
