//
//  GSLeftMenuRouter.swift
//  GSBase
//
//  Created Dsi Soporte Tecnico on 21/12/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

/// GSLeftMenu Module Router (aka: Wireframe)
class GSLeftMenuRouter: GSLeftMenuRouterProtocol {
    func navigateToContingencyModule(view: UIViewController) {
        let gs = GSContingencyMain.createModule()
        gs.modalPresentationStyle = .overFullScreen
        view.present(gs, animated: false, completion: nil)
    }
    
    func navigateToNotificationsModule(view: UIViewController) {
        let gs = GSNotificationsMain.createModule()
        gs.modalPresentationStyle = .overFullScreen
        view.present(gs, animated: false, completion: nil)
    }
    
    func navigateToCallsModule(view: UIViewController) {
        let gs = GSCallsMain.createModule()
        gs.modalPresentationStyle = .overFullScreen
        view.present(gs, animated: false, completion: nil)
    }
    
    func navigateToToRoot() {
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifierListeningRoot"), object: nil)
    }
    
    func navigateToFacetecAuthModule(view: UIViewController) {
        let gs = GSFacetecAuthMain.createModule(delegate: self)
        gs.modalPresentationStyle = .overFullScreen
        view.present(gs, animated: true, completion: nil)
    }
}

extension GSLeftMenuRouter: GSFacetecAuthViewDelegate{
    func notifySuccessValidation() {
        let gs = NotificationSwitchMain.createModule()
        gs.modalPresentationStyle = .overFullScreen
    }
}
