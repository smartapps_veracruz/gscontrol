//
//  NotificationSwitchViewC.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 30/12/21.
//

import UIKit
import SDKGSCommonUtils
import SDKGSServicesManager

class NotificationSwitchViewC: UIViewController {
    var currentType: typeOfMovement = .simulacrum
    private var ui : NotificationSwitchContainer?
    var presenter: NotificationSwitchPresenterProtocol?
    
    
    override func loadView() {
        ui = NotificationSwitchContainer(delegate: self)
        view = ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = GSColorsManager.noneColor
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension NotificationSwitchViewC: NotificationSwitchViewProtocol {
    
    func responseSuccessTelegram() {
        ui?.sucessProcess1.alpha = 1
        ui?.sucessProcessCompleted1.alpha = 1
        ui?.sucessProcessCompletedIcon1.alpha = 1
        
        let date = Date()
        let dateFormatter = DateFormatter()

        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        
        let dateToString = dateFormatter.string(from: date)
        
        let stringToSimulacrumWhatsapp : String = "Notificación Importante\n\nFecha y hora: \(dateToString).\n\nIngeniero Ángel Carranza / Karla Lobato / Yhalia Arce \n\nSe informa que se da inicio al proceso protocolo del simulacro \n\nNOC IT CDMX comenzará a desalojar el inmueble, se transfiere la operación de los servicios de IT a NOC IT MTY \n\nAtentamente: \nNOC IT"
        
        let stringToEarthquakeWhatsapp : String = "Ingeniero Ángel Carranza / Karla Lobato / Yhalia Arce \n\nSe informa que se presenta temblor en la Cd de México \n\nNOC IT CDMX comenzará a desalojar el inmueble, se transfiere la operación de los servicios de IT a NOC IT MTY \n\nAtentamente: \nNOC IT"
        
        switch currentType {
        case .earthqueake:
            sendWhatsappMessage(type: currentType,message : stringToEarthquakeWhatsapp)
        case .simulacrum:
            sendWhatsappMessage(type: currentType,message : stringToSimulacrumWhatsapp)
        }
        
    }
    
    func responseSuccessWhatsapp() {
        ui?.sucessProcess2.alpha = 1
        ui?.sucessProcessCompleted2.alpha = 1
        ui?.sucessProcessCompletedIcon2.alpha = 1
    }
    
    func responseErrorTelegram(msg: String) {
        
        ui?.sucessProcessCompleted1.text = "Error"
        ui?.sucessProcessCompleted1.textColor = GSColorsManager.contingencyLabelColor
        ui?.sucessProcessCompletedIcon1.image = UIImage(named: "error_icon", in: .local_gs_utils, compatibleWith: nil)
        ui?.sucessProcessCompleted1.alpha = 1
        ui?.sucessProcessCompletedIcon1.alpha = 1
        ui?.sucessProcess1.alpha = 1
        
        let date = Date()
        let dateFormatter = DateFormatter()

        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        
        let dateToString = dateFormatter.string(from: date)
        
        let stringToSimulacrumWhatsapp : String = "Notificación Importante\n\nFecha y hora: \(dateToString).\n\nIngeniero Ángel Carranza / Karla Lobato / Yhalia Arce \n\nSe informa que se da inicio al proceso protocolo del simulacro \n\nNOC IT CDMX comenzará a desalojar el inmueble, se transfiere la operación de los servicios de IT a NOC IT MTY \n\nAtentamente: \nNOC IT"
        
        let stringToEarthquakeWhatsapp : String = "Ingeniero Ángel Carranza / Karla Lobato / Yhalia Arce \n\nSe informa que se presenta temblor en la Cd de México \n\nNOC IT CDMX comenzará a desalojar el inmueble, se transfiere la operación de los servicios de IT a NOC IT MTY \n\nAtentamente: \nNOC IT"
        
        switch currentType {
        case .earthqueake:
            sendWhatsappMessage(type: currentType,message : stringToEarthquakeWhatsapp)
        case .simulacrum:
            sendWhatsappMessage(type: currentType,message : stringToSimulacrumWhatsapp)
        }
    }
    
    func responseErrorWhatsapp(msg: String) {
        
        ui?.sucessProcessCompleted2.text = "Error"
        ui?.sucessProcessCompleted2.textColor = GSColorsManager.contingencyLabelColor
        ui?.sucessProcessCompletedIcon2.image = UIImage(named: "error_icon", in: .local_gs_utils, compatibleWith: nil)
        ui?.sucessProcessCompleted2.alpha = 1
        ui?.sucessProcessCompletedIcon2.alpha = 1
        ui?.sucessProcess2.alpha = 1
    }
    
    
    func sendTelegramMessage(type: typeOfMovement,message : String) {
        presenter?.sendTelegramMessage(type: type,message : message)
    }
    
    func sendWhatsappMessage(type: typeOfMovement,message : String) {
        presenter?.sendWhatsappMessage(type: type,message : message)
    }
}

extension NotificationSwitchViewC: NotificationSwitchDelegateProtocol {
    func sendMessage(type: typeOfMovement) {
        currentType = type
        let date = Date()
        let dateFormatter = DateFormatter()

        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        
        let dateToString = dateFormatter.string(from: date)
        
        let stringToEarthquakeTelegram : String = "Ingeniero Ángel Carranza / Karla Lobato / Yhalia Arce \n\nSe informa que se presenta temblor en la Cd de México \n\nNOC IT CDMX comenzará a desalojar el inmueble, se transfiere la operación de los servicios de IT a NOC IT MTY \n\nAtentamente: \nNOC IT"
        
        let stringToSimulacrumTelegram  : String = "Notificación Importante\n\nFecha y hora: \(dateToString).\n\nIngeniero Ángel Carranza / Karla Lobato / Yhalia Arce \n\nSe informa que se da inicio al proceso protocolo del simulacro \n\nNOC IT CDMX comenzará a desalojar el inmueble, se transfiere la operación de los servicios de IT a NOC IT MTY \n\nAtentamente: \nNOC IT"
       
        
        switch type {
        case .earthqueake:
            sendTelegramMessage(type: type,message : stringToEarthquakeTelegram)
          
        case .simulacrum:
            sendTelegramMessage(type: type,message : stringToSimulacrumTelegram)
        }
    }
    
    func notifyDismiss() {
            self.dismiss(animated: true, completion: nil)

    }
    
    
}



