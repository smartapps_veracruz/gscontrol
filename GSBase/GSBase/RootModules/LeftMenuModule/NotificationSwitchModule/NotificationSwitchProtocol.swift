//
//  NotificationSwitchProtocol.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 30/12/21.
//

import Foundation
import UIKit

protocol NotificationSwitchViewProtocol: AnyObject {
    func responseSuccessTelegram()
    func responseSuccessWhatsapp()
    func responseErrorTelegram(msg:String)
    func responseErrorWhatsapp(msg:String)
    func sendTelegramMessage(type : typeOfMovement, message : String)
    func sendWhatsappMessage(type : typeOfMovement, message : String)
}

//MARK: Interactor -
protocol NotificationSwitchInteractorProtocol: AnyObject {
    // Fetch Object from Data Layer
    func sendTelegramMessage(type : typeOfMovement,message : String)
    func sendWhatsappMessage(type : typeOfMovement,message : String)
}

//MARK: Presenter -
protocol NotificationSwitchPresenterProtocol: AnyObject {
    
    func responseErrorTelegram(msg:String)
    func responseErrorWhatsapp(msg:String)
    func responseSuccessTelegram()
    func responseSuccessWhatsapp()
    
    func sendTelegramMessage(type : typeOfMovement,message : String)
    func sendWhatsappMessage(type : typeOfMovement,message : String)
}

//MARK: Router (aka: Wireframe) -
protocol NotificationSwitchRouterProtocol:AnyObject {
    
}

protocol NotificationSwitchDelegateProtocol:AnyObject {
    func notifyDismiss()
    func sendMessage(type : typeOfMovement)
}

public enum typeOfMovement {
    case earthqueake
    case simulacrum
}
