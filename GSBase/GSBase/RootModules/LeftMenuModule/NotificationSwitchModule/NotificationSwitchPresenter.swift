//
//  NotificationSwitchPresenter.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 30/12/21.
//

import Foundation

class NotificationSwitchPresenter {

    
    weak var view: NotificationSwitchViewProtocol?
    var interactor: NotificationSwitchInteractorProtocol?
    var router: NotificationSwitchRouterProtocol?
    
}

extension NotificationSwitchPresenter: NotificationSwitchPresenterProtocol {
    func responseSuccessTelegram() {
        view?.responseSuccessTelegram()
    }
    
    func responseSuccessWhatsapp() {
        view?.responseSuccessWhatsapp()
    }
    
    func responseErrorTelegram(msg: String) {
        view?.responseErrorTelegram(msg: msg)
    }
    
    func responseErrorWhatsapp(msg: String) {
        view?.responseErrorWhatsapp(msg: msg)
    }
    
    func sendTelegramMessage(type: typeOfMovement,message : String) {
        interactor?.sendTelegramMessage(type: type,message : message)
    }
    
    func sendWhatsappMessage(type: typeOfMovement,message : String) {
        interactor?.sendWhatsappMessage(type: type,message : message)
    }
    
    
    
    
}

