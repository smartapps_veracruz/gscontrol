//
//  NotificationSwitchContainer.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 30/12/21.
//

import Foundation
import UIKit
import SDKGSCommonUtils

class NotificationSwitchContainer : UIView {
    
    var delegate: NotificationSwitchDelegateProtocol?
    
    private var progressBarBlackColumnsWidthAnchor: NSLayoutConstraint = NSLayoutConstraint()
    
    private var auxTimer: Timer?
    var counter = 10
    var timer = Timer()
    var sentMessage = false
    
    lazy var topBackground: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "background_top_ic", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var line : UIView = {
        let line = UIView()
        line.translatesAutoresizingMaskIntoConstraints = false
        line.backgroundColor = GSColorsManager.viewBorderColor.withAlphaComponent(1)
        return line
    }()
    
    lazy var titleLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.Montserrat_Bold_18.withSize(28)
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "Swicheo de Operación \nTotalplay Monterrey"
        return label
    }()
    
    lazy var parenthesisLeft: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "parenthesisLeft", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var parenthesisRight: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "parenthesisRight", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    lazy var simulacrumIcon: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "simulacrumIcon", in: .local_gs_utils, compatibleWith: nil)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.isUserInteractionEnabled = true
        return image
    }()
    
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        simulacrumIcon.alpha = 0.5
        simulacrumIcon.isUserInteractionEnabled = false
        timerToActivateSimulacrum.isHidden = true
        activateSimulacrumLabel.isHidden = true
        sentMessage = true
        animateProgressbar()
        delegate?.sendMessage(type: .simulacrum)
    }
    
    private lazy var activateSimulacrumLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Regular_20
        label.textColor = UIColor.white
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.textAlignment = .center
        label.text = "Tiempo restante para activar botón Modo simulacro : "
        return label
    }()
    
    private lazy var timerToActivateSimulacrum: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Regular_20
        label.textColor = UIColor.white
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.textAlignment = .center
        label.text = "10 segundos ..."
        return label
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Regular_20
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.textAlignment = .center
        label.text = "Swicheo en proceso .... 0%"
        return label
    }()
    
    private lazy var senderImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        image.image = UIImage(named: "buildingSelect", in: .local_gs_utils, compatibleWith: nil)
        return image
    }()
    private lazy var senderNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = GSColorsManager.buttonBlueAqua
        label.font = .Montserrat_Bold_18
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.numberOfLines = 3
        label.text = "CDMX"
        return label
    }()
    
    private lazy var progressBarContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.layer.cornerRadius = 12
        view.layer.borderColor = GSColorsManager.buttonBlueAqua.cgColor
        view.layer.borderWidth = 1
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var progressBarblackColumnsContainerView: UIView = {
       let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        
        let iterations = Int((UIScreen.main.bounds.width / 10))
        
        for n in 0 ..< iterations {
            let column = UIView()
            column.backgroundColor = .black
            column.translatesAutoresizingMaskIntoConstraints = false
            
            view.addSubview(column)
            
            NSLayoutConstraint.activate([
                column.topAnchor.constraint(equalTo: view.topAnchor, constant: -1),
                column.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 1),
                column.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: CGFloat((12 * n) + 6)),
                column.widthAnchor.constraint(equalToConstant: 6),
            ])
        }
        
        return view
    }()
    
    private lazy var progressBarView: UIProgressView = {
        let progress = UIProgressView()
        progress.translatesAutoresizingMaskIntoConstraints = false
        progress.progressViewStyle = .bar
        progress.trackTintColor = .clear
        progress.progressTintColor = GSColorsManager.labelStrongColor
        
        return progress
    }()
    // success_ic
    private lazy var receiverImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        image.image = UIImage(named: "buildingUnselect", in: .local_gs_utils, compatibleWith: nil)
        return image
    }()
    private lazy var receiverNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = GSColorsManager.buttonBlueAqua
        label.textAlignment = .center
        label.font = .Montserrat_Bold_18
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        label.numberOfLines = 3
        label.text = "Monterrey"
        return label
    }()
    
    private lazy var successIcon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        image.image = UIImage(named: "success_ic", in: .local_gs_utils, compatibleWith: nil)
        image.tintColor = GSColorsManager.successColor
        image.alpha = 0
        return image
    }()
    
    
    
    lazy var contentView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    
    
    lazy var sucessProcess1 : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Envio de mensaje Telegram"
        label.textColor = GSColorsManager.successColor
        label.font = UIFont.Montserrat_Regular_14
        label.alpha = 0
        return label
    }()
    
    lazy var sucessProcessCompleted1 : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Correcto"
        label.textColor = GSColorsManager.successColor
        label.font = UIFont.Montserrat_Bold_16
        label.alpha = 0
        return label
    }()
    
     lazy var sucessProcessCompletedIcon1: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        image.image = UIImage(named: "success_ic", in: .local_gs_utils, compatibleWith: nil)
        image.tintColor = GSColorsManager.successColor
        image.alpha = 0
        return image
    }()
    
    
    lazy var sucessProcess2 : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Envio de mensaje WhatsApp"
        label.textColor = GSColorsManager.successColor
        label.font = UIFont.Montserrat_Regular_14
        label.alpha = 0
        return label
    }()
    
    lazy var sucessProcessCompleted2 : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Correcto"
        label.textColor = GSColorsManager.successColor
        label.font = UIFont.Montserrat_Bold_16
        label.alpha = 0
        return label
    }()
    
     lazy var sucessProcessCompletedIcon2: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .clear
        image.image = UIImage(named: "success_ic", in: .local_gs_utils, compatibleWith: nil)
        image.tintColor = GSColorsManager.successColor
        image.alpha = 0
        return image
    }()
    
//    lazy var sucessProcess3 : UILabel = {
//        let label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.text = "Envio de mensaje WhatsApp 2"
//        label.textColor = GSColorsManager.successColor
//        label.font = UIFont.Montserrat_Regular_14
//        label.alpha = 0
//        return label
//    }()
//
//    lazy var sucessProcessCompleted3 : UILabel = {
//        let label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.text = "Correcto"
//        label.textColor = GSColorsManager.successColor
//        label.font = UIFont.Montserrat_Bold_16
//        label.alpha = 0
//        return label
//    }()
//    
//    private lazy var sucessProcessCompletedIcon3: UIImageView = {
//        let image = UIImageView()
//        image.translatesAutoresizingMaskIntoConstraints = false
//        image.contentMode = .scaleAspectFit
//        image.backgroundColor = .clear
//        image.image = UIImage(named: "success_ic", in: .local_gs_utils, compatibleWith: nil)
//        image.tintColor = GSColorsManager.successColor
//        image.alpha = 0
//        return image
//    }()
    
    public lazy var continueButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("OK", for: UIControl.State.normal)
        button.tintColor = .black
        button.backgroundColor = GSColorsManager.buttonBlueAqua
        button.alpha = 0
        button.isUserInteractionEnabled = false
        button.addTarget(self, action: #selector(self.onClick), for: UIControl.Event.touchUpInside)
        button.layer.cornerRadius = 20
        return button
    }()
    
    
    convenience init(delegate: NotificationSwitchDelegateProtocol) {
        self.init()
        self.delegate = delegate
        setupUIElements()
        setupConstraints()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        simulacrumIcon.addGestureRecognizer(tapGestureRecognizer)
        setTimerToSimulacrum()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func setupUIElements() {
        addSubview(topBackground)
        addSubview(contentView)
        
        [line,titleLabel,parenthesisLeft,parenthesisRight].forEach { (component) in
            topBackground.addSubview(component)
        }
        
        [senderImage,senderNameLabel,progressBarContainer,/*progressBarView,*/receiverImage,receiverNameLabel,successIcon, simulacrumIcon,continueButton].forEach { (component) in
            contentView.addSubview(component)
        }
        self.progressBarContainer.addSubview(self.progressBarView)
        self.progressBarContainer.addSubview(self.progressBarblackColumnsContainerView)
        
        [sucessProcess1,sucessProcess2/*,sucessProcess3*/,subtitleLabel,activateSimulacrumLabel,timerToActivateSimulacrum].forEach { (component) in
            contentView.addSubview(component)
        }
        
        
        [sucessProcessCompleted1,sucessProcessCompleted2/*,sucessProcessCompleted3*/].forEach { (component) in
            contentView.addSubview(component)
        }
        
        [sucessProcessCompletedIcon1,sucessProcessCompletedIcon2/*,sucessProcessCompletedIcon3*/].forEach { (component) in
            contentView.addSubview(component)
        }
        
        
    }
    
    fileprivate func setupConstraints() {
        self.progressBarBlackColumnsWidthAnchor = self.progressBarblackColumnsContainerView.widthAnchor.constraint(equalToConstant: 0)
        
        NSLayoutConstraint.activate([
            topBackground.topAnchor.constraint(equalTo: topAnchor),
            topBackground.leadingAnchor.constraint(equalTo: leadingAnchor),
            topBackground.trailingAnchor.constraint(equalTo: trailingAnchor),
            topBackground.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.2, constant: 0),
            
            line.topAnchor.constraint(equalTo: topBackground.bottomAnchor),
            line.heightAnchor.constraint(equalToConstant: 1),
            line.leadingAnchor.constraint(equalTo: self.leadingAnchor,constant: 55),
            line.trailingAnchor.constraint(equalTo: self.trailingAnchor,constant: -55),
            
            titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor,constant: 38),
            titleLabel.bottomAnchor.constraint(equalTo: line.bottomAnchor,constant: -33),
            
            parenthesisLeft.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor,constant: -33),
            parenthesisLeft.topAnchor.constraint(equalTo: titleLabel.topAnchor),
            parenthesisLeft.bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            
            parenthesisRight.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor,constant: 33),
            parenthesisRight.topAnchor.constraint(equalTo: titleLabel.topAnchor),
            parenthesisRight.bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            
            contentView.topAnchor.constraint(equalTo: line.bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            simulacrumIcon.topAnchor.constraint(equalTo: self.contentView.topAnchor,constant: 30),
            simulacrumIcon.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor,constant: 63),
            
            activateSimulacrumLabel.leadingAnchor.constraint(equalTo: self.simulacrumIcon.trailingAnchor,constant: 30),
            activateSimulacrumLabel.topAnchor.constraint(equalTo: self.simulacrumIcon.topAnchor,constant: 15),
            
            timerToActivateSimulacrum.leadingAnchor.constraint(equalTo: self.activateSimulacrumLabel.trailingAnchor),
            timerToActivateSimulacrum.topAnchor.constraint(equalTo: self.activateSimulacrumLabel.topAnchor),
            
            //self.senderImage.centerYAnchor.constraint(equalTo: self.progressBarContainer.centerYAnchor),
            self.senderImage.topAnchor.constraint(equalTo: simulacrumIcon.bottomAnchor,constant: 15),
            self.senderImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor,constant: 64),
            self.senderImage.heightAnchor.constraint(equalToConstant: 120),
            self.senderImage.widthAnchor.constraint(equalToConstant: 120),
            
            self.senderNameLabel.topAnchor.constraint(equalTo: self.senderImage.bottomAnchor, constant: 20),
            self.senderNameLabel.leadingAnchor.constraint(equalTo: self.senderImage.leadingAnchor),
            self.senderNameLabel.trailingAnchor.constraint(equalTo: self.senderImage.trailingAnchor),
            
            //self.progressBarContainer.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            self.progressBarContainer.centerYAnchor.constraint(equalTo: self.senderImage.centerYAnchor),
            self.progressBarContainer.leadingAnchor.constraint(equalTo: self.senderImage.trailingAnchor, constant: 20),
            self.progressBarContainer.trailingAnchor.constraint(equalTo: self.receiverImage.leadingAnchor, constant: -20),
            
            
            self.progressBarblackColumnsContainerView.leadingAnchor.constraint(equalTo: self.progressBarView.leadingAnchor),
            self.progressBarblackColumnsContainerView.topAnchor.constraint(equalTo: self.progressBarView.topAnchor, constant: -1),
            self.progressBarblackColumnsContainerView.bottomAnchor.constraint(equalTo: self.progressBarView.bottomAnchor, constant: 1),
            self.progressBarBlackColumnsWidthAnchor,
            
            
            self.progressBarView.topAnchor.constraint(equalTo: self.progressBarContainer.topAnchor, constant: 25),
            self.progressBarView.leadingAnchor.constraint(equalTo: self.progressBarContainer.leadingAnchor, constant: 25),
            self.progressBarView.trailingAnchor.constraint(equalTo: self.progressBarContainer.trailingAnchor, constant: -25),
            self.progressBarView.bottomAnchor.constraint(equalTo: self.progressBarContainer.bottomAnchor, constant: -25),
            self.progressBarView.heightAnchor.constraint(equalToConstant: 30),
            
            //self.receiverImage.centerYAnchor.constraint(equalTo: self.progressBarContainer.centerYAnchor),
            self.receiverImage.topAnchor.constraint(equalTo: senderImage.topAnchor),
            self.receiverImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor,constant: -60),
            self.receiverImage.heightAnchor.constraint(equalToConstant: 120),
            self.receiverImage.widthAnchor.constraint(equalToConstant: 120),
            
            self.receiverNameLabel.topAnchor.constraint(equalTo: self.receiverImage.bottomAnchor, constant: 20),
            self.receiverNameLabel.leadingAnchor.constraint(equalTo: self.receiverImage.leadingAnchor),
            self.receiverNameLabel.trailingAnchor.constraint(equalTo: self.receiverImage.trailingAnchor),
            
            self.successIcon.bottomAnchor.constraint(equalTo: self.receiverImage.topAnchor),
            self.successIcon.centerXAnchor.constraint(equalTo: self.receiverImage.centerXAnchor),
            self.successIcon.widthAnchor.constraint(equalToConstant: 35),
            self.successIcon.heightAnchor.constraint(equalToConstant: 35),
            
            self.subtitleLabel.bottomAnchor.constraint(equalTo: self.progressBarContainer.topAnchor,constant: -34),
            self.subtitleLabel.centerXAnchor.constraint(equalTo: self.progressBarContainer.centerXAnchor),
            
            
            self.sucessProcess1.topAnchor.constraint(equalTo: progressBarContainer.bottomAnchor,constant: 53),
            self.sucessProcess1.trailingAnchor.constraint(equalTo: progressBarContainer.centerXAnchor,constant: -18),
            
            self.sucessProcessCompleted1.leadingAnchor.constraint(equalTo: progressBarContainer.centerXAnchor, constant: 18),
            self.sucessProcessCompleted1.topAnchor.constraint(equalTo: self.sucessProcess1.topAnchor),
            
            self.sucessProcessCompletedIcon1.leadingAnchor.constraint(equalTo: self.sucessProcessCompleted1.trailingAnchor,constant: 20),
            self.sucessProcessCompletedIcon1.topAnchor.constraint(equalTo: self.sucessProcessCompleted1.topAnchor),
            self.sucessProcessCompletedIcon1.widthAnchor.constraint(equalToConstant: 20),
            self.sucessProcessCompletedIcon1.heightAnchor.constraint(equalToConstant: 20),
            
            self.sucessProcess2.topAnchor.constraint(equalTo: sucessProcess1.bottomAnchor,constant: 17),
            self.sucessProcess2.leadingAnchor.constraint(equalTo: sucessProcess1.leadingAnchor),
            
            self.sucessProcessCompleted2.leadingAnchor.constraint(equalTo: progressBarContainer.centerXAnchor, constant: 18),
            self.sucessProcessCompleted2.topAnchor.constraint(equalTo: self.sucessProcess2.topAnchor),
            
            self.sucessProcessCompletedIcon2.leadingAnchor.constraint(equalTo: self.sucessProcessCompleted2.trailingAnchor,constant: 20),
            self.sucessProcessCompletedIcon2.topAnchor.constraint(equalTo: self.sucessProcessCompleted2.topAnchor),
            self.sucessProcessCompletedIcon2.widthAnchor.constraint(equalToConstant: 20),
            self.sucessProcessCompletedIcon2.heightAnchor.constraint(equalToConstant: 20),
            
//            self.sucessProcess3.topAnchor.constraint(equalTo: sucessProcess2.bottomAnchor,constant: 17),
//            self.sucessProcess3.leadingAnchor.constraint(equalTo: sucessProcess2.leadingAnchor),
//
//            self.sucessProcessCompleted3.leadingAnchor.constraint(equalTo: progressBarContainer.centerXAnchor, constant: 18),
//            self.sucessProcessCompleted3.topAnchor.constraint(equalTo: self.sucessProcess3.topAnchor),
//
//            self.sucessProcessCompletedIcon3.leadingAnchor.constraint(equalTo: self.sucessProcessCompleted3.trailingAnchor,constant: 20),
//            self.sucessProcessCompletedIcon3.topAnchor.constraint(equalTo: self.sucessProcessCompleted3.topAnchor),
//            self.sucessProcessCompletedIcon3.widthAnchor.constraint(equalToConstant: 20),
//            self.sucessProcessCompletedIcon3.heightAnchor.constraint(equalToConstant: 20),
            
            self.continueButton.centerXAnchor.constraint(equalTo: self.progressBarContainer.centerXAnchor),
            self.continueButton.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -30),
            self.continueButton.widthAnchor.constraint(equalToConstant: 250),
            self.continueButton.heightAnchor.constraint(equalToConstant: 40),
            
            
        ])
        
    }
    
    public func setProgress(progress: Float) {
        var auxProgress = progress
        
        if progress > 1.0 {
            auxProgress = 1.0
        } else if progress < 0 {
            auxProgress = 0
        }
        
        var currentProgress: Float = self.progressBarView.progress
        
        if let timer = self.auxTimer {
            timer.invalidate()
            self.auxTimer = nil
        }
        var progressBarBlackColumnsWidth: CGFloat = 0.0
        
        Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { [self] timer in
            self.auxTimer = timer
            if currentProgress <= auxProgress {
                currentProgress += 0.005
                self.progressBarView.setProgress(currentProgress, animated: true)
                self.subtitleLabel.text = "Swicheo en proceso .... \(Int(currentProgress * 100))%"
                
                progressBarBlackColumnsWidth = CGFloat(Int(self.progressBarView.bounds.width * CGFloat(currentProgress)))
                self.progressBarBlackColumnsWidthAnchor.constant = progressBarBlackColumnsWidth
                self.layoutIfNeeded()
            }else {
                if self.progressBarView.progress >= 1 {
                    continueButton.alpha = 1
                    continueButton.isUserInteractionEnabled = true
                    self.animateSuccess()
                }
                timer.invalidate()
                self.auxTimer = nil
            }
        }
    }
    
    private func setTimerToSimulacrum() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
    }
    
    @objc func timerAction() {
        counter -= 1
        
        if counter > 0 {
            timerToActivateSimulacrum.text = " \(counter) segundos ..."
        } else if counter == 0{
            timerToActivateSimulacrum.text = " \(counter) segundos ..."
            timer.invalidate()
            simulacrumIcon.isUserInteractionEnabled = false
            simulacrumIcon.alpha = 0.5
            timerToActivateSimulacrum.isHidden = true
            activateSimulacrumLabel.isHidden = true
            if sentMessage == false{
                animateProgressbar()
                delegate?.sendMessage(type: .earthqueake)
            }
            
        }
        
    }
    
    private func animateSuccess(){
        UIView.animate(withDuration: 0.3) {
//            self.successIcon.alpha = 1
            self.senderImage.image = UIImage(named: "buildingUnselect", in: .local_gs_utils, compatibleWith: nil)
            self.receiverImage.image = UIImage(named: "buildingSelect", in: .local_gs_utils, compatibleWith: nil)
            self.progressBarView.progressTintColor = GSColorsManager.successColor
            self.subtitleLabel.textColor = GSColorsManager.successColor
            self.layoutIfNeeded()
        }
        setTimerToSimulacrum()
    }
    
    private func animateProgressbar() {
        self.sucessProcess1.alpha = 0
        self.sucessProcessCompleted1.alpha = 0
        self.sucessProcessCompletedIcon1.alpha = 0
        
        self.sucessProcess2.alpha = 0
        self.sucessProcessCompleted2.alpha = 0
        self.sucessProcessCompletedIcon2.alpha = 0
        
        self.setProgress(progress: 0.50)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.setProgress(progress: 0.45)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.setProgress(progress: 0.65)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.setProgress(progress: 1)
//                    self.sucessProcess3.alpha = 1
//                    self.sucessProcessCompleted3.alpha = 1
//                    self.sucessProcessCompletedIcon3.alpha = 1
                }
            }
        }
        
    }
    
    @objc func onClick(){
        UIView.animate(withDuration: 0.5) {
            self.alpha = 0
        } completion: { _ in
            self.delegate?.notifyDismiss()
        }
    }
    
}
