//
//  NotificationSwitchInteranctor.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 30/12/21.
//

import Foundation
import SDKGSServicesManager


class NotificationSwitchInteractor {
    weak var presenter: NotificationSwitchPresenterProtocol?
    private let facade = NOCServicesFacade.sharedRedButton
    
    private func sendMultipleWhatsappMessage(request: SendMessageWhatsRequest){
        self.facade.sendMessageWhats(request: request) { response, _ in
            DispatchQueue.main.async {
                if response?.result == "Ok"{
                    self.presenter?.responseSuccessWhatsapp()
                }else{
                    self.presenter?.responseErrorWhatsapp(msg: "")
                }
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self.presenter?.responseErrorWhatsapp(msg: emptyMessage ?? "")
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self.presenter?.responseErrorWhatsapp(msg: errorMessage ?? "")
            }
        }
    }
}

extension NotificationSwitchInteractor: NotificationSwitchInteractorProtocol {
    func sendTelegramMessage(type: typeOfMovement,message : String) {
        let request = SendMessageTelegramRequest(message: message, chatId: "-1001732208328")
        
        self.facade.sendMessageTelegram(request: request) { response, _ in
            DispatchQueue.main.async {
                if response?.ok == true{
                    self.presenter?.responseSuccessTelegram()
                }else{
                    self.presenter?.responseErrorTelegram(msg: "")
                }
            }
        } empty: { emptyMessage in
            DispatchQueue.main.async {
                self.presenter?.responseErrorTelegram(msg: emptyMessage ?? "")
            }
        } failure: { errorMessage in
            DispatchQueue.main.async {
                self.presenter?.responseErrorTelegram(msg: errorMessage ?? "")
            }
        }
    }
    
    func sendWhatsappMessage(type: typeOfMovement,message : String) {
        let arrayOfNumbers = ["5215588391157"]
        for numbers in arrayOfNumbers {
            sendMultipleWhatsappMessage(request: SendMessageWhatsRequest(message: message, phoneNumber: numbers))
        }
    }
    
}

