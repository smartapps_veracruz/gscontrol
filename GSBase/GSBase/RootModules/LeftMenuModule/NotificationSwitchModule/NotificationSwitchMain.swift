//
//  NotificationSwitchMain.swift
//  GSBase
//
//  Created by Armando Carrillo - EKT on 30/12/21.
//

import UIKit

class NotificationSwitchMain: NSObject {

    static func createModule()->UIViewController{
        
        let viewController  :   NotificationSwitchViewC?   =  NotificationSwitchViewC()
        if let view = viewController {
            let presenter   =   NotificationSwitchPresenter()
            let router      =   NotificationSwitchRouter()
            let interactor  =   NotificationSwitchInteractor()
            
            view.presenter  =   presenter
            
            presenter.view          =   view
            presenter.interactor    =   interactor
            presenter.router        =   router
            
            interactor.presenter    =   presenter
            return view
        }
        return UIViewController()
    }
}
