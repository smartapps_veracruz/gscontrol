//
//  GSFrontViewController.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 22/12/21.
//

import UIKit

class GSFrontViewController: UIViewController {

    lazy var buttonClose: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "close_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
        button.addTarget(self, action: #selector(self.onDissmisAction(_:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        view.addSubview(buttonClose)
        NSLayoutConstraint.activate([
            buttonClose.heightAnchor.constraint(equalToConstant: 45),
            buttonClose.widthAnchor.constraint(equalToConstant: 45),
            buttonClose.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30),
            buttonClose.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30)
        ])
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func onDissmisAction(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}
