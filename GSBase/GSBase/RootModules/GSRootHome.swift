//
//  GSRoorHome.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 21/12/21.
//

import UIKit
import SDKGSCommonUtils

class GSRootHome: UISplitViewController {
    let rootHome = GSHomeMain.createModule()
    let rootLeftMenu = GSLeftMenuMain.createModule()
    
    override func viewDidLoad() {
    
        let navigation = UINavigationController(rootViewController: rootHome)
        navigation.isNavigationBarHidden = true
        navigation.initRootViewController(rootHome)
        navigation.navigationBar.barStyle = .default
        viewControllers = [rootLeftMenu,rootHome]
        preferredPrimaryColumnWidthFraction = 12/100
        view.backgroundColor = GSColorsManager.noneColor
        
    }
}
