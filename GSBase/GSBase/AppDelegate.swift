//
//  AppDelegate.swift
//  GSBase
//
//  Created by Dsi Soporte Tecnico on 19/12/21.
//

import UIKit
import SDKGSCommonUtils
import Firebase
import IQKeyboardManagerSwift


@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        NotificationCenter.default.addObserver(self, selector: #selector(self.listeningRoot), name:  Notification.Name("NotificationIdentifierListeningRoot"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.listeningHome), name:  Notification.Name("NotificationIdentifierListeningHome"), object: nil)
        AppUtility.lockOrientation(.landscapeRight, andRotateTo: .landscapeLeft)
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 100.0
        
        let root = LoginViewMain.createModule()
        let navigation = UINavigationController(rootViewController: root)
        navigation.isNavigationBarHidden = true
        navigation.initRootViewController(root)
        navigation.navigationBar.barStyle = .default
        setRootViewController(navigation, animated: true)
        return true
    }
    
    @objc private func listeningHome(){
        let root = GSRootHome()
        setRootViewController(root, animated: true)
    }
    
    @objc private func listeningRoot(){
        let root = GSOperationMenuMain.createModule()
        let navigation = UINavigationController(rootViewController: root)
        navigation.isNavigationBarHidden = true
        navigation.initRootViewController(root)
        navigation.navigationBar.barStyle = .default
        setRootViewController(navigation, animated: true)
    }
    
    private func setRootViewController(_ vc: UIViewController, animated: Bool = true) {
        guard animated, let window = self.window else {
            self.window?.rootViewController  = nil
            self.window?.rootViewController = vc
            self.window?.backgroundColor = .black
            self.window?.makeKeyAndVisible()
            return
        }
        window.rootViewController  = nil
        window.rootViewController = vc
        window.backgroundColor = .black
        window.makeKeyAndVisible()
        UIView.transition(with: window,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }
    var orientationLock = UIInterfaceOrientationMask.all
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
        return self.orientationLock
    }
    struct AppUtility {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
           // UINavigationController.attemptRotationToDeviceOrientation()
        }
    }
    
}
