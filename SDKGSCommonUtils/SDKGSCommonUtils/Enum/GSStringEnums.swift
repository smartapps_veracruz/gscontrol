//
//  GSStringEnums.swift
//  SDKGSCommonUtils
//
//  Created by Gustavo Tellez on 24/12/21.
//

import Foundation

public enum GSSExceptionStatus{
    case stable
    case caution
    case warning
}
