//
//  GSColors.swift
//  SDKGSCommonUtils
//
//  Created by Dsi Soporte Tecnico on 19/12/21.
//

import UIKit

open class GSColorsManager: NSObject {
    
    public static let primaryColor =  #colorLiteral(red: 0.2235294118, green: 0.5098039216, blue: 0.9490196078, alpha: 1)
    public static let secondaryColor = #colorLiteral(red: 0.3647058824, green: 0.7568627451, blue: 0.2156862745, alpha: 1)
    public static let labelColor =  #colorLiteral(red: 0.2235294118, green: 0.5098039216, blue: 0.9490196078, alpha: 1)
    public static let labelStrongColor = #colorLiteral(red: 0.03137254902, green: 0.537254902, blue: 1, alpha: 1)
    public static let labelLightColor =  #colorLiteral(red: 0.7176470588, green: 0.8666666667, blue: 1, alpha: 1)
    public static let labelExtraLightColor = #colorLiteral(red: 0.5764705882, green: 0.5960784314, blue: 0.937254902, alpha: 1)
    public static let labelGoldColor = #colorLiteral(red: 0.8588235294, green: 0.6039215686, blue: 0, alpha: 1)
    public static let cellLabel = #colorLiteral(red: 0.01960784314, green: 0.937254902, blue: 0.9921568627, alpha: 1)
    public static let noneColor = #colorLiteral(red: 0, green: 0.01568627451, blue: 0.07058823529, alpha: 1)
    public static let buttonSelected = #colorLiteral(red: 0, green: 0.1019607843, blue: 0.3450980392, alpha: 1)
    public static let cell = #colorLiteral(red: 0.07058823529, green: 0.2901960784, blue: 0.8352941176, alpha: 0.2843643087)
    public static let viewBorderColor = #colorLiteral(red: 0.537254902, green: 0.7490196078, blue: 1, alpha: 0.4)
    public static let contingencyViewBorderColor = #colorLiteral(red: 0.7098039216, green: 0.1019607843, blue: 0, alpha: 0.4)
    public static let contingencyLabelColor = #colorLiteral(red: 0.5333333333, green: 0.07450980392, blue: 0, alpha: 1)
    public static let contingencyCellLabelColor = #colorLiteral(red: 0.05098039216, green: 0.5607843137, blue: 0.7215686275, alpha: 1)
    public static let callsBackgroundColor = #colorLiteral(red: 0.1803921569, green: 0.3450980392, blue: 0.03921568627, alpha: 1)
    public static let successColor = #colorLiteral(red: 0.4509803922, green: 0.6470588235, blue: 0.4392156863, alpha: 1)
    public static let borderBlue = #colorLiteral(red: 0.003921568627, green: 0.07058823529, blue: 0.1176470588, alpha: 1)
    public static let buttonBlueAqua = #colorLiteral(red: 0.2588235294, green: 0.5607843137, blue: 0.7176470588, alpha: 1)
    public static let collectionColor = #colorLiteral(red: 0.04302125424, green: 0.06335199624, blue: 0.1219628528, alpha: 1)
    public static let labelDarkColor = #colorLiteral(red: 0.0436213389, green: 0.1047766134, blue: 0.2114592195, alpha: 1)
    public static let labelGrayColor = #colorLiteral(red: 0.5137254902, green: 0.5137254902, blue: 0.5137254902, alpha: 1)
}
