//
//  GSCallingContactsView.swift
//  SDKGSCommonUtils
//
//  Created by Branchbit on 27/12/21.
//

import Foundation
import UIKit

open class GSCallingContactsListView: UIScrollView {
    private var contactsListElements: [CallingContactElement] = []
    
    lazy var contentView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var callingContactsStack: UIStackView = {
       let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.backgroundColor = .clear
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fill
        stack.spacing = 100
        return stack
    }()
    
    public convenience init(contacts: [GSContactEntity]){
        self.init()
        
        
        self.setUI()
        self.setConstraints()
        
        self.setContacts(contacts: contacts)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUI(){
        self.addSubview(self.contentView)
        self.contentView.addSubview(self.callingContactsStack)
    }
    
    func setConstraints(){
        let contentWidthAnchor = self.contentView.widthAnchor.constraint(equalTo: self.widthAnchor)
        contentWidthAnchor.priority = .defaultLow
        NSLayoutConstraint.activate([
            self.contentView.topAnchor.constraint(equalTo: self.topAnchor),
            self.contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.callingContactsStack.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            self.callingContactsStack.leadingAnchor.constraint(greaterThanOrEqualTo: self.contentView.leadingAnchor),
            self.callingContactsStack.trailingAnchor.constraint(lessThanOrEqualTo: self.contentView.trailingAnchor),
            self.callingContactsStack.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            self.callingContactsStack.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            contentWidthAnchor
        ])
    }
    
    public func setContacts(contacts: [GSContactEntity]){
        self.callingContactsStack.arrangedSubviews.forEach { subview in
            self.callingContactsStack.removeArrangedSubview(subview)
        }
        
        for contact in contacts {
            let element = CallingContactElement(contact: contact)
            
            self.callingContactsStack.addArrangedSubview(element)
            self.contactsListElements.append(element)
        }
    }
    public func setTalking(contacts: [GSContactEntity]){
        var auxContactsElementsNotTalking: [CallingContactElement] = self.contactsListElements
        
        for contact in contacts {
            if let number = contact.phoneNumber {
                for element in self.contactsListElements {
                    if let auxContact = element.getContact() {
                        if auxContact.phoneNumber == number {
                            element.setTalking(talking: true)
                            auxContactsElementsNotTalking.removeAll(where: { $0.getContact()?.phoneNumber == auxContact.phoneNumber } )
                            break
                        }
                    }
                }
            }
        }
        
        for auxNotTalking in auxContactsElementsNotTalking {
            auxNotTalking.setTalking(talking: false)
        }
    }
}






internal class CallingContactElement: UIView {
    private var name: String = ""
    private var contact: GSContactEntity?
    
    private let talkingColor = GSColorsManager.callsBackgroundColor
    private let notTalkingOutlineColor = GSColorsManager.labelLightColor
    private let notTalkingFilledColor = UIColor.white.withAlphaComponent(0.18)
 
    lazy var circleOutlineImage: UIImageView = {
       let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.contentMode = .scaleAspectFit
        icon.backgroundColor = .clear
        icon.tintColor = self.notTalkingOutlineColor
        icon.image = UIImage(named: "circleOutline_ic", in: .local_gs_utils, compatibleWith: nil)
        return icon
    }()
    
    lazy var circleFilledImage: UIImageView = {
       let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.contentMode = .scaleAspectFit
        icon.backgroundColor = .clear
        icon.tintColor = self.notTalkingFilledColor
        icon.image = UIImage(named: "circleFilled_ic", in: .local_gs_utils, compatibleWith: nil)
        return icon
    }()
    
    lazy var userIdentifierLetterLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.text = String(self.name.first ?? " ")
        label.textColor = GSColorsManager.labelLightColor
        label.font = .Montserrat_Semibold_40
        return label
    }()
    
    
    lazy var userNameLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.text = self.name
        label.textColor = GSColorsManager.labelLightColor
        label.font = .Montserrat_Regular_14
        return label
    }()
    
    
    public convenience init(contact: GSContactEntity){
        self.init()
        
        self.contact = contact
        self.name = self.contact?.name ?? ""
        
        self.setUI()
        self.setConstraints()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUI(){
        self.addSubview(self.circleOutlineImage)
        self.addSubview(self.circleFilledImage)
        self.addSubview(self.userIdentifierLetterLabel)
        self.addSubview(self.userNameLabel)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.circleOutlineImage.topAnchor.constraint(equalTo: self.topAnchor),
            self.circleOutlineImage.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.circleOutlineImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.circleOutlineImage.widthAnchor.constraint(equalToConstant: 150),
            self.circleOutlineImage.heightAnchor.constraint(equalToConstant: 150),
            
            self.circleFilledImage.centerYAnchor.constraint(equalTo: self.circleOutlineImage.centerYAnchor),
            self.circleFilledImage.centerXAnchor.constraint(equalTo: self.circleOutlineImage.centerXAnchor),
            self.circleFilledImage.widthAnchor.constraint(equalToConstant: 130),
            self.circleFilledImage.heightAnchor.constraint(equalToConstant: 130),
            
            self.userIdentifierLetterLabel.centerYAnchor.constraint(equalTo: self.circleFilledImage.centerYAnchor),
            self.userIdentifierLetterLabel.leadingAnchor.constraint(equalTo: self.circleFilledImage.leadingAnchor),
            self.userIdentifierLetterLabel.trailingAnchor.constraint(equalTo: self.circleFilledImage.trailingAnchor),
            
            self.userNameLabel.topAnchor.constraint(equalTo: self.circleOutlineImage.bottomAnchor, constant: 30),
            self.userNameLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.userNameLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.userNameLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    public func getContact() -> GSContactEntity? {
        return self.contact
    }
    
    public func setTalking(talking: Bool){
        UIView.animate(withDuration: 0.2) {
            self.circleOutlineImage.tintColor = talking ? self.talkingColor : self.notTalkingOutlineColor
            self.circleFilledImage.tintColor = talking ? self.talkingColor : self.notTalkingFilledColor
            self.layoutIfNeeded()
        }
    }
}
