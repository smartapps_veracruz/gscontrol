//
//  GSContactsCheckListView.swift
//  SDKGSCommonUtils
//
//  Created by Branchbit on 27/12/21.
//

import Foundation
import UIKit

public struct GSContactEntity {
    public var name: String?
    public var phoneNumber: String?
    
    public init(name: String, phoneNumber: String){
        self.name = name
        self.phoneNumber = phoneNumber
    }
}

public protocol ContactsCheckListProtocol {
    func notifyContactSelected()
}

open class GSContactsCheckListView: UIScrollView, GSCheckButtonProtocol {
    private var checkListdelegate: ContactsCheckListProtocol?
    private var contactsList: [GSContactEntity] = []
    private var contactsElementsList: [ContactCheckListElement] = []
    
    lazy var contactsStack: UIStackView = {
       let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.backgroundColor = .clear
        stack.axis = .horizontal
        stack.alignment = .center
        stack.spacing = 70
        return stack
    }()
    
    
    public convenience init(contactsList: [GSContactEntity],
                            allChecked: Bool,
                            checkListdelegate: ContactsCheckListProtocol){
        self.init()
        
        self.checkListdelegate = checkListdelegate
        self.contactsList = contactsList
        
        self.setUI()
        self.setConstraints()
        
        self.setContacts(contactsList: self.contactsList, allChecked: allChecked)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUI(){
        self.addSubview(contactsStack)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.contactsStack.topAnchor.constraint(equalTo: self.topAnchor),
            self.contactsStack.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.contactsStack.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.contactsStack.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }
    
    public func setContacts(contactsList: [GSContactEntity], allChecked: Bool){
        self.contactsList = contactsList
        self.contactsStack.arrangedSubviews.forEach { subview in
            self.contactsStack.removeArrangedSubview(subview)
        }
        for contact in contactsList {
            let contactStackElement = ContactCheckListElement(contact: contact, checked: allChecked, delegate: self)
            self.contactsStack.addArrangedSubview(contactStackElement)
            self.contactsElementsList.append(contactStackElement)
        }
    }
    
    public func areAllSelected() -> Bool {
        for element in self.contactsElementsList {
            if !element.checked {
                 return false
            }
        }
        return true
    }
    
    public func areNoneSelected() -> Bool {
        for element in self.contactsElementsList {
            if element.checked {
                 return false
            }
        }
        return true
    }
    
    public func getSelectedContacts() -> [GSContactEntity] {
        var auxSelected: [GSContactEntity] = []
        for element in self.contactsElementsList {
            if element.checked {
                if let contact = element.getContact() {
                    auxSelected.append(contact)
                }
            }
        }
        return auxSelected
    }
    
    public func setAllElementsStatuses(selected: Bool){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            for element in self.contactsElementsList {
                element.checked = selected
            }
        }
    }
    
    public func notifyOnCheckButtonClicked(_ sender: GSCheckButton) {
        self.checkListdelegate?.notifyContactSelected()
    }
}






internal protocol ContactCheckListElementProtocol {
    func notifyCheckListElementSelected()
}

internal class ContactCheckListElement: GSCheckButton {
    internal var contact: GSContactEntity?
    
    public convenience init(contact: GSContactEntity, checked: Bool, delegate: GSCheckButtonProtocol){
        self.init(checked: checked, delegate: delegate)

        self.checked = checked
        self.contact = contact
        self.titleString = self.contact?.name ?? ""
    }
    
    override init(checked: Bool, delegate: GSCheckButtonProtocol) {
        super.init(checked: checked, delegate: delegate)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func getContact() -> GSContactEntity? {
        return self.contact
    }
}
