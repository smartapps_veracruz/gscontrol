//
//  GSGenericFrontView.swift
//  SDKGSCommonUtils
//
//  Created by Branchbit on 22/12/21.
//

import UIKit

public enum GSGenericFrontViewWindowType {
    case contingency
    case notification
    case call
    case calling
    
    func getTitle() -> String {
        switch self {
        case .contingency:
            return "Contingencia"
        case .notification:
            return "Notificaciones"
        case .call:
            return "Llamada"
        case .calling:
            return "Llamada"
        }
    }
    
    func getAppearFromWhere() -> GSGenericFrontViewAppearFromWhere {
        switch self {
        case .contingency:
            return GSGenericFrontViewAppearFromWhere.top
        case .notification:
            return GSGenericFrontViewAppearFromWhere.left
        case .call:
            return GSGenericFrontViewAppearFromWhere.left
        case .calling:
            return GSGenericFrontViewAppearFromWhere.center
        }
    }
    
    func getViewBorderColor() -> UIColor {
        switch self {
        case .contingency:
            return GSColorsManager.contingencyViewBorderColor
        case .notification:
            return GSColorsManager.viewBorderColor
        case .call:
            return GSColorsManager.viewBorderColor
        case .calling:
            return GSColorsManager.viewBorderColor
        }
    }
    func getTitleBorderColor() -> UIColor {
        switch self {
        case .contingency:
            return GSColorsManager.contingencyViewBorderColor.withAlphaComponent(1)
        case .notification:
            return GSColorsManager.viewBorderColor.withAlphaComponent(1)
        case .call:
            return GSColorsManager.viewBorderColor.withAlphaComponent(1)
        case .calling:
            return GSColorsManager.viewBorderColor.withAlphaComponent(1)
        }
    }
    
    func getLabelColor() -> UIColor {
        switch self {
        case .contingency:
            return GSColorsManager.contingencyLabelColor
        case .notification:
            return GSColorsManager.labelColor
        case .call:
            return GSColorsManager.labelColor
        case .calling:
            return GSColorsManager.labelLightColor
        }
    }
    
    func getIcon() -> UIImage {
        switch self {
        case .contingency:
            return UIImage(named: "contingency_ic", in: .local_gs_utils, compatibleWith: nil) ?? UIImage()
        case .notification:
            return UIImage()
        case .call:
            return UIImage(named: "callContainer_ic", in: .local_gs_utils, compatibleWith: nil) ?? UIImage()
        case .calling:
            return UIImage(named: "callContainer_ic", in: .local_gs_utils, compatibleWith: nil) ?? UIImage()
        }
    }
}

public enum GSGenericFrontViewAppearFromWhere {
    case top
    case right
    case center
    case left
    case bottom
}

public protocol GSGenericFrontProtocol {
    func notifyDismiss()
}

open class GSGenericFrontView: UIView {
    private var delegate: GSGenericFrontProtocol?
    private var titleString: String = "Notificaciones"
    private var appearFromWhere: GSGenericFrontViewAppearFromWhere = .left
    private var animationDuration: Double = 0.3
    private var viewType: GSGenericFrontViewWindowType = .notification
    
    private var showDarkBackground: Bool = true
    
    private var cardViewTopAnchor: NSLayoutConstraint = NSLayoutConstraint()
    private var cardViewLeadingAnchor: NSLayoutConstraint = NSLayoutConstraint()
    private var cardViewTrailingAnchor: NSLayoutConstraint = NSLayoutConstraint()
    private var cardViewBottomAnchor: NSLayoutConstraint = NSLayoutConstraint()
    
    private var topConstant: CGFloat = 0
    private var leadingConstant: CGFloat = 0
    private var trailingConstant: CGFloat = 0
    private var bottomConstant: CGFloat = 0

    
    private lazy var darkBackgroundView: UIView = {
        let background = UIView()
        background.translatesAutoresizingMaskIntoConstraints = false
        background.backgroundColor = .clear
        return background
    }()
    
    
    private lazy var cardView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    
    private lazy var buttonClose: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "close_ic", in: .local_gs_utils, compatibleWith: nil), for: .normal)
        button.addTarget(self, action: #selector(self.onDissmisAction(_:)), for: .touchUpInside)
        return button
    }()
    
    
    private lazy var mainContainerView: UIView = {
       let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = .black
        container.layer.cornerRadius = 19
        container.layer.borderWidth = 0.6
        container.layer.borderColor = self.viewType.getViewBorderColor().cgColor
        container.clipsToBounds = true
        
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "background_body_ic", in: .local_gs_utils, compatibleWith: nil)
        
        container.addSubview(image)
        
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: container.topAnchor),
            image.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: container.trailingAnchor),
            image.bottomAnchor.constraint(equalTo: container.bottomAnchor),
        ])
        
        return container
    }()
    
    
    lazy var titleBarContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.clipsToBounds = true
        
        return view
    }()
    
    
    lazy var separatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = self.viewType.getTitleBorderColor()
        
        return view
    }()
    
    
    lazy var titleIconImageview: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.image = self.viewType.getIcon()
        return image
    }()
    
    
    lazy var callPhoneImageView: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.image = UIImage(named: "callPhone_ic", in: .local_gs_utils, compatibleWith: nil)
        image.tintColor = self.viewType == .calling ? GSColorsManager.labelLightColor : .black
        return image
    }()
    
    
    lazy var titleLabelContainerView: UIView = {
        let view = UIView()
        view.layer.borderColor = self.viewType == .contingency ? self.viewType.getLabelColor().cgColor : UIColor.clear.cgColor
        view.layer.borderWidth = self.viewType == .contingency ? 1 : 0
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.textColor = self.viewType.getLabelColor()
        label.font = .Montserrat_Semibold_16
        label.text = self.viewType.getTitle()
        return label
    }()
    
    
    lazy var titleContainer: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        let contingencySubtitleLabel = UILabel()
        contingencySubtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        contingencySubtitleLabel.font = .Montserrat_Regular_16
        contingencySubtitleLabel.textColor = GSColorsManager.contingencyLabelColor
        contingencySubtitleLabel.numberOfLines = 1
        contingencySubtitleLabel.text = "Acciones de Contingencia"
        contingencySubtitleLabel.isHidden = self.viewType != .contingency
        
        view.addSubview(self.titleLabelContainerView)
        
        view.addSubview(contingencySubtitleLabel)
        
        self.titleLabelContainerView.addSubview(self.titleLabel)
        
        if self.viewType != .notification {
            self.titleLabelContainerView.addSubview(self.titleIconImageview)
            if self.viewType == .call || self.viewType == .calling {
                self.titleIconImageview.addSubview(self.callPhoneImageView)
            }
        }
        
        switch self.viewType {
        case .contingency:
            NSLayoutConstraint.activate([
                self.titleIconImageview.centerYAnchor.constraint(equalTo: self.titleLabel.centerYAnchor),
                self.titleIconImageview.widthAnchor.constraint(equalToConstant: 17),
                self.titleIconImageview.heightAnchor.constraint(equalToConstant: 14),
                self.titleIconImageview.trailingAnchor.constraint(equalTo: self.titleLabelContainerView.trailingAnchor, constant: -10),
                
                titleLabel.leadingAnchor.constraint(equalTo: self.titleLabelContainerView.leadingAnchor, constant: 10),
                titleLabel.trailingAnchor.constraint(equalTo: self.titleIconImageview.leadingAnchor, constant: -10),
            ])
            break
            
        case .notification:
            NSLayoutConstraint.activate([
                titleLabel.leadingAnchor.constraint(equalTo: self.titleLabelContainerView.leadingAnchor, constant: 10),
                titleLabel.trailingAnchor.constraint(equalTo: self.titleLabelContainerView.trailingAnchor, constant: -10),
            ])
            break
            
        case .call:
            NSLayoutConstraint.activate([
                self.titleIconImageview.leadingAnchor.constraint(equalTo: self.titleLabelContainerView.leadingAnchor, constant: 10),
                self.titleIconImageview.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
                self.titleIconImageview.widthAnchor.constraint(equalToConstant: 28),
                self.titleIconImageview.heightAnchor.constraint(equalToConstant: 28),
                
                self.callPhoneImageView.topAnchor.constraint(equalTo: self.titleIconImageview.topAnchor, constant: 5),
                self.callPhoneImageView.leadingAnchor.constraint(equalTo: self.titleIconImageview.leadingAnchor, constant: 5),
                self.callPhoneImageView.trailingAnchor.constraint(equalTo: self.titleIconImageview.trailingAnchor, constant: -5),
                self.callPhoneImageView.bottomAnchor.constraint(equalTo: self.titleIconImageview.bottomAnchor, constant: -5),
                
                titleLabel.leadingAnchor.constraint(equalTo: self.titleIconImageview.trailingAnchor, constant: 10),
                titleLabel.trailingAnchor.constraint(equalTo: self.titleLabelContainerView.trailingAnchor, constant: -10),
            ])
            break
            
        case .calling:
            NSLayoutConstraint.activate([
                self.titleIconImageview.leadingAnchor.constraint(equalTo: self.titleLabelContainerView.leadingAnchor, constant: 10),
                self.titleIconImageview.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
                self.titleIconImageview.widthAnchor.constraint(equalToConstant: 28),
                self.titleIconImageview.heightAnchor.constraint(equalToConstant: 28),
                
                self.callPhoneImageView.topAnchor.constraint(equalTo: self.titleIconImageview.topAnchor, constant: 5),
                self.callPhoneImageView.leadingAnchor.constraint(equalTo: self.titleIconImageview.leadingAnchor, constant: 5),
                self.callPhoneImageView.trailingAnchor.constraint(equalTo: self.titleIconImageview.trailingAnchor, constant: -5),
                self.callPhoneImageView.bottomAnchor.constraint(equalTo: self.titleIconImageview.bottomAnchor, constant: -5),
                
                titleLabel.leadingAnchor.constraint(equalTo: self.titleIconImageview.trailingAnchor, constant: 10),
                titleLabel.trailingAnchor.constraint(equalTo: self.titleLabelContainerView.trailingAnchor, constant: -10),
            ])
            break
        }
        
        NSLayoutConstraint.activate([
            self.titleLabelContainerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 5),
            self.titleLabelContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: self.titleLabelContainerView.topAnchor, constant: 4),
            titleLabel.bottomAnchor.constraint(equalTo: self.titleLabelContainerView.bottomAnchor, constant: -4),
            
            contingencySubtitleLabel.centerYAnchor.constraint(equalTo: self.titleLabelContainerView.centerYAnchor),
            contingencySubtitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -90),
        ])
        
        return view
    }()
    
    lazy var titleBarBackground: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 19
        view.backgroundColor = GSColorsManager.callsBackgroundColor.withAlphaComponent(self.viewType == .calling ? 1 : 0)
        return view
    }()
    
    public lazy var contentView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    public convenience init(delegate: GSGenericFrontProtocol,
                            viewType: GSGenericFrontViewWindowType,
                            appearAnimation: GSGenericFrontViewAppearFromWhere? = nil,
                            showDarkBackground: Bool = true,
                            showCloseButton: Bool = true) {
        self.init()
        
        self.viewType = viewType
        if let fromWhereAnimation = appearAnimation {
            self.appearFromWhere = fromWhereAnimation
        }else {
            self.appearFromWhere = self.viewType.getAppearFromWhere()
        }
        
        self.adjustConstants()
        
        self.showDarkBackground = showDarkBackground
        
        self.delegate = delegate
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            self.setUI()
            self.setConstraints()
            self.viewAppearAnimation()
        }
        
        self.buttonClose.isHidden = !showCloseButton
        self.buttonClose.isUserInteractionEnabled = showCloseButton
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setUI(){
        self.addSubview(self.darkBackgroundView)
        self.addSubview(self.cardView)
        
        self.cardView.addSubview(self.mainContainerView)
        self.cardView.addSubview(self.buttonClose)
        
        self.mainContainerView.addSubview(self.contentView)
        
        self.mainContainerView.addSubview(self.titleBarContainerView)
        self.titleBarContainerView.addSubview(self.titleBarBackground)
        self.titleBarBackground.addSubview(self.titleContainer)
        
        self.mainContainerView.addSubview(self.separatorView)
    }
    
    private func setConstraints(){
        self.cardViewTopAnchor = self.cardView.topAnchor.constraint(equalTo: self.topAnchor, constant: self.topConstant)
        self.cardViewLeadingAnchor = self.cardView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: self.leadingConstant)
        self.cardViewTrailingAnchor =  self.cardView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: self.trailingConstant)
        self.cardViewBottomAnchor = self.cardView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: self.bottomConstant)
        
        if self.viewType == .contingency {
            NSLayoutConstraint.activate([
                self.mainContainerView.topAnchor.constraint(equalTo: self.cardView.topAnchor, constant: 30),
                self.mainContainerView.bottomAnchor.constraint(equalTo: self.cardView.bottomAnchor, constant: -40),
                
                self.titleBarContainerView.heightAnchor.constraint(equalToConstant: 60),
                self.titleBarBackground.heightAnchor.constraint(equalToConstant: 60),
                
                self.buttonClose.centerYAnchor.constraint(equalTo: self.titleContainer.centerYAnchor),
                self.buttonClose.trailingAnchor.constraint(equalTo: self.mainContainerView.trailingAnchor, constant: -10),
            ])
        } else {
            NSLayoutConstraint.activate([
                self.mainContainerView.topAnchor.constraint(equalTo: self.cardView.topAnchor, constant: 95),
                self.mainContainerView.bottomAnchor.constraint(equalTo: self.cardView.bottomAnchor, constant: -105),
                
                self.titleBarContainerView.heightAnchor.constraint(equalToConstant: 30),
                self.titleBarBackground.heightAnchor.constraint(equalToConstant: 70),
                
                self.buttonClose.trailingAnchor.constraint(equalTo: self.mainContainerView.trailingAnchor, constant: -10),
                self.buttonClose.bottomAnchor.constraint(equalTo: self.mainContainerView.topAnchor, constant: -5),
            ])
        }
        
        NSLayoutConstraint.activate([
            self.darkBackgroundView.topAnchor.constraint(equalTo: self.topAnchor),
            self.darkBackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.darkBackgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.darkBackgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.cardViewTopAnchor,
            self.cardViewLeadingAnchor,
            self.cardViewTrailingAnchor,
            self.cardViewBottomAnchor,
            
            self.buttonClose.widthAnchor.constraint(equalToConstant: 45),
            self.buttonClose.heightAnchor.constraint(equalToConstant: 45),
            
            self.mainContainerView.leadingAnchor.constraint(equalTo: self.cardView.leadingAnchor, constant: 40),
            self.mainContainerView.trailingAnchor.constraint(equalTo: self.cardView.trailingAnchor, constant: -40),
            
            self.titleBarContainerView.topAnchor.constraint(equalTo: self.mainContainerView.topAnchor, constant: 10),
            self.titleBarContainerView.leadingAnchor.constraint(equalTo: self.mainContainerView.leadingAnchor),
            self.titleBarContainerView.trailingAnchor.constraint(equalTo: self.mainContainerView.trailingAnchor),
            
            self.titleBarBackground.topAnchor.constraint(equalTo: self.titleBarContainerView.topAnchor),
            self.titleBarBackground.leadingAnchor.constraint(equalTo: self.titleBarContainerView.leadingAnchor, constant: 15),
            self.titleBarBackground.trailingAnchor.constraint(equalTo: self.titleBarContainerView.trailingAnchor, constant: -15),
            
            self.titleContainer.centerYAnchor.constraint(equalTo: self.titleBarContainerView.centerYAnchor, constant: self.viewType == .contingency ? 5 : 0),
            self.titleContainer.leadingAnchor.constraint(equalTo: self.titleBarBackground.leadingAnchor),
            self.titleContainer.trailingAnchor.constraint(equalTo: self.titleBarBackground.trailingAnchor),
            
            self.separatorView.topAnchor.constraint(equalTo: self.titleBarContainerView.bottomAnchor, constant: 10),
            self.separatorView.leadingAnchor.constraint(equalTo: self.titleBarBackground.leadingAnchor),
            self.separatorView.trailingAnchor.constraint(equalTo: self.titleBarBackground.trailingAnchor),
            self.separatorView.heightAnchor.constraint(equalToConstant: 0.4),
            
            self.contentView.topAnchor.constraint(equalTo: self.separatorView.bottomAnchor, constant: 15),
            self.contentView.leadingAnchor.constraint(equalTo: self.mainContainerView.leadingAnchor, constant: 15),
            self.contentView.trailingAnchor.constraint(equalTo: self.mainContainerView.trailingAnchor, constant: -15),
            self.contentView.bottomAnchor.constraint(equalTo: self.mainContainerView.bottomAnchor, constant: -15),
        ])
        self.layoutIfNeeded()
    }
    
    private func viewAppearAnimation(){
        UIView.animate(withDuration: self.animationDuration) {
            if self.showDarkBackground {
                self.darkBackgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            }
            switch self.appearFromWhere {
            case .top:
                self.cardViewTopAnchor.constant = 0
                self.cardViewBottomAnchor.constant = 0
                break
                
            case .right:
                self.cardViewLeadingAnchor.constant = 0
                self.cardViewTrailingAnchor.constant = 0
                break
            
            case .center:
                self.cardView.alpha = 1
                break
                
            case .left:
                self.cardViewLeadingAnchor.constant = 0
                self.cardViewTrailingAnchor.constant = 0
                break
                
            case .bottom:
                self.cardViewTopAnchor.constant = 0
                self.cardViewBottomAnchor.constant = 0
                break
            }
            self.layoutIfNeeded()
        }
    }
    
    private func adjustConstants(){
        switch self.appearFromWhere {
        case .top:
            self.topConstant = -UIScreen.main.bounds.height
            self.leadingConstant = 0
            self.trailingConstant = 0
            self.bottomConstant = -UIScreen.main.bounds.height
            break
            
        case .right:
            self.topConstant = 0
            self.leadingConstant = UIScreen.main.bounds.width
            self.trailingConstant = UIScreen.main.bounds.width
            self.bottomConstant = 0
            break
        
        case .center:
            self.topConstant = 0
            self.leadingConstant = 0
            self.trailingConstant = 0
            self.bottomConstant = 0
            
            self.cardView.alpha = 0
            break
            
        case .left:
            self.topConstant = 0
            self.leadingConstant = -UIScreen.main.bounds.width
            self.trailingConstant = -UIScreen.main.bounds.width
            self.bottomConstant = 0
            break
            
        case .bottom:
            self.topConstant = UIScreen.main.bounds.height
            self.leadingConstant = 0
            self.trailingConstant = 0
            self.bottomConstant = UIScreen.main.bounds.height
            break
        }
    }
    
    public func viewDismissAnimation(){
        DispatchQueue.main.async {
            UIView.animate(withDuration: self.animationDuration) {
                if self.showDarkBackground {
                    self.darkBackgroundView.backgroundColor = UIColor.black.withAlphaComponent(0)
                }
                
                switch self.appearFromWhere {
                case .top:
                    self.cardViewTopAnchor.constant = -UIScreen.main.bounds.height
                    self.cardViewBottomAnchor.constant = -UIScreen.main.bounds.height
                    break
                    
                case .right:
                    self.cardViewLeadingAnchor.constant = UIScreen.main.bounds.width
                    self.cardViewTrailingAnchor.constant = UIScreen.main.bounds.width
                    break
                    
                case .center:
                    self.cardView.alpha = 0
                    break
                    
                case .left:
                    self.cardViewLeadingAnchor.constant = -UIScreen.main.bounds.width
                    self.cardViewTrailingAnchor.constant = -UIScreen.main.bounds.width
                    break
                    
                case .bottom:
                    self.cardViewTopAnchor.constant = UIScreen.main.bounds.height
                    self.cardViewBottomAnchor.constant = UIScreen.main.bounds.height
                    break
                }
                self.layoutIfNeeded()
            } completion: { completion in
                self.delegate?.notifyDismiss()
            }
        }
    }
    
    @objc func onDissmisAction(_ sender: UIButton){
        self.viewDismissAnimation()
    }
}
