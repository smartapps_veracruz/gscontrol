//
//  GSExceptionStatusCollectionCell.swift
//  SDKGSCommonUtils
//
//  Created by Gustavo Tellez on 24/12/21.
//

import UIKit

public class GSExceptionStatusCollectionCell: UICollectionViewCell {
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = GSColorsManager.cell
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var imgStatus: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    public static let identifier  = "GSExceptionStatusCollectionCell"
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildUIElements()
        buildConstraints()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildUIElements(){
        self.contentView.addSubview(containerView)
        containerView.addSubview(imgStatus)
    }
    
    private func buildConstraints(){
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10.0),
            containerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -5.0),
            containerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 5.0),
            containerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10.0),
            
            imgStatus.heightAnchor.constraint(equalToConstant: 15.0),
            imgStatus.widthAnchor.constraint(equalToConstant: 15.0),
            imgStatus.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            imgStatus.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
        ])
    }
    
    public func updateStatus(to status: String?){
        
        guard let exceptionStatus = status else{
            imgStatus.image = UIImage(named: "icon_warning", in: .local_gs_utils, compatibleWith: nil)
            return
        }
        
        if exceptionStatus == "OK"{
            imgStatus.image = UIImage(named: "icon_lightBlueCheck", in: .local_gs_utils, compatibleWith: nil)
            
        }else if exceptionStatus == "WARN"{
            imgStatus.image = UIImage(named: "icon_caution", in: .local_gs_utils, compatibleWith: nil)
            
        }else if exceptionStatus == "ERROR"{
            imgStatus.image = UIImage(named: "icon_warning", in: .local_gs_utils, compatibleWith: nil)
            
        }else if exceptionStatus == "NOTEXIST" || exceptionStatus == "PENDING"{
            imgStatus.image = UIImage(named: "icon_information", in: .local_gs_utils, compatibleWith: nil)
            
        }else{
            imgStatus.image = UIImage(named: "icon_noneStatus", in: .local_gs_utils, compatibleWith: nil)
        }
    }
}
