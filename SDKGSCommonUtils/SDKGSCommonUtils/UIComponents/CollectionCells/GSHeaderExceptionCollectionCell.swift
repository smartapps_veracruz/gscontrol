//
//  GSExceptionCollectionCell.swift
//  SDKGSCommonUtils
//
//  Created by Gustavo Tellez on 24/12/21.
//

import UIKit

public class GSHeaderExceptionCollectionCell: UICollectionViewCell {
    
    public lazy var lbName: UILabel = {
        let lb = UILabel()
        lb.textAlignment = .center
        lb.numberOfLines = 2
        lb.font = UIFont.Montserrat_Regular_12
        lb.textColor = GSColorsManager.labelLightColor
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    public static let identifier  = "GSExceptionCollectionCell"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildUIElements()
        buildConstraints()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildUIElements(){
        self.contentView.addSubview(lbName)
    }
    
    private func buildConstraints(){
        NSLayoutConstraint.activate([
            lbName.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            lbName.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -5.0),
            lbName.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 5.0),
            lbName.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
        ])
    }
}
