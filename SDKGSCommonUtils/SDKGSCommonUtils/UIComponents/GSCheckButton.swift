//
//  GSCheckButton.swift
//  SDKGSCommonUtils
//
//  Created by Branchbit on 30/12/21.
//
import UIKit

public protocol GSCheckButtonProtocol {
    func notifyOnCheckButtonClicked(_ sender: GSCheckButton)
}

open class GSCheckButton: UIView {
    private var delegate: GSCheckButtonProtocol?
    
    public var id: String = ""
    
    public var titleString: String = "" {
        didSet {
            self.titleLabel.text = self.titleString
            self.refresh()
        }
    }
    
    public var checkedIcon: UIImage = UIImage(named: "success_ic", in: .local_gs_utils, compatibleWith: nil) ?? UIImage() {
        didSet {
            self.refresh()
        }
    }
    public var uncheckedIcon: UIImage = UIImage(named: "circle_ic", in: .local_gs_utils, compatibleWith: nil) ?? UIImage() {
        didSet {
            self.refresh()
        }
    }
    
    public var checkedColor: UIColor = GSColorsManager.successColor {
        didSet {
            self.refresh()
        }
    }
    public var uncheckedColor: UIColor = GSColorsManager.labelLightColor {
        didSet {
            self.refresh()
        }
    }
    
    public var checked: Bool = true {
        didSet {
            self.refresh()
        }
    }
    
    
    lazy var checkContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var checkIcon: UIImageView = {
        let icon = UIImageView()
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.backgroundColor = .clear
        icon.tintColor = self.checked ? self.checkedColor : self.uncheckedColor
        icon.image = self.checked ? self.checkedIcon : self.uncheckedIcon
        icon.contentMode = .scaleAspectFit
        return icon
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = self.titleString
        label.textColor = self.checked ? self.checkedColor : self.uncheckedColor
        label.font = .Montserrat_Regular_14
        return label
    }()
    
    
//    public convenience init(){
//        self.init()
//        
//
//    }
    public init(checked: Bool,
                delegate: GSCheckButtonProtocol) {
        self.init()
        
        self.delegate = delegate
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onSelected))
        tapGesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapGesture)
        self.isUserInteractionEnabled = true
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            self.checked = checked
            self.setUI()
            self.setConstraints()
        }
    }
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUI(){
        self.addSubview(self.checkContainer)
        self.checkContainer.addSubview(self.checkIcon)
        self.checkContainer.addSubview(self.titleLabel)
    }
    
    func setConstraints(){
        NSLayoutConstraint.activate([
            self.checkContainer.topAnchor.constraint(equalTo: self.topAnchor),
            self.checkContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.checkContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.checkContainer.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.checkIcon.topAnchor.constraint(equalTo: self.checkContainer.topAnchor),
            self.checkIcon.leadingAnchor.constraint(equalTo: self.checkContainer.leadingAnchor),
            self.checkIcon.bottomAnchor.constraint(equalTo: self.checkContainer.bottomAnchor),
            self.checkIcon.widthAnchor.constraint(equalToConstant: 20),
            self.checkIcon.heightAnchor.constraint(equalToConstant: 20),
            
            self.titleLabel.leadingAnchor.constraint(equalTo: self.checkIcon.trailingAnchor, constant: 15),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.checkContainer.trailingAnchor),
            self.titleLabel.centerYAnchor.constraint(equalTo: self.checkIcon.centerYAnchor)
        ])
    }
    
    private func refresh() {
        UIView.animate(withDuration: 0.2) {
            self.titleLabel.textColor = self.checked ? self.checkedColor : self.uncheckedColor
            self.checkIcon.tintColor = self.checked ? self.checkedColor : self.uncheckedColor
            self.checkIcon.image = self.checked ? self.checkedIcon : self.uncheckedIcon
            self.layoutIfNeeded()
        }
    }
    
    @objc func onSelected(){
        self.checked = !self.checked
        self.delegate?.notifyOnCheckButtonClicked(self)
    }
}
