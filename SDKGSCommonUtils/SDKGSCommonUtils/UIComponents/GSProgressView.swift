//
//  GSProgressView.swift
//  SDKGSCommonUtils
//
//  Created by Branchbit on 30/12/21.
//

import Foundation
import UIKit
open class eee: UIView {
    private lazy var darkBackgroundView: UIView = {
        let background = UIView()
        background.translatesAutoresizingMaskIntoConstraints = false
        background.backgroundColor = .clear
        return background
    }()
    
    private lazy var mainContainerView: UIView = {
       let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = .black
        container.layer.cornerRadius = 21
        container.layer.borderWidth = 2
        container.layer.borderColor = GSColorsManager.viewBorderColor.withAlphaComponent(0.2).cgColor
        container.clipsToBounds = true
        
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "background_top_ic", in: .local_gs_utils, compatibleWith: nil)
        
        container.addSubview(image)
        
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: container.topAnchor),
            image.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: container.trailingAnchor),
            image.heightAnchor.constraint(equalToConstant: 150),
        ])
        
        return container
    }()
    
    
    private lazy var titleContainerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        let separator = UIView()
        separator.translatesAutoresizingMaskIntoConstraints = false
        separator.backgroundColor = GSColorsManager.viewBorderColor.withAlphaComponent(1)
        
        view.addSubview(separator)
        
        NSLayoutConstraint.activate([
            separator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            separator.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            separator.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            separator.heightAnchor.constraint(equalToConstant: 1)
        ])
        
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Bold_18
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        return label
    }()
    
    
    private lazy var contentView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .Montserrat_Bold_18
        label.textColor = GSColorsManager.contingencyCellLabelColor
        label.numberOfLines = 3
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.6
        return label
    }()
    
    
    private lazy var senderImage: UIImageView = {
       let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .white.withAlphaComponent(0.1)
        return image
    }()
    private lazy var senderNameLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = GSColorsManager.buttonBlueAqua
        label.font = .Montserrat_Bold_18
        return label
    }()
    
    private lazy var progressBarContainer: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.layer.cornerRadius = 12
        view.layer.borderColor = GSColorsManager.buttonBlueAqua.cgColor
        view.layer.borderWidth = 1
        return view
    }()
    
    private lazy var progressBarView: UIProgressView = {
       let progress = UIProgressView()
        progress.translatesAutoresizingMaskIntoConstraints = false
        return progress
    }()
    
    private lazy var receiverImage: UIImageView = {
       let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.backgroundColor = .white.withAlphaComponent(0.1)
        return image
    }()
    private lazy var receiverNameLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = GSColorsManager.buttonBlueAqua
        label.font = .Montserrat_Bold_18
        return label
    }()
    
    
    public convenience init(serviceName: String, from: String, to: String) {
        self.init()

        self.titleLabel.text = serviceName
        self.senderNameLabel.text = from
        self.subtitleLabel.text = "\(serviceName) en proceso ... 10%"
        self.receiverNameLabel.text = to
        self.progressBarView.progress = 0.3
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setUI(){
        self.addSubview(self.darkBackgroundView)
        self.addSubview(self.mainContainerView)
        
        self.mainContainerView.addSubview(self.titleContainerView)
        
        self.titleContainerView.addSubview(self.titleLabel)
        
        self.mainContainerView.addSubview(self.contentView)
        
        self.contentView.addSubview(self.subtitleLabel)
        
        self.contentView.addSubview(self.senderImage)
        self.contentView.addSubview(self.senderNameLabel)
        
        self.contentView.addSubview(self.progressBarContainer)
        self.progressBarContainer.addSubview(self.progressBarView)
        
        self.contentView.addSubview(self.receiverImage)
        self.contentView.addSubview(self.receiverNameLabel)
    }
    
    private func setConstraints(){
        NSLayoutConstraint.activate([
            self.darkBackgroundView.topAnchor.constraint(equalTo: self.topAnchor),
            self.darkBackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.darkBackgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.darkBackgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            self.mainContainerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 100),
            self.mainContainerView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -100),
            self.mainContainerView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 40),
            self.mainContainerView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -40),
            
            self.titleContainerView.topAnchor.constraint(equalTo: self.mainContainerView.topAnchor),
            self.titleContainerView.leadingAnchor.constraint(equalTo: self.mainContainerView.leadingAnchor, constant: 40),
            self.titleContainerView.trailingAnchor.constraint(equalTo: self.mainContainerView.trailingAnchor, constant: -40),
            self.titleContainerView.heightAnchor.constraint(equalToConstant: 130),
            
            self.titleLabel.centerYAnchor.constraint(equalTo: self.titleContainerView.centerYAnchor),
            self.titleLabel.leadingAnchor.constraint(equalTo: self.titleContainerView.leadingAnchor),
            self.titleLabel.trailingAnchor.constraint(equalTo: self.titleContainerView.trailingAnchor),
            
            self.contentView.topAnchor.constraint(equalTo: self.titleContainerView.bottomAnchor, constant: 20),
            self.contentView.leadingAnchor.constraint(equalTo: self.mainContainerView.leadingAnchor, constant: 60),
            self.contentView.trailingAnchor.constraint(equalTo: self.mainContainerView.trailingAnchor, constant: -60),
            self.contentView.bottomAnchor.constraint(equalTo: self.mainContainerView.bottomAnchor, constant: -60),
            
            
            self.subtitleLabel.bottomAnchor.constraint(equalTo: self.progressBarContainer.topAnchor, constant: -30),
            self.subtitleLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            self.subtitleLabel.trailingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            
            self.senderImage.centerYAnchor.constraint(equalTo: self.progressBarContainer.centerYAnchor),
            self.senderImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            self.senderImage.trailingAnchor.constraint(equalTo: self.progressBarContainer.leadingAnchor, constant: -20),
            self.senderImage.heightAnchor.constraint(equalToConstant: 50),
            
            self.senderNameLabel.topAnchor.constraint(equalTo: self.senderImage.bottomAnchor, constant: 20),
            self.senderNameLabel.leadingAnchor.constraint(equalTo: self.senderImage.leadingAnchor),
            self.senderNameLabel.trailingAnchor.constraint(equalTo: self.senderImage.trailingAnchor),
            
            self.progressBarContainer.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
            self.progressBarContainer.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            self.progressBarContainer.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            
            self.progressBarView.topAnchor.constraint(equalTo: self.progressBarContainer.topAnchor, constant: 20),
            self.progressBarView.leadingAnchor.constraint(equalTo: self.progressBarContainer.leadingAnchor, constant: 20),
            self.progressBarView.trailingAnchor.constraint(equalTo: self.progressBarContainer.trailingAnchor, constant: -20),
            self.progressBarView.bottomAnchor.constraint(equalTo: self.progressBarContainer.bottomAnchor, constant: -20),
            self.progressBarView.heightAnchor.constraint(equalToConstant: 100),
            
            self.receiverImage.centerYAnchor.constraint(equalTo: self.progressBarContainer.centerYAnchor),
            self.receiverImage.leadingAnchor.constraint(equalTo: self.progressBarContainer.trailingAnchor, constant: 20),
            self.receiverImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            self.receiverImage.heightAnchor.constraint(equalToConstant: 50),
            
            self.receiverNameLabel.topAnchor.constraint(equalTo: self.receiverImage.bottomAnchor, constant: 20),
            self.receiverNameLabel.leadingAnchor.constraint(equalTo: self.receiverImage.leadingAnchor),
            self.receiverNameLabel.trailingAnchor.constraint(equalTo: self.receiverImage.trailingAnchor),
        ])
    }
}
