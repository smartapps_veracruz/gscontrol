//
//  UIStackView+Extension.swift
//  SDKGSCommonUtils
//
//  Created by Gustavo Tellez on 03/01/22.
//

import UIKit

public extension UIStackView {
    
    func removeAllArrangedSubviews() {
        arrangedSubviews.forEach {
            self.removeArrangedSubview($0)
            NSLayoutConstraint.deactivate($0.constraints)
            $0.removeFromSuperview()
        }
    }
}
