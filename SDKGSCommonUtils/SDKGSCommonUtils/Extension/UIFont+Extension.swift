//
//  UIFont+Extension.swift
//  SDKGSCommonUtils
//
//  Created by Dsi Soporte Tecnico on 19/12/21.
//

import UIKit

public extension UIFont {

    private static var fontsRegistered: Bool = false

    static func registerFontsIfNeeded() {
        guard
            !fontsRegistered,
            let fontURLs = Bundle.local_gs_utils.urls(forResourcesWithExtension: "ttf", subdirectory: nil)
        else { return }
        
        fontURLs.forEach({ CTFontManagerRegisterFontsForURL($0 as CFURL, .process, nil) })
        fontsRegistered = true
    }
    
    class var Montserrat_Bold_18 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Bold", size: 18.0) ?? UIFont.systemFont(ofSize: 18.0)
    }
    class var Montserrat_Bold_16 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Bold", size: 16.0) ?? UIFont.systemFont(ofSize: 16.0)
    }
    
    class var Montserrat_Semibold_40 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Semibold", size: 40.0) ?? UIFont.systemFont(ofSize: 40.0)
    }
    class var Montserrat_Semibold_20 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Semibold", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
    }
    class var Montserrat_Semibold_16 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Semibold", size: 16.0) ?? UIFont.systemFont(ofSize: 16.0)
    }
    class var Montserrat_Semibold_14 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Semibold", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
    }
    
    class var Montserrat_Medium_20 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Medium", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
    }
    class var Montserrat_Medium_16 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Medium", size: 16.0) ?? UIFont.systemFont(ofSize: 16.0)
    }
    class var Montserrat_Medium_12 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Medium", size: 12.0) ?? UIFont.systemFont(ofSize: 12.0)
    }
    
    class var Montserrat_Regular_20 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Regular", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
    }
    class var Montserrat_Regular_16 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Regular", size: 16.0) ?? UIFont.systemFont(ofSize: 16.0)
    }
    class var Montserrat_Regular_14 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Regular", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
    }
    class var Montserrat_Regular_12 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Regular", size: 12.0) ?? UIFont.systemFont(ofSize: 12.0)
    }
    class var Montserrat_Regular_9 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Regular", size: 9.0) ?? UIFont.systemFont(ofSize: 14.0)
    }
    
    class var Montserrat_Regular_10 : UIFont {
        registerFontsIfNeeded()
        return UIFont(name: "Montserrat-Regular", size: 10.0) ?? UIFont.systemFont(ofSize: 14.0)
    }
    /*  ----------------------------------------- **/
}
