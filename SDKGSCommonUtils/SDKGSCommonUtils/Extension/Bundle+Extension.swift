//
//  Bundle+Extension.swift
//  SDKGSCommonUtils
//
//  Created by Dsi Soporte Tecnico on 28/02/22.
//

import UIKit

public extension Bundle {
    static let local_gs_utils: Bundle = Bundle.init(for: GSColorsManager.self)
}
