//
//  String+Extension.swift
//  SDKGSCommonUtils
//
//  Created by Dsi Soporte Tecnico on 21/12/21.
//

import UIKit

public extension String {
    
    func decorative(color: UIColor,
                    font: UIFont,
                    spacing: Double = 0,
                    lineBreakMode: NSLineBreakMode = .byWordWrapping)->NSMutableAttributedString{
       
        let attibute = NSMutableParagraphStyle()
        attibute.alignment = .left
        attibute.lineBreakMode = lineBreakMode
        let attributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.font : font,
                NSAttributedString.Key.paragraphStyle : attibute
        ]
        let stringAttribute = NSMutableAttributedString(string: self, attributes: attributes)
        stringAttribute.addAttributes([.kern : spacing], range: NSRange(location: 0, length: stringAttribute.length - 1))
        return stringAttribute
    }
    
    func floatValue() -> Float? {
        return Float(self)
    }
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
}
