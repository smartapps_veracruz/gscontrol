//
//  MoviesServicesFacade.swift
//  SDKRMoviesServices
//
//  Created by Dsi Soporte Tecnico on 10/12/21.
//

import UIKit
import SDKGSCommonUtils

open class NOCServicesFacade {
    private var contingencyQuery : CCContingencyQueryService?
    private var contingencyUpdate : CCContingencyUpdateService?
    private var panicButtonQuery : PanicButtonQueryService?
    private var panicButtonUpdate : PanicButtonUpdateService?
    private var sendMessageTelegram : SendMessageTelegramService?
    private var sendMessageWhats : SendMessageWhatsService?
    private var listBackendStatus : ListBackendStatusService?
    private var listFrontStatus : ListFrontStatusService?
    private var listMiddlewareStatus : ListMiddlewareStatusService?
    private var listDocumentsByCategory : ListDocumentsByCategoryService?
    private var listCategory : ListCategoriesService?
    private var documentDetail : DocumentDetailService?
    private var listArquitecture : ListArquitecturesService?
    private var arquitectureDetail : ArquitectureDetailService?
    private var searchDocument : SearchDocumentService?
    private var listSplunk : ListSplunkService?
    private var listOEM : ListOEMService?
    
    public static let sharedHome = NOCServicesFacade(homeModules: true)
    public static let sharedRedButton = NOCServicesFacade(redButtonModules: true)
    public static let sharedContingency = NOCServicesFacade(contingencyModules: true)
    public static let sharedNocTeca = NOCServicesFacade(nocTecaModules: true)
    
    private init(homeModules: Bool = true){
        listBackendStatus = ListBackendStatusService()
        listFrontStatus = ListFrontStatusService()
        listMiddlewareStatus = ListMiddlewareStatusService()
        listSplunk = ListSplunkService()
        listOEM = ListOEMService()
    }
    
    private init(redButtonModules: Bool = true){
        
        sendMessageTelegram = SendMessageTelegramService()
        sendMessageWhats = SendMessageWhatsService()
    }
    
    private init(contingencyModules: Bool = true){
        contingencyQuery = CCContingencyQueryService()
        contingencyUpdate = CCContingencyUpdateService()
        panicButtonQuery = PanicButtonQueryService()
        panicButtonUpdate = PanicButtonUpdateService()
    }
    
    private init(nocTecaModules: Bool = true){
        listDocumentsByCategory = ListDocumentsByCategoryService()
        listCategory = ListCategoriesService()
        documentDetail = DocumentDetailService()
        listArquitecture = ListArquitecturesService()
        arquitectureDetail = ArquitectureDetailService()
        searchDocument = SearchDocumentService()
    }
    //Desborde de llamadas en IVR Residencial
    //*Lista de estatus por opción
    public func queryCallCenterContingency(request: CCContingencyQueryServiceProtocol.Request,
                                           success: @escaping (CCContingencyQueryServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){
        contingencyQuery?.post(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    //Desborde de llamadas en IVR Residencial
    //*Actualizar la opciion de desborde
    public func updateCallCenterContingency(request: CCContingencyUpdateServiceProtocol.Request,
                                           success: @escaping (CCContingencyUpdateServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){
        contingencyUpdate?.post(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func queryPanicButton(request: PanicButtonQueryServiceProtocol.Request,
                                           success: @escaping (PanicButtonQueryServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){
        panicButtonQuery?.post(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func updatePanicButton(request: PanicButtonUpdateServiceProtocol.Request,
                                           success: @escaping (PanicButtonUpdateServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){
        panicButtonUpdate?.post(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func sendMessageWhats(request: SendMessageWhatsServiceProtocol.Request,
                                           success: @escaping (SendMessageWhatsServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){
        sendMessageWhats?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func sendMessageTelegram(request: SendMessageTelegramServiceProtocol.Request,
                                           success: @escaping (SendMessageTelegramServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){
        sendMessageTelegram?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    
    public func fetchListFrontStatus(request: ListFrontStatusServiceProtocol.Request,
                                           success: @escaping (ListFrontStatusServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){
        listFrontStatus?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func fetchListBackendStatus(request: ListBackendStatusServiceProtocol.Request,
                                           success: @escaping (ListBackendStatusServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){
        listBackendStatus?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func fetchListMiddlewareStatus(request: ListMiddlewareStatusServiceProtocol.Request,
                                           success: @escaping (ListMiddlewareStatusServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){
        listMiddlewareStatus?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func fetchListCategory(request: ListCategoriesServiceProtocol.Request,
                                           success: @escaping (ListCategoriesServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){

        listCategory?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func fetchListDocumentByCategory(request: ListDocumentsByCategoryServiceProtocol.Request,
                                           success: @escaping (ListDocumentsByCategoryServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){

        listDocumentsByCategory?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func fetchDocumentDetail(request: DocumentDetailServiceProtocol.Request,
                                           success: @escaping (DocumentDetailServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){

        documentDetail?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func fetchListArquitectures(request: ListArquitecturesServiceProtocol.Request,
                                           success: @escaping (ListArquitecturesServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){

        listArquitecture?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func fetchArquitectureDetail(request: ArquitectureDetailServiceProtocol.Request,
                                           success: @escaping (ArquitectureDetailServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){

        arquitectureDetail?.fetch(params: request,
        success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func searchDocument(request: SearchDocumentServiceProtocol.Request,
                                           success: @escaping (SearchDocumentServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){

        searchDocument?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }
    
    public func listSplunk(request: ListSplunkServiceProtocol.Request,
                                           success: @escaping (ListSplunkServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){

        listSplunk?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            let filterSplunk = responseData?.salesforce?.listGraph?.filter({ element in
                return element.trace16175 != nil
            })
            let lastValue = responseData?.salesforce?.skip == nil ? filterSplunk?.last?.trace16175 :  responseData?.salesforce?.skip
            let saleForce = ListSplunkResponse.salesforceInfo(listGraph: filterSplunk, skip: lastValue)
            let rebuild = ListSplunkResponse(salesforce: saleForce, internetTrace: responseData?.internetTrace, dataCenter: responseData?.dataCenter)
            success(rebuild, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }

    public func listOEM(request: ListOEMServiceProtocol.Request,
                                           success: @escaping (ListOEMServiceProtocol.Response, String?)->Void,
                                           empty: @escaping (String?)->Void,
                                           failure: @escaping (String?) -> Void){

        listOEM?.fetch(params: request, success: { responseData, responseCode, responseMessage in
            success(responseData, responseMessage)
        }, empty: { responseMessage in
            empty(responseMessage)
        }, failure: { responseMessage in
            failure(responseMessage)
        })
    }

}
