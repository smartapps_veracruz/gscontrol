//
//  SendMessageTelegramRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct SendMessageWhatsRequest: Encodable {

    public var message : String?
    public var phoneNumber : String?
    
    public init(message: String, phoneNumber: String) {
        self.message = message
        self.phoneNumber = phoneNumber
    }
    
    enum CodingKeys: String, CodingKey {
        
        case message = "message"
        case phoneNumber = "phoneNumber"
    }
}
