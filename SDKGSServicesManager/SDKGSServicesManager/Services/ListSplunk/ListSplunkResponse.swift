//
//  ListSplunkResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 23/02/22.
//

import UIKit

public struct ListSplunkResponse: Decodable {

    public let salesforce: salesforceInfo?
    public let internetTrace: internetTraceInfo?
    public let dataCenter: dataCenterInfo?
    
    enum CodingKeys: String, CodingKey {
        
        case salesforce    =   "salesforceInfo"
        case internetTrace =   "internetTraceInfo"
        case dataCenter    =   "dataCenterInfo"
    }
    
    public struct salesforceInfo: Decodable {

        public let listGraph: [listGraphInfo]?
        public let skip: Int?
        
        enum CodingKeys: String, CodingKey {
            
            case listGraph    =   "listGraphInfo"
            case skip        = "skip"
        }
    }
    public struct internetTraceInfo: Decodable {

        public let listGraphInfo: [skipInfo]?
        public let skip: skipInfo?
        
        enum CodingKeys: String, CodingKey {
            
            case listGraphInfo    =   "listGraphInfo"
            case skip    =   "skipInfo"
        }
    }
    public struct dataCenterInfo: Decodable {

        public let listGraphInfo: [skipInfo]?
        public let skip: skipInfo?
        
        enum CodingKeys: String, CodingKey {
            
            case listGraphInfo    =   "listGraphInfo"
            case skip    =   "skipInfo"
        }
    }

}

public struct skipInfo: Decodable {
    public let sourceType: String?
    public let skip: Int?
    public let ipFrom : String?
    public let ipTo : String?
    
    enum CodingKeys: String, CodingKey {
        
        case sourceType   =   "sourceType"
        case skip         =   "skip"
        case ipFrom       =   "ipFrom"
        case ipTo         =   "ipTo"
    }
}

public struct listGraphInfo: Decodable {
    public let dataTime: String?
    public let trace16175: Int?
    public let trace1781 : Int?
    public let trace1882 : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case dataTime       =   "dataTime"
        case trace16175     =   "trace16175"
        case trace1781      =   "trace1781"
        case trace1882      =   "trace1882"
    }
}
