//
//  CCContingencyGetResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit


public struct ListFrontStatusResponse: Decodable {
    
    public let dashboard    :   _status?
    public let ffm          :   _status?
    public let mesaControl  :   _status?
    public let hogarSeguro  :   _status?
    public let totalshop    :   _status?
    public let portales     :   _status?
    public let crm          :   _status?
    public let qliksense    :   _status?
    public let salesforce   :   _status?
   
    enum CodingKeys: String, CodingKey {
        
        case dashboard      =   "dashboard"
        case ffm            =   "ffm"
        case mesaControl    =   "mesaControl"
        case hogarSeguro    =   "hogarSeguro"
        case totalshop      =   "totalshop"
        case portales       =   "portales"
        case crm            =   "crm"
        case qliksense      =   "qliksense"
        case salesforce     =   "salesforce"
    }

    public struct _status: Decodable{
        
        public let memoria          :   String?
        public let stuckThread      :   String?
        public let brokenPipe       :   String?
        public let timeOut          :   String?
        public let sqlException     :   String?
        public let dataSource       :   String?
        public let errorIis         :   String?
        
        enum CodingKeys: String, CodingKey {
            
            case memoria        =   "memoria"
            case stuckThread    =   "stuckThread"
            case brokenPipe     =   "brokenPipe"
            case timeOut        =   "timeOut"
            case sqlException   =   "sqlException"
            case dataSource     =   "dataSource"
            case errorIis       =   "errorIis"
        }
        
        func getExceptionList() -> [GSAppsFrontException]{
            
            let exceptionList = [
                GSAppsFrontException(name: "Memoria Java", status: self.memoria),
                GSAppsFrontException(name: "Stuck Thread", status: self.stuckThread),
                GSAppsFrontException(name: "Broken Pipe", status: self.brokenPipe),
                GSAppsFrontException(name: "Time Out", status: self.timeOut),
                GSAppsFrontException(name: "SQLException", status: self.sqlException),
                GSAppsFrontException(name: "Data Source", status: self.dataSource),
                GSAppsFrontException(name: "Otras excepciones", status: self.errorIis)
            ]
            
            return exceptionList
        }
    }
    
    public func getAppsFrontList() -> [GSAppsFrontList]{
        
        let list = [
            GSAppsFrontList(name: "Dashboard", exceptions: self.dashboard?.getExceptionList() ?? []),
            GSAppsFrontList(name: "FFM", exceptions: self.ffm?.getExceptionList() ?? []),
            GSAppsFrontList(name: "Mesa de Control", exceptions: self.mesaControl?.getExceptionList() ?? []),
            GSAppsFrontList(name: "Hogar Seguro", exceptions: self.hogarSeguro?.getExceptionList() ?? []),
            GSAppsFrontList(name: "Totalshop", exceptions: self.totalshop?.getExceptionList() ?? []),
            GSAppsFrontList(name: "Portales", exceptions: self.portales?.getExceptionList() ?? []),
            GSAppsFrontList(name: "CRM", exceptions: self.crm?.getExceptionList() ?? []),
            GSAppsFrontList(name: "Qlik Sense", exceptions: self.qliksense?.getExceptionList() ?? []),
            GSAppsFrontList(name: "Salesforce", exceptions: self.salesforce?.getExceptionList() ?? [])
        ]
        
        return list
    }
}

public struct GSAppsFrontList{
    public var name        :   String
    public var exceptions  :   [GSAppsFrontException]
    
    public func getExceptionsList() -> [String]{
        let list = self.exceptions.map({$0.name})
        return list
    }
}

public struct GSAppsFrontException{
    public var name    :   String
    public var status  :   String?
}
