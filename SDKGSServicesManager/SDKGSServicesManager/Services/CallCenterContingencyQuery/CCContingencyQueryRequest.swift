//
//  CCContingencyGetRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct CCContingencyQueryRequest: Encodable {
    
    public var ip : String?
    public var password : String?
    public var userId : String?
    public var ctcStatus : String?
  
    
    public init(ip: String, password: String, userId: String, ctcStatus: String) {
        self.ip = ip
        self.password = password
        self.userId = userId
        self.ctcStatus = ctcStatus
    }
    
    public enum CodingKeys: String, CodingKey {
        
        case ip = "ip"
        case password = "password"
        case userId = "userId"
        case ctcStatus = "ctcStatus"
    }
}
