//
//  CCContingencyGetResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit


public struct CCContingencyQueryResponse: Decodable {

    public let result: BaseResponse200?
    public let listTKM: [_listTKM]?
    
    enum CodingKeys: String, CodingKey {
        
        case result = "result"
        case listTKM = "listTKM"
    }

    public struct _listTKM: Decodable{
        
        public let ctcId: String?
        public let ctcNameTkm: String?
        public let ctcStatus: String?
        enum CodingKeys: String, CodingKey {
            
            case ctcId = "ctcId"
            case ctcNameTkm = "ctcNameTkm"
            case ctcStatus = "ctcStatus"
        }
    }
}
