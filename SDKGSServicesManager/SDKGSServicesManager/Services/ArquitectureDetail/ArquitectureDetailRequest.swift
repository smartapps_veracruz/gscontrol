//
//  CCContingencyGetRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct ArquitectureDetailRequest: Encodable {
    
    public var architectureId : String?
    
    public init(architectureId: String) {
        self.architectureId = architectureId
    }
    
    enum CodingKeys: String, CodingKey {
        
        case architectureId = "architectureId"
    }
    
}
