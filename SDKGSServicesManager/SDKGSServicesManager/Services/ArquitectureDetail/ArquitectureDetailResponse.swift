//
//  CCContingencyGetResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit


public struct ArquitectureDetailResponse: Decodable {
    
    public let architectureId: Int?
    public let image: String?
    
    enum CodingKeys: String, CodingKey {
        
        case architectureId   =   "architectureId"
        case image            =   "image"
    }
}
