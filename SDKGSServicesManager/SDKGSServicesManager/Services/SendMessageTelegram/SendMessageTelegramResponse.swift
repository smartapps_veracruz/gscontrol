//
//  SendMessageTelegramResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 13/01/22.
//

import UIKit

public struct SendMessageTelegramResponse: Decodable {

    public let ok : Bool?
    public let result : _result?

    public struct _result: Decodable {
        public let message_id: Int?
        public let date: String?
        public let text: String?
        public let from: _from?
        public let chat: _chat?
        
        public struct _from: Decodable {
            public let id: Int?
            public let is_bot: Bool?
            public let first_name: String?
            public let username: String?
        }
        
        public struct _chat: Decodable {
            public let id: String?
            public let type: String?
        }
        enum CodingKeys: String, CodingKey {
        
            case message_id = "message_id"
            case date = "date"
            case text = "text"
            case from = "from"
            case chat = "chat"
        }
    }

    
    enum CodingKeys: String, CodingKey {
        
        case ok = "ok"
        case result = "result"
    }
}
