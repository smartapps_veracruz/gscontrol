//
//  SendMessageTelegramRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct SendMessageTelegramRequest: Encodable {

    public var message : String?
    public var chatId : String?
    
    public init(message: String, chatId: String) {
        self.message = message
        self.chatId = chatId
    }
    
    enum CodingKeys: String, CodingKey {
        
        case message = "message"
        case chatId = "chatId"
    }
}
