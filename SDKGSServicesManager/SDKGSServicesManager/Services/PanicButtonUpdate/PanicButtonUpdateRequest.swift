//
//  CCContingencyPostRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct PanicButtonUpdateRequest: Encodable {

    public var ip : String?
    public var password : String?
    public var userId : String?
    public var estatusBP : String?
    
    public init(ip: String, password: String, userId: String, estatusBP: String) {
        self.ip = ip
        self.password = password
        self.userId = userId
        self.estatusBP = estatusBP
    }
    
    enum CodingKeys: String, CodingKey {
        
        case ip = "ip"
        case password = "password"
        case userId = "userId"
        case estatusBP = "estatusBP"
    }
}
