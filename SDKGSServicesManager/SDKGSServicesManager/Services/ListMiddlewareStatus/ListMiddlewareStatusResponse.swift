//
//  CCContingencyGetResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit
import SDKGSCommonUtils


public struct ListMiddlewareStatusResponse: Codable {
    
    public let infoIvrIptv: [infoMiddleware]?
    public let infoPortalesFFM: [infoMiddleware]?
    public let infoSalesforce: [infoMiddleware]?
    public let infoDashboard: [infoMiddleware]?
    public let infoVentas: [infoMiddleware]?
    public let infoTotalshop: [infoMiddleware]?
    public let infoHogarSeguro: [infoMiddleware]?
    public let infoMesaControl: [infoMiddleware]?
    public let infoColombia: [infoMiddleware]?
    
    
    public struct infoMiddleware: Codable{
        public let id: String?
        public let serviceName: String?
        public let dataBase: String?
        public let ip: String?
        public let monitor: [_monitor]?
        
        enum CodingKeys: String, CodingKey {
            
            case id       =   "id"
            case serviceName   =   "serviceName"
            case dataBase =   "dataBase"
            case ip       =   "ip"
            case monitor  =   "monitorValues"
        }
        
        
        public struct _monitor: Codable{
            public let sequence: String?
            public let hour: String?
            public let value: String?
            public let status: String?
            
            enum CodingKeys: String, CodingKey {
                
                case sequence   =   "sequence"
                case hour       =   "hour"
                case value      =   "value"
                case status     =   "status"
            }
        }
        
    }
    public func sortById()->ListMiddlewareStatusResponse{
        return ListMiddlewareStatusResponse(
            infoIvrIptv: infoIvrIptv?.reorder(by: ["28132","28479","28133","28475"]),
            infoPortalesFFM: infoPortalesFFM?.reorder(by: ["28524","28540","29102","28473","28483","28484","28544","28561","28543"]),
            infoSalesforce: infoSalesforce?.reorder(by: ["28572","28472","28478"]),
            infoDashboard: infoDashboard?.reorder(by: ["28476","29170"]),
            infoVentas: infoVentas?.reorder(by: ["28477"]),
            infoTotalshop: infoTotalshop?.reorder(by: ["28556","28555","28474"]),
            infoHogarSeguro: infoHogarSeguro?.reorder(by: ["28628"]),
            infoMesaControl: infoMesaControl?.reorder(by: ["29096"]),
            infoColombia: infoColombia?.reorder(by: ["29097"]))
    }
}

extension ListMiddlewareStatusResponse.infoMiddleware: Reorderable{
    public typealias OrderElement = String
    public var orderElement: OrderElement { id ?? "" }
}
