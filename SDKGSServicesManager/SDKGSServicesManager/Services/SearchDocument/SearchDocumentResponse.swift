//
//  CCContingencyGetResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit


public struct SearchDocumentResponse: Decodable {
    
    public let documentId: Int?
    public let documentName: String?
    public let summary: String?
    
    enum CodingKeys: String, CodingKey {
        
        case documentId      =   "documentId"
        case documentName    =   "documentName"
        case summary    =   "summary"
    }
}
