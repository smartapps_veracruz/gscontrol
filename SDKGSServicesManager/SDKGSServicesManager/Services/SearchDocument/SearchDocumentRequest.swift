//
//  CCContingencyGetRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct SearchDocumentRequest: Encodable {
    
    public var content : String?
    
    public init(content: String) {
        self.content = content
    }
    
    enum CodingKeys: String, CodingKey {
        
        case content = "content"
    }
    
}
