//
//  ListOEMRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 23/02/22.
//

import UIKit

public struct ListOEMRequest: Encodable {

    public var dashboardType : String?
    
    public init(dashboardType: String) {
        self.dashboardType = dashboardType
    }
    
    enum CodingKeys: String, CodingKey {
        
        case dashboardType = "dashboardType"
    }
}
