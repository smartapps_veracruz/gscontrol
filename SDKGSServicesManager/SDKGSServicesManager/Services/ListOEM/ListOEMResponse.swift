//
//  ListOEMResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 23/02/22.
//

import UIKit

public struct ListOEMResponse: Decodable {

    public let availability: String?
    public let status: String?
    public let sessionsLimit: String?
    public let sessionsNumber: String?
    public let fileSystemOracle19c: String?
    public let fileSystemCloudcontrol: String?
    public let fileSystemOradump: String?
    public let fileSystemgrid: String?
   
    enum CodingKeys: String, CodingKey {
        
        case availability    =   "availability"
        case status =   "status"
        case sessionsLimit    =   "sessionsLimit"
        case sessionsNumber =   "sessionsNumber"
        case fileSystemOracle19c    =   "fileSystemOracle19c"
        case fileSystemCloudcontrol    =   "fileSystemCloudcontrol"
        case fileSystemOradump    =   "fileSystemOradump"
        case fileSystemgrid    =   "fileSystemgrid"
    }
}
