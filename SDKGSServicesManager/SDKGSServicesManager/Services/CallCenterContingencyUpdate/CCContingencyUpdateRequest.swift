//
//  CCContingencyPostRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct CCContingencyUpdateRequest: Encodable {

    public var ip : String?
    public var password : String?
    public var userId : String?
    public var ctcId : String?
    
    public init(ip: String, password: String, userId: String, ctcId: String) {
        self.ip = ip
        self.password = password
        self.userId = userId
        self.ctcId = ctcId
    }
    
    enum CodingKeys: String, CodingKey {
        
        case ip = "ip"
        case password = "password"
        case userId = "userId"
        case ctcId = "ctcId"
    }
}
