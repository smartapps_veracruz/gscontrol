//
//  ListMovies.swift
//  SDKRMoviesServices
//
//  Created by Dsi Soporte Tecnico on 10/12/21.
//

import UIKit
import SDKGSCommonUtils

public protocol ListBackendStatusServiceProtocol{
    typealias Request = ListBackendStatusRequest?
    typealias Response = ListBackendStatusResponse?
}

internal class ListBackendStatusService: ListBackendStatusServiceProtocol {
    private var servicesManager = GSServicesManager.shareManager
    private var rootPath : String = ""
    private var basePath : String = ""
    private var endpoint : String = ""
    
    init(){
        rootPath = "http://10.218.53.43"
        basePath = "/ms-monitoreo"
        endpoint = "/getInfo"
    }
    
    func fetch(params: Request,
                      success: @escaping (Response, Int?, String?) -> Void,
                      empty:   @escaping (String?)->Void,
                      failure: @escaping (String?) -> Void) {
        
        var paramsBuilder = [URLQueryItem]()
        for case let (label?, value) in Mirror(reflecting: params!)
                .children.map({ ($0.label, $0.value) }) {
            paramsBuilder.append(URLQueryItem(name: label, value: value as? String))
        }
        
//        if let path = Bundle.init(for: ListBackendStatusService.self).path(forResource: "responseBACK", ofType: "json")
//        {
//            if let jsonData = NSData(contentsOfFile: path)
//            {
//                guard let model = servicesManager.decode(JSONObject: jsonData as Data, entity: Response.self) else{
//                   return
//                }
//
//                success(model?.sortById(), 200, "")
//
//                return
//             }
//        }
//    }
        servicesManager.getServices(
            method: .GET,
            route: "\(rootPath)\(basePath)\(endpoint)",
            params: paramsBuilder) { [self] ( response, responseCode, responseMessage) in
                switch responseCode{
                case 200,201:
                    guard let response = response, let responseModel = servicesManager.decode(JSONObject: response, entity: Response.self) else{
                        failure("No se pudo obtener una respuesta valida")
                        return
                    }
                    success(responseModel?.sortById(), responseCode, responseMessage)
                    break
                case 400:
                    guard let response = response, let responseModel = servicesManager.decode(JSONObject: response, entity: BaseResponse400.self) else{
                        failure("No se pudo obtener una respuesta valida")
                        return
                    }
                    failure(responseModel.error ?? "No se pudo obtener una respuesta valida")
                    break
                default:
                    failure("Review code \(responseCode)")
                    break
                }

            } failure: { _, responseMessage in
                failure(responseMessage)
            }
    }
    
}
