//
//  CCContingencyGetRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct ListBackendStatusRequest: Encodable {
    
    public var dashboardType : String?
    
    public init(dashboardType: String) {
        self.dashboardType = dashboardType
    }
    
    enum CodingKeys: String, CodingKey {
        
        case dashboardType = "dashboardType"
    }
}
