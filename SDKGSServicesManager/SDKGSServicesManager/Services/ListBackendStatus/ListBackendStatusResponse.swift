//
//  CCContingencyGetResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit
import SDKGSCommonUtils


public struct ListBackendStatusResponse: Codable {

    public let infoProvisioning: [ListMiddlewareStatusResponse.infoMiddleware]?
    public let infoGSS: [ListMiddlewareStatusResponse.infoMiddleware]?
    public let infoRelayCorreo: [ListMiddlewareStatusResponse.infoMiddleware]?
    
    public func sortById()->ListBackendStatusResponse{
        return ListBackendStatusResponse(
            infoProvisioning: infoProvisioning?.reorder(by: ["16207","16208","16209","28481","29178","28629","29114","28508","29135"]),
            infoGSS: infoGSS?.reorder(by: ["28809","28808"]),
            infoRelayCorreo: infoRelayCorreo?.reorder(by: ["29173","29183"]))
    }
    
}
