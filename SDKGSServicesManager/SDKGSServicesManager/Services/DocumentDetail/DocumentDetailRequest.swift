//
//  CCContingencyGetRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct DocumentDetailRequest: Encodable {
    
    public var documentId : String?
    
    public init(documentId: String) {
        self.documentId = documentId
    }
    
    enum CodingKeys: String, CodingKey {
        
        case documentId = "documentId"
    }
    
}
