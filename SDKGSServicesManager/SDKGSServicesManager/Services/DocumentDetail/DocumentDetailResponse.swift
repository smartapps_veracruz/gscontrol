//
//  CCContingencyGetResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit


public struct DocumentDetailResponse: Decodable {
    
    public let documentContent: String?
    
    enum CodingKeys: String, CodingKey {
        
        case documentContent  = "documentContent"
    }
}
