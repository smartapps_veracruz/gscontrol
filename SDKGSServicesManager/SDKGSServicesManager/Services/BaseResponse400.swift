//
//  BaseResponse400.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct BaseResponse400: Decodable {
    
    public let timestamp: String?
    public let status: Int?
    public let error: String?
    public let path: String?
    
    enum CodingKeys: String, CodingKey {
        
        case timestamp = "timestamp"
        case status = "status"
        case error = "error"
        case path = "path"
    }
}
