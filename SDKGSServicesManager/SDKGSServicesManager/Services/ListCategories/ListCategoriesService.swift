//
//  ListMovies.swift
//  SDKRMoviesServices
//
//  Created by Dsi Soporte Tecnico on 10/12/21.
//

import UIKit
import SDKGSCommonUtils

public protocol ListCategoriesServiceProtocol{
    typealias Request = BaseRequestEmpty?
    typealias Response = [ListCategoriesResponse]
}

internal class ListCategoriesService:ListCategoriesServiceProtocol {
    private var servicesManager = GSServicesManager.shareManager
    private var rootPath : String = ""
    private var basePath : String = ""
    private var endpoint : String = ""
    
    init(){
        rootPath = "http://94.74.77.207"
        basePath = "/ms-documentos"
        endpoint = "/procedures/getCategories"
    }
    
    func fetch(params: Request,
                      success: @escaping (Response, Int?, String?) -> Void,
                      empty:   @escaping (String?)->Void,
                      failure: @escaping (String?) -> Void) {
        
        var paramsBuilder = [URLQueryItem]()
        for case let (label?, value) in Mirror(reflecting: params!)
                .children.map({ ($0.label, $0.value) }) {
            paramsBuilder.append(URLQueryItem(name: label, value: value as? String))
        }
        
        servicesManager.getServices(
            method: .GET,
            route: "\(rootPath)\(basePath)\(endpoint)",
            params: paramsBuilder) { [self] ( response, responseCode, responseMessage) in
                switch responseCode{
                case 200,201:
                    guard let response = response, let responseModel = servicesManager.decode(JSONObject: response, entity: Response.self) else{
                        failure("No se pudo obtener una respuesta valida")
                        return
                    }
                    if responseModel.count == 0 {
                        empty("Sin resultados")
                    }else{
                        success(responseModel, responseCode, responseMessage)
                    }
                    break
                case 400:
                    guard let response = response, let responseModel = servicesManager.decode(JSONObject: response, entity: BaseResponse400.self) else{
                        failure("No se pudo obtener una respuesta valida")
                        return
                    }
                    failure(responseModel.error ?? "No se pudo obtener una respuesta valida")
                    break
                default:
                    failure("Review code \(responseCode)")
                    break
                }
                
            } failure: { _, responseMessage in
                failure(responseMessage)
            }
    }
    
}
