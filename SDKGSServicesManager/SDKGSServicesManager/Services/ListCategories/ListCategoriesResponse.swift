//
//  CCContingencyGetResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit


public struct ListCategoriesResponse: Decodable {
    
    public let categoryId: String?
    public let description: String?
    
    enum CodingKeys: String, CodingKey {
        
        case categoryId  =   "categoryId"
        case description =   "description"
    }
}
