//
//  CCContingencyGetResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit


public struct ListArquitecturesResponse: Decodable {
    
    public let architectureId: Int?
    public let name: String?
    
    enum CodingKeys: String, CodingKey {
        
        case architectureId  =   "architectureId"
        case name            =   "name"
    }
}
