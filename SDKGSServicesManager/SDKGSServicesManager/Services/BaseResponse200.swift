//
//  BaseResponseManager.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct BaseResponse200: Decodable {
    
    public let result: String?
    public let information: String?
    public let errorCode: String?
    
    enum CodingKeys: String, CodingKey {
        
        case result = "result"
        case information = "information"
        case errorCode = "errorCode"
    }
}
