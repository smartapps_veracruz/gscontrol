//
//  CCContingencyGetResponse.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit


public struct ListDocumentsByCategoryResponse: Decodable {
    
    public let documentId: Int?
    public let documentName: String?
    
    enum CodingKeys: String, CodingKey {
        
        case documentId      =   "documentId"
        case documentName    =   "documentName"
    
    }
}
