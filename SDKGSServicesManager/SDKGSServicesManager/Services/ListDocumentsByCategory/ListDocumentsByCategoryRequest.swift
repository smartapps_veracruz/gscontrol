//
//  CCContingencyGetRequest.swift
//  SDKGSServicesManager
//
//  Created by Dsi Soporte Tecnico on 29/12/21.
//

import UIKit

public struct ListDocumentsByCategoryRequest: Encodable {
    
    public var categoryId : String?
    
    public init(categoryId: String) {
        self.categoryId = categoryId
    }
    
    enum CodingKeys: String, CodingKey {
        
        case categoryId = "categoryId"
    }
    
}
